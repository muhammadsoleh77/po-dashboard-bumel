/**
 * @author v.lugovksy
 * created on 15.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme')

    .run(function ($rootScope, $state, AuthService, AUTH_EVENTS, $http, API_ENDPOINT) {
  $rootScope.$on('$stateChangeStart', function (event,next, nextParams, fromState) {
    $rootScope.dataLoaded = 9;
    if (!AuthService.isAuthenticated()) {
      if (next.name !== "login" && next.name !== "forget") {

        $state.go('login');
        event.preventDefault();
      }
    } else {
      $http.get(API_ENDPOINT.url +'/user').success(function(data) {
        $rootScope.user = data;
        // $http.get(API_ENDPOINT.url +'/po/8').success(function (result) {

        if (!$rootScope.user.idPo) {
          $state.go('login');
        }

        if (!$rootScope.listAgens || !$rootScope.lisTrayeks) {
          $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/trayeks').success(function (data) {
            $rootScope.lisTrayeks = [];
            $rootScope.trayeks = [
              {
                name : "Semua Trayek",
                value : '',
                id : null
              }
            ];
            data.forEach(function (doc) {
              $rootScope.trayeks.push({
                name : doc.trayek,
                value : doc.trayek,
                id : doc.id,
                tempatBerangkat : doc.tempatberangkat,
                rute : doc.lintasan,
                arah : doc.arahtrayek,
              });
              $rootScope.lisTrayeks.push({
                name : doc.trayek,
                id : doc.id,
                value : doc.trayek,
                tempatBerangkat : doc.tempatberangkat,
                rute : doc.lintasan,
                arah : doc.arahtrayek,
              });
            });
          });

          $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/agen').success(function (data) {
            $rootScope.listAgens = [];
            $rootScope.namaAgen = [];
            $rootScope.agens = [
              {
                name : "Semua Agen",
                value : '',
                id : 0
              }
            ];
            data.forEach(function (doc) {
              $rootScope.agens.push({
                name : doc.namaChannel,
                value : doc.namaChannel,
                id : doc.idChannel
              });
              $rootScope.listAgens.push({
                name : doc.namaChannel,
                value : doc.namaChannel,
                alamat : doc.alamat,
                kontakPerson : doc.kontakPerson,
                nomorTelepon : doc.nomorTelepon,
                id : doc.idChannel,
              });
              $rootScope.namaAgen.push(doc.namaChannel);
            });
          });
          $rootScope.isLoggedIn = true;
        } else {
          $rootScope.isLoggedIn = true;
        }
        // console.log($rootScope.po);
        $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo).success(function (result) {
          // make themes color template
          // if ($rootScope.user.idPo === 1) {
            // $rootScope.color = "#1243f2";
          // }else{
            // $rootScope.color = "#f24d12";
          // }
          // end

              $rootScope.po = result;
        });
         // $state.go('app');
    });

    }
    $rootScope.isLoggedIn = true;
  });
})
    .run(themeRun);
  /** @ngInject */
  function themeRun($timeout, $rootScope, layoutPaths, preloader, $q, baSidebarService, themeLayoutSettings, $http, API_ENDPOINT, AuthService, $state) {

    $rootScope.isLoggedIn = false;
    if (!AuthService.isAuthenticated()) {
        $state.go('login');
    } else {
      $http.get(API_ENDPOINT.url +'/user').success(function(data) {
        $rootScope.user = data;
        // $http.get(API_ENDPOINT.url +'/po/8').success(function (result) {
        if (!$rootScope.user.idPo) {
          $state.go('login');
        }

        if (!$rootScope.listAgens || !$rootScope.lisTrayeks) {
          $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/trayeks').success(function (data) {
            $rootScope.lisTrayeks = [];
            $rootScope.trayeks = [
              {
                name : "Semua Trayek",
                value : '',
                id : null
              }
            ];
            data.forEach(function (doc) {
              $rootScope.trayeks.push({
                name : doc.trayek,
                value : doc.trayek,
                id : doc.id,
                tempatBerangkat : doc.tempatberangkat,
                rute : doc.lintasan,
                arah : doc.arahtrayek,
              });
              $rootScope.lisTrayeks.push({
                name : doc.trayek,
                id : doc.id,
                value : doc.trayek,
                tempatBerangkat : doc.tempatberangkat,
                rute : doc.lintasan,
                arah : doc.arahtrayek,
              });
            });
          });

          $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/agen').success(function (data) {
            $rootScope.listAgens = [];
            $rootScope.namaAgen = [];
            $rootScope.agens = [
              {
                name : "Semua Agen",
                value : '',
                id : 0
              }
            ];
            data.forEach(function (doc) {
              $rootScope.agens.push({
                name : doc.namaChannel,
                value : doc.namaChannel,
                id : doc.idChannel
              });
              $rootScope.listAgens.push({
                name : doc.namaChannel,
                value : doc.namaChannel,
                alamat : doc.alamat,
                kontakPerson : doc.kontakPerson,
                nomorTelepon : doc.nomorTelepon,
                id : doc.idChannel,
              });
              $rootScope.namaAgen.push(doc.namaChannel);
            });
          });
          $rootScope.isLoggedIn = true;
        } else {
          $rootScope.isLoggedIn = true;
        }

        $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo).success(function (result) {
              $rootScope.po = result;
              // $rootScope.bg = '#cc9825';
        });
         // $state.go('app');
    });

    }

    var whatToWait = [
      preloader.loadAmCharts(),
      $timeout(3000)
    ];

    var theme = themeLayoutSettings;
    if (theme.blur) {
      if (theme.mobile) {
        whatToWait.unshift(preloader.loadImg(layoutPaths.images.root + 'blur-bg-mobile.jpg'));
      } else {
        whatToWait.unshift(preloader.loadImg(layoutPaths.images.root + 'blur-bg.jpg'));
        whatToWait.unshift(preloader.loadImg(layoutPaths.images.root + 'blur-bg-blurred.jpg'));
      }
    }

    $q.all(whatToWait).then(function () {
      $rootScope.$pageFinishedLoading = true;
    });
    $rootScope.$baSidebarService = baSidebarService;
  }
})();
