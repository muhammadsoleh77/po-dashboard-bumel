/**
 * @author a.demeshko
 * created on 23.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme')
    .filter('plainText', function () {
      return function(text) {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
      };
    })

    .filter('date_transaksi', function () {
      return function (date_trans) {
        var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        // memisahkan bagian dengan split
        var dateArray = date_trans.split(" ");
        // console.log(dateArray);

        // mengubah nama bulan menjadi angka
        var monthArray = new Date(Date.parse(dateArray[1] + "0")).getMonth()+1;
        // console.log(monthArray);

        // output ditampilkan
        var newTanggal = dateArray[2] + " " + namaBulan[(String(monthArray) - 1)] + " " + dateArray[5];
        // console.log(newTanggal);

        return newTanggal;
      };
    })

    .filter('date_time', function () {
      return function (dates) {

        var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        var dateArray = dates.split("-");
        var dayArray = dateArray[2].split(" ")
        if (dateArray[1].charAt(0) == "0") {
          dateArray[1] = dateArray[1].slice(1);
        }
        // console.log(dateArray)
        // console.log(dayArray)
        var newDate = dayArray[0] + " " + namaBulan[(String(dateArray[1]) - 1)] + " " + dateArray[0] + ", Jam " + dayArray[1];

        return newDate;
      };
    })

    .filter('reverse', function() {
      return function(baliktanggal) {
        return baliktanggal.slice().reverse();
      };
    })

    // .filter('sortAscending', function() {
    //   return function(urutNomor) {
    //     return urutNomor.sort();
    //   };
    // })

    .filter('toRoman', function () {
      return function (m) {
        var result;
        switch (m) {
          case 1: result = 'I'; break;
          case 2: result = 'II'; break;
          case 3: result = 'III'; break;
          case 4: result = 'IV'; break;
          case 5: result = 'V'; break;
          case 6: result = 'VI'; break;
          case 7: result = 'VII'; break;
          case 8: result = 'VIII'; break;
          case 9: result = 'IX'; break;
          case 10: result = 'X'; break;
          case 11: result = 'XI'; break;
          case 12: result = 'XII';
        }
        return result;
      };
    })

    .filter('date_v1', function () {
      return function (dates) {

        var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        var dateArray = dates.split("-");
        if (dateArray[1].charAt(0) == "0") {
          dateArray[1] = dateArray[1].slice(1);
        }

        var newDate = dateArray[2] + " " + namaBulan[(String(dateArray[1]) - 1)] + " " + dateArray[0];

        return newDate;
      };
    })

    .filter('date_v2', function () {
      return function (date) {
        var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

        var time = new Date((date));
        var DD = time.getUTCDay();
        var dd = time.getUTCDate();
        var mm = time.getUTCMonth();
        var yyyy = time.getUTCFullYear();

        var newDate = namaHari[DD] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;

        return newDate;
      };
    })
    .filter('date_v4', function () {
      return function (date) {
        var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

        var time = new Date((date));
        var DD = time.getDay();
        var dd = time.getDate();
        var mm = time.getMonth();
        var yyyy = time.getFullYear();

        var newDate = namaHari[DD] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;

        return newDate;
      };
    })
    .filter('is_anak', function () {
      return function (nomor, usia) {
        var newNomor;
        if (usia == "Anak") {
          newNomor = "";
        } else {
          newNomor = nomor;
        }
        return newNomor;
      };
    })
    .filter('reverse', function () {
      return function(items) {
        if(typeof items === 'undefined') { return; }
        return angular.isArray(items) ?
          items.slice().reverse() : // If it is an array, split and reverse it
          (items + '').split('').reverse().join(''); // else make it a string (if it isn't already), and reverse it
    };
    })
    .filter('jam_berangkat', function () {
      return function (items) {
        items = items.slice(0, -3);
        return items;
      };
    })
    .filter('terbilang', function () {
      return function (x) {
        function terbilang(a){
        	var bilangan = ['','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas'];
          var utama, depan, belakang, kalimat;
        	// 1 - 11
        	if(a < 12){
        		kalimat = bilangan[a];
        	}
        	// 12 - 19
        	else if(a < 20){
        		kalimat = bilangan[a-10]+' Belas';
        	}
        	// 20 - 99
        	else if(a < 100){
        		utama = a/10;
        		depan = parseInt(String(utama).substr(0,1));
        		belakang = a%10;
        		kalimat = bilangan[depan]+' Puluh '+bilangan[belakang];
        	}
        	// 100 - 199
        	else if(a < 200){
        		kalimat = 'Seratus '+ terbilang(a - 100);
        	}
        	// 200 - 999
        	else if(a < 1000){
        		utama = a/100;
        		depan = parseInt(String(utama).substr(0,1));
        		belakang = a%100;
        		kalimat = bilangan[depan] + ' Ratus '+ terbilang(belakang);
        	}
        	// 1,000 - 1,999
        	else if(a < 2000){
        		kalimat = 'Seribu '+ terbilang(a - 1000);
        	}
        	// 2,000 - 9,999
        	else if(a < 10000){
        		utama = a/1000;
        		depan = parseInt(String(utama).substr(0,1));
        		belakang = a%1000;
        		kalimat = bilangan[depan] + ' Ribu '+ terbilang(belakang);
        	}
        	// 10,000 - 99,999
        	else if(a < 100000){
        		utama = a/100;
        		depan = parseInt(String(utama).substr(0,2));
        		belakang = a%1000;
        		kalimat = terbilang(depan) + ' Ribu '+ terbilang(belakang);
        	}
        	// 100,000 - 999,999
        	else if(a < 1000000){
        		utama = a/1000;
        		depan = parseInt(String(utama).substr(0,3));
        		belakang = a%1000;
        		kalimat = terbilang(depan) + ' Ribu '+ terbilang(belakang);
        	}
        	// 1,000,000 - 	99,999,999
        	else if(a < 100000000){
        		utama = a/1000000;
        		depan = parseInt(String(utama).substr(0,4));
        		belakang = a%1000000;
        		kalimat = terbilang(depan) + ' Juta '+ terbilang(belakang);
        	}
        	else if(a < 1000000000){
        		utama = a/1000000;
        		depan = parseInt(String(utama).substr(0,4));
        		belakang = a%1000000;
        		kalimat = terbilang(depan) + ' Juta '+ terbilang(belakang);
        	}
        	else if(a < 10000000000){
        		utama = a/1000000000;
        		depan = parseInt(String(utama).substr(0,1));
        		belakang = a%1000000000;
        		kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
        	}
        	else if(a < 100000000000){
        		utama = a/1000000000;
        		depan = parseInt(String(utama).substr(0,2));
        		belakang = a%1000000000;
        		kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
        	}
        	else if(a < 1000000000000){
        		utama = a/1000000000;
        		depan = parseInt(String(utama).substr(0,3));
        		belakang = a%1000000000;
        		kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
        	}
        	else if(a < 10000000000000){
        		utama = a/10000000000;
        		depan = parseInt(String(utama).substr(0,1));
        		belakang = a%10000000000;
        		kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
        	}
        	else if(a < 100000000000000){
        		utama = a/1000000000000;
        		depan = parseInt(String(utama).substr(0,2));
        		belakang = a%1000000000000;
        		kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
        	}

        	else if(a < 1000000000000000){
        		utama = a/1000000000000;
        		depan = parseInt(String(utama).substr(0,3));
        		belakang = a%1000000000000;
        		kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
        	}

          else if(a < 10000000000000000){
        		utama = a/1000000000000000;
        		depan = parseInt(String(utama).substr(0,1));
        		belakang = a%1000000000000000;
        		kalimat = terbilang(depan) + ' Kuadriliun '+ terbilang(belakang);
        	}

        	var pisah = kalimat.split(' ');
        	var full = [];
        	for(var i=0;i<pisah.length;i++){
        	 if(pisah[i] !== ""){full.push(pisah[i]);}
        	}
        	return full.join(' ');
        }
        var nominal;
        if (x) {
        nominal   = terbilang(x);
        } else {
          nominal = '';
        }
        return nominal;
      };

    });

  /** @ngInject */


})();
