/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')

  //     .controller('PageTopCtrl', ['$scope', 'AuthService', 'API_ENDPOINT', '$http', '$state', function($scope, AuthService, API_ENDPOINT, $http, $state) {
  //   $scope.destroySession = function() {
  //     AuthService.logout();
  //   };

  //   $scope.getInfo = function() {
  //     $http.get(API_ENDPOINT.url + '/user').then(function(result) {
  //       $scope.memberinfo = result.data.msg;
  //     });
  //   };

  //   $scope.logout = function() {
  //     //console.log("Haaiiaiisaisia222");
  //     AuthService.logout();
  //     $state.go('outside.login');
  //   };
  // }])

      .directive('pageTop', pageTop);
  /** @ngInject */
  function pageTop() {
    return {
      restrict: 'E',
      transclude: true,
      scope: {},
      controller: ['$scope', 'AuthService', 'API_ENDPOINT', '$http', '$state','$rootScope', function($scope, AuthService, API_ENDPOINT, $http, $state, $rootScope) {
        $scope.destroySession = function() {
          AuthService.logout();
        };

        $rootScope.$watch('bg', function () {
          $rootScope.bg = $rootScope.bg;
        });

        $scope.getInfo = function() {
          $http.get(API_ENDPOINT.url + '/user').then(function(result) {
            $http.get(API_ENDPOINT.url + '/po/' + result.data.idPo).success(function (data) {
              $scope.user = $rootScope.user;
              $scope.logo = data.logo;
              $scope.nama = data.nama;
            });
          });
        };

        $scope.getInfo();

        $scope.logout = function() {
          AuthService.logout();
          $state.go('login');
        };
      }],
      templateUrl: 'app/theme/components/pageTop/pageTop.html'
    };
  }

  /** @ngInject */
  //   function pageTopCtrl($scope, AuthService, API_ENDPOINT, $http, $state) {
  //   $scope.destroySession = function() {
  //     AuthService.logout();
  //   };

  //   $scope.getInfo = function() {
  //     $http.get(API_ENDPOINT.url + '/user').then(function(result) {
  //       $scope.memberinfo = result.data.msg;
  //     });
  //   };

  //   $scope.logout = function() {
  //     //console.log("Haaiiaiisaisia222");
  //     AuthService.logout();
  //     $state.go('outside.login');
  //   };
  // }
})();
