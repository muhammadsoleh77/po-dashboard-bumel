(function () {

  angular
    .module('BlurAdmin.theme.components')
    .directive('notFound', notFound);

    function notFound() {
      return {
        restrict : 'E',
        templateUrl: 'app/theme/components/notFound/notFound.template.html',
        scope : {
          info : '=info'
        }
      };
    }

})();
