/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
      .directive('contentTop', contentTop);

  /** @ngInject */
  function contentTop($location, $state) {
    return {
      restrict: 'E',
      templateUrl: 'app/theme/components/contentTop/contentTop.html',
      link: function($scope) {
        $scope.$watch(function () {
          if ($state.current.parrentState) {
            $scope.parrentState = $state.current.parrentState;
            $scope.parrentTitle = $state.current.parrentTitle;
            $scope.back = function () {
              $state.go($scope.parrentState);
            };
          } else {
            $scope.parrentState = "";
            $scope.parrentTitle = "";
          }
          $scope.tag = $state.current.tag;
          $scope.activePageTitle = $state.current.title;
        });
      }
    };
  }

})();
