/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
    .controller('BaSidebarCtrl', BaSidebarCtrl);

  /** @ngInject */
  function BaSidebarCtrl($scope, baSidebarService, API_ENDPOINT, $http, $rootScope, $state) {

    // $scope.user = $rootScope.user;, $rootScope
    // $scope.po = $rootScope.po;
    $scope.menuItems = baSidebarService.getMenuItems();
    $scope.defaultSidebarState = $scope.menuItems[0].stateRef;

    $scope.hoverItem = function ($event) {
      $scope.showHoverElem = true;
      $scope.hoverElemHeight =  $event.currentTarget.clientHeight;
      var menuTopValue = 66;
      $scope.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - menuTopValue;
    };

    $scope.$on('$stateChangeSuccess', function () {
      if (baSidebarService.canSidebarBeHidden()) {
        baSidebarService.setMenuCollapsed(true);
      }
    });
    $http.get(API_ENDPOINT.url + 'po/' + $rootScope.user.idPo).success(function(data){
      console.log(data);
      if (!data.akap) {
        console.log('akap');
        for (var i = 0; i < $scope.menuItems.length; i++) {
          if ($scope.menuItems[i].title === "AKAP") {
            $scope.menuItems.splice(i, 1)
            // console.log('ilang1');
          }
        }
      }
      if (!data.akdp) {
        console.log('akdp');
        for (var i = 0; i < $scope.menuItems.length; i++) {
          if ($scope.menuItems[i].title === "AKDP") {
            $scope.menuItems.splice(i, 1)
            // console.log('ilang2');
          }
        }
      }
      if (!data.pariwisata) {
        console.log('pariwisata');
        for (var i = 0; i < $scope.menuItems.length; i++) {
          if ($scope.menuItems[i].title === "Pariwisata") {
            $scope.menuItems.splice(i, 1)
            // console.log('ilang2');
          }
        }
      }
      if (!data.paket) {
        console.log('paket');
        for (var i = 0; i < $scope.menuItems.length; i++) {
          if ($scope.menuItems[i].title === "Paket") {
            $scope.menuItems.splice(i, 1)
            // console.log('ilang2');
          }
        }
      }
      if (!data.travel) {
        console.log('travel');
        for (var i = 0; i < $scope.menuItems.length; i++) {
          if ($scope.menuItems[i].title === "Travel") {
            $scope.menuItems.splice(i, 1)
            // console.log('ilang2');
          }
        }
      }

      console.log($scope.menuItems);
    });
  }
})();
