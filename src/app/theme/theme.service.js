/**
 * Created by k.danovsky on 12.05.2016.
 */

(function () {
  'use strict';

  angular.module('BlurAdmin.theme')
    .service('themeLayoutSettings', themeLayoutSettings)

    .service('AuthService', function($q, $http, API_ENDPOINT) {
  var LOCAL_TOKEN_KEY = 'X-Auth-Token';
  var isAuthenticated = false;
  var authToken;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }

  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }

  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;

    // Set the token as header for your requests!
    // $http.defaults.headers.common.Authorization = 'Basic b3RvTW9iaWxlOk11dGlhcmExMjM=';
    // $http.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
    $http.defaults.headers.common['X-Auth-Token'] = authToken;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
    // $http.defaults.headers.common.Authorization = undefined;
    // $http.defaults.headers.common.Authorization = 'Basic b3RvTW9iaWxlOk11dGlhcmExMjM=';
    $http.defaults.headers.common['X-Auth-Token'] = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }

  var register = function(user) {
    return $q(function(resolve, reject) {
       $http({
                method: 'POST',
                url: API_ENDPOINT.url + '/user/register',
                // headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: user //$.param({username: user.username, password: user.password, })
            }).success(function (result) {
      // $http.post(API_ENDPOINT.url + '/signup', user).then(function(result) {
        if (result.message=="success") {
          resolve(result);
        } else {
          reject(result);
        }
      }) .catch(function (error) {
            // Catch and handle exceptions from success/error/finally functions
            alert('Pendaftaran Gagal');
            //console.log("Gagal "+error);
          });
    });
  };

  var login = function(user) {

    return  $http({
                method: 'POST',
                url: API_ENDPOINT.url + '/user/authenticate?username=' + user.username + '&password=' + user.password,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                // data: $.param({username: user.username, password: user.password})
            }).success(function (result) {

      // $http.post(API_ENDPOINT.url + '/user/authenticate', user).then(function(result) {
        if (result.success) {
          storeUserCredentials(result.token);
          // resolve(result.message);
        } else {
          // reject(result.message);
          alert(result.message);
        }
        // if (result.data.success) {
        //   storeUserCredentials(result.data.token);
        //   resolve(result.data.msg);
        // } else {
        //   reject(result.data.msg);
        // }
      });
  };

  var logout = function() {
    destroyUserCredentials();
  };

  loadUserCredentials();

  return {
    login: login,
    register: register,
    logout: logout,
    isAuthenticated: function() {return isAuthenticated;},
  };
})



.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
      }[response.status], response);
      return $q.reject(response);
    }
  };
});

  /** @ngInject */
  function themeLayoutSettings(baConfig) {
    var isMobile = (/android|webos|iphone|ipad|ipod|blackberry|windows phone/).test(navigator.userAgent.toLowerCase());
    var mobileClass = isMobile ? 'mobile' : '';
    var blurClass = baConfig.theme.blur ? 'blur-theme' : '';
    angular.element(document.body).addClass(mobileClass).addClass(blurClass);

    return {
      blur: baConfig.theme.blur,
      mobile: isMobile,
    }
  }

})();
