/**
 * Created by k.danovsky on 13.05.2016.
 */

(function () {
  'use strict';

  var basic = {
    default: '#ffffff',
    defaultText: '#666666',
    border: '#dddddd',
    borderDark: '#aaaaaa',
  };

  // main functional color scheme
  var colorScheme = {
    primary: '#209e91',
    info: '#2dacd1',
    success: '#90b900',
    warning: '#dfb81c',
    danger: '#e85656',
  };

  // dashboard colors for charts
  var dashboardColors = {
    blueStone: '#005562',
    surfieGreen: '#0e8174',
    silverTree: '#6eba8c',
    gossip: '#b9f2a1',
    white: '#10c4b5',
  };

  angular.module('BlurAdmin.theme')
    .provider('baConfig', configProvider)
    .service('chartColors', chartColors);

  /** @ngInject */


  function configProvider(colorHelper) {
    var conf = {
      theme: {
        blur: false,
      },
      colors: {
        default: basic.default,
        defaultText: basic.defaultText,
        border: basic.border,
        borderDark: basic.borderDark,

        primary: colorScheme.primary,
        info: colorScheme.info,
        success: colorScheme.success,
        warning: colorScheme.warning,
        danger: colorScheme.danger,

        primaryLight: colorHelper.tint(colorScheme.primary, 30),
        infoLight: colorHelper.tint(colorScheme.info, 30),
        successLight: colorHelper.tint(colorScheme.success, 30),
        warningLight: colorHelper.tint(colorScheme.warning, 30),
        dangerLight: colorHelper.tint(colorScheme.danger, 30),

        primaryDark: colorHelper.shade(colorScheme.primary, 15),
        infoDark: colorHelper.shade(colorScheme.info, 15),
        successDark: colorHelper.shade(colorScheme.success, 15),
        warningDark: colorHelper.shade(colorScheme.warning, 15),
        dangerDark: colorHelper.shade(colorScheme.danger, 15),

        dashboard: {
          blueStone: dashboardColors.blueStone,
          surfieGreen: dashboardColors.surfieGreen,
          silverTree: dashboardColors.silverTree,
          gossip: dashboardColors.gossip,
          white: dashboardColors.white,
        },
      }
    };

    conf.changeTheme = function(theme) {
      angular.merge(conf.theme, theme)
    };

    conf.changeColors = function(colors) {
      angular.merge(conf.colors, colors)
    };

    conf.$get = function () {
      delete conf.$get;
      return conf;
    };
    return conf;
  }
  function chartColors() {
    var chartColors = [
      '#236899',
      '#985146',
      '#2b7f47',
      '#6dc2b8',
      '#d85d34',
      '#604523',
      '#809a8c',
      '#055b4a',
      '#fa9d69',
      '#6caa16',
      '#42d64d',
      '#1cb24e',
      '#b35bc5',
      '#26327c',
      '#064d37',
      '#22f30d',
      '#b5763e',
      '#bd5937',
      '#f8c69c',
      '#2dc01c',
      '#ef30b6',
      '#a49636',
      '#0ed3ee',
      '#36b98b',
      '#fca992',
      '#10f425',
      '#5f78f7',
      '#9f65c7',
      '#19179f',
      '#de874d',
      '#deb427',
      '#25d348',
      '#c19271',
      '#11f74d',
      '#fd7576',
      '#43363b',
      '#05136b',
      '#8a8521',
      '#fecfab',
      '#2473d3',
      '#2c0c32',
      '#bd5937',
      '#f8c69c',
      '#2dc01c',
      '#a49636',
      '#0ed3ee',
      '#985146',
      '#236899',
      '#b35bc5',
      '#42d64d',
      '#ef30b6',
      '#36b98b',
      '#fca992',
      '#10f425',
      '#2b7f47',
      '#6dc2b8',
      '#d85d34',
      '#604523',
      '#809a8c',
      '#055b4a',
      '#fa9d69',
      '#6caa16',
      '#5f78f7',
      '#19179f',
      '#de874d',
      '#deb427',
      '#25d348',
      '#c19271',
      '#11f74d',
      '#fd7576',
      '#43363b',
      '#05136b',
      '#8a8521',
      '#fecfab',
      '#2473d3',
      '#2c0c32',
    ];
    return chartColors;
  }
})();
