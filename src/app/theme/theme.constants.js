/**
 * @author v.lugovsky
 * created on 15.12.2015
 */
(function () {
  'use strict';

  var IMAGES_ROOT = 'assets/img/';

  angular.module('BlurAdmin.theme')
    .constant('layoutSizes', {
      resWidthCollapseSidebar: 1200,
      resWidthHideSidebar: 500
    })
    .constant('layoutPaths', {
      images: {
        root: IMAGES_ROOT,
        profile: IMAGES_ROOT + 'app/profile/',
        amMap: 'assets/img/theme/vendor/ammap//dist/ammap/images/',
        amChart: 'assets/img/theme/vendor/amcharts/dist/amcharts/images/'
      }
    })
    .constant('colorHelper', {
      tint: function(color, weight) {
        return mix('#ffffff', color, weight);
      },
      shade: function(color, weight) {
        return mix('#000000', color, weight);
      },
    })

    .constant('AUTH_EVENTS', {
      notAuthenticated: 'auth-not-authenticated'
    })

    .constant('API_ENDPOINT', {
      // url: 'https://localhost:8443'
       url: 'https://dev.otodata.co.id:8383/'
       // url: 'https://demo.otodata.co.id:8383/'
       // url: 'https://live.otobus.co.id:8282/'
       // url: 'http://192.168.0.26:8383/'
       // url: 'https://live.otobus.co.id:8585/'
      //  For a simulator use: url: 'http://127.0.0.1:8181/api'
    })

    .filter('rupiah', function(){
      return function toRp(angka){
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for(var i = 0; i < rev.length; i++){
          rev2 += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
              rev2 += '.';
            }
        }
        // return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
        return 'Rp ' + rev2.split('').reverse().join('');
      };
    });

  function shade(color, weight) {
    return mix('#000000', color, weight);
  }

  function tint(color, weight) {
    return mix('#ffffff', color, weight);
  }

  //SASS mix function
  function mix(color1, color2, weight) {
    // convert a decimal value to hex
    function d2h(d) {
      return d.toString(16);
    }
    // convert a hex value to decimal
    function h2d(h) {
      return parseInt(h, 16);
    }

    var result = "#";
    for(var i = 1; i < 7; i += 2) {
      var color1Part = h2d(color1.substr(i, 2));
      var color2Part = h2d(color2.substr(i, 2));
      var resultPart = d2h(Math.floor(color2Part + (color1Part - color2Part) * (weight / 100.0)));
      result += ('0' + resultPart).slice(-2);
    }
    return result;
  }

  angular.module('BlurAdmin.theme')
  .constant('logo', {
    url : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAI+hJREFUeNrtXXl4lNXV/937vrNlMtk3sm8sSVjCJqtssoggttXK51K1KqKiqLV+tp9Wrdraz7ba1lqta60iAopsLoBCJLIEEoKQhSUJIZlsk22yzExm5n3v+f6YmRAQWWYSsN/jeZ55Hsi8c8+553fuufeee+55ge8xERGIKEZV1e1dXV2dPT09ViJ6VAgBIhrrcjobLRYLud3uY0SUQUSXWuQLJn6pBTgPYh0dHaY/P/+8acvmzaEAwhhjRgDGhsZGtuLdd9HW2ioB0F5qQf0h+VILcD6k1WrVkaNykZSUJIQQP+3q6hpjDDKGRkdHh82ZOxdBRuMgAHcB2EVEHEC5y+U6qNFowPl/go19T8nrgqKJ6F9E9DURHag4dsy1fNkyKtxXSJYmCz2w7D7K27adVFV17/jqK3tpSUkPET1DREwIcam7cE76TzAPk91uH7V3T8HIpqamDIPBICclJ8NoDIIhyICJkydhUPwgOJ1Oef3H6wx7Cwp0ADSXWuj/F+QdARk1NTXHbr/lVvr8009VIiqw2WyNn2zcRB+vXUs2m42EEORwOMhca6bW1tYuInqMiPh/wgj43pFX6ZyIQogom4judTp7LEcOH6H2tjY3Ea1obWk5fO01P6I5M2dRU1MTVVVW0qO/fIQ+++RT8rqrh4no90R0PRHleNti38dV0vdiEu6jmGAAwwHMstvtUxsaGrIqjh6LjY2N1Q8fOQLHjh6Vq6qqrk1LS5dvv/MOmM1mWNvbYbVacaC4GIMHDwYAC4DEA8UHHrQ0NTkyB2c2xg0aVB4UFLQTwJdEVAqgGwAYY5e665cWgD6KjwUwx+l0XnO8qmryvr37Ygv37ZVKDpXAbDZj6d13IyUtFb994knUmc261956k2bOmiUefeQRvuLf72Lc+PGYMHECLp8+DQDSAEjbvvwCr73yqiExKSlt+IjhaePGj5837rLLlqWlpe3S6XTrAWwloibg0gJxSQDoo/hoAAs7Ozt/dqC4eMIXW7cG7cj7CjUnTsDpdIIxBiKCEAKyJCNjcCZCQkJgMBhAAAFAQ2MjPli5EnFxcbj2pz9FZUXl1a0tLcxus6O7uxuHy8tRXlaGDevWS8kpKfHTZ0y/7oo5c67KHT26ICQk5D0AG4mo+VIBcdEB8CpfD2CmzWa7f39R0Yy1az40bN+2DS0tLQAAzhgkSQIA+CbSYFMwnnjqKQ8YsswsTU3SzbfcAhKETz/5BHq9HgDwwcr3dV/vyMfQYcNO2QO43W5UHDuGimPHsP7jdUEzZs2aee11100cM27sdUaj8e8AthOR42KDcNEA6GP1KUKI+44dPXrr+ytWRG9ctx4WiwWMsbNumlwuFwp274GskZGRmYnf/M9jqK+vx8/vuB0Lrr4askZGYmIiZs+Zg5ycHDpQfIARUa9VM8Z6/93a2ooPV6/Gjrw8w9XXLJp/w003jRsydOi/OecvEdEJ3/P/bwDwKp8DmNbd3f3k5s8/n/r6q6/KZaVlEEKcc7fKGENnZyf+8Pvfw2g04vk//wk9PT2w2WxIT89AbW0tigoLUbx/P2659dauCRMnNlVUVGT2BeD09hhjsFgsePvNt7Bn957oJXcvfeDK+fPHBQcH/xbAV0QkLgYIAw6AV/k6ADfUmc2Pv/XGmxkfrFyJDqsVnPPzDhVIkoSY2Bjo9XoEGY34n9/8BkKoSExMwttvvok1q1cjNTUVVy1Y6IiNi6sHIQM4uwZ9vEtLSvDkY4/LZSWl0+9YcucbCYmJzwJ4n4icAw3CgALgVX4QgHtLS0of/csLL0Rt3bwZqqqet+IZAEVRQILw22eeAQB8uGo1Sg4dwoJFV2NYVhYiIiMQHx+PmNhYaLQa5v3ZeRPnHJ2dnXjrjTdQU1OT/tDDv3g+Z/jwKAAvE5F9IEEYMAC8yg8G8FDhvn2/fO7Z34UU7Nlzii8+H2Kco6mxEU88/jjuWHInsnNysGfPbnz2yaeIiYsFCYLRGIxn//AcZWRkdiQlJTUAcOECQWCMQVVVfP7pp2hva4v69WOPPT7usvF6AC8SUfdAgTAgsSCv8g0A7ivcu++Rp37zREjBnj3gnPs1uWl1WmRkZMAYHAxZlnHF7Nm49ec/R1paGl55+WW89cYbkCRJyc7JXmkwGJ4H0Onhc2E7X99CoGDPHjz1xBMhhfv2PQLgPgCGgdpF9zsAXkElAD87dPDgo08/9ZTpQHGx32FhIQQiI6OwbPn9SEpKghACiUlJ6OzswJdffIG6ujpcNmECwsLC5BPV1dcfP378OafTOScQdXHOcaC4GM889VtTyaFD/w3gZgDSQIDQry6oj4Dzqo9XP/78H/4QVly0P+CYvCRJKDl0CG++/jrCQsNgNJmw+bPPoSgKQkJDcdvtt6Ors4v9/Fe3RAabTJH/+6c/gvPAXAbnHPuLivD8c38If+b3v/tNSmqqGcBn37Wy8pf6DQC32+3757D29vYn/vnKK0k78r4CC1ARgMc11NfV45ONm6DT6ZCSkgLOOWRZhsvpxBuvvwaX04WKigpERUXB6XT2S58YY/gqLw//fOXVpP/+9a+eCAsLOw7gcH+C0G8uSJZlAAgWQvxiw7p1l320Zg36KxwsiJCRmYGZV1wBl8uFw4cP97atKAq+3pGPgj17MGLkSCxbvhxJSUnoL3chhMBHa9Zgw7p1E4QQD8GzsOg36hcAiAj7d+8GgB/tLyq6/s3X32B2u73frISEQHZODq697jof0KeQJEmQJAl1ZjNstm5oNBooitIv/BljsNlsePO111nx/v2LAVxTXV3dbwD3mwsaM2lSSofVuuzdd94Jraqs7NezWMYYKisqUVRYCCHEd+5uW1pa8M5bb+Ob4gPQaLXgnPeLojjnqKysxLvvvBOaOXjwstTU1K8BnOiPvgWspT5hhhvztm8ft3Xzlv6zfAAEhqamJrz95pt45+23oSjKWRXV1NSE3bt3ob6+HoIudCH63cQYw5bNW5C3fft4ADcC4P0Cbj/Jl2mxWG5avWqV3NHR0S8ACAJkDuTGc5j3bcCHq1efVfm9HeIcdpsduvYyjIxjkLmnrUCJMYYOqxVrPlglWyyWGwFk9ofiAgLAawEMwKKd+flDi/YV9ovrEQQMMjEsm8jx4lXAmGg7bBcwpwghMGGQGy9eRbhvIsegENYvIHDOUbhvH3bm5w8DsAhAwMec/TEC4tvb2q7btGGj3NXVFbD1E4Ax8QzPzgHuHCMQb/Is+S60VQYg3kS4Y6zAs7OBMQksYHfEGENXVxc2bdgot7e1XQcgPlDl+Q1AH+QnHC4vH76/qChg6ycAU1IYnprFMDmJwBngr4ERPCOJA5icRHhqFsPUlMBB4JyjqKgI5eXlwwFMOE0XF95egPJoVVWdl78j39jS0hKQ9RMB4xIYfnU5YXCk6LcJ1AfE4AiBRy8njEtgfoMKeEZBa0sL8r/aYVRVdR4CTIkMFIAES1PT5N27dga06RIEpIQzPDQZyIjon0nzTDwyIoCHJnt4BcJDCIHdu3ahqalpCoDEQOTyC4A+yh5ZVVmVWllZ5bf7IQAGDcPPcoFRcTQgyu+Vmzw8fpbLYND4744456iqqkRVZWUqgJEAoKqqf2358yPGGHbk5QHAiCNHjhg7A1h6EgETEoGrhpzr/Kp/iDHgqiGEyxL9n18YY+js6MTRI0eMAEasXr3KbwP02wVNmzHD0NPTM7y8rIz1CcRdEBEAo5Zh4TAgTE8B+ebz5kkeXlcP8/D2l6Xb7UZZaRkcDsfw669fbPBXnkDmgLDOzs7M41VVASljWLRnBFzMpEGCh+ewaP9HAQAcr6pCV1dXBoAwf9sIBIDo9ra22MbGRr/dD2PAZYkMEYaLY/0+IgIiDIQJif67PcYYmhob0dbaGgcgxl9ZAgEgtrWlxdTZ2ekXAATAIANZ0QTpEiTJSxzIivHI4A/2vlSZ1tZWEzyplX7RBXe9zwooot1q1Tp7evxiTASEGhiSwwJzA/4SEZAcCoQZ/N8X9DidsFqtWgDhp+nmvOmCAfDlawIItdtsmvMJkH0XheuBMP3F9f8+Inh4h+n9b0NVFNhsNhlAqL+nZH6dByiKAo1Go7HbHUxVVf9cEAEmHaCXcU4EiAD1AlEieOJBdJYHdLJHBvI9fAHkS2Nx2B0MgMarkwvWQyAHMkQU2JEjZwA/h/YJQE4M4aZRF9b28Bg658jiIPBAg4ceHfg9iP0CwLfp0Gg0/sd/GNCjMLgFAPbdgR8iYFKSwMQL3PAzJs7u2xngFgw9CvPw96cLjPVa/UXbiBGRL3XcZjQGq5Ik+RUNZAA6egg297lHP4N3tFzA53zatLk9MvhjQkQESZZhNAarAGz+6uGCAeiDtDXYFOzWav0LBjIA7Q6gqfuC3W+/EIOHd7vDf/5ajQYmk8kNwHqabs6bAlmBt0RERPQEBQX5pwAGdLsIlW0BSBAgVbZ5ZPDXiwYZgxAREeEA0OyvDIEA0BQVFdUWERnh94GEWwWKGwCHcnFHAYOHZ3G9RwZ/iIgQERGJyKjINgBN/soS0AgIDQurSUwMLAmqqA6oamcXJRLqI8Y8PIvq/W+DiJCUlITQsLAaAK3+thMIAN0mk+nwkGFD/V8BMKC+k7ClAlAv4p1qVQBbKjy8/c2c5JxjyLChMJlMR+C99upXO/7+UAihcM4PZWdnuw0Gg9+jQBDw+VGgvIX5rYwL6jDz8Pr8qP8nb0QEg8GA7OxsN+f8oBDC73CA3wcyXqv/JnPw4LbomBi/AeAMqLES/rWfocM5sK6IMaDTyfCvYoYaq//WT0SIiY1BRmZmK4Bv/L33AAR+JnwkKTn54Kjc3MBSABnwZaXAyoMMbnVgJmQGz4T7/kGGbZUUEBMiwqjc0UhOTj4E4GggcgUEwLNPP91uMpm2Tps+XQ3EDTEATgV4Zz9hbRmDQv17PMkYoBCwtozhnWJCj9u/zRdw0v1Mmz5NDTaZtv72scfbA5HN71hQn6jo9vGXjW9ITUtLPFxeHtDhjLWH8NIehh6F47ocQrA28EN6zoBuF8OHpQyvFxKsDv9dD+ABIC0tDePGj28AsP3J3z0bUDpOfxyFlCanpGy/YvbsgBOzOAPa7YSXdhP+tJOhsp31hiH8aYsBqGxn+NNOhpd2E9rtgSkf8Kx+Zs2ejeSUlO0ASgNVXn/khjo0Gs3K+QsWtCUlJwd8KYMxwKEQ1hwS+OVnwMpDDPVdnsNziXm+P5MOmfe3EvPE9eq7GFYeYvjlZ8CaQwIOxf8dr4+EEEhKTsb8BQvaNBrNSgCOQHNDA7of0Cf/fmd2Tvb2+Vddde1rr74aWC9xUsGHmwnP53t899QUjglJhLQwwKQjaKWTzxEAlwp0ORmqrUBBLUP+CUJFK8GpnF9w7nz7O/+qq5Cdk50HYJfvb/3RV7+pjwXMLS8r+/fyZffFlpeV9dsFDYInJM0YEKpjGBQCxBqBSCODTvKMNqfK0WojNNmAhi6gs8czd7B+Ujzgsf6srCz87eWXLVk52bcA2AwEXlMi4BsyfSbjHcOGDfvopp/dfM/vnn6G9fT09M8VIZxcEXX0EKw9QBnhtBi+APoonDH/5o3vIiKCXq/HjTffTEOzhn0E4Ctf3wOl/sxH6GGcv7zw6kXfzL1yXr/doepLPsVK3OPrT/lwr6sZgE0EEWHulfOw8JpFBznnLwPwLxPhDNQvAPSxhLKo6KgXlyy9uz0rO7vfbkleShJCYFjWMCxZutQaHR39Irwrn+/dNdU+Aq3JHZ375vKHHlSio6P/o0EQQiA6OhoPPPSQkjt69FsA1pzW14CpX1OivII5ALw4d968DcuW30/BwcED4o4GmogIwcHBuPf++zD3yis3MsZeANDvlVMGKietXqfTPb74hhvyl9y9FIGEKS4F+SbdO5fehf+64YYdOp3ucQB1A8Gr3wHoYyHlJpPpF3csWVJw191LYTQa/yPckRACRqMRS++5B3fedVeBKSTkYQBlp/Wt//Q1UB3pY/GXdXZ2/nnFv9+d+so//oG21tbvbUFtT2WWSNyzbBlu/NnNX4d4lL8XGLgacgN6BNIHhBEOh+O5TRs2XvnSX/8qVVVWXnDhpoGWk4iQnpGB5Q8+oC68etHneoP+1wAOAQNbwG/ANdAHhGRVVf+ncN++m//+t78Zv96RD7fbfclHgxACGq0Gl18+DfctX949dvy49yRJ+j2AWmDgqydeNBP0AhEC4Kb6urqH16xenbFyxQqYa82XZDT4rD4xKQk33nQTrlt8fWV8fPyfAbwHoOtiyXNRe92nmtY4p9P5wDcHDly9auXK4K1btqKt1ZNYMNAjwrcQiIiMxJy5c7D4hhu6R+XmbtTpdH8BUARAvZjGcNGdcB+XFAZgQWdn552F+/ZN/GTjJv2OvDw0Njb21hLtt6If3vLHnHPExsVh+vTpWLDo6p5x48fvCQkJeQPAJgAdwMUvX3zJZsE+QAwCsLC7u/unZaWll+3I+yp0965dOHrkCDo6OuBLf+9bAfd82vW5GIlLCAkLxZChQzBp8mRMmz69I2f48L3BwcFr4FF8w/m0O1B0yZchfYCIBDBFVdSFzS3Nkw6Xl6cV799vLC0pQc2JGlgsFti6u+FyuaCq6rc2dsxbb1qr1cJoNCI6JgbJKSkYPnw4cseMtmdlZR2Pio7eLcvyRgA74U2mutQrsUsOgI/6KFQLIAHAWFVVJ9tstlxre3tKQ31DRGNjQ5DFYtFY29tZd7et159zzmA0BiMsPJxiYqLdcYMG2QcNim8Pjwg/YTQaD0iStBMe/14HT03RS654H30/pDgD9ZmwQ+Epc58AIAlAPBFFKYpihKcqLwA4ZFm2Mcaa4XEptfAo2wKPb7+oE+uF0PdTqu+gPqOEez+S9/8qAOH9fG+s+wf6gX6gH+gH+oF+oO8xnXG50Ge14VtlCAA0EKuL01Y27Lt4CSEAz2106SypD+eUk7ztgHPpW98BggEEIk+WC2PnHZjy9sO33JXO8MgZZZNPawAAotxuZYzV2p7b3d2dIISQ9Xp9e3h4eBkRFQCoBqD6tvqMsSEul2ucoihnhYcAyLIsdDrdHiI67hUkQlGUMVarNbe7qytRVYVGr9d1hIWHl3t5VQFQeoXmPFo4u5aRoz3mWxUmmERMY2hlupBDRLQHnn3AKR0WigskFIBLuWRvvZWc3VrPdXoCk/XEgqLWCVvzF8wQDkiaqcLedj25bPysuS6e33IWFHUAkvwWAdHksN5Lzo4IAB4kJZmxoKg9TNa9L4iUvpfD5T7KD+rq6lpQVFi4ZF/B3vGHDx8OaWlp5kJVERxsQnpGes+YseOOTZk6ZUXcoEH/AtBUXlaGxKSkmR+tXvNCaWmp9N2RTALnEluwcGHztBnTFzPGzHabbWFRYdGSvQUFEw8fLg9ttjRzVVVgNAYjLS3NOWbc2MrJU6Z8kJCY+Jbb7a6Dp3RlqFKz+2bX3tczSHWi7wBmXAL0oZCihtql5IkH5cTxrzGtcc0pb7/gGjAGLmwt1zt3//1Bta4QYBJAAjw0CbrJ90dJMVm71NZKOzNGj3QVvnWPUp0vgUn4TiIVcuo0aCfe+8dNssm90GWb4Sp+9xfKsS0GMA6AwHQh0E1aNkFOnrQd3nOGXgC8yjfVmc2/+HD1mgfWrFoVXltb+60qtfk7dug3rFs/Yt78K5++5bbbckeOGvXr1tbW6tCwMN3WLVt0edu3S753f51peEZGRmLq5VOrANTV19c/9PFHHz2yeuUHUdXV1d/i9XV+vm7D+vXZs+fOeeKW224bM3bcuEdcbneFVqMBdVtIMe8F3A6cyYMqfHMQL/14omb0zTnakddn8ODY/xVEXfxknmK0aimbrpRvgOiohcfzEURTKeT06ROlmKyhIKUYwi2pzUeh1uzBWQFgDHLypC6mNX61iEijtlbOV45+blBrC07+jnFIcSOGSEkTxpKtuVaoKrhXVxyA3NLcfNfbb771yMsvvRTuqwze15p9V5KsVitWf7BK++KfX1hcWlLy1LTp00MZECSE6K2j7HszUt83JAkhkJaejpzhww90WK0L3/3XO4+99Je/RlVWVn4nr87OTqz98CP5hT/96ZpDBw/+TqvRRAAQYAyMSQDv8/Ep18fPegKunX81uQ6seFA4u25jABdEIFsLCDRGrS3IFt0WgGsALgNcBrntUGv3Jghn1xTXgRWAUIKh9Hi05Gv7dJ4gMH0YeNzIKsbYNwDS1YZvJorWCkDSnHwWAkrNnmDRXj2HB8dq0Ke/MoCJn37yyT0fvP++0W6392Y8R0VHIz09nTQaDWprapjZbO615m1ffMFSU1N/mpaW9pVGq9WlZ2Swru4u9Dh6UH38eK9Fa7VapKWnQ6vVYuYVs2zJKSn2tR9+uGzFe++FdHd39/KKjIxEemYGaTVaMpvNvLamxjO/gOHrHflsRdJ7ix751aNfRkZGbjulpgQRWFAE5IyZguytUOv2c+rpBBgHubrhKnrHyCOH3KvNWrgdQinhxihZ7ai7UjUXhkF1A1wGM0aBeqyA4oRi3idrWo7ONsx++h1hb9XwmCzIqgsk3BBNpSBXtwcNxiFFDwMzRoGHpUCKHb4fQJNwdd+i1uxOpp4OgDEwQwTgtoPcdoiWIxCWw9OliPRkEFX2AlBbU3PrZ59+ltbe3g5fvYMxY8fSnUvvOjhixMj1ska2H6+qmvPuO/+evuXzz2UhBFRVxdbNm4NmXXHFTy6fPu29Zcvvf0lVVd3RI0fmP/rwL5Oam5tBRIiNi8PTv3v22KD4+C9DQ0Ja21rbIj/d9Mng1paWXl4jRo7EkqVLS0ePGb1O1mg6aqpPzFrx3ruzPt30iVZVVaiqiu3btumvvGr+4hkzZx48dfIlcFMsdJMf2MX0IfvdpetucO76WzTZ2zwgdDdBqdg6WE67fL5qLijRZM6NF83l09Tmw97LBBrI6TOgVOWBuptAHTVQGw9eJieMzWH6sO26ifcwCEUStpYfOzbcn0OWMo/+JQ00425v1wxb8B5ItDJ92DYAWtFePV+p3++t3cAgJU8EtVVBtZSBHFYodYUZ8uA5k0SnudJ3QCQfP3587rGjR7jPGmPj4nDXPXcfXLBw4Z0AChVFQWJi4vtBQUF/qT1x4ieHDh0C5xz19fUoLS0Zefn0aeVms3nVxIkTw2tOnEiXZDnJpx6NLCMoKKg4NTX1YQBy3vbtK8rLyiRfRnVERARuv/OO0h9f+5MlAPYQCUpMTHwvJDTkhfq6+v/at3cvOOewNDWhrKQ0e8bMmWnwBtz6zL6AUCp4cOxjmpGLm9Xmw0+6v/mgd3WnNh6ShLV2tCZzroZU9zilZk8m2TyVBZghDJqhCyDaKqF2N4LcPVDNhbEi+8ezuCHs9wiK3MEAAzm7BjMu55ysQsTAuNTGDeEvEXAMqguANFY0lkyg9hOeZyQtNOkzoOhMUC2lAKlQawv0oq1qlhQ9dA0BTgDgzRZLQldnV6+vzsjMQG5u7np44ueQZRnV1dW1OcOHrxgzbmxvap7b7UZ9XV2ooigxQzzv8VXpDOlv3r+pAAyNjY3xvhpzQgikZaRj9NixGwAUACDGOOrr6xuGZWW9O3LUqE4fL0VRYDabDQoQ8e0ZvtdL93BD2BYpPrcVshY+ZZGjDWRviQUQLqw181Tz3mCQChCBh6USHzTCwqOH9dZsUev3c9F8eAa8Zci8nTjrBsix7l4Ih3WqcmJXnMdNEbgxCnzQqBYpbrgDkhYAh2g5BrXx0FQA6RCeGglcVdVTXs+k1WgF59z6+qv/JN+kaDKZoNfrbYagIOWkTASXy8UURZHOUbbMJ7ykuN1S3+w4nVZHWq22fdnSu3vDyHq9HrIsd+h0OrcPACKCoriZEOe8z+BiXFZP2T+RAIQiAUhXm8uniZYKABxgDFJMloubBu2S4kZ1QtYDYBAddVDrinIJGEkuO86Hgq59I0y0Hpun1hfJXoHBo4aAh6cU84gMM9OHev7s7IJavz+V3I5pyglPuWduMpk6fSVnGGNobGzkFotlypK7l4b7Oh8ZGSmbzeYJlccqjD6wJElCZFS0U6/XdxqDz+u9No6wsLAO32tnGWNoaGhg5traqS//89VIH6+IiAippbn58tramtCTJ14c4RERipafoSSAFyMAjFT3aLW1KoIU3x6BwGQDmDbYSiRGqzV7Uqin3XvRQAIpTo1SvnEq2VtCmKT1/ER1Qq0riiZb81yuDTr32+BUNwCMUBtLxoqOOu+KjAFMglK5bbxoq0pkkq+kD0E179OI1spZmrRpQWAMPDklZX98fHzvcrCyogKrP1h1ZW1t7eMARgPIslqtyzeuX39PQUFBr/8OCQ3F4MGDqwFUn0fJGgagKzU9vSwhMbE3Q6HmxAl8/OFHc05UVz8FYCyAYZ2dnfesX7d+2c6vd8o+XiaTCUOHDqsHUINTFv/eSluyLoZI3O6u+OKXytHP9PCVUiMCD08BC09tFM1HL1dr9xp653AiuEvXcsemB6Ncu/6mIbcNPv+uNh6Eajk8HUDUuQBw7n0NwtY6Ta3ZHQ3FuzdhHMrxr9Cz6eEw57ZnDaLb0guMaKuC0lA8AcAQKE7IqalpKyZNmTLmyJEjIUQEt9uNNatWGevr65aPGz9+sVarVUpLSmK3f7lN3+X136qqYtz48TRm7NgvAFjOI0bE6urqlMGDB2+ZMXPmjWWlpSG+Fc7Ha9caGhsb775swoSf6A16V3lZWey2L7cZ2tvaenmNHDUKo3JHbQFg9sy6PuIgeyvcJWuvIEf7LKVqu160H4dvBwpZCyllSjs3xZnd1fm3eb5jvQDA7TipXeZRHBiH6GqEat6bLaddPpoBeWfpF+knLYtwV+fPVhsO9LmRRoDqhGck9mkbDOSyQTUXJYjsH83s/seEA7Ix2LjuJ9ddO6estPT6XTt3QpIkOBwObN28Rc7btj2Bcw6Xy9XrNlRVRXpGBq5fvHh/ckryuw6HQzUYzl06OSEhAQC2XbVwwccHDhy4ZUdeHmOMoaenB9u+/FLO37Ej/ky8UlJScOPNN5VkZGa+De/K4SSsDKK7Cc6df9F5JjU6iQ8R5JSp0Aye+ykUp6LWFiSRs8vzPRF49FDISRMEGBMAY6qlTFLN+3rdimouDKfO+itZSPyus4wCQcBoteGbUaKrobdtZoqFnDFLMEkrAAbRVS8pVXnM666gNhTLorViVshDZW/LVqu1ZVRu7jPLlt8fK8vy9N27dvXmbPadXH2+PzsnB3fdvfToFXNmPwGg/BTlkyfaKITwJEOdtihy2B0dw0eMeO7+5cvjZUmanb9jB3O5XGfkRUQYOmwo7rxradWcefOe2LRpU8nChQszTnE9vot6wltgghhAKsBlyClToZt0324pJutVpW7/g4p5n9bjfjy3+TRZC226KQ++AqEegayNdpd8dH9PU+kgctkAxqBayqBayqfzkPhBvQxP2wQC0FBXw2y1dm84FJdnt0wCcspkVX/Fk+8zjSEfXDao5sI7RFPZSE/og0G0n4BaVzhajh89Ug4LC8PxqqqSGTNn3hsVHf3oxvXrF+76emdEbU0N7A4HQAStToe4uDiMHTvWvujHP9o5YeLE52RZzvNZqg8cvUGPhMREaHWeviYkJECr0/XKbAgywGq1Hpk0ZfK94RHhv9q4fv01X+fnR9acqGF2ux1EBK1Wi5jYWIweM8ax6JpFBZOnTPmDRqvdumDhQgBg4BKgMYDhtBt5jAGyATwkAVLqlG5Nzo+/lGKHPwlAL5rLJ1F3M5jW6HlWHwoeNbSScfnvxOUTDDDxqCFTeETaArW1AoxxwGWDWl88WM68YiwYE5D1Hr6MAxoDwCUCEKs2H5kmWo6BaYPgWf9rIMWPsXJ96Fuk9OQxxjkLiU/gUUNGClszGJdAQoXacDBWONoXsb7WDU813Cvr6upurDlxYmJ7e3uUEMRNpuDOxKTkb5JTkteEhoauhfe2SN9lIoDgzo7OP1ZXH5/kcrlUANDr9VJaevrnRqPxSQDOvmABMDns9rl1dXU31tTUTG5rbY0RgnhwsLErMSnpUHJy8kdh4eEfwjPx+kLfmWpb1Wdq/YFM0Gm1xrgGzBDWyUMSDvCQ+JVMa1wrejosTBf6C7Wp5HnRXH4yoqYJgpww9gNuivs5AT0MYMLZ/Yxq3vcY2Zo9YBKBhyVBShj/KrntkWptwU/J0d67wpHic4/yiPS/ibaqp9X64ojeiZ/LkOLH7JfCU64moJ7sbWBa42K1vvhtYa0x+NpmxijISROP/h8iQuzU1deNYwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNy0wOC0yOFQwNjoxMDoxMSswMDowMLqYeLUAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTctMDgtMjhUMDY6MTA6MTErMDA6MDDLxcAJAAAARnRFWHRzb2Z0d2FyZQBJbWFnZU1hZ2ljayA2LjcuOC05IDIwMTQtMDUtMTIgUTE2IGh0dHA6Ly93d3cuaW1hZ2VtYWdpY2sub3Jn3IbtAAAAABh0RVh0VGh1bWI6OkRvY3VtZW50OjpQYWdlcwAxp/+7LwAAABh0RVh0VGh1bWI6OkltYWdlOjpoZWlnaHQAMTkyDwByhQAAABd0RVh0VGh1bWI6OkltYWdlOjpXaWR0aAAxOTLTrCEIAAAAGXRFWHRUaHVtYjo6TWltZXR5cGUAaW1hZ2UvcG5nP7JWTgAAABd0RVh0VGh1bWI6Ok1UaW1lADE1MDM5MDA2MTFH9HWWAAAAD3RFWHRUaHVtYjo6U2l6ZQAwQkKUoj7sAAAAVnRFWHRUaHVtYjo6VVJJAGZpbGU6Ly8vbW50bG9nL2Zhdmljb25zLzIwMTctMDgtMjgvNGFjODY0NDc2NzNhODdmZjM2YmRhMjY1ZTUxMjIzZmIuaWNvLnBuZ7+XtZEAAAAASUVORK5CYII="
  });

})();
