'use strict';

angular.module('BlurAdmin', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  'xeditable',
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',
  'restangular',
  'ui.grid',
  'ui.grid.resizeColumns',
  'ui.grid.pagination',
  'ui.grid.autoResize',
  'ui.grid.selection',
  'ui.grid.expandable',
  'ui.grid.pinning',
  'ui.grid.exporter',
  'BlurAdmin.theme',
  'BlurAdmin.pages',
  '720kb.datepicker',
  'ngFileUpload',
  'ui.rCalendar',
  'growlNotifications',
])

.config(function (RestangularProvider) {
 // RestangularProvider.setBaseUrl('https://localhost:8443');
   RestangularProvider.setBaseUrl('https://dev.otodata.co.id:8383//');
   // RestangularProvider.setBaseUrl('https://live.otobus.co.id:8585//');
});
