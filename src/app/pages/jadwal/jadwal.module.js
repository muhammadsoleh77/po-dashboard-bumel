(function () {
  'use strict';

  angular.module('BlurAdmin.pages.jadwal', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        // .state('dashboard.jadwal', {
        //   url: '/jadwal',
        //   template : '<div ui-view></div>',
        //   abstract: true,
        //   title: 'Penjadwalan',
        //   sidebarMeta: {
        //     icon: 'ion-calendar',
        //     order: 201,
        //   },
        // })
        .state('dashboard.jadwal.add', {
        url: '/add',
        templateUrl: 'app/pages/jadwal/jadwal.rencana.form.html',
        sidebarMeta: {
            order: 1,
        },
        title: 'Rencana Jadwal Pelayanan',
          controller: "RencanaAddCtrl",
          controllerAs: "rencanaAddtrl",
      })
      //   .state('dashboard.jadwal.listed', {
      //   url: '/listed',
      //   templateUrl: 'app/pages/jadwal/jadwal.listed.list.html',
      //   title: 'Listed',
      //     controller: "JadwalListedCtrl",
      //     controllerAs: "jadwalListedCtrl",
      //     sidebarMeta: {
      //       order: 2,
      //     },
      // })
        .state('dashboard.jadwal.pertanggal', {
        url: '/pertanggal',
        templateUrl: 'app/pages/jadwal/jadwal.pertanggal.list.html',
        title: 'Jadwal Pelayanan Terdaftar',
          controller: "JadwalsListPerDayCtrl",
          controllerAs: "jadwalsListPerDayCtrl",
          sidebarMeta: {
            order: 3,
          },
      });

  }

})();
