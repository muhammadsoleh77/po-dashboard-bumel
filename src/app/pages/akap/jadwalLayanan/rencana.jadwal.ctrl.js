(function () {
  'use strict';

angular.module('BlurAdmin.pages.akap')

/* Define the Repository that interfaces with Restangular */
.factory('JadwalRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function JadwalRepository() {
      AbstractRepository.call(this, restangular, 'jadwals');
    }

    AbstractRepository.extend(JadwalRepository);
    return new JadwalRepository();
  }
])

.controller('RencanaCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {

  $state.current.tag = 'C.1';

  $http.get(API_ENDPOINT.url + 'armadas/valid/' + $rootScope.user.idPo).success(function (data) {
    $scope.armadas = data;
  });

  var paginationOptions = {
      pageNumber: 0,
      pageSize: 25,
      sort: null
    };

  // $scope.trayek = {};
  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    enableRowSelection: true,
    expandableRowTemplate: 'app/pages/expandGrid.html',
    expandableRowHeight: 60,
    columnDefs: [
      { field: 'tanggal', displayName: 'Tanggal', cellFilter: 'date_v1'},
      // { name: 'kodesubtrayek' },
      // { name: 'namabus'},
      // { name: 'jumlahkursi'},
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
       gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });
    }
  };

  var getPage = function() {
    var url= API_ENDPOINT.url+'/po/'+$scope.user.idPo +'/nonappjadwals?page='+paginationOptions.pageNumber+'&per_page='+paginationOptions.pageSize + '&armada=' + $scope.armada.idBus;

    $http.get(url).success(function(data) {
      // $scope.gridOptions.data = data.content;
      for(var i = 0; i < data.jvs.length; i++){
        data.jvs[i].subGridOptions = {
          columnDefs: [
          {name:"Asal", field:"namaawal"},
          {name:"Tujuan", field:"namaakhir"},
          {name:"Harga Pokok (Rp)", field:"hargadasar", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.hargadasar | currency:"":0 }}</p>'},
          {name:"Komisi Pemesanan (Rp)", field:"komisipemesanan", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.komisipemesanan | currency:"":0 }}</p>'},
          {name:"Komisi Pemberangkatan (Rp)", field:"komisipemberangkatan", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.komisipemberangkatan | currency:"":0 }}</p>'} ],
          data: data.jvs[i].hargadasar
        };
      }
      $scope.gridOptions.data = data.jvs;

      $scope.expandAllRows = function() {
         $scope.gridApi.expandable.expandAllRows();
       };

      $scope.gridOptions.totalItems =  data.totalPageSize;
      // if (paginationOptions.pageNumber>=1) {
      //   var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      // } else {
      //   var firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      // }
      // $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

      $scope.docDefinition = {
        defaultStyle : {
          fontSize : 9
        },
        content : [
          {
            widths: ['*','*','*','*',],
            fontSize: 16,
            marginBottom : 5,
            bold: true,
            alignment: 'left',
            text: 'PO.'+$rootScope.po.nama, style: 'header'},
          {
            text:  'Rencana Perjalanan Armada ' + $scope.armada.id,
            marginBottom : 5,
            fontSize: 12,
            alignment: 'left',
          },
          {
            color: '#444',
            margin: [0, 5, 0, 10],
            table: {
              widths: ['*',60,60,70,'*','*'],
              headerRows: 2,
              fontSize : 6,
              body: [
                [
                  {text: 'Tanggal', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Asal', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Tujuan ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Harga Pokok', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Komisi Pemesanan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Komisi Pemberangkatan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                ],
                [
                  {},
                  {},
                  {},
                  {},
                  {},
                  {}
                ]
              ]
            }
          }
        ]
      };

      $scope.exportPdf = function () {
        $scope.gridOptions.data.forEach(function (doc) {
          for (var i = 0; i < doc.hargadasar.length; i++) {
            $scope.docDefinition.content[2].table.body.push([
              {text: $filter('date_v1')(doc.tanggal), fontSize: 7, rowSpan: doc.hargadasar.length},
              {text: doc.hargadasar[i].namaawal, fontSize: 7, },
              {text: doc.hargadasar[i].namaakhir, fontSize: 7, },
              {text: $filter('currency')(doc.hargadasar[i].hargadasar,'',0), fontSize: 7, alignment: 'right'},
              {text: $filter('currency')(doc.hargadasar[i].komisipemesanan,'',0), fontSize: 7, alignment: 'right'},
              {text: $filter('currency')(doc.hargadasar[i].komisipemberangkatan,'',0), fontSize: 7, alignment: 'right'},
            ]);
          }
        });
        pdfMake.createPdf($scope.docDefinition).open();
      };

    });

  };

  $scope.tampil = function () {
    getPage();
  };

})

.controller('JadwalListedCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {

  $state.current.tag = 'C.2';

  $http.get(API_ENDPOINT.url + 'armadas/valid/' + $rootScope.user.idPo).success(function (data) {
    $scope.armadas = data;
  });

var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };
  // $scope.trayek = {};
  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    enableRowSelection: true,
    expandableRowTemplate: 'app/pages/expandGrid.html',
    expandableRowHeight: 60,
    columnDefs: [
      { field: 'tanggal', displayName: 'Tanggal', cellFilter: 'date_v1'},
      // { field: 'kodesubtrayek', displayName: 'Detail Trayek' },
      // { field: 'namabus', displayName: 'Armada'},
      // { field: 'jumlahkursi', displayName: 'Jumlah Kursi', cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.jumlahkursi }}</p>'},
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
       gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
        $log.log(msg);
      });

      //

    }
  };



  var getPage = function() {
    var url= API_ENDPOINT.url+'/po/'+$scope.user.idPo+'/appjadwals?page='+paginationOptions.pageNumber+'&per_page='+paginationOptions.pageSize+'&armada='+$scope.armada.idBus;

    $http.get(url).success(function(data) {
      // $scope.gridOptions.data = data.content;
      for(var i = 0; i < data.jvs.length; i++){
        data.jvs[i].subGridOptions = {
          columnDefs: [
          {name:"", field:"tes"},
          {name:"Asal", field:"namaawal"},
          {name:"Tujuan", field:"namaakhir"},
          {name:"Harga Pokok (Rp)", field:"hargadasar", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.hargadasar | currency:"":0 }}</p>'},
          {name:"Komisi Pemesanan (Rp)", field:"komisipemesanan", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.komisipemesanan | currency:"":0 }}</p>'},
          {name:"Komisi Pemberangkatan (Rp)", field:"komisipemberangkatan", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.komisipemberangkatan | currency:"":0 }}</p>'} ],
          data: data.jvs[i].hargadasar
        };
      }
      // $scope.gridOptions.data = data;
      $scope.refreshData = function () {
        $scope.noData = {
          status : false,
          message : ''
        };

        $scope.gridOptions.data = data.jvs;

        $scope.docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Jadwal Perjalanan Armada ' + $scope.armada.id,
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              color: '#444',
              margin: [0, 5, 0, 10],
              table: {
                widths: ['*',60,60,70,'*','*'],
                headerRows: 2,
                fontSize : 6,
                body: [
                  [
                    {text: 'Tanggal', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Asal', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Tujuan ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Harga Pokok', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Komisi Pemesanan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Komisi Pemberangkatan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  ],
                  [
                    {},
                    {},
                    {},
                    {},
                    {},
                    {}
                  ]
                ]
              }
            }
          ]
        };

        $scope.exportPdf = function () {
          $scope.gridOptions.data.forEach(function (doc) {
            for (var i = 0; i < doc.hargadasar.length; i++) {
              $scope.docDefinition.content[2].table.body.push([
                {text: $filter('date_v1')(doc.tanggal), fontSize: 7, rowSpan: doc.hargadasar.length},
                {text: doc.hargadasar[i].namaawal, fontSize: 7, },
                {text: doc.hargadasar[i].namaakhir, fontSize: 7, },
                {text: $filter('currency')(doc.hargadasar[i].hargadasar,'',0), fontSize: 7, alignment: 'right'},
                {text: $filter('currency')(doc.hargadasar[i].komisipemesanan,'',0), fontSize: 7, alignment: 'right'},
                {text: $filter('currency')(doc.hargadasar[i].komisipemberangkatan,'',0), fontSize: 7, alignment: 'right'},
              ]);
            }
          });
          pdfMake.createPdf($scope.docDefinition).open();
        };

        $scope.expandAllRows = function() {
           $scope.gridApi.expandable.expandAllRows();
         };

         $timeout(function () {
           $scope.expandAllRows();
         }, 400);

        if ($scope.gridOptions.data.length === 0) {
          $scope.noData = {
            status : true,
            message : "Tidak Ada Data Setoran Penjualan"
          };
        }
        // console.log($scope.errorMesssage);
      };

      $scope.refreshData();
      $scope.gridOptions.totalItems = data.totalPageSize;

    });

  };

  $scope.tampil = function () {
    getPage();
  };


})

.controller('JadwalsListPerDayCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $uibModal) {

  $state.current.tag = 'C.2';

  var date = new Date();
  var yyyy = date.getFullYear();
  var mm = date.getMonth()+1;
  var dd = date.getDate();
  var d = date.getDay();

  var thisTglawal = yyyy + '-' + mm + '-01';
  var thisTglakhir = yyyy + '-' + mm + '-31';

  var getJadwal = false;
  $scope.tampil = function() {
    getJadwal = true;
    if (!$scope.trayek) {
      return false;
    }
    // getData(thisTglawal, thisTglakhir);
    // $scope.currentDate = new Date();
    getData(thisTglawal, thisTglakhir);
  };

  $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/trayeks?type=1').success(function(data) {
    $scope.trayeks = data;
   });

  $scope.$watch('currentDate', function () {
    if ($scope.currentDate.date) {
      yyyy = $scope.currentDate.date.getFullYear();
      mm = $scope.currentDate.date.getMonth()+1;
      dd = $scope.currentDate.date.getDate();
      d = $scope.currentDate.date.getDay();
      thisTglawal = yyyy + '-' + mm + '-01';
      thisTglakhir = yyyy + '-' + mm + '-31';
      if ($scope.trayek && getJadwal) {
        getData(thisTglawal, thisTglakhir);
        $scope.$broadcast('eventSourceChanged',$scope.eventSource);
      }
    } else if ($scope.currentDate.events) {
      openJadwalDetail($scope.currentDate.events);
    }
  });

  var openJadwalDetail = function (events) {
    $uibModal.open({
      animation: true,
      templateUrl: '/app/pages/akap/jadwalLayanan/modals/main.html',
      controller: 'detailJadwalCtrl',
      size: 'md',
      resolve: {
        items: function () {
          return {
            jadwal : events,
          };
        }
      }
    });
  };


  $scope.loadData = false;

  $scope.gridOptions = {
  };

  var getData = function(tglawal, tglakhir) {
    $scope.loadData = true;
    $scope.gridOptions.data = [];
    var url = API_ENDPOINT.url + '/jadwals?idtrayek=' + $scope.trayek.id + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir;

    $http.get(url).success(function(data) {

      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          data[i]['title'] = data[i].namabus + ' ' + data[i].subkelas;
          data[i]['endTime'] = new Date(data[i].tanggal);
          data[i]['startTime'] = new Date(data[i].tanggal);
        }
      }
      $scope.eventSource = data;
      $scope.gridOptions.data = data;

      $scope.loadData = false;

      $scope.docDefinition = {
        defaultStyle : {
          fontSize : 9
        },
        content : [
          {
            widths: ['*','*','*','*',],
            fontSize: 16,
            marginBottom : 5,
            bold: true,
            alignment: 'left',
            text: 'PO.'+$rootScope.po.nama, style: 'header'},
          {
            text:  'Jadwal Perjalanan Trayek ' + $scope.trayek.trayek,
            marginBottom : 5,
            fontSize: 12,
            alignment: 'left',
          },
          {
            text:  'Tanggal : ' + $filter('date_v2')($scope.currentDate),
            fontSize: 8,
            marginBottom : 20,
            alignment: 'left',
          },
          {
            color: '#444',
            margin: [0, 5, 0, 10],
            table: {
              widths: ['*',60,60,70,'*','*'],
              headerRows: 2,
              fontSize : 6,
              body: [
                [
                  {text: 'Tanggal', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Asal', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Tujuan ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Harga Pokok', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Komisi Pemesanan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Komisi Pemberangkatan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                ],
                [
                  {},
                  {},
                  {},
                  {},
                  {},
                  {}
                ]
              ]
            }
          }
        ]
      };

      $scope.exportPdf = function () {
        $scope.gridOptions.data.forEach(function (doc) {
          for (var i = 0; i < doc.hargadasar.length; i++) {
            $scope.docDefinition.content[3].table.body.push([
              {text: $filter('date_v1')(doc.tanggal), fontSize: 7, rowSpan: doc.hargadasar.length},
              {text: doc.hargadasar[i].namaawal, fontSize: 7, },
              {text: doc.hargadasar[i].namaakhir, fontSize: 7, },
              {text: $filter('currency')(doc.hargadasar[i].hargadasar,'',0), fontSize: 7, alignment: 'right'},
              {text: $filter('currency')(doc.hargadasar[i].komisipemesanan,'',0), fontSize: 7, alignment: 'right'},
              {text: $filter('currency')(doc.hargadasar[i].komisipemberangkatan,'',0), fontSize: 7, alignment: 'right'},
            ]);
          }
        });
        pdfMake.createPdf($scope.docDefinition).open();
      };

    });

  };

})

/* Here the controllers are defines */
.controller('JadwalsPilihPoCtrl', function ($filter, $scope, JadwalRepository, $rootScope, Restangular, $http, API_ENDPOINT, $state) {
   $scope.pos = Restangular.all('pilihpo').getList().$object;

  $scope.tampil = function() {
    if ($scope.po == 0) {
      $http.get(API_ENDPOINT.url +'/trayeks').success(function(data) {
                  $rootScope.trayeks = data;

                });
    } else {
    $http.get(API_ENDPOINT.url +'/po/'+$scope.po+'/trayeks?type=1').success(function(data) {
                  $rootScope.trayeks = data;
        });
     }
     $state.go('app.jadwals.pilih');
  }
})

.controller('JadwalsPilihCtrl', function ($filter, $scope, JadwalRepository, $rootScope, Restangular, $http, API_ENDPOINT, $state) {
  // $scope.jadwals = JadwalRepository.getList();
  $scope.trayeks = $rootScope.trayeks;
  // $scope.trayeks = Restangular.one('po', $rootScope.po.idPo).getList('trayeks').$object;
  // $scope.tglawal={};
  // $scope.tglakhir={};
  $scope.tampil = function() {
    $http.get(API_ENDPOINT.url +'/jadwals?idtrayek='+$scope.trayek+"&tglawal="+$filter('date')($scope.tanggalawal, "yyyy-MM-dd")+"&tglakhir="+$filter('date')($scope.tanggalakhir, "yyyy-MM-dd")).success(function(data) {
         $rootScope.jadwals = data;
    });
    $state.go('app.jadwals.list');
  }

})

 .controller('JadwalsListCtrl', function ($filter, $scope, JadwalRepository, $rootScope, Restangular, $http, API_ENDPOINT, $state) {
    $scope.jadwals=$rootScope.jadwals;

     $scope.delete = function (data) {
      if(window.confirm('Apakah anda yakin?')) {
        JadwalRepository.remove(data).then(function () {
            $scope.jadwals = JadwalRepository.getList();
          });
      }
    };
  })

.controller('JadwalsItemCtrl', function ($scope, $stateParams, JadwalRepository) {
  $scope.jadwal = JadwalRepository.get($stateParams.id).then(function (data) {
    $scope.jadwal = data;
  });
})

.controller('JadwalsEditCtrl', function ($scope, $stateParams, $location, JadwalRepository) {
  $scope.jadwal = JadwalRepository.get($stateParams.id).then(function (data) {
    $scope.jadwal = data;
  });
  $scope.save = function () {
    $scope.jadwal.put().then(function () {
      $location.path('/jadwals/' + $stateParams.id);
    });
  };
})

.controller('RencanaAddCtrl', function ($rootScope, $scope, $location, $filter, Restangular, $state, $q, $http, API_ENDPOINT, JadwalRepository, $uibModal, $window, $timeout) {

  // $state.current.parrentState = 'dashboard.jadwal.rencana.list';
  // $state.current.parrentTitle = 'Rencana';
  $state.current.tag = 'C.1';

  $scope.clicked = false;

  var url = [
    // 'app/pages/jadwal/modals/form.html',
    // 'app/pages/jadwal/modals/select.bus.html',
    // 'app/pages/jadwal/modals/select.trayek.html',
    'app/pages/akap/jadwalLayanan/modals/form.html',
    'app/pages/akap/jadwalLayanan/modals/select.bus.html',
    'app/pages/akap/jadwalLayanan/modals/select.trayek.html',
    'app/pages/akap/jadwalLayanan/jadwal.output.html',
  ];

  var date = new Date();
  var yyyy = date.getFullYear();
  var mm = date.getMonth() + 1;
  var dd = date.getDate() - 1;
  if (mm < 10) {
    mm = '0' + mm;
  }
  if (dd < 1) {
    dd = '0' + dd;
  }
  var now = yyyy + '-' + mm + '-' + dd +',23:59';
  $scope.now = new Date(now); // tanggal sekarang
  $scope.limit = String($scope.now); // tanggal sekarang

  $scope.templateUrl = url[0];
  //ambil data trayek, subtrayek, armada, hargadasar, waktutiba

  if ($rootScope.selectedTrayekTmp && $rootScope.selectedIdTrayekTmp && $rootScope.selectedTanggalTmp) {
    $scope.trayek = $rootScope.selectedTrayekTmp;
    $scope.subtrayeks = Restangular.one('trayek', $rootScope.selectedIdTrayekTmp).getList('subtrayeks').$object;
    $scope.tglawal = $rootScope.selectedTanggalTmp;
  }

  $scope.trayek_change = function(trayek) {
  };

  $scope.openBusSelection = function () {
    $scope.templateUrl = url[1];
    $scope.armadas = Restangular.one('armadas/valid/').getList($rootScope.user.idPo).$object;
    // $scope.armadas = Restangular.one('po', $rootScope.user.idPo).getList('armadas').$object;
    $scope.selectBus = function (data) {
      $scope.armada = data;
      $scope.selectedArmada = "Bus " + data.id + " (" + data.kelas + ")";
      $scope.templateUrl = url[0];
    };
  };

  $scope.openTrayekSelection = function () {
    $scope.templateUrl = url[2];
    // $scope.trayeks = Restangular.one('po', $rootScope.user.idPo).getList('trayeks').$object;
    $scope.trayeks = Restangular.one('po', $rootScope.user.idPo).getList('trayeks?type=1').$object;
    $scope.selectTrayek = function (data) {
      $scope.subtrayeks = Restangular.one('trayek', data.id).getList('subtrayeks').$object;
      $scope.hargadasars = [];

      // validasi dp minimal berdasarkan track by $index
      $scope.dpMinimal = function(index) {
        if($scope.hargadasars[index].minimalDp > $scope.hargadasars[index].hargadasar) {
          $scope.hargadasars[index].minimalDp = $scope.hargadasars[index].hargadasar;
          alert('DP Minimum tidak boleh melebihi harga pokok');
        }
        else if($scope.hargadasars[index].minimalDp < 0) {
          $scope.hargadasars[index].minimalDp = $scope.hargadasars[index].hargadasar;
          alert('DP Minimum tidak boleh di bawah nol');
        }
      }
      // end

      // validasi harga minimal berdasarkan track by $index
      $scope.minimalHarga = function(index) {
        if($scope.hargadasars[index].hargaMinimal > $scope.hargadasars[index].hargadasar) {
          $scope.hargadasars[index].hargaMinimal = $scope.hargadasars[index].hargadasar;
          alert('Harga Minimum tidak boleh melebihi harga pokok');
        }
        else if($scope.hargadasars[index].hargaMinimal < 0) {
          $scope.hargadasars[index].hargaMinimal = $scope.hargadasars[index].hargadasar;
          alert('Harga Minimum tidak boleh di bawah nol');
        }
      }
      // end

      $scope.trayek = data.trayek;
      $scope.templateUrl = url[0];
      $scope.ubahHarga = data.ubahHarga;
      $scope.isDp = data.isDp;
    };
  };

  $scope.back = function () {
    $scope.templateUrl = url[0];
  };

  $scope.subtrayek_change = function(subtrayek) {
      $scope.subtrayek = subtrayek;
      $scope.hargadasars = Restangular.one('subtrayek', subtrayek).getList('hargadasars').$object;
    };

    $scope.tampil = function(data) {
      // $scope.clicked_tampil = true;

      var valid = 5;
      $scope.errorForm1 = "";
      $scope.errorForm2 = "";
      $scope.errorForm3 = "";
      $scope.errorForm4 = "";
      $scope.errorForm5 = "";

      if (!$scope.trayek) {
      $scope.errorForm1 = "Pilih Trayek!";
      valid--;
      }
      if (!$scope.tglawal) {
        $scope.errorForm2 = "Pilih Tanggal Awal!";
        valid--;
      }
      if (!$scope.tglakhir) {
        $scope.errorForm3 = "Pilih Tanggal Akhir!";
        valid--;
      }
      if (!$scope.selectedArmada) {
        $scope.errorForm4 = "Pilih Armada!";
        valid--;
      }
      if (!$scope.subtrayek) {
        $scope.errorForm5 = "Pilih Detail Trayek!";
        valid--;
      }

      if (valid !== 5) {
        $window.scrollTo(0,0);
        // $scope.clicked_tampil = false;
        return false;
      }

      $scope.templateUrl = url[3];

    }

    $scope.saveBaru = function () {
      $scope.clicked = true;

      var valid = 5;
      $scope.errorForm1 = "";
      $scope.errorForm2 = "";
      $scope.errorForm3 = "";
      $scope.errorForm4 = "";
      $scope.errorForm5 = "";

      if (!$scope.trayek) {
      $scope.errorForm1 = "Pilih Trayek!";
      valid--;
      }
      if (!$scope.tglawal) {
        $scope.errorForm2 = "Pilih Tanggal Awal!";
        valid--;
      }
      if (!$scope.tglakhir) {
        $scope.errorForm3 = "Pilih Tanggal Akhir!";
        valid--;
      }
      if (!$scope.selectedArmada) {
        $scope.errorForm4 = "Pilih Armada!";
        valid--;
      }
      if (!$scope.subtrayek) {
        $scope.errorForm5 = "Pilih Detail Trayek!";
        valid--;
      }

      if (valid !== 5) {
        $window.scrollTo(0,0);
        $scope.clicked = false;
        return false;
      }
      var detailrencana = [];
      // while (awal<=akhir) {
        for (var i=0;i<$scope.hargadasars.length;i++){
            // var dateStrToSend = $scope.tglBerangkat.getFullYear() + '-' + $scope.tglBerangkat.getMonth() +  '-' + $scope.tglBerangkat.getDate();
              var detail = {
                  hargadasar : {id : $scope.hargadasars[i].id},
                  hargaAsal : $scope.hargadasars[i].hargadasar,
                  komisiPemesanan : $scope.hargadasars[i].komisipemesanan,
                  komisiPemberangkatan : $scope.hargadasars[i].komisipemberangkatan,
                  biayaAdmin : $scope.hargadasars[i].adminsistem,
                  iuranIpomi : $scope.hargadasars[i].iuranipomi,
                  hargaJual : $scope.hargadasars[i].hargadasar+$scope.hargadasars[i].komisipemesanan+$scope.hargadasars[i].komisipemberangkatan  +$scope.hargadasars[i].adminsistem+$scope.hargadasars[i].iuranipomi,
                  kotaAwal : $scope.hargadasars[i].namaawal,
                  kotaAkhir : $scope.hargadasars[i].namaakhir,
                  hargaMinimal : $scope.hargadasars[i].hargaMinimal || $scope.hargadasars[i].hargadasar,
                  minimalDp : $scope.hargadasars[i].minimalDp || $scope.hargadasars[i].hargadasar,
                  // hargaMinimal : $scope.hargadasars[i].ubahHarga,
                  // minimalDp : $scope.hargadasars[i].isDp,
                };
                detailrencana.push(detail);
        }


        $scope.rencanajadwal = {
            // tglawal : $filter('date')($scope.tglawal, "yyyy-MM-dd"),
            // tglakhir : $filter('date')($scope.tglakhir, "yyyy-MM-dd"),
            tglawal : $scope.tglawal,
            tglakhir : $scope.tglakhir,
            bus : {idBus : $scope.armada.idBus},
            subtrayek : {id: $scope.subtrayek},
            drs : detailrencana,
            status : false
          };

          $http.post(API_ENDPOINT.url +'/createjadwals', $scope.rencanajadwal)
            .success(function () {
              $rootScope.createJadwal = true;
              // alert("Rencana Jadwal Berhasil Dibuat");
              $uibModal.open({
                animation: true,
                templateUrl: '/app/pages/akap/jadwalLayanan/modals/create.jadwal.html',
                controller: 'thisWeekJadwalCtrl',
                size: 'sm',
                resolve: {
                  items: function () {
                    return {
                      jadwal : null,
                      // trayek : detail.trayek,
                      // idTrayek : detail.idTrayek,
                      // tanggal : jadwal.tanggal
                    };
                  }
                }
              }).result
                .then(function (c) {
                  if (c) {
                    $state.reload();
                  }
                });
              // $state.go('dashboard.today');
            });
              // JadwalRepository.create($scope.jadwal).then(function () {
              // });
      //   awal=new Date(awal.setDate(awal.getDate()+1));
      // }

  };

  $scope.getTglAwal = function (tglawal) {
    $scope.tglawal = tglawal;
  };

  $scope.getTglAkhir = function (tglakhir) {
    $scope.tglakhir = tglakhir;
  };


  $scope.saveJadwal = function () {
          for (var i=0;i<$scope.hargadasars.length;i++){
            // var dateStrToSend = $scope.tglBerangkat.getFullYear() + '-' + $scope.tglBerangkat.getMonth() +  '-' + $scope.tglBerangkat.getDate();
            $scope.jadwal = {

                tglBerangkat : $scope.tglBerangkat,
                hargaPokok : $scope.hargadasars[i].hpp,
                hargaJual : $scope.hargadasars[i].harga,
                komisiAgen : $scope.hargadasars[i].komisi,
                hargadasar : {id : $scope.hargadasars[i].id},
                bus : {id : $scope.armada}
              };
              JadwalRepository.create($scope.jadwal).then(function () {
                $state.go('app.jadwals.list');
              });
          };
  };

});
})();
