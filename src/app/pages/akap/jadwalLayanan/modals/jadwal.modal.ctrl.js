/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akap')
      .controller('detailJadwalCtrl', detailJadwalCtrl);

  /** @ngInject */
  function detailJadwalCtrl($uibModalInstance, $scope, items, $state, $rootScope) {

    var templateUrl = [
      // '/app/pages/jadwal/modals/jadwal.html',
      // '/app/pages/jadwal/modals/detail.html',
      '/app/pages/akap/jadwalLayanan/modals/jadwal.html',
      '/app/pages/akap/jadwalLayanan/modals/detail.html',
    ];

    $scope.templateUrl = templateUrl[0];

    $scope.jadwal = items.jadwal;

    $scope.showDetail = function (detail) {
      $scope.detail = detail;
      $scope.templateUrl = templateUrl[1];
    };

    $scope.back = function () {
      $scope.templateUrl = templateUrl[0];
    };

    // $scope.addJadwal = function () {
    //   $rootScope.selectedTrayekTmp = items.trayek;
    //   $rootScope.selectedIdTrayekTmp = items.idTrayek;
    //   $rootScope.selectedTanggalTmp = items.tanggal;
    //   $uibModalInstance.dismiss();
    //   $state.go('dashboard.jadwal.add');
    // };
    $scope.close = function () {
      $uibModalInstance.dismiss();
    };
  }
})();
