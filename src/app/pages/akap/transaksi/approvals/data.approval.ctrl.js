(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akap')
  .controller('approvalsCtrl', approvalsCtrl);

  function approvalsCtrl(API_ENDPOINT, $http, $uibModal, $scope, $state, $rootScope) {

    $scope.loadData = true;

    // var templateUrl = [
    //   'app/pages/akap/laporan/approvals/approval.templates.html',
    // ];
    //
    // $scope.templateUrl = templateUrl[0];

    $http.get(API_ENDPOINT.url + 'po/cancel/approval/' + $scope.user.idPo).success(function(data) {
      $scope.approval = data.cancelApproval;
      $scope.loadData = false;
      console.log($scope.approval);

      if($scope.approval.length === 0){
        $scope.empty = true;
        $scope.message = 'Data approval kosong';
      }

      var body = {
        transaksiDetail : []
      };

      $scope.klik_approve = function(index) {

        body.transaksiDetail = $scope.approval[index]['transaksiDetail']

        $http.put(API_ENDPOINT.url + 'po/cancelation/void/' + $scope.approval[index].idtransaksi + '/' + $scope.approval[index].idVoid + '?status=true', body).success(function(data) {
          alert('Approve berhasil');
          $scope.approval.splice(index, 1)
          $state.go('dashboard.today');
        });
      }

      $scope.klik_cancel = function(index) {

        body.transaksiDetail = $scope.approval[index]['transaksiDetail']

        $http.put(API_ENDPOINT.url + 'po/cancelation/void/' + $scope.approval[index].idtransaksi + '/' + $scope.approval[index].idVoid + '?status=false', body).success(function(data) {
          alert('Cancel berhasil');
          $scope.approval.splice(index, 1)
          $state.go('dashboard.today');
        });
      }

      $scope.klik_detail = function(index) {
        $http.get(API_ENDPOINT.url + 'po/cancel/approval/' + $scope.user.idPo).success(function(data) {
          $scope.approval = data.cancelApproval;
          console.log($scope.approval);
          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/akap/transaksi/approvals/modal/detail.html',
            controller: 'detailCtrl',
            size: 'lg',
            resolve: {
              items: function() {
                return {
                  data : data.cancelApproval[index]['transaksiDetail']
                };
              }
            }
          })
          // .result.then(function(){
          //   $http.get(API_ENDPOINT.url + 'po/cancel/approval/' + $scope.user.idPo).success(function(data) {
          //     $scope.approval = data.cancelApproval;
          //   });
          // });
        });
      }

    });

  };
})();
