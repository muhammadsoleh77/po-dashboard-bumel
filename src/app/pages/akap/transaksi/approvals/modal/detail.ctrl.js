(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akap')
  .controller('detailCtrl', detailCtrl);

  function detailCtrl(API_ENDPOINT, $scope, $http, $state, items, $uibModal, $uibModalInstance, $rootScope) {

    $scope.detail_modal = items.data;
    // console.log($scope.detail_modal);

    $scope.close = function(){
      $uibModalInstance.close(true);
    }
  }
})();
