(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .controller('keuanganSetoranCtrl', keuanganSetoranCtrl);

    function keuanganSetoranCtrl($filter, $scope, $rootScope, $http, API_ENDPOINT, $state, $log, $timeout, $window) {

      $state.current.tag = 'D.1';
      $scope.loadData = false;

      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth();
      var dd = date.getDate();
      var d = date.getDay();

      $scope.today = date.getTime();

      $scope.getJamBerangkat = function (idTrayek) {
        $http.get(API_ENDPOINT.url + '/trayek/' + idTrayek + '/subtrayeks').success(function (data) {
          var jamBerangkat = ['Semua Keberangkatan'];
          data.forEach(function (item) {
            jamBerangkat.push(item.tempatberangkat[0].waktutiba);
          });
          $scope.listJamBerangkat =  jamBerangkat;
        });
      };

      var bln, tgl;

      if (mm < 10) {
        bln = '0' + (mm + 1);
      }
      if (dd < 10) {
        tgl = '0' + dd;
      } else {
        tgl = dd;
      }

      var now = yyyy + '-' + bln + '-' + dd;
      $scope.now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);
      $scope.limit = now;

      // $scope.dataSetoran = data;

        var getPage = function () {
          $scope.loadData = true;
          var url;
          $scope.templateUrl = '';
          if ($scope.transaksi === 'penjualan') {

            $scope.next = function (data) {
              $state.go('dashboard.akap.transaksi.terimaPenjualan', {
                data : data,
                tgl : $scope.tanggal
              });
            };
            url = API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;
            $http.get(url).success(function (data) {
              if ($scope.trayek) {
                $scope.trayek.id = $scope.trayek.idTrayek || $scope.trayek.id;
              }
              if ($scope.trayek.id !== null) {
                data = $filter('filter')(data, {idTrayek : $scope.trayek.id}, true);
              }
              $scope.datas = data;
              $scope.templateUrl = "/app/pages/akap/transaksi/setoran/penjualan/setoran.penjualan.template.html";
            });
          }

          else if ($scope.transaksi === 'pemesanan') {

            $scope.next = function (data) {
              $state.go('dashboard.akap.transaksi.terimaPemesanan', {
                data : data,
                tgl : $scope.tanggal
              });
            };

            url = API_ENDPOINT.url + '/po/setoran/pemesanan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;

            $http.get(url).success(function (data) {
              if ($scope.agen) {
                $scope.agen.id = $scope.agen.id || $scope.agen.idAgen;
              }
              if ($scope.agen.id !== 0) {
                data = $filter('filter')(data, {idAgen : $scope.agen.id}, true);
              }
              $scope.datas = data;
              $scope.templateUrl = "/app/pages/akap/transaksi/setoran/pemesanan/setoran.pemesanan.template.html";
            });

          }

          // untuk setoran sekarang => bank
          if($scope.po.setoranLangsung == true && $scope.po.setoranCash == false){
            if($scope.agenSekarang) {
              $scope.next = function (data) {
                $state.go('dashboard.akap.transaksi.setoranCash', {
                  data : data,
                  tgl : $scope.tanggal
                });
              };

              url = API_ENDPOINT.url + '/po/setoran/transaksi/bank?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;
              // console.log(url);
              $http.get(url).success(function (data) {
                $scope.datas = data;
                console.log($scope.datas);
                $scope.templateUrl = "app/pages/akap/transaksi/setoran/cash/cash.template.html";
              });
            }
          }
          // end

          // untuk setoran nanti => bank
          if($scope.po.setoranLangsung == false && $scope.po.setoranCash == false){
            if($scope.agenNanti) {
              $scope.next = function (data) {
                $state.go('dashboard.akap.transaksi.setoranNanti', {
                  data : data,
                  tgl : $scope.tanggal
                });
              };

              url = API_ENDPOINT.url + '/po/setoran/berangkat/bank?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;
              $http.get(url).success(function (data){
                $scope.datas = data;
                console.log($scope.datas);
                $scope.templateUrl = "app/pages/akap/transaksi/setoran/nanti/nanti.template.html";
              });
            }
          }
          // end

          // untuk setoran nanti => cash
          if($scope.po.setoranLangsung == false && $scope.po.setoranCash == true) {
            if($scope.trayek) {
              $scope.next = function (data) {
                $state.go('dashboard.akap.transaksi.setoranNantiTrayek', {
                    data : data,
                    tgl : $scope.tanggal
                });
              };

              url = API_ENDPOINT.url + '/po/setoran/berangkat/cash?idPo=' + $scope.user.idPo + '&tgl=' + $scope.tanggal;
              $http.get(url).success(function (data) {
                $scope.datas = data;
                console.log($scope.datas);
                $scope.templateUrl = "app/pages/akap/transaksi/setoran/nantiTrayek/nantiTrayek.template.html";

                // if($scope.datas.transaksi.length === 0){
                //
                // }

                // $scope.isObjectEmpty = function(item.transaksi){
                //   // return Object.keys(item.transaksi).length === 0;
                //   return $scope.datas.transaksi.length === 0;
                // }

              });
            }
          }
          // end

          $scope.loadData = false;
        };

        $scope.tampil = function () {
          getPage();
        };

        if ($state.params.query.tgl) {
          $scope.transaksi = $state.params.query.transaksi;
          if ($state.params.query.trayek) {
            $scope.trayek = $state.params.query.trayek;
          } else {
            $scope.agen = $state.params.query.agen;
          }
          $scope.tanggal = $state.params.query.tgl;
          $scope.tampil();
        }
    }
})();
