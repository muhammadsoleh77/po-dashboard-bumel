(function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.akap')
    .controller('transaksiAkapPenjualanCtrl', function ($scope, Restangular, $rootScope, $http, API_ENDPOINT, $state, $uibModal, $window) {

      $state.current.tag = 'D.4.1';

      /**
       * Path untuk template halaman pemesanan
       */
      var url = [
        'app/pages/akap/transaksi/transaksiManual/penjualan/jual-tiket.html',
        'app/pages/akap/transaksi/transaksiManual/penjualan/pilih-bus.html',
        'app/pages/akap/transaksi/transaksiManual/penjualan/detail-bus.html',
        'app/pages/akap/transaksi/transaksiManual/penjualan/data-penumpang.html',
        'app/pages/akap/transaksi/transaksiManual/penjualan/bayar-tiket.html',
      ];

      $scope.templateUrl = url[0]; // halaman jual-tiket
      $state.current.tag = 'D.4.1'; // halaman pilih-bus

      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth() + 1;
      var dd = date.getDate();
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (dd < 10) {
        dd = '0' + dd;
      }

      var now = yyyy + '-' + mm + '-' + dd;
      $scope.tanggal = now; // tanggal sekarang
      /**
       * Get agen by ID PO
       */
      $http.get(API_ENDPOINT.url +'/po/'+ $scope.user.idPo +'/agen').success(function(data) {
          $scope.agens = data;
          /**
           * Get trayek by ID PO
           */
           $scope.idAgen_change = function (idAgen) {
             $http.get(API_ENDPOINT.url + '/agen/' + idAgen).success(function (data) {
               $scope.akhir = [];
               $scope.asal = data.idWilayah;

               $http.get(API_ENDPOINT.url + '/agen/' + idAgen + '/tujuankota/' + $scope.asal + '?idPo='  + $rootScope.po.idPo).success(function (data) {
                 $scope.akhir = data;
               });
             });
           };

      });
      /**
       * ke halaman pilih-bus
       */
      $scope.next = function () {
        $window.scrollTo(0,0);
        if (!$scope.agen || !$scope.tujuan || !$scope.asal || !$scope.tanggal || !$scope.jmlPenumpang) {
          $scope.errorNext = "Semua data harus diisi!";
          return false;
        }
        $scope.pesanError = "Mencari Tiket...";
        $rootScope.listTicket = [];
        var datas = [];
        $http.get(API_ENDPOINT.url + '/agen/detailpenjualan/' + $scope.user.idPo + '/' + $scope.asal + '/' + $scope.tujuan + '?idAgenBrgkt=' + $scope.agen).success(function (data) {
          $scope.pesanError = '';
          if (data.length === 0) {
            $scope.pesanError = "Tiket tidak tersedia";
          }
          // console.log(datas);
          $rootScope.listTicket = data.penjualans;
        }).error(function () {
          $scope.pesanError = "Tiket tidak tersedia";
        });
        $scope.templateUrl = url[1];
        $state.current.tag = 'D.4.1.1'; // halaman pilih-bus
      };

      /**
       * kembali kehalaman sebelumnya
       */
      $scope.backSearch = function (index) {
        if (index == 2) {
          $scope.kursis = [];
          $scope.listKursi.forEach(function (doc) {
            $scope.kursis.push({
              kursi : doc
            });
          });
          // console.log($scope.kursis);
          $http.put(API_ENDPOINT.url + '/agen/jadwal/' + $scope.ticketDetail.pemesananJadwal.idJadwal + '/bookingkursi', $scope.kursis).success(function (data) {
            // $uibModalInstance.close($scope.kursiTerpilih);
          });
        }
        $window.scrollTo(0,0);
        $state.current.tag = index > 0 ? 'D.4.2.' + (index) : 'D.4.2'; // halaman pilih-bus
        $scope.templateUrl = url[index];
      };

      $http.get(API_ENDPOINT.url + '/po/' + $rootScope.po.idPo + '/trayeks?type=1').success(function (data) {
        $scope.trayeks = data;
      });
      /**
       * ke halaman detail-bus
       */
      $scope.detailBus = function (data) {
        $window.scrollTo(0,0);
          $scope.trayek = [];
          for (var x=0;x<$scope.trayeks.length;x++) {
            if ($scope.trayeks[x].id === data.pemesananTrayek.idTrayek) {
              // console.log($scope.trayek[x].id === data.pemesananTrayek.idTrayek);
              $scope.trayek.push($scope.trayeks[x]);
            }
          }
        $scope.ticketDetail = data;
        $scope.templateUrl = url[2]; // halaman detail-bus
        $state.current.tag = 'D.4.1.2'; // halaman pilih-bus
      };
      $scope.listKursi = []; // Array untuk menyimpan nomor kursi terpilih
      $scope.penumpangs = []; // Array untuk menyimpan data penumpang
      /**
       * ke modal pilih-kursi
       */
      $scope.selectSeat = function (data) {
        $uibModal.open({
          animation: true,
          templateUrl: '/app/pages/akap/transaksi/transaksiManual/penjualan/pilih-kursi.html',
          controller: 'seatModalCtrl',
          size: 'lg',
          resolve: {
            items: function () {
                var item = {
                  data : data,
                  max : $scope.jmlPenumpang
                };
              return item;
            }
          },
          backdrop: 'static'
        })
        .result.then(
          function (listKursi) {
            $window.scrollTo(0,0);
            for (var i = 0; i < $scope.penumpangs.length; i++) {
              $scope.penumpangs.splice(i, 1);
            }
            $scope.templateUrl = url[3]; // halaman data-penumpang
            $state.current.tag = 'D.4.1.3'; // halaman pilih-bus

            $scope.listKursi = listKursi;

            $scope.noGender = [];
            $scope.noCategory = [];
            $scope.noName = [];
            $scope.noPhone = [];
            for (var j = 0; j < $scope.listKursi.length; j++) {
              $scope.noGender.push({
                status : false,
                message : ''
              });
              $scope.noCategory.push({
                status : false,
                message : ''
              });
              $scope.noName.push({
                status : false,
                message : ''
              });
              $scope.noPhone.push({
                message : ''
              });
            }

          }
        );
      };
      /**
       * ke halaman pembayaran
       */
      $scope.pembayaran = function () {
        $scope.errorSubmit = "";
        if (Object.keys($scope.penumpangs).length !== $scope.listKursi.length) {
          $scope.errorSubmit = "Harap isi data penumpang!";
          return false;
        }
        var valid = 0;
        for (var x=0;x<$scope.penumpangs.length;x++) {
          $scope.penumpangs[x].id = x + 1;
          $scope.penumpangs[x].kursi = $scope.listKursi[x];

          if (!$scope.penumpangs[x].kategoriUsia) {
            $scope.noCategory[x].status = true;
            $scope.noCategory[x].message = "Pilih Kategori Usia!";
          } else {
            $scope.noCategory[x].status = false;
            $scope.noCategory[x].message = "";
            valid++;
          }
          if (!$scope.penumpangs[x].gender) {
            $scope.noGender[x].status = true;
            $scope.noGender[x].message = "Pilih Jenis Kelamin!";
          } else {
            $scope.noGender[x].status = false;
            $scope.noGender[x].message = "";
            valid++;
          }
          if (!$scope.penumpangs[x].nama) {
            $scope.noName[x].status = true;
            $scope.noName[x].message = "Masukan Nama Penumpang!";
          } else {
            $scope.noName[x].status = false;
            $scope.noName[x].message = "";
            valid++;
          }
          if (!$scope.penumpangs[x].telpon) {
            $scope.noPhone[x].status = true;
            $scope.noPhone[x].message = "Masukan Nomor Telepon Penumpang!";
          } else {
            $scope.noPhone[x].status = false;
            $scope.noPhone[x].message = "";
            valid++;
          }
        }

        if (valid !== ($scope.listKursi.length * 4)) {
          $window.scrollTo(0,0);
          return false;
        }

        $scope.title = "Penjualan";
        //console.log($scope.penumpangs);
        $scope.templateUrl = url[4];
        $state.current.tag = 'D.4.1.4'; // halaman pilih-bus
         // halaman bayar-tiket
      };

      /**
       * Validasi Anak
       */
       $scope.changeCategory = function (index) {
         if ($scope.penumpangs[index].kategoriUsia == "Anak") {
           $scope.penumpangs[index].telpon = '081';
         } else {
           $scope.penumpangs[index].telpon = '';
         }
       };

      /**
       * Validasi Panjang Nomor Telepon
       */
       $scope.phoneNumber = function (index) {
         if ($scope.penumpangs[index].telpon.length > 13) {
              $scope.penumpangs[index].telpon = $scope.penumpangs[index].telpon.substr(0,13);
         }
       };

       /**
        * Validasi Panjang Nama Penumpang
        */
        $scope.passangerName = function (index) {
          if ($scope.penumpangs[index].nama.length > 20) {
               $scope.penumpangs[index].nama = $scope.penumpangs[index].nama.substr(0,20);
          }
        };

      /**
       * pesan kursi , nomor kursi diambil dari nomor paling kosong didepan
       */
       $scope.pesanKursi = function (data) {
         $window.scrollTo(0,0);
         $http.get(API_ENDPOINT.url + '/agen/jadwal/' + data.pemesananJadwal.idJadwal + '/kursi').success(function (seat) {
           var s1 = seat.seatMap;
           $scope.tesLayout = [];
           s1.forEach(function (doc) {
             doc.forEach(function (doc1) {
               $scope.tesLayout.push(doc1);
             });
           });
           $scope.listKursi = [];
           $scope.kursiKosong = [];
           for (var i = 0; i < $scope.tesLayout.length; i++) {
             if ($scope.tesLayout[i][4] === '0') {
               $scope.kursiKosong.push($scope.tesLayout[i][2]);
             }
           }
          //  console.log($scope.kursiKosong);
          //  $scope.kursiKosong = $scope.kursiKosong.sort(function (a,b) {
          //    return a-b;
          //  });
           for (i = 0; i < $scope.kursiKosong.length; i++) {
                 if ($scope.listKursi.length < parseInt($scope.jmlPenumpang)) {
                   $scope.listKursi.push(String($scope.kursiKosong[i]));
                 } else {
                   i = i + $scope.tesLayout.length;
                 }
           }
           $scope.noGender = [];
           $scope.noCategory = [];
           $scope.noName = [];
           $scope.noPhone = [];
           for (var j = 0; j < $scope.listKursi.length; j++) {
             $scope.noGender.push({
               status : false,
               message : ''
             });
             $scope.noCategory.push({
               status : false,
               message : ''
             });
             $scope.noName.push({
               status : false,
               message : ''
             });
             $scope.noPhone.push({
               status : false,
               message : ''
             });
           }
           $scope.kursis = [];
           $scope.listKursi.forEach(function (doc) {
             $scope.kursis.push({
               kursi : doc
             });
           });
           $http.post(API_ENDPOINT.url + '/agen/jadwal/' + $scope.ticketDetail.pemesananJadwal.idJadwal + '/bookingkursi', $scope.kursis).success(function (data) {
             // $uibModalInstance.close($scope.kursiTerpilih);
             $scope.templateUrl = url[3]; // halaman data-penumpang
             $state.current.tag = 'D.4.1.3'; // halaman pilih-bus
           });
         });
         $scope.terpilih = 0;
       };

      $scope.info = {};
      /**
       * Ke modal tambah info
       */
      $scope.tambahInfo = function () {
        $uibModal.open({
          animation: true,
          templateUrl: '/app/pages/akap/transaksi/transaksiManual/penjualan/tambah-info.html',
          controller: 'infoModalCtrl',
          size: 'lg',
          backdrop: 'static',
          resolve: {
            items : function () {
              return $scope.info;
            }
          }
        })
        .result.then(
          function (info) {
            $scope.info = info;
            $window.scrollTo(0,0);
          }
        );
      };

      $scope.clicked = false;
      $scope.bayar = function () {
        $scope.clicked = true;

        $scope.dataPembayaran = {
          asalKota : $scope.asal,
          tujuanKota : $scope.tujuan,
          idAgenBerangkat : $scope.agen,
          idPemesan : $scope.agen,
          idIssuer : $scope.agen,
          idJadwal : $scope.ticketDetail.pemesananJadwal.idJadwal,
          jmlPenumpang : $scope.penumpangs.length,
          metodePayment : "Tunai",
          penumpangs : $scope.penumpangs,
          tgl : $scope.ticketDetail.pemesananJadwal.tglBerangkat,
          waktu : $scope.ticketDetail.pemesananJadwal.waktuBerangkat,
          idArahTrayek : $scope.ticketDetail.pemesananSubTrayek.idArahTrayek,
          hargaAsal : $scope.ticketDetail.pemesananJadwal.hargaAsal * $scope.penumpangs.length,
          hargaJual : $scope.ticketDetail.pemesananJadwal.hargaJual * $scope.penumpangs.length,
          iuranIpomi : $scope.ticketDetail.pemesananJadwal.iuranIpomi * $scope.penumpangs.length,
          komisiPemberangkatan : $scope.ticketDetail.pemesananJadwal.komisiPemberangkatan * $scope.penumpangs.length,
          komisiPenjualan : $scope.ticketDetail.pemesananJadwal.komisiPemesanan * $scope.penumpangs.length,
          biayaAdmin : $scope.ticketDetail.pemesananJadwal.biayaAdmin * $scope.penumpangs.length,
          subAgen : $scope.info.subAgen,
          turun : $scope.info.turun,
          penjemputan : $scope.info.turun,
          keterangan : $scope.info.keterangan,
          // namaPemesan : 0,
          isDP : false,
          isGrup : false,
          hargaBorongan : 0
        };
        $http.post(API_ENDPOINT.url + '/agen/pesantiket', $scope.dataPembayaran).success(function (data) {
          if (data.status === 212) {
            alert("Mohon Maaf Pilih Ulang kursi");
            $scope.selectSeat($scope.ticketDetail);
          } else {
            $uibModal.open({
              animation: true,
              backdrop: 'static',
              templateUrl: '/app/pages/akap/transaksi/transaksiManual/modals/transaksi.hasil.html',
              controller: 'hasilTransaksiCtrl',
              size: 'sm',
              resolve: {
                items: function () {
                  return {
                    success : true,
                  };
                }
              }
            });
          }
        }).error(function (err) {
          $uibModal.open({
            animation: true,
            backdrop: 'static',
            templateUrl: '/app/pages/akap/transaksi/transaksiManual/modals/transaksi.hasil.html',
            controller: 'hasilTransaksiCtrl',
            size: 'sm',
            resolve: {
              items: function () {
                return {
                  success : false,
                };
              }
            }
          });
          $scope.clicked = false;
        });
      };
    })
    .controller('hasilTransaksiCtrl', function ($uibModalInstance, $state, $scope, items) {

      if (items.success) {
        $scope.message = "Transaksi berhasil, Buat transaksi baru?";
      } else {
        $scope.message = "Transaksi Gagal, Ulangi transaksi?";
      }

      $scope.newTransaksi = function () {
        $uibModalInstance.dismiss();
        $state.go('dashboard.akap.transaksi.transaksiManual');
      };
      $scope.close = function () {
        $uibModalInstance.dismiss();
        $state.go('dashboard.today');
      };
    });
  })
  ();
