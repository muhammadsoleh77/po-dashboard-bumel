(function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.akap')
    .controller('seatModalCtrl', function ($scope, items, $http, API_ENDPOINT, $uibModalInstance, $state) {
      var data = items.data; // data jadwal terpilih
      /**
       * Get kursi dari ID Jadwal
       */
      $http.get(API_ENDPOINT.url + '/agen/jadwal/' + data.pemesananJadwal.idJadwal + '/kursi').success(function (seat) {
        if (seat.status === 405) {
          alert("Maaf, Kursi Habis Terjual");
          $uibModalInstance.dismiss('cancel');
          $state.go('dashboard.today');
          return false;
        }
        var s1 = seat.seatMap;
        $scope.tesLayout = [];
        s1.forEach(function (doc) {
          doc.forEach(function (doc1) {
            $scope.tesLayout.push(doc1);
          });
        });
        if (s1[0].length === 6) {
          $scope.tipe2 = true;
        } else {
          $scope.tipe2 = false;
        }
      });
      $scope.max = items.max;
      $scope.terpilih = 0;
      $scope.kursiTerpilih = [];
      $scope.pilih = function (index) {
        //console.log(items.max);
        if ($scope.tesLayout[index][4] === '4') {
          return false;
        }
        if ($scope.tesLayout[index][4] === '5') {
          return false;
        }
        if ($scope.tesLayout[index][4] === '6') {
          return false;
        }
        if ($scope.tesLayout[index][4] === '7') {
          return false;
        }
        if ($scope.tesLayout[index][4] === '8') {
          return false;
        }
        if ($scope.tesLayout[index][2] === 'NO') {
          return false;
        }
        if ($scope.tesLayout[index][4] === '0') {
          if ($scope.terpilih === parseInt(items.max)) {
            alert("batas maksimal");
            return false;
          }
          $scope.tesLayout[index][4] = '1';
          $scope.kursiTerpilih.push($scope.tesLayout[index][2]);
          //console.log($scope.kursiTerpilih);
          $scope.tesLayout = $scope.tesLayout;
          $scope.terpilih++;
          // //console.log(terpilih);
        } else {
          $scope.tesLayout[index][4] = '0';
          for (var x=0;x<$scope.kursiTerpilih.length;x++) {
            if ($scope.kursiTerpilih[x] === $scope.tesLayout[index][2] ) {
              $scope.kursiTerpilih.splice(x, 1);
            }
          }
          //console.log($scope.kursiTerpilih);
          $scope.tesLayout = $scope.tesLayout;
          $scope.terpilih--;
        }
      };
      /**
       * Tutup modal (batal)
       */
      $scope.batal = function () {
        $uibModalInstance.dismiss('cancel');
      };
      /**
       * Tutup modal (lanjutkan)
       */
      $scope.error = '';
      $scope.ok = function () {
        $scope.error = '';
        if ($scope.kursiTerpilih.length === 0 ) {
          $scope.error = 'Pilih Kursi!';
          return false;
        }
        $scope.kursis = [];
        $scope.kursiTerpilih.forEach(function (doc) {
          $scope.kursis.push({
            kursi : doc
          });
        });
        // console.log($scope.kursis);
        $http.post(API_ENDPOINT.url + '/agen/jadwal/' + data.pemesananJadwal.idJadwal + '/bookingkursi', $scope.kursis)
        .success(function (data) {
          $uibModalInstance.close($scope.kursiTerpilih);
        })
        .error(function () {
          $scope.error = 'Pilih ulang kursi!';
          $http.get(API_ENDPOINT.url + '/agen/jadwal/' + data.pemesananJadwal.idJadwal + '/kursi').success(function (seat) {
            if (seat.status === 405) {
              alert("Maaf, Kursi Habis Terjual");
              $uibModalInstance.dismiss('cancel');
              $state.go('dashboard.today');
              return false;
            }
            var s1 = seat.seatMap;
            $scope.tesLayout = [];
            s1.forEach(function (doc) {
              doc.forEach(function (doc1) {
                $scope.tesLayout.push(doc1);
              });
            });
            if (s1[0].length === 6) {
              $scope.tipe2 = true;
            } else {
              $scope.tipe2 = false;
            }
          });
        });
      };
    })

    .controller('infoModalCtrl', function ($scope, $http, API_ENDPOINT, $uibModalInstance, items) {
      /**
       * Tutup modal (batal)
       */
      $scope.batal = function () {
        $uibModalInstance.dismiss('cancel');
      };

      $scope.data = items;

      /**
       * Tutup modal (lanjutkan)
       */
       $scope.ok = function () {
         $uibModalInstance.close($scope.data);
       };
    });
  })();
