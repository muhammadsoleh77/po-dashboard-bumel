(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .controller('terimaPenjualanCtrl', terimaPenjualanCtrl);

    function terimaPenjualanCtrl($scope, $state, $uibModal, $http, API_ENDPOINT) {
      if (!$state.params.data.namaTrayek) {
        $state.go('dashboard.akap.transaksi.setoran');
      }
      $state.current.tag = 'D.1.1';
      $state.current.parrentState = 'dashboard.akap.transaksi.setoran';
      $state.current.parrentTitle = 'Penerimaan Setoran';
      $scope.data = $state.params.data;
      $scope.tanggal = $state.params.tgl;
      $scope.data.biayaOp = [];
      $scope.data.keteranganBiaya = "Penggunaan/Biaya";
      $scope.subtotal = 0;
      $scope.data.diterima = $scope.data.setoran - $scope.subtotal;

      $scope.biayaTambahan = [];

      $scope.openModal = function (data) {
        $uibModal.open({
        animation: true,
        templateUrl: '/app/pages/today/modals/form-modal.html',
        controller: 'penjualanModalsCtrl',
        size: 'md',
        resolve: {
          items: function () {
            return data;
          }
        }
      }).result.then(function (tambahan) {
        if (tambahan.nominal === "") {
          tambahan.nominal = 0;
        }
        $scope.biayaTambahan.push(tambahan);
        //console.log($scope.biayaTambahan);
        $scope.data.diterima = $scope.data.diterima - tambahan.nominal;
        });
      };
      $scope.update = function () {
        $scope.biayaTambahan.forEach(function (doc) {
          if (doc.nominal === undefined || doc.nominal === null || doc.nominal === "") {
            doc.nominal = 0 ;
          }
          $scope.subtotal = $scope.subtotal + doc.nominal;
        });
        $scope.data.setoran = $scope.data.jmlPenjualan - $scope.data.komisiAgen;
        $scope.data.diterima = $scope.data.setoran - $scope.subtotal;
        // return parseInt($scope.data.diterima);
      };

      $scope.editSetoran = function() {
        $scope.data.setoran = $scope.data.setoran;
      };

      $scope.remove = function (index) {
        $scope.data.diterima = $scope.data.diterima + parseInt($scope.biayaTambahan[index].nominal);
        $scope.biayaTambahan.splice(index, 1);
        // $scope.data.diterima = $scope.update();
      };

      $scope.kirim = function () {
        $scope.biayaTambahan.forEach(function (doc) {
          $scope.data.biayaOp.push({
            keteranganBiaya : doc.namaBiaya,
            kodeBiaya : doc.idBiayaPo,
            jumlah : parseInt(doc.nominal)
          });
        });
        //console.log($scope.data);
        $http.post(API_ENDPOINT.url + '/po/penerimaan/penjualan', $scope.data).success(function (data) {
          alert("Berhasil Tersimpan");
          $state.go('dashboard.today');
        });
      };

      $scope.kembali = function () {
        $state.go('dashboard.akap.transaksi.setoran');
      };
    }
})();
