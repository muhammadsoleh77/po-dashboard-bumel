(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .controller('setoranAdminSistemListCtrl', setoranAdminSistemListCtrl);

    function setoranAdminSistemListCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, $timeout, setoranAdminSistemListData) {

      // $state.current.tag = 'D.2';
      $scope.exportPage = false;

      $scope.loadData = true;
      $scope.empty = true;
      $scope.message = "";
      $http.get(API_ENDPOINT.url + '/po/invoice/tracker/unpayed?idpo=' + $rootScope.user.idPo).success(function (data) {

        $scope.datas = data;
        $scope.loadData = false;
        $scope.total = setoranAdminSistemListData.getTotalAdminSistem(data);
        if ($scope.datas.length === 0) {
          $scope.empty = true;
          $scope.message = "Tidak Ada Data Pembayaran";
        } else {
          $scope.empty = false;
        }

        $scope.exportXls = function(){
          $scope.exportPage = true;
          var blob = new Blob([document.getElementById('dataTable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
          });
          saveAs(blob, "Setoran Admin Sistem.xls");
          $scope.exportPage = false;
        };

        $scope.loadData = false;
        $scope.next = function (date) {
          $state.go('dashboard.akap.transaksi.invoice', {date : date});
        };
      }).error(function () {
        $scope.loadData = false;
        $scope.empty = true;
        $scope.message = "Tidak Ada Data Pembayaran";
      });

    }
})();
