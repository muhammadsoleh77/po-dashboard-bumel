(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('setoranAdminSistemListData', setoranAdminSistemListData);

    function setoranAdminSistemListData() {

      var getTotalAdminSistem = function (d) {
        var total = 0;
        d.forEach(function (res) {
          total = total + res.totalBayar;
        });
        return total;
      };

      return {
        getTotalAdminSistem : getTotalAdminSistem
      };

    }

})();
