(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('setoranInvoicePrint', setoranInvoicePrint);

    function setoranInvoicePrint($filter, logo) {

      var exportPdf = function (data) {

        var docDefinition = {
          defaultStyle : {
            fontSize : 9.5,
            columnGap : 20,
          },
          content : []
        };

        docDefinition.content.push({
          columns : [
            {
              image : logo.url,
              width : 40
            },
            {
              text : data.namaPtOtodata + '\n' +
                    data.alamatOtodata,
              fontSize : 9
            }
          ]
        });

        docDefinition.content.push(
          {
            margin: [0, 5, 0, 15],
            width: '*',
      			table: {
      				headerRows: 1,
      				body: [
      					[{text: '', style: 'tableHeader'}],
      				]
      			},
      			layout: 'headerLineOnly'
      		}
        );

        docDefinition.content.push({
          columns : [
            {
              marginTop : 35,
              width : 260,
              marginBottom : 20,
              fontSize : 12,
              text : 'E-INVOICE'
            },
            {
              marginTop : 35,
              marginBottom : 20,
              fontSize : 12,
              text : 'Nomor : ' + data.noInvoice
            }
          ],
        });

        docDefinition.content.push({
          columns : [
            {
              marginBottom : 70,
              width : 20,
              text : 'Dari :'
            },
            {
              width : 220,
              text : data.namaPtOtodata + '\n' +
                     data.alamatOtodata
            },
            {
              width : 20,
              text : 'Untuk :'
            },
            {
              width : '*',
              text :  data.namaPT + '\n' +
                      data.alamatPo
            },
          ]
        });

        docDefinition.content.push({
          columns : [
            {
              text : 'A. Penjualan'
            }
          ]
        });

        var table1 = {
          color: '#444',
          margin: [0, 5, 0, 10],
          fontSize : 9,
          table: {
            widths: [20,'*',50,50,90],
            headerRows: 2,
            body: [
              [
                {text: 'ID', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Deskripsi', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Qty', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: '@', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: 'Total', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {},
              ]
            ]
          }
        };

        var penjualan = data.penjualan;

        for (var i = 0; i < penjualan.length; i++) {
          table1.table.body.push([
            { text : i + 1 },
            { text : penjualan[i].namaBus + '-' + penjualan[i].namaTrayek + '- ' + $filter('date')(new Date(penjualan[i].tglBerangkat), 'dd/MM/yyyy') + ' ' + penjualan[i].jamBerangkat },
            { text : penjualan[i].jmlKursiTerjual, alignment : 'right' },
            { text : $filter('currency')((penjualan[i].biayaAdminPerkursi), '', 0), alignment : 'right' },
            { text : $filter('currency')((penjualan[i].biayaAdmin), '', 0), alignment : 'right' },
          ]);
        }

        table1.table.body.push([
          { text : 'Total Penjualan', colSpan : 4, bold : true },
          {},
          {},
          {},
          { text : $filter('currency')(data.totalBiayaAdminPenjualan, '', 0), alignment : 'right', bold : true },
        ]);

        docDefinition.content.push(table1);

        docDefinition.content.push({
          columns : [
            {
              text : 'B. Pemesanan'
            }
          ]
        });

        var table2 = {
          color: '#444',
          margin: [0, 5, 0, 10],
          fontSize : 9,
          table: {
            widths: [20,'*',50,50,90],
            headerRows: 2,
            body: [
              [
                {text: 'ID', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Deskripsi', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Qty', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: '@', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: 'Total', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {},
              ]
            ]
          }
        };

        var pemesanan = data.pemesanan;

        for (i = 0; i < pemesanan.length; i++) {
          table2.table.body.push([
            { text : penjualan.length + i + 1 },
            { text : pemesanan[i].namaTrayek },
            { text : pemesanan[i].kursiTerpesan, alignment : 'right' },
            { text : $filter('currency')((pemesanan[i].biayaAdmin / pemesanan[i].kursiTerpesan), '', 0), alignment : 'right' },
            { text : $filter('currency')((pemesanan[i].biayaAdmin), '', 0), alignment : 'right' },
          ]);
        }

        table2.table.body.push([
          { text : 'Total Pemesanan', colSpan : 4, bold : true },
          {},
          {},
          {},
          { text : $filter('currency')(data.totalBiayaAdminPemesanan, '', 0), alignment : 'right', bold : true },
        ]);

        docDefinition.content.push(table2);

        docDefinition.content.push({
          columns : [
            {
              text : 'Total Invoice (A + B) = ' + $filter('currency')(data.totalBiayaAdminPenjualan, 'Rp.', 0) + ' + ' + $filter('currency')(data.totalBiayaAdminPemesanan, 'Rp.', 0) + ' = ' + $filter('currency')((data.totalBiayaAdminPenjualan + data.totalBiayaAdminPemesanan), 'Rp.', 0),
              bold : true,
              fontSize : 11,
            }
          ]
        });

        docDefinition.content.push({
          columns : [
            {
              text : 'Terbilang : ' + $filter('terbilang')((data.totalBiayaAdminPenjualan + data.totalBiayaAdminPemesanan)) + ' Rupiah',
              fontSize : 11,
              bold : true,
            }
          ]
        });

        docDefinition.content.push({
          columns : [
            {
              width : 280,
              text : ''
            },
            {
              width : '*',
              marginTop : 40,
              text : 'Jakarta, ' + $filter('date_v1')(data.tglTransaksi) + '\n' +
                      data.namaPtOtodata + '\n\n\n' +
                      data.namaTerTanda + '\n' +
                      'COO \n' +
                      'Tidak Diperlukan Tanda Tangan'
            },
          ]
        });

        pdfMake.createPdf(docDefinition).open();

      };

      return {
        exportPdf : exportPdf
      };

    }

})();
