(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .controller('setoranInvoiceCtrl', setoranInvoiceCtrl);

    function setoranInvoiceCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, setoranInvoiceData, setoranInvoicePrint) {

      if (!$state.params.date) {
        $state.go('dashboard.akap.transaksi.setoranadminsistem');
      }

      $state.current.tag = 'D.2.1';
      $state.current.parrentState = 'dashboard.akap.transaksi.setoranadminsistem';
      $state.current.parrentTitle = 'Setoran Admin Sistem';

      $http.get(API_ENDPOINT.url + 'po/invoice?idPo=' + $rootScope.user.idPo + '&tgl=' + $state.params.date).success(function (data) {
        $scope.invoice = data;
        $scope.print = function () {
          setoranInvoicePrint.exportPdf($scope.invoice);
        };
      });


      $scope.toExcel = function () {
        var blob = new Blob([document.getElementById('invoice').innerHTML], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "invoice.xls");
      };

    }
})();
