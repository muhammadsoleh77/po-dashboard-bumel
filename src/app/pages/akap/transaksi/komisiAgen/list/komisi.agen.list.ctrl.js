(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .controller('komisiAgenListCtrl', komisiAgenListCtrl);

    function komisiAgenListCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, $timeout, adminSistemListData) {

      $state.current.tag = 'D.3';

      $scope.loadData = true;
      $scope.empty = true;
      $scope.message = "";
      $http.get(API_ENDPOINT.url + '/po/transaksikomisiagen?idPo=' + $rootScope.user.idPo).success(function (data) {
        $scope.datas = data;
        if ($scope.datas.length === 0) {
          $scope.empty = true;
          $scope.message = "Tidak Ada Data Pembayaran";
        } else {
          $scope.empty = false;
        }
        $scope.loadData = false;

        $scope.next = function (idAgen, namaAgen, index, data) {
          $state.go('dashboard.akap.transaksi.komisiAgenItem', {idAgen : idAgen, namaAgen : namaAgen, index : index, data : data});
        };

        $scope.bayar = function (data, index) {
          $state.go('dashboard.akap.transaksi.komisiAgenProcess', {data : data, index : index});
        };

      }).error(function () {
        $scope.loadData = false;
        $scope.empty = true;
        $scope.message = "Tidak Ada Data Pembayaran";
      });

    }
})();
