(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .controller('komisiAgenProcessCtrl', komisiAgenProcessCtrl);

    function komisiAgenProcessCtrl($state, $scope, $uibModal) {

      $state.current.tag = 'D.3.' + $state.params.index;

      if (!$state.params.data) {
        $state.go('dashboard.akap.transaksi.komisiAgen');
      }

      $scope.isNull = false;

      $scope.data = $state.params.data;
      $scope.data.diterima = 0;

      $scope.update = function () {
        $scope.isNull = false;

        if ($scope.data.diterima >= $scope.data.sisaBayar) {
          $scope.data.diterima = $scope.data.sisaBayar;
          $scope.data.status = true;
        }
        else {
          $scope.data.status = false;
        }

      };

      $scope.bayar = function () {
        if($scope.onlyNumbers == /^\d+$/ || $scope.data.diterima <= 0) {
          $scope.isNull = true;
          return false;
        }
        else if ($scope.data.diterima === 0 || $scope.data.diterima === '0' || $scope.data.diterima === '00') {
          $scope.isNull = true;
          return false;
        }
        // else if ($scope.data.diterima <= 0 || $scope.data.diterima <= '0' || $scope.data.diterima <= '00'){
        //   $scope.isNull = true;
        //   return false;
        // }

        $uibModal.open({
          animation : true,
          templateUrl : '/app/pages/akap/transaksi/komisiAgen/modals/modal.main.html',
          controller : 'komisiModalProcessCtrl',
          size : 'sm',
          resolve : {
            items : function () {
              return {
                data : $scope.data,
              };
            }
          }
        });
      };
    }

})();
