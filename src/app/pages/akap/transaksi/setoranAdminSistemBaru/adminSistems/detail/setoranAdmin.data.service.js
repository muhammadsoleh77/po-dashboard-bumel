(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('setoranInvoiceDatabaru', setoranInvoiceDatabaru);

    function setoranInvoiceDatabaru($rootScope, $http, $state, API_ENDPOINT, $filter, $timeout) {

      var dataPemesanan = function (d) {
        var datas= [];
        var data = [];
        d.forEach(function (doc) {
          doc.transaksiAgen.forEach(function (doc2) {
            var biayaAdmin = 0;
            doc2.transaksi.forEach(function (doc3) {
              biayaAdmin = biayaAdmin + doc3.biayaAdmin + doc3.iuranIpomi;
            });
            data.push({
              namaTrayek : doc2.namaTrayek,
              jamBerangkat : doc2.jamBerangkat,
              kursiTerpesan : doc2.kursiTerpesan,
              biayaAdmin : biayaAdmin
            });
          });
        });

        var getDatas = function () {
          for (var i = 0; i < $rootScope.lisTrayeks.length; i++) {
            var biayaAdminTmp = 0;
            var kursiTerpesanTmp = 0;
            for (var j = 0; j < data.length; j++) {
              if ($rootScope.lisTrayeks[i].value === data[j].namaTrayek) {
                biayaAdminTmp = biayaAdminTmp + data[j].biayaAdmin;
                kursiTerpesanTmp = kursiTerpesanTmp + data[j].kursiTerpesan;
              }
            }
            if (biayaAdminTmp !== 0) {
              datas.push({
                namaTrayek : $rootScope.lisTrayeks[i].value,
                kursiTerpesan : kursiTerpesanTmp,
                biayaAdmin : biayaAdminTmp
              });
            }
          }
        };

          if ($rootScope.lisTrayeks.length === 0) {
            $timeout(function () {
              getDatas();
            }, 7000);
            return datas;
          } else {
            getDatas();
            return datas;
          }
      };

      var dataPenjualan = function (data) {
        var datas = [];

        for (var i = 0; i < data.length; i++) {
          if (data[i].biayaAdmin > 0) {
            datas.push(data[i]);
          }
        }
        return datas;
      };

      return {
        dataPemesanan : dataPemesanan,
        dataPenjualan : dataPenjualan
      };
    }
})();
