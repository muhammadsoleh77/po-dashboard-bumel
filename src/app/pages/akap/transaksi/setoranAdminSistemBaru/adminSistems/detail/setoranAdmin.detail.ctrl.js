(function () {

  angular.module('BlurAdmin.pages.akap')
  .controller('setoranInvoicebaruCtrl', setoranInvoicebaruCtrl);

  function setoranInvoicebaruCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, setoranInvoiceDatabaru, invoicePrintbaru) {

    if (!$state.params.date) {
      $state.go('dashboard.akap.transaksi.setoranadminsistembaru');
    }

    $state.current.tag = 'D.2.1';
    $state.current.parrentState = 'dashboard.akap.transaksi.setoranadminsistembaru';
    $state.current.parrentTitle = 'Setoran Admin Sistem';

    $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/invoice/tracker/unpayeded/detail?tgl=' + $state.params.date).success(function(data) {
      $scope.invoice = data;
    });

    $scope.print = function(){
      invoicePrintbaru.exportPdf($scope.invoice);
    };

  }
})();
