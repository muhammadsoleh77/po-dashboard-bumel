(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('invoicePrintbaru', invoicePrintbaru);

    function invoicePrintbaru($filter, logo) {

      var exportPdf = function (data) {

        var docDefinition = {
          defaultStyle : {
            fontSize : 9.5,
            columnGap : 20,
          },
          content : []
        };

        docDefinition.content.push({
          columns : [
            {
              image : logo.url,
              width : 40
            },
            {
              text : data.namaPtOtodata + '\n' +
                    data.alamatOtodata,
              fontSize : 9
            }
          ]
        });

        docDefinition.content.push(
          {
            margin: [0, 5, 0, 15],
            width: '*',
      			table: {
      				headerRows: 1,
      				body: [
      					[{text: '', style: 'tableHeader'}],
      				]
      			},
      			layout: 'headerLineOnly'
      		}
        );

        docDefinition.content.push({
          columns : [
            {
              marginTop : 35,
              width : 260,
              marginBottom : 20,
              fontSize : 12,
              text : 'E-INVOICE'
            },
            {
              marginTop : 35,
              marginBottom : 20,
              fontSize : 12,
              text : 'Nomor : ' + data.noInvoice
            }
          ],
        });

        docDefinition.content.push({
          columns : [
            {
              marginBottom : 70,
              width : 20,
              text : 'Dari :'
            },
            {
              width : 220,
              text : data.namaPtOtodata + '\n' +
                     data.alamatOtodata
            },
            {
              width : 20,
              text : 'Untuk :'
            },
            {
              width : '*',
              text :  data.namaPT + '\n' +
                      data.alamatPo
            },
          ]
        });

        docDefinition.content.push({
          columns : [
            {
              text : 'A. Penjualan'
            }
          ]
        });

        var table1 = {
          color: '#444',
          margin: [0, 5, 0, 10],
          fontSize : 9,
          table: {
            widths: [20,'*',50,50,90],
            headerRows: 2,
            body: [
              [
                {text: 'ID', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Deskripsi', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Qty', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: '@', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: 'Total', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {},
              ]
            ]
          }
        };

        var penjualan = data.penjualan;

        for (var i = 0; i < penjualan.length; i++) {
          // console.log((penjualan[i]));
          table1.table.body.push([
            { text : i + 1 },
            { text : penjualan[i].kodeBus + '-' + penjualan[i].namaTrayek + '- ' + $filter('date')(data.tglTransaksi, 'dd/MM/yyyy') },
            { text : penjualan[i].jmlPenumpang, alignment : 'right' },
            { text : $filter('currency')((penjualan[i].adminPerTiket), '', 0), alignment : 'right' },
            { text : $filter('currency')((penjualan[i].totalAdmin), '', 0), alignment : 'right' },
          ]);
        }

        table1.table.body.push([
          { text : 'Total Penjualan', colSpan : 4, bold : true },
          {},
          {},
          {},
          { text : $filter('currency')(data.totalBiayaAdminPenjualan, '', 0), alignment : 'right', bold : true },
        ]);

        docDefinition.content.push(table1);

        docDefinition.content.push({
          columns : [
            {
              text : 'B. Pemesanan'
            }
          ]
        });

        var table2 = {
          color: '#444',
          margin: [0, 5, 0, 10],
          fontSize : 9,
          table: {
            widths: [20,'*',50,50,90],
            headerRows: 2,
            body: [
              [
                {text: 'ID', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Deskripsi', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Qty', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: '@', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: 'Total', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {},
              ]
            ]
          }
        };

        var pemesanan = data.pemesanan;

        for (i = 0; i < pemesanan.length; i++) {
          table2.table.body.push([
            { text : penjualan.length + i + 1 },
            { text : pemesanan[i].kodeBus + '-' + pemesanan[i].namaTrayek },
            { text : pemesanan[i].jmlPenumpang, alignment : 'right' },
            { text : $filter('currency')((pemesanan[i].adminPerTiket), '', 0), alignment : 'right' },
            { text : $filter('currency')((pemesanan[i].totalAdmin), '', 0), alignment : 'right' },
          ]);
        }

        table2.table.body.push([
          { text : 'Total Pemesanan', colSpan : 4, bold : true },
          {},
          {},
          {},
          { text : $filter('currency')(data.totalBiayaAdminPemesanan, '', 0), alignment : 'right', bold : true },
        ]);

        docDefinition.content.push(table2);

        docDefinition.content.push({
          columns : [
            {
              text : 'C. Void'
            }
          ]
        });

        var table3 = {
          color: '#444',
          margin: [0, 5, 0, 10],
          fontSize : 9,
          table: {
            widths: [20,'*','*',90],
            headerRows: 2,
            body: [
              [
                {text: 'ID', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Deskripsi', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Jumlah Penumpang Batal', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: 'Total', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
              ]
            ]
          }
        };

        var pembatalan = data.pembatalan;

        // for (i = 0; i < pembatalan.length; i++) {
          table3.table.body.push([
            { text : penjualan.length + pemesanan.length + 1 },
            { text : 'Void' },
            { text : data.pembatalan.penumpangNonRefund, alignment : 'right' },
            { text : $filter('currency')((data.pembatalan.totalNonRefund), '', 0), alignment : 'right' },
          ]);
        // }

        // table3.table.body.push([
        //   { text : 'Total Cancellation', colSpan : 3, bold : true },
        //   {},
        //   {},
        //   { text : $filter('currency')(data.pembatalan.totalNonRefund, '', 0), alignment : 'right', bold : true },
        // ]);

        docDefinition.content.push(table3);

        docDefinition.content.push({
          columns : [
            {
              text : 'D. Void di Refund'
            }
          ]
        });

        var table4 = {
          color: '#444',
          margin: [0, 5, 0, 10],
          fontSize : 9,
          table: {
            widths: [20,'*','*',90],
            headerRows: 2,
            body: [
              [
                {text: 'ID', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Deskripsi', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
                {text: 'Jumlah Penumpang Batal', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true,  fontColor: 'black', rowSpan: 2},
                {text: 'Total', style: 'tableHeader', alignment: 'center', fontSize : 9, bold : true, fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
              ]
            ]
          }
        };

        var pembatalans = data.pembatalan;

        // for (i = 0; i < pembatalan.length; i++) {
          table4.table.body.push([
            { text : penjualan.length + pemesanan.length + 2 },
            { text : 'Void di Refund' },
            { text : data.pembatalan.penumpangRefund, alignment : 'right' },
            { text : $filter('currency')((data.pembatalan.totalRefund), '', 0), alignment : 'right' },
          ]);
        // }

        // table4.table.body.push([
        //   { text : 'Total Cancellation', colSpan : 3, bold : true },
        //   {},
        //   {},
        //   { text : $filter('currency')(data.pembatalan.totalRefund, '', 0), alignment : 'right', bold : true },
        // ]);

        docDefinition.content.push(table4);

        docDefinition.content.push({
          columns : [
            {
              text : 'Total Invoice (A + B - C + D) = ' + $filter('currency')(data.totalBiayaAdminPenjualan, 'Rp.', 0) + ' + ' + $filter('currency')(data.totalBiayaAdminPemesanan, 'Rp.', 0) + ' - ' + $filter('currency')(data.pembatalan.totalNonRefund, 'Rp.', 0) + '+' + $filter('currency')(data.pembatalan.totalRefund, 'Rp.', 0) + ' = ' + $filter('currency')((data.totalBayar), 'Rp.', 0),
              bold : true,
              fontSize : 11,
            }
          ]
        });

        docDefinition.content.push({
          columns : [
            {
              text : 'Terbilang : ' + $filter('terbilang')((data.totalBayar)) + ' Rupiah',
              fontSize : 11,
              bold : true,
            }
          ]
        });

        docDefinition.content.push({
          columns : [
            {
              width : 280,
              text : ''
            },
            {
              width : '*',
              marginTop : 40,
              text : 'Jakarta, ' + $filter('date_v1')(data.tglTransaksi) + '\n' +
                      data.namaPtOtodata + '\n\n\n' +
                      data.namaTerTanda + '\n' +
                      'COO \n' +
                      'Tidak Diperlukan Tanda Tangan'
            },
          ]
        });
        console.log(docDefinition);
        pdfMake.createPdf(docDefinition).open();

      };

      return {
        exportPdf : exportPdf
      };

    }

})();
