(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('adminSistemList', adminSistemList);

    function adminSistemList() {

      var getTotalAdminSistem = function (d) {
        console.log(d);
        var total = 0;
        d.invoiceTrackerView.forEach(function (data) {
          total = total + data.amountDue;
        });
        return total;
      };

      return {
        getTotalAdminSistem : getTotalAdminSistem
      };

    }

})();
