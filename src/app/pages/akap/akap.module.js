(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akap', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.akap', {
          url: '/akap',
          title: 'AKAP',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              icon: 'ion-android-bus',
              order: 204
            }
        })
        // .state('dashboard.akap.pengaturan', {
        //   url: '/pengaturan',
        //   title: 'Pengaturan',
        //   template : '<div ui-view></div>',
        //   abstract: true,
        //   sidebarMeta: {
        //       icon: 'ion-android-bus',
        //       order: 205
        //     }
        // })
            .state('dashboard.akap.pengaturan.grupAgen', {
              url: '/grupAgen',
              title: 'Grup Agen',
              templateUrl: 'app/pages/akap/pengaturan/akap.pengaturan.grupAgen.html',
              controller: 'grupAgenCtrl',
              sidebarMeta: {
                  order: 1
                }
            })
            .state('dashboard.akap.pengaturan.agen', {
              url: '/agen',
              title: 'Agen',
              templateUrl: 'app/pages/akap/pengaturan/akap.pengaturan.agen.html',
              controller: 'agenCtrl',
              sidebarMeta: {
                  order: 2
                }
            })
            .state('dashboard.akap.pengaturan.trayek', {
              url: '/trayek',
              title: 'Trayek',
              templateUrl: 'app/pages/akap/pengaturan/akap.pengaturan.trayek.html',
              controller: 'trayekCtrl',
              sidebarMeta: {
                  order: 3
                }
            })
            .state('dashboard.akap.pengaturan.subTrayek', {
              url: '/subTrayek',
              title: 'Sub Trayek',
              templateUrl: 'app/pages/akap/pengaturan/akap.pengaturan.subTrayek.html',
              controller: 'subTrayekCtrl',
              sidebarMeta: {
                  order: 4
                }
            })
        .state('dashboard.akap.jadwalLayanan', {
          url: '/jadwalLayanan',
          title: 'Jadwal Layanan',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              // icon: 'ion-android-bus',
              order: 206
            }
        })
            .state('dashboard.akap.jadwalLayanan.rencanaJadwal', {
              url: '/rencanaJadwal',
              title: 'Rencana Jadwal',
              templateUrl: 'app/pages/akap/jadwalLayanan/rencana.jadwal.html',
              controller: "RencanaAddCtrl",
              controllerAs: "rencanaAddtrl",
              sidebarMeta: {
                  order: 1
                }
            })
            .state('dashboard.akap.jadwalLayanan.jadwalTerdaftar', {
              url: '/jadwalTerdaftar',
              title: 'Jadwal Terdaftar',
              templateUrl: 'app/pages/akap/jadwalLayanan/jadwal.terdaftar.html',
              controller: "JadwalsListPerDayCtrl",
              controllerAs: "jadwalsListPerDayCtrl",
              sidebarMeta: {
                  order: 2
                }
            })
        .state('dashboard.akap.transaksi', {
          url: '/transaksi',
          title: 'Transaksi',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              // icon: 'ion-android-bus',
              order: 207
            }
        })
            .state('dashboard.akap.transaksi.setoran', {
              url: '/penerimaansetoran',
              title: 'Penerimaan Setoran',
              templateUrl: 'app/pages/akap/transaksi/akap.transaksi.setoranSekarang.template.html',
              controller: "keuanganSetoranCtrl",
              controllerAs: "keuanganSetoranCtrl",
              sidebarMeta: {
                  order: 1,
                },
              params : {
                query : {}
              }
            })
                .state('dashboard.akap.transaksi.terimaPenjualan', {
                  url: '/penerimaan/penjualan',
                  templateUrl: 'app/pages/akap/transaksi/setoran/penjualan/keuangan.penjualanharian.html',
                  title: 'Penerimaan Setoran Penjualan',
                  controller: "terimaPenjualanCtrl",
                    controllerAs: "terimaPenjualanCtrl",
                  params : {
                    data : {},
                    tgl : ''
                  }
                })
                .state('dashboard.akap.transaksi.terimaPemesanan', {
                  url: '/penerimaan/pemesanan',
                  templateUrl: 'app/pages/akap/transaksi/setoran/pemesanan/keuangan.pemesananharian.html',
                  title: 'Penerimaan Setoran Pemesanan',
                  controller: "terimaPemesananCtrl",
                    controllerAs: "terimaPemesananCtrl",
                  params : {
                    data : {},
                    tgl : ''
                  }
                })
                .state('dashboard.akap.transaksi.setoranCash', {
                  url: '/penerimaan/cash',
                  templateUrl: 'app/pages/akap/transaksi/setoran/cash/keuangan.cash.html',
                  title: 'Penerimaan Setoran Cash',
                  controller: "terimaCashCtrl",
                    controllerAs: "terimaCashCtrl",
                  params : {
                    data : {},
                    tgl : ''
                  }
                })
                .state('dashboard.akap.transaksi.setoranNanti', {
                  url: '/penerimaan/nanti',
                  templateUrl: 'app/pages/akap/transaksi/setoran/nanti/keuangan.nanti.html',
                  title: 'Penerimaan Setoran Nanti Bank',
                  controller: "terimaNantiCtrl",
                    controllerAs: "terimaNantiCtrl",
                  params : {
                    data : {},
                    tgl : ''
                  }
                })
                .state('dashboard.akap.transaksi.setoranNantiTrayek', {
                  url: '/penerimaan/trayek',
                  templateUrl: 'app/pages/akap/transaksi/setoran/nantiTrayek/keuangan.nantiTrayek.html',
                  title: 'Penerimaan Setoran Nanti Trayek',
                  controller: "terimaTrayekCtrl",
                    controllerAs: "terimaTrayekCtrl",
                  params : {
                    data : {},
                    tgl : ''
                  }
                })
                .state('dashboard.akap.transaksi.setoranpemesananagen', {
                   url: '/penerimaan/setoranpemesanan',
                   templateUrl: 'app/pages/akap/transaksi/akap.penerimaan.setoran.pemesanan.html',
                   title: 'Penerimaan Setoran Pemesanan',
                     controller: "penerimaanPemesananCtrl",
                     controllerAs: "penerimaanPemesananCtrl",
                 })
                 .state('dashboard.akap.transaksi.penjualanharian', {
                   url: '/penjualanharian/{id}',
                   templateUrl: 'app/pages/akap/transaksi/keuangan.penjualanharian.html',
                   title: 'Penjualan Hari Ini',
                     controller: "penjualanharianCtrl",
                     controllerAs: "penjualanharianCtrl",
                 })
                 .state('dashboard.akap.transaksi.pemesananharian', {
                   url: '/pemesananharian/{id}',
                   templateUrl: 'app/pages/akap/transaksi/keuangan.pemesananharian.html',
                   title: 'Pemesanan Hari Ini',
                     controller: "pemesananharianCtrl",
                     controllerAs: "pemesananharianCtrl",
                 })
            .state('dashboard.akap.transaksi.komisiAgen', {
              url: '/komisiagen',
              title: 'Komisi Agen',
              templateUrl : 'app/pages/akap/transaksi/komisiAgen/list/komisi.agen.list.template.html',
              controller : 'komisiAgenListCtrl',
              controllerAs : 'komisiAgenListCtrl',
              sidebarMeta: {
                  order: 2
                }
            })
                .state('dashboard.akap.transaksi.komisiAgenItem', {
                  url : '/komisi/agen/item?idAgen&namaAgen&index',
                  templateUrl : 'app/pages/akap/transaksi/komisiAgen/item/komisi.agen.item.template.html',
                  title : 'Komisi Agen',
                  controller : 'komisiAgenItemCtrl',
                  controllerAs : 'komisiAgenItemCtrl',
                  params : {
                    idAgen : '',
                    namaAgen : '',
                    index : '',
                    data : null
                  }
                })
                .state('dashboard.akap.transaksi.komisiAgenProcess', {
                  url : '/komisi/agen/process',
                  templateUrl : 'app/pages/akap/transaksi/komisiAgen/process/komisi.agen.process.template.html',
                  title : 'Komisi Agen',
                  controller : 'komisiAgenProcessCtrl',
                  controllerAs : 'komisiAgenProcessCtrl',
                  params : {
                    data : null,
                    index : ''
                  }
                })
            .state('dashboard.akap.transaksi.transaksiManual', {
              url: '/transaksimanual',
              title: 'Transaksi Manual',
              templateUrl: 'app/pages/akap/transaksi/transaksiManual/pilih.html',
              controller: "transaksiAkapManualCtrl",
              controllerAs: "transaksiAkapManualCtrl",
              sidebarMeta: {
                  order: 3
                }
            })
                .state('dashboard.akap.transaksi.transaksipenjualanagen', {
                  url: '/penjualanagen',
                  templateUrl: 'app/pages/akap/transaksi/transaksiManual/main.html',
                  title: 'Transaksi Penjualan Agen',
                    controller: "transaksiAkapPenjualanCtrl",
                    controllerAs: "transaksiAkapPenjualanCtrl",
                })
                .state('dashboard.akap.transaksi.transaksipemesananagen', {
                  url: '/pemesananagen',
                  templateUrl: 'app/pages/akap/transaksi/transaksiManual/main.html',
                  title: 'Transaksi Pemesanan Agen',
                    controller: "transaksiAkapPemesananCtrl",
                    controllerAs: "transaksiAkapPemesananCtrl",
                })
            // .state('dashboard.akap.transaksi.setoranadminsistem', {
            //   url: '/setoranAdminsistem',
            //   title: 'Setoran Admin Sistem',
            //   templateUrl: 'app/pages/akap/transaksi/setoranAdminSistem/adminSistem/list/setoranAdminSistem.list.template.html',
            //   controller: "setoranAdminSistemListCtrl",
            //   controllerAs: "setoranAdminSistemListCtrl",
            //   sidebarMeta: {
            //       order: 4
            //     }
            // })
            //     .state('dashboard.akap.transaksi.invoice', {
            //     url: '/setoran/invoice?date',
            //     templateUrl: 'app/pages/akap/transaksi/setoranAdminSistem/adminSistem/detail/setoranAdminSistem.detail.template.html',
            //     title: 'E-Invoice',
            //     controller: "setoranInvoiceCtrl",
            //     controllerAs: "setoranInvoiceCtrl",
            //     params : {
            //       date : ''
            //     }
            //   })
          .state('dashboard.akap.transaksi.setoranadminsistembaru', {
            url: '/setoranAdminsistembaru',
            title: 'Setoran Admin Sistem',
            templateUrl: 'app/pages/akap/transaksi/setoranAdminSistemBaru/adminSistems/list/setoranAdmin.list.template.html',
            controller: "setoranAdminSistemCtrl",
            controllerAs: "setoranAdminSistemCtrl",
            sidebarMeta: {
                order: 5
              }
          })
              .state('dashboard.akap.transaksi.invoicebaru', {
              url: '/setoran/invoicebaru?date',
              templateUrl: 'app/pages/akap/transaksi/setoranAdminSistemBaru/adminSistems/detail/setoranAdmin.detail.template.html',
              title: 'E-Invoice',
              controller: "setoranInvoicebaruCtrl",
              controllerAs: "setoranInvoicebaruCtrl",
              params : {
                date : ''
              }
            })
            .state('dashboard.akap.transaksi.approvals', {
              url: '/approvals',
              templateUrl: 'app/pages/akap/transaksi/approvals/approval.templates.html',
              title: 'Data Approval',
              controller: "approvalsCtrl",
              controllerAs: "approvalsCtrl",
              sidebarMeta: {
                order: 6
              }
            })
        .state('dashboard.akap.laporan', {
          url: '/laporan',
          title: 'Laporan',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              // icon: 'ion-android-bus',
              order: 208
            }
        })
        .state('dashboard.akap.laporan.hasil', {
          url: '/hasilpenjualanpemesanan',
          title: 'Setoran Hasil Penjualan',
          templateUrl: 'app/pages/akap/laporan/penjualanPemesanan/akap.pilih.html',
          controller: 'akapSetoranPenjualanCtrl',
          controllerAs: 'akapSetoranPenjualanCtrl',
          sidebarMeta: {
              order: 1
            }
        })
            .state('dashboard.akap.laporan.setoranPenjualan', {
              url: '/setoranpenjualan',
              templateUrl: 'app/pages/akap/laporan/penjualanPemesanan/akap.setoranPenjualan.html',
              title: 'Setoran Penjualan',
                controller: 'akapSetoranPenjualanCtrl',
                controllerAs: 'akapSetoranPenjualanCtrl',
            })
            .state('dashboard.akap.laporan.setoranPemesanan', {
              url: '/setoranpemesanan',
              templateUrl: 'app/pages/akap/laporan/penjualanPemesanan/akap.setoranPemesanan.html',
              title: 'Setoran Pemesanan',
                controller: 'setoranPemesananCtrl',
                controllerAs: 'setoranPemesananCtrl',
            })
            .state('dashboard.akap.laporan.dataSetoran', {
              url: '/datasetoran',
              templateUrl: 'app/pages/akap/laporan/penjualanPemesanan/akap.dataSetoran.html',
              title: 'Data Setoran Agen',
                controller: 'dataSetoranCtrl',
                controllerAs: 'dataSetoranCtrl',
            })
        .state('dashboard.akap.laporan.manifestpenumpang', {
          url: '/manifestpenumpang',
          title: 'Manifest Penumpang',
          templateUrl: 'app/pages/akap/laporan/manifestPenumpang/akap.manifestPenumpang.html',
          controller: 'manifestPenumpangCtrl',
          controllerAs: 'manifestPenumpangCtrl',
          sidebarMeta: {
              order: 2
            }
        })
        .state('dashboard.akap.laporan.terimasetoranagen', {
          url: '/terimasetoranagen',
          title: 'Laporan Setoran Agen (History Komisi)',
          templateUrl: 'app/pages/akap/laporan/setoranAgen/akap.setoranAgen.html',
          controller: 'penerimaanSetoranAgenCtrl',
          controllerAs: 'penerimaanSetoranAgenCtrl',
          sidebarMeta: {
              order: 3
            }
        })
        .state('dashboard.akap.laporan.bagikomisiagen', {
          url: '/bagikomisiagen',
          title: 'Laporan Komisi Agen',
          templateUrl: 'app/pages/akap/laporan/komisiAgen/akap.pembagianKomisiAgen.template.html',
          controller: 'pembagianKomisiAgenCtrl',
          controllerAs: 'pembagianKomisiAgenCtrl',
          sidebarMeta: {
              order: 4
            }
        })
        .state('dashboard.akap.laporan.perTanggal', {
          url: '/pertanggal',
          title: 'Transaksi Per Tanggal',
          templateUrl: 'app/pages/akap/laporan/perTanggal/akap.perTanggal.template.html',
          controller: 'laporanPerTanggalCtrl',
          controllerAs: 'laporanPerTanggalCtrl',
          sidebarMeta: {
              order: 5
            },
            params : {
              tgl : '',
              trayek : '',
              agen : '',
              rute : [],
              arah : [],
              transaksi : '',
              jenisTransaksi : '',
              idTrayek : null
            }
        })
        .state('dashboard.akap.laporan.perPeriode', {
          url: '/perperiode',
          title: 'Transaksi Per Periode',
          templateUrl: 'app/pages/akap/laporan/perPeriode/akap.perPeriode.template.html',
          controller: 'laporanPerPeriodeCtrl',
          controllerAs: 'laporanPerPeriodeCtrl',
          sidebarMeta: {
              order: 6
            },
            params : {
              tgl : '',
              transaksi : '',
              jenisTransaksi : '',
            }
        })
        .state('dashboard.akap.laporan.cancellation', {
          url: '/cancellation',
          title: 'Laporan Cancellation',
          templateUrl: 'app/pages/akap/laporan/cancellation/akap.cancellation.pilih.html',
          controller: 'laporanCancellationCtrl',
          controllerAs: 'laporanCancellationCtrl',
          sidebarMeta: {
              order: 7
            },
            // params : {
            //   tgl : '',
            //   transaksi : '',
            //   jenisTransaksi : '',
            // }
            params : {
              query : {}
            }
        })
            .state('dashboard.akap.laporan.koreksi', {
              url: '/koreksi',
              templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/main.html',
              title: 'Laporan Cancellation',
                controller: "laporanTransaksiKoreksiCtrl",
                controllerAs: "laporanTransaksiKoreksiCtrl",
            })
            .state('dashboard.akap.laporan.pembatalan', {
              url: '/pembatalan',
              templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/main.html',
              title: 'Laporan Cancellation',
                controller: "laporanTransaksiPembatalanCtrl",
                controllerAs: "laporanTransaksiPembatalanCtrl",
            })
            .state('dashboard.akap.laporan.refund', {
              url: '/refund',
              templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/main.html',
              title: 'Laporan Cancellation',
                controller: "laporanTransaksiRefundCtrl",
                controllerAs: "laporanTransaksiRefundCtrl",
            })
        // .state('dashboard.akap.laporan.komisibatalagen', {
        //   url: '/komisipembatalanagen',
        //   title: 'Komisi Pembatalan Agen',
        //   templateUrl: 'app/pages/akap/laporan/komisiPembatalanAgen/pembatalanAgen.main.html',
        //   controller: 'komisiBatalAgenCtrl',
        //   controllerAs: 'komisiBatalAgenCtrl',
        //   sidebarMeta: {
        //       order: 8
        //     }
        // })
        .state('dashboard.akap.trayek', {
          url: '/trayek',
          title: 'Trayek',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              // icon: 'ion-android-bus',
              order: 209
            }
        })
          .state('dashboard.akap.trayek.trayeklist', {
            url: '/trayeklist',
            title: 'Trayek',
            templateUrl: 'app/pages/akap/trayek/trayek/trayekList.html',
            controller: "TrayekCtrls",
            controllerAs: "trayekCtrls",
            sidebarMeta: {
                order: 1
              }
          })
            .state('dashboard.akap.trayek.trayekitem', {
              url: '/detail/{id}',
              templateUrl: 'app/pages/akap/trayek/trayek/trayekItem.html',
                title: 'Detail Trayek',
                controller: "TrayekItemCtrls",
                controllerAs: "trayekItemCtrls",
            })
          .state('dashboard.akap.trayek.subtrayeklist', {
            url: '/subtrayeklist',
            title: 'Detail Layanan Trayek',
            templateUrl: 'app/pages/akap/trayek/subtrayek/subtrayekList.html',
            controller: "SubTrayekCtrls",
            controllerAs: "subTrayekCtrls",
            sidebarMeta: {
                order: 2
              }
          })
            .state('dashboard.akap.trayek.subtrayekitem', {
              url: '/detail/{trayek}/{id}',
              templateUrl: 'app/pages/akap/trayek/subtrayek/subtrayekForm.html',
                title: 'Data Detail Layanan Trayek',
                controller: "SubtrayekItemCtrls",
                controllerAs: "SubtrayekItemCtrls",
            });
  }

})();
