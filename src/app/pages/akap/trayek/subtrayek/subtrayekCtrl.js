(function () {
  'use strict';
  angular.module('BlurAdmin.pages.akap')

.controller('SubTrayekCtrls', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {

  $state.current.tag = 'B.5';

  $http.get(API_ENDPOINT.url +'/po/'+$scope.user.idPo+'/subtrayeks').success(function(data) {
    $scope.trayeks = data;
  });

  $scope.tampil = function() {

    $http.get(API_ENDPOINT.url + '/trayek/' + $scope.trayek + '/subtrayeks').success(function(data) {
      $scope.subtrayek = data;
      $scope.loadData = true;

      $scope.templateUrl = 'app/pages/akap/trayek/subtrayek/subtrayekDetail.html';
      $scope.loadData = false;
    });

    $scope.Lihat = function(index) {
      $state.go('dashboard.akap.trayek.subtrayekitem', {trayek : $scope.subtrayek[index].idtrayek, id : index});
    }
  };
})


.controller('SubtrayekItemCtrls', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $stateParams) {

  $state.current.parrentState = 'dashboard.akap.trayek.subtrayeklist';
  $state.current.parrentTitle = 'Data Detail Layanan Trayek';
  $state.current.tag = 'B.6.' + $stateParams.id;

  $http.get(API_ENDPOINT.url +'/trayek/'+$stateParams.trayek+'/subtrayeks').success(function (data) {
    $scope.data = data[$stateParams.id];
  });
});


})();
