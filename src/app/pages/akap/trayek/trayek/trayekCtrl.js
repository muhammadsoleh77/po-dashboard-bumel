(function () {
  'use strict';
  angular.module('BlurAdmin.pages.akap')

.controller('TrayekCtrls', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state) {

  $state.current.tag = 'B.4';

  $scope.loadData = true;
  $scope.empty = true;
  $scope.message = "";

  $http.get(API_ENDPOINT.url + '/po/' + $rootScope.po.idPo + '/trayeks').success(function(data) {
    $scope.trayek = data;
    $scope.loadData = false;
    if($scope.trayek.length === 0) {
      $scope.empty = true;
      $scope.message = "Tidak Ada Data Trayek";
    }else {
      $scope.empty = false;
    }
  });

  $scope.Lihat = function(index){
    $state.go('dashboard.akap.trayek.trayekitem', {id: index});
  }

})

.controller('TrayekItemCtrls', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $stateParams) {

  $state.current.parrentState = 'dashboard.akap.trayek.trayeklist';
  $state.current.parrentTitle = 'Data Trayek';
  $state.current.tag = 'B.5.' + $stateParams.id;

  $http.get(API_ENDPOINT.url +'/po/'+$rootScope.po.idPo+'/trayeks').success(function (data) {
    $scope.data = data[$stateParams.id];
  });
});
})();
