(function (){
  'use strict';

  angular.module('BlurAdmin.pages.akap')

  .controller('historiModalCtrl', historiModalCtrl);

  function historiModalCtrl($uibModalInstance, items, $scope, $http, API_ENDPOINT, $rootScope){

    $scope.width = '90vw!important;';

    $scope.templateUrl = 'app/pages/akap/laporan/modalKomisi/modal.data.detail.html';
    $scope.data = items.data;

    $http.get(API_ENDPOINT.url + 'po/transaksikomisiagen/history/detail?idTrans=' + $scope.data.idTransKomisi).success(function(data){
      $scope.data.dataKomisi = data;
      for (var i = 0; i < data.length; i++) {
        data[i].statusBayar = data[i].statusBayar === false ? "Belum lunas" : "Lunas"
      }
    })
    .error(function(){
      $scope.data.dataKomisi = [];
    });

    $scope.close = function() {
      $uibModalInstance.dismiss();
    };
  }
})();
