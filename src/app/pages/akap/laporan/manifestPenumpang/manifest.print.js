(function() {

  angular
  .module('BlurAdmin.pages.akap')
  .service('manifestPrint', manifestPrint);

  function manifestPrint($filter, $rootScope){

    var exportPdf = function(data, tanggal) {
      var docDefinition = {
        defaultStyle : {
          fontSize : 9
        },
        content : [
          {
            widths: ['*','*','*','*',],
            fontSize: 16,
            marginBottom : 5,
            bold: true,
            alignment: 'left',
            text: 'PO.'+$rootScope.po.nama, style: 'header'},
          {
            text:  'Manifest Penumpang',
            marginBottom : 5,
            fontSize: 12,
            alignment: 'left',
          },
          {
            text:  'Tanggal : ' + $filter('date_v1')(tanggal),
            fontSize: 10,
            marginBottom : 20,
            alignment: 'left',
          }
        ]
      };

      var table = {
        color: '#444',
        margin: [0, 5, 0, 10],
        table: {
          widths: [25,'*','*',35,'*','*','*','*'],
          headerRows: 2,
          fontSize : 8,
          body: [
            [
              {text: 'No. Kursi', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Nama Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Jenis Kelamin', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Usia', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Tujuan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'No. Telepon', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Tgl Transaksi', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
            ],
            [
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {}
            ]
          ]
        }
      };

      data.forEach(function(x){
        console.log(x);
        x.penumpangs.forEach(function(y){
          if (!x.alamatBrgkt) x.alamatBrgkt=x.berangkat; 
          table.table.body.push(
            [
              y.kursi,
              y.nama,
              y.gender,
              y.kategoriUsia,
              y.alamatBrgkt,
              y.tujuan,
              y.telpon,
              $filter('date')(y.tglTrans, 'dd/MM/yyyy'),
            ]
          );
        });

      });



      docDefinition.content.push(table);
      console.log(docDefinition);
      pdfMake.createPdf(docDefinition).open();

    };

    return {
      exportPdf : exportPdf
    };

  }
})();
