(function () {
  angular.module('BlurAdmin.pages.akap')
  .controller('manifestPenumpangCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, manifestPrint, uiGridConstants, $parse) {

    $state.current.tag = 'E.3';

    var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    $scope.limit = String(date);

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 1) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;

    $scope.loadData = false;

    $scope.noData = {
      status : true,
      message : ""
    };

    $scope.getJamBerangkat = function (idTrayek) {
      $http.get(API_ENDPOINT.url + '/trayek/' + idTrayek + '/subtrayeks').success(function (data) {
        var jamBerangkat = ['Semua Keberangkatan'];
        data.forEach(function (item) {
          jamBerangkat.push(item.tempatberangkat[0].waktutiba);
        });
        $scope.listJamBerangkat =  jamBerangkat;
        if ($scope.listJamBerangkat.length === 2) {
          $scope.jamBerangkat = $scope.listJamBerangkat[1];
        }
      });
    };

    $scope.changeTgl = function () {
      // console.log($scope.trayek);
      $scope.dataLoaded = false;
    };

    $scope.showFilter = false;

   var paginationOptions = {
      pageNumber: 0,
      pageSize: 25,
      sort: null
    };

    // $scope.trayek = {};

    $scope.gridOptions = {
      enableColumnResizing: true,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: false,
      enableRowSelection: true,
      enableGridMenu: true,
      // expandableRowTemplate: 'app/pages/laporan/expandGrid.html',
      expandableRowHeight: 150,
      columnDefs: [
        // { name: 'kursi', displayName:'No.Kursi', width: 100, cellTemplate : "<span>{{ row.entity.kursi | orderBy:'kursi' }}</span>"},
        { name: 'kursi', displayName:'No.Kursi', width: 90, enableSorting: false, cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'nama', displayName:'Nama Penumpang', width: 100, cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'gender', displayName:'Gender', width: 80, cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'kategoriUsia', displayName:'Usia', width: 80, cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'berangkat', displayName:'Berangkat', cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'tujuan', displayName:'Tujuan', cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'telpon', displayName:'No.Telepon', cellFilter:'is_anak:row.entity.kategoriUsia', cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if(row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'tglTrans', displayName:'Tgl Transaksi', cellTemplate : "<span>{{ row.entity.tglTrans | date:'dd-MM-yyyy' }}</span>", cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        { name: 'tmptBeli', displayName:'Pembelian', cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.penumpangOtobus === true) {
            return 'green';
          }
        }},
        // { name: 'tmptBeli', displayName:'Pembelian', cellTemplate : "<span>{{ row.entity.penumpangOtobus === false ? 'Agen' : 'Otobus' }}</span>", cellClass : function(grid, row, col, rowRenderIndex, colRenderIndex) {
        //   if (row.entity.penumpangOtobus === true) {
        //     return 'green';
        //   }
        // }},
      ],
    };


    var getPage = function() {

      if (!$scope.tanggal) {
        return false;
      }

      $scope.gridOptions.exporterCsvFilename = 'Manifest Penumpang ' + $scope.hariIni + '.xls';

      $scope.noData = {
        status : false,
        message : ''
      };

      $scope.showData = {
        status : false,
        message : ''
      };

      $scope.loadData = true;
      $scope.showFilter = false;
      var url = API_ENDPOINT.url +'/po/penumpang?idPo=' + $rootScope.po.idPo + '&tgl=' + $scope.tanggal;

      $http.get(url).success(function(data) {

        var dataPenumpang = [];
        var dataPenumpangS = [];
        var listJamBerangkat = [{
          name : "Semua Jam Berangkat",
          value : ''
        }];
        data.forEach(function (doc) {
          listJamBerangkat.push({
            name : doc.jamBerangkat,
            value : doc.jamBerangkat
          });
          // console.log(doc.penumpangs);

          // START membuat validasi sorting berdasarkan no.kursi integer dan no.kursi yg ada angka hurufnya
          doc.penumpangs.forEach(function (doc2) {
            var letters = /[A-Za-z]/g;
            if(doc2.kursi.match(letters)){
              // console.log(doc2.kursi);
              dataPenumpangS.push({
                kursi : doc2.kursi,
                nama : doc2.nama,
                gender : doc2.gender,
                kategoriUsia : doc2.kategoriUsia,
                berangkat : doc2.alamatBrgkt,
                asal : doc2.asal,
                tujuan : doc2.tujuan,
                telpon : doc2.telpon,
                jamBerangkat : doc.jamBerangkat,
                trayek : doc.namaTrayek,
                idTrayek : doc.idTrayek,
                agenBerangkat : doc2.agenBerangkat,
                tgl : doc.tgl,
                tglTrans : doc2.tglTrans,
                penumpangOtobus : doc2.penumpangOtobus,
                tmptBeli : doc2.tmptBeli
              });
            }else{
              dataPenumpang.push({
                kursi : parseInt(doc2.kursi),
                nama : doc2.nama,
                gender : doc2.gender,
                kategoriUsia : doc2.kategoriUsia,
                berangkat : doc2.alamatBrgkt,
                asal : doc2.asal,
                tujuan : doc2.tujuan,
                telpon : doc2.telpon,
                jamBerangkat : doc.jamBerangkat,
                trayek : doc.namaTrayek,
                idTrayek : doc.idTrayek,
                agenBerangkat : doc2.agenBerangkat,
                tgl : doc.tgl,
                tglTrans : doc2.tglTrans,
                penumpangOtobus : doc2.penumpangOtobus,
                tmptBeli : doc2.tmptBeli
              });
            }
          });
        });
        // sort tabel berdasarkan no.kursi
        var sortByProperty = function (property) {
          return function (x, y) {
              return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
          };
        };
        dataPenumpang.sort(sortByProperty('kursi'));
        dataPenumpangS.sort(function(a, b){
          if(a.kursi < b.kursi) return -1;
          if(a.kursi > b.kursi) return 1;
          return 0;
        });
        // dataPenumpang.push(dataPenumpangS);
        dataPenumpangS.forEach(function (doc3) {
          dataPenumpang.push(doc3);
        });
        // end

        $scope.exportXls = function(){

          $scope.excelData='';

          $scope.excelData+='<tr><th>No. Kursi</th><th>Nama Penumpang</th><th>Jenis Kelamin</th><th>Usia</th><th>Berangkat</th><th>Tujuan</th><th>No.Telepon</th><th>Tgl Transaksi</th><th>pnp Otobus</th></tr>\n';

          // $scope.gridOptions.data = dataPenumpang;
          $scope.gridOptions.data.forEach(function (doc) {
            $scope.excelData+='<tr><td>'+doc.kursi+'</td><td>'+doc.nama+'</td><td>'+doc.gender+'</td><td>'+doc.kategoriUsia+'</td><td>'+doc.berangkat+'</td><td>'+doc.tujuan+'</td><td>'+doc.telpon+'</td><td>'+ $filter('date')(doc.tglTrans, 'dd/MM/yyyy')+'</td><td>'+(doc.penumpangOtobus === false ? 'Agen' : 'Otobus')+'</td></tr>';
            $scope.excelData+='\n';
          });
          // console.log($scope.gridOptions.data);

          $scope.exportPage = true;

          var excelFile='<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http:\/\/www.w3.org\/TR\/REC-html40">';
          excelFile+='<head><meta http-equiv=Content-Type content="text/html; charset=windows-1252"><meta name=ProgId content=Excel.Sheet><meta name=Generator content="Microsoft Excel 11">';
          excelFile+='<!--[ifgte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>';
          excelFile+='</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table  x:str border=0>';
           excelFile+=$scope.excelData;
           excelFile+='</table></body></html>';

          var blob = new Blob([excelFile], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
          });
          saveAs(blob, "Laporan Manifest Penumpang " + $scope.tanggal + ".xls");
          $scope.exportPage = false;
        };

        // $scope.gridOptions.data = data.content;
        for(var i = 0; i < data.length; i++){
          data[i].subGridOptions = {
            columnDefs: [ {name:"Asal", field:"namaTempatBrgkt"},
            {name:"Tujuan", field:"kotaTujuan"},
            {name:"Jumlah Penumpang", field:"jmlPenumpang"},
            {name:"Total Bayar", cellFilter: 'number', field:"totalBayar"}],
            data: data[i].transaksi
          };
        }

        $scope.gridOptions.data = dataPenumpang;
        $scope.dataLoaded = true;
        var pdfHeader = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*'],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Manifest Penumpang',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')($scope.tanggal),
              fontSize: 8,
              marginBottom : 20,
              alignment: 'left',
            },
            {
              color: '#444',
              margin: [0, 5, 0, 10],
              table: {
                widths: [50,70,70,40,90,50,50],
                headerRows: 2,
                fontSize : 6,
                body: [
                  [
                    {text: 'No.Kursi ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Nama Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jenis Kelamin ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Usia', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Tujuan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'No.Telepon', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  ],
                  [
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {}
                  ]
                ]
              }
            }
          ]
        };

        $scope.docDefinitionx=[];
        $scope.docDefinitionx.push(pdfHeader);
        $scope.dataPdf=data;


        $scope.exportPdf = function () {

          // manifestPrint.exportPdf(data, $scope.tanggal);
          $scope.docDefinitionx=[];
          $scope.docDefinitionx.push(pdfHeader);
          $scope.docDefinition=$scope.docDefinitionx[0];
          $scope.docDefinition.content[3].table.body=[
            [
              {text: 'No.Kursi ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Nama Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Jenis Kelamin ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Usia', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'Tujuan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              {text: 'No.Telepon', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
            ],
            [
              {},
              {},
              {},
              {},
              {},
              {},
              {}
            ]
          ];

          $scope.gridOptions.data.forEach(function (doc) {
            $scope.docDefinition.content[3].table.body.push([
              {text: doc.kursi, fontSize: 7},
              {text: doc.nama, fontSize: 7, },
              {text: doc.gender, fontSize: 7, },
              {text: doc.kategoriUsia, fontSize: 7},
              {text: doc.berangkat, fontSize: 7},
              {text: doc.tujuan, fontSize: 7},
              {text: doc.telpon, fontSize: 7},
              // {text: $filter('is_anak')(doc.telepon, doc.kategoriUsia), fontSize: 7},
            ]);
          });
          pdfMake.createPdf($scope.docDefinition).open();
          // $scope.docDefinition='';
        };

        $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 70; // your header height
           if (($scope.gridOptions.data.length * rowHeight + headerHeight) > 400) {
             height = 400;
           } else {
             height = $scope.gridOptions.data.length * rowHeight + headerHeight;
           }
           return {
              height: height + "px"
           };
         };

        if ($scope.gridOptions.data.length === 0) {
          $scope.noData = {
            status : true,
            message : "Tidak ada Data Manifest Penumpang"
          };
        } else {
          $scope.showData = {
            status : true,
            message : 'Manifest Penumpang ' + $scope.hariIni
          };
        }

        $scope.loadData = false;
        $scope.showFilter = true;

        $scope.refreshData = function () {

          // $scope.jam = "Semua Keberangkatan";

          if ($scope.trayek) {
            $scope.getJamBerangkat($scope.trayek.id);
            $scope.getJamBerangkat($scope.trayek.kode);
          }

          if ($scope.trayek.id) {
            $scope.gridOptions.data = $filter('filter')(dataPenumpang, {idTrayek : $scope.trayek.id}, true);
            console.log($scope.trayek.id);
          }
          else {
            $scope.gridOptions.data = $filter('filter')(dataPenumpang, '');
          }

          if ($scope.jam && $scope.jam === "Semua Keberangkatan") {
            $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, '');
          } else {
            $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, $scope.jam);
          }

          if($scope.trayek.kode) {
            $scope.gridOptions.data = $filter('filter')(dataPenumpang, {agenBerangkat : $scope.trayek.kode}, true);
            // console.log($scope.agen.id);
          }
          // else {
          //   $scope.gridOptions.data = $filter('filter')(dataPenumpang, '');
          // }

          // if($scope.agen) {
          //   $scope.gridOptions.data = $filter('filter')(dataPenumpang, $scope.agen);
          //   console.log($scope.agen);
          // }

          // $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, $scope.agen);
          // console.log($scope.agen);

          // $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data.push({berangkat : doc2.alamatBrgkt}), $scope.agen);
          console.log($scope.gridOptions.data);

          // console.log($scope.gridOptions.data);
          // $scope.dataPdf=[];
          // $scope.dataPdf.push({"penumpangs":$scope.gridOptions.data});
        };
    });
    };

    $scope.tampil = function(tanggal) {
      getPage();
      $scope.trayek = '';
      $scope.jam = '';
      $scope.agen = '';
    };


  });
})();
