(function () {
  angular.module('BlurAdmin.pages.akap')
  .controller('setoranPemesananCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {

    $state.current.tag = 'E.4.2';

    var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    $scope.limit = String(date);

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 1) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;

    $scope.loadData = false;

    var getPage = function() {

      if (!$scope.tanggal) {
        return false;
      }

      $scope.loadData = true;

      var url = API_ENDPOINT.url +'/po/laporan/pemesanan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;

      $http.get(url).success(function(data) {
        // $scope.gridOptions.data = data.content;
        var dataSetoran = [];
        data.forEach(function (doc) {
          if (doc.penjualan > 0) {
            dataSetoran.push(doc);
          }
        });
        $scope.datas = dataSetoran;

        $scope.loadData = false;
        $scope.docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Setoran Pemesanan',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')($scope.tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        $scope.exportPdf = function(){
          $scope.datas.forEach(function (doc) {
            $scope.docDefinition.content.push({
              columns : [
                {
                  width: 70,
                  text: 'Agen',
                  bold: true,
                },
                {
                  width: 'auto',
                  text : ': ' + doc.namaAgen,
                }
              ]
            });

            var table = {
        			color: '#444',
              margin: [0, 5, 0, 10],
        			table: {
        				widths: ['*','*','*','*'],
        				headerRows: 2,
                fontSize : 8,
        				body: [
        					[
                    {text: 'Jumlah Penjualan ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jumlah Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Komisi Agen ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jumlah Setoran ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  ],
                  [
                    {},
                    {},
                    {},
                    {},
                  ],
                  [
                    { text : $filter('currency')(doc.penjualan, '', 0), alignment : 'right' },
                    { text : doc.kursiTerpesan, alignment : 'right' },
                    { text : $filter('currency')(doc.komisiAgen, '', 0), alignment : 'right' },
                    { text : $filter('currency')(doc.setoran, '', 0), alignment : 'right' },
                  ]
        				]
        			}
        		};

            $scope.docDefinition.content.push(table);
          });
          pdfMake.createPdf($scope.docDefinition).open();
        };

        $scope.exportXls = function(){
          var blob = new Blob([document.getElementById('dataTable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
          });
          saveAs(blob, "Report.xls");
        };
    });
    };

    $scope.tampil = function(tanggal) {
      getPage();
    };
  });
})();
