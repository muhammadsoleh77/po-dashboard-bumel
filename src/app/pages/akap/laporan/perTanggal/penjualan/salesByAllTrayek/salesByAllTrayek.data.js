(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('salesByAllTrayekData', salesByAllTrayekData);

    function salesByAllTrayekData ($rootScope, $filter, chartColors ) {
      var getData = function(data, trayek){
        var datas = [];
        var getAgenProfile = function (idPemesan) {
          if (idPemesan === 0) {
            profile = {
              name : "",
              value : "",
              alamat : "",
              kontakPerson : "",
              nomorTelepon : "",
              id : 0,
            }
          }

          for (var i = 0; i < $rootScope.listAgens.length; i++) {
            if ($rootScope.listAgens[i].id === idPemesan) {
              profile = $rootScope.listAgens[i];
            }
          }
          // return profile;
        };

        var dataPenjualan = [];
        data.forEach(function (doc) {
          var profile, transaksi;
          doc.transaksi.forEach(function (doc2) {
            profile = {
              idPemesan : getAgenProfile(doc2.idAgen).name,
              jmlKursiTerjual : doc.jmlKursiTerjual,
              jmlPenjualan : doc.jmlPenjualan,
              // komisiAgen : doc.komisiAgen,
              setoran : doc.setoran,

              namaBus : doc.namaBus === undefined ? "" : doc.namaBus,
              namaTrayek : doc.namaTrayek,
              jamBerangkat : doc.jamBerangkat,
              idTrayek : doc.idTrayek,
              idAgen : doc2.idAgen,
            };

            if (doc2.jenis === 1) {
              transaksi = {
                jmlKursi : doc2.jmlKursi,
                biayaAdmin : doc2.biayaAdmin,
                jmlRupiah : doc2.jmlRupiah,
                jenis : doc2.jenis,
                komisiAgen : doc2.komisiAgen,
              };
            } else if (doc2.jenis === 2) {
              transaksi = {
                jmlKursi : 0,
                biayaAdmin : 0,
                jmlRupiah : 0,
                jenis : doc2.jenis,
                komisiAgen : 0,
              };
            } else {
              transaksi = {
                jmlKursi : 0,
                biayaAdmin : 0,
                jmlRupiah : 0,
                jenis : doc2.jenis,
                komisiAgen : doc2.komisiAgen,
              };
            }
            dataPenjualan.push(Object.assign(transaksi, profile));
          });
        });
        var jmlKursiTerjual, jmlPenjualan, setoran;

        var dataTmp;
        var jmlKursi;
        var jmlRupiah;
        var komisiAgen;
        var biayaAdmin;
        for (var j = 0; j < $rootScope.lisTrayeks.length; j++) {
          var namaBus;
          if (dataPenjualan.length > 0) {
            for (i = 0; i < dataPenjualan.length; i++) {
              if (dataPenjualan[i].idTrayek === $rootScope.lisTrayeks[j].id) {
                namaBus = dataPenjualan[i].namaBus;
                i = dataPenjualan.length;
              } else {
                namaBus = "";
              }
            }
          } else {
            namaBus = "";
          }
          var step1 = [];
          for (i = 0; i < $rootScope.lisTrayeks[j].tempatBerangkat.length; i++) {
            jmlKursiTerjual = 0;
            setoran = 0;
            jmlKursi = 0;
            jmlRupiah = 0;
            komisiAgen = 0;
            biayaAdmin = 0;
            dataTmp = $filter('filter')(dataPenjualan, {idTrayek : $rootScope.lisTrayeks[j].id}, true);
            dataTmp = $filter('filter')(dataTmp, {idAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode}, true);
            for (var k = 0; k < dataTmp.length; k++) {
              setoran = setoran + (dataTmp[k].jmlRupiah - dataTmp[k].komisiAgen);
              jmlKursi = jmlKursi + dataTmp[k].jmlKursi;
              jmlRupiah = jmlRupiah + dataTmp[k].jmlRupiah;
              komisiAgen = komisiAgen + dataTmp[k].komisiAgen;
              biayaAdmin = biayaAdmin + dataTmp[k].biayaAdmin;
            }
            if (jmlKursi >= 0) {
              step1.push({
                namaAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].nama,
                idAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode,
                profileAgen : getAgenProfile($rootScope.lisTrayeks[j].tempatBerangkat[i].kode),

                jmlKursi : jmlKursi,
                jmlRupiah : jmlRupiah,
                komisiAgen : komisiAgen,
                biayaAdmin : biayaAdmin,
                dataPenumpang : []
              });
            }
          }
          if (step1.length > 0) {
            var totalJual = 0;
            var totalKursi = 0;
            var totalKomisi = 0;
            var totalBiayaAdmin = 0;
            var totalSetoranPo = 0;
            var totalSetoranAgen = 0;
            step1.forEach(function (doc) {
              totalJual = totalJual + doc.jmlRupiah;
              totalKomisi = totalKomisi + doc.komisiAgen;
              totalKursi = totalKursi + doc.jmlKursi;
              totalBiayaAdmin = totalBiayaAdmin + doc.biayaAdmin;
            });
            datas.push({

              namaTrayek : $rootScope.lisTrayeks[j].value,
              color: chartColors[j],
              idTrayek : $rootScope.lisTrayeks[j].id,
              rute : $rootScope.lisTrayeks[j].rute,
              arah : $rootScope.lisTrayeks[j].arah,
              namaBus : namaBus,
              transaksi : step1,

              totalJual : totalJual,
              totalKomisi : totalKomisi,
              totalKursi : totalKursi,
              totalBiayaAdmin : totalBiayaAdmin,
              totalSetoranPo : totalSetoranPo,
              totalSetoranAgen : totalSetoranAgen,
              setoran : totalJual - totalKomisi,
              dataPenumpang : []
            });
          }
        }

        for (var i = 0; i < data.length; i++) {
          for (var x = 0; x < datas.length; x++) {
            if (data[i].idTrayek == datas[x].idTrayek) {
              datas[x].dataPenumpang = data[i].dataPenumpang;
            }
          }
        }

        for (i = 0; i < datas.length; i++) {
          for (var z = 0; z < datas[i].dataPenumpang.length; z++) {
            for (var y = 0; y < datas[i].transaksi.length; y++) {
              if (datas[i].dataPenumpang[z].agenBeli == datas[i].transaksi[y].namaAgen) {
                datas[i].transaksi[y].dataPenumpang.push(datas[i].dataPenumpang[z]);
              }
            }
          }
        }
        datas = $filter('orderBy')(datas, '-totalJual');
        return {
          detail : datas,
          // total : {
          //   kursi : data.totalPenjualanHariIni.jmlKursiTerjual,
          //   penjualan : data.totalPenjualanHariIni.jmlPenjualan,
          //   komisiAgen  : data.totalPenjualanHariIni.komisiAgen,
          //   biayaAdmin : data.totalPenjualanHariIni.biayaAdmin,
          //   setoran : data.totalPenjualanHariIni.setoran
          // }
        };
      };

      return {
        getData : getData
      };
    }

})();
