(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('salesByTrayekChart', salesByTrayekChart);

    function salesByTrayekChart ($rootScope, $filter, baConfig) {

      var layoutColors = baConfig.colors;
      var barChart = function (data) {
        barChart = AmCharts.makeChart('barChart', {
          type: 'serial',
          color: layoutColors.defaultText,
          dataProvider: data,
          valueAxes: [
            {
              axisAlpha: 0,
              position: 'left',
              title: 'Penjualan Per Agen',
              gridAlpha: 0.5,
              gridColor: '#ddd',
            }
          ],
          startDuration: 1,
          graphs: [
            {
              balloonText: '<b>[[category]]: [[value]]</b>',
              fillColorsField: 'color',
              fillAlphas: 0.6,
              lineAlpha: 0.2,
              type: 'column',
              // valueField: 'totalBayar'
              valueField: 'jmlRupiah'
            }
          ],
          chartCursor: {
            categoryBalloonEnabled: false,
            cursorAlpha: 0,
            zoomable: false
          },
          categoryField: 'namaAgen',
          categoryAxis: {
            gridPosition: 'start',
            labelRotation: 45,
            gridAlpha: 0.5,
            gridColor: '#ddd',
          },
          export: {
            enabled: true
          },
          creditsPosition: 'top-right',
          // pathToImages: layoutPaths.images.amChart
        });
      };

      return {
        barChart : barChart
      };
    }

})();
