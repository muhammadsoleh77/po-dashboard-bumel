(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('salesByAgenData', salesByAgenData);

    function salesByAgenData ($rootScope, $filter, chartColors, $http, API_ENDPOINT ) {

      var getData = function(data, agen, tgl){
        var datas = [];
        var getAgenProfile = function (idPemesan) {
          if (idPemesan === 0) {
            profile = {
              name : "",
              value : "",
              alamat : "",
              kontakPerson : "",
              nomorTelepon : "",
              id : 0,
            }
          }

          for (var i = 0; i < $rootScope.listAgens.length; i++) {
            if ($rootScope.listAgens[i].id === idPemesan) {
              profile = $rootScope.listAgens[i];
            }
          }
          return profile;
        };

        var dataPenjualan = [];
        data.forEach(function (doc) {
          var profile, transaksi;
          doc.transaksi.forEach(function (doc2) {
            profile = {

              jmlRupiah : doc2.jmlRupiah,
              jmlKursi : doc2.jmlKursi,

              namaBus : doc.namaBus === undefined ? "" : doc.namaBus,
              namaTrayek : doc.namaTrayek,
              jamBerangkat : doc.jamBerangkat,
              idTrayek : doc.idTrayek,
              idAgen : doc2.idAgen,
              // idIssuer : doc2.transaksi.idIssuer,
              // idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
              // namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
              // idPemesan : getAgenProfile(doc2.transaksi.idIssuer).name,
              // tglBerangkat : doc2.transaksi.tglBerangkat,
            };

            // if (doc2.transaksi.jenis === 1) {
            if (doc2.jenis === 1) {
              transaksi = {
                // jmlPenumpang : doc2.transaksi.jmlPenumpang,
                // biayaAdmin : (doc2.transaksi.biayaAdmin + doc2.transaksi.iuranIpomi),
                // totalBayar : doc2.transaksi.totalBayar,
                // jenis : doc2.transaksi.jenis,
                // totalKomisi : doc2.transaksi.idIssuer === doc2.transaksi.idTempatBrgkt ? (doc2.transaksi.komisiPembayaran + doc2.transaksi.komisiPemberangkatan + doc2.transaksi.komisiPenjualan) : doc2.transaksi.komisiPemberangkatan,
                // setoranPo : doc2.transaksi.idSetoranPo ? doc2.transaksi.totalBayar : 0,
                // setoranAgen : doc2.transaksi.idSetoranAgen ? doc2.transaksi.totalBayar : 0,
                jmlKursi : doc2.jmlKursi,
                biayaAdmin : doc2.biayaAdmin,
                jmlRupiah : doc2.jmlRupiah,
                jenis : doc2.jenis,
                komisiAgen : doc2.komisiAgen,
              };
            // } else if (doc2.transaksi.jenis === 2) {
            } else if (doc2.jenis === 2) {
              transaksi = {
                // jmlPenumpang : 0,
                // biayaAdmin : 0,
                // totalBayar : 0,
                // jenis : doc2.transaksi.jenis,
                // totalKomisi : 0,
                // setoranPo :  0,
                // setoranAgen : 0,
                jmlKursi : doc2.jmlKursi,
                biayaAdmin : 0,
                jmlRupiah : 0,
                jenis : doc2.jenis,
                komisiAgen : 0,
              };
            } else {
              transaksi = {
                // jmlPenumpang : 0,
                // biayaAdmin : 0,
                // totalBayar : 0,
                // jenis : doc2.transaksi.jenis,
                // totalKomisi :  doc2.transaksi.komisiPemberangkatan,
                // setoranPo : doc2.transaksi.idSetoranPo ? doc2.transaksi.komisiPemberangkatan : 0,
                // setoranAgen : doc2.transaksi.idSetoranAgen ? doc2.transaksi.komisiPemberangkatan : 0,
                jmlKursi : 0,
                biayaAdmin : 0,
                jmlRupiah : 0,
                jenis : doc2.jenis,
                komisiAgen : doc2.komisiAgen,
              };
            }
            dataPenjualan.push(Object.assign(transaksi, profile));
          });
        });

        var jmlRupiah;
        var jmlKursi;

        var dataTmp;
        var jmlPenumpang;
        var totalBayar;
        var totalKomisi;
        var komisiAgen;
        var setoran;
        var biayaAdmin;
        var setoranPo;
        var setoranAgen;
        for (var j = 0; j < $rootScope.lisTrayeks.length; j++) {
          var namaBus;
          if (dataPenjualan.length > 0) {
            for (i = 0; i < dataPenjualan.length; i++) {
              if (dataPenjualan[i].idTrayek === $rootScope.lisTrayeks[j].id) {
                namaBus = dataPenjualan[i].namaBus;

                jmlRupiah = dataPenjualan[i].jmlRupiah;
                jmlKursi = dataPenjualan[i].jmlKursi;

                i = dataPenjualan.length;
              } else {
                namaBus = "";
              }
            }
          } else {
            namaBus = "";
          }
          var step1 = [];
          for (i = 0; i < $rootScope.lisTrayeks[j].tempatBerangkat.length; i++) {
            // jmlRupiah = 0;
            // jmlKursi = 0;
            // jmlPenumpang = 0;
            // totalBayar = 0;
            // totalKomisi = 0;
            // biayaAdmin = 0;
            // setoranPo = 0;
            // setoranAgen = 0;
            // dataTmp = $filter('filter')(dataPenjualan, {idTrayek : $rootScope.lisTrayeks[j].id}, true);
            // dataTmp = $filter('filter')(dataTmp, {idTempatBrgkt : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode}, true);
            jmlKursiTerjual = 0;
            setoran = 0;
            jmlKursi = 0;
            jmlRupiah = 0;
            komisiAgen = 0;
            biayaAdmin = 0;
            dataTmp = $filter('filter')(dataPenjualan, {idTrayek : $rootScope.lisTrayeks[j].id}, true);
            dataTmp = $filter('filter')(dataTmp, {idAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode}, true);
            for (var k = 0; k < dataTmp.length; k++) {

              // jmlRupiah = jmlRupiah + dataTmp[k].jmlRupiah;
              // jmlKursi = jmlKursi + dataTmp[k].jmlKursi;
              //
              // jmlPenumpang = jmlPenumpang + dataTmp[k].jmlPenumpang;
              // totalBayar = totalBayar + dataTmp[k].totalBayar;
              // totalKomisi = totalKomisi + dataTmp[k].totalKomisi;
              // biayaAdmin = biayaAdmin + dataTmp[k].biayaAdmin;
              // setoranPo = setoranPo + dataTmp[k].setoranPo;
              // setoranAgen = setoranAgen + dataTmp[k].setoranAgen;
              setoran = setoran + (dataTmp[k].jmlRupiah - dataTmp[k].komisiAgen);
              jmlKursi = jmlKursi + dataTmp[k].jmlKursi;
              jmlRupiah = jmlRupiah + dataTmp[k].jmlRupiah;
              komisiAgen = dataTmp[k].komisiAgen;
              biayaAdmin = biayaAdmin + dataTmp[k].biayaAdmin;
            }
            // if (jmlPenumpang >= 0) {
            if (jmlKursi >= 0) {
              step1.push({
                // jmlRupiah : jmlRupiah,
                // jmlKursi : jmlKursi,
                //
                // namaAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].nama,
                // idAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode,
                // profileAgen : getAgenProfile($rootScope.lisTrayeks[j].tempatBerangkat[i].kode),
                // jmlPenumpang : jmlPenumpang,
                // totalBayar : totalBayar,
                // totalKomisi : totalKomisi,
                // biayaAdmin : biayaAdmin,
                // setoranPo : setoranPo,
                // setoranAgen : setoranAgen,
                // dataPenumpang : []
                namaAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].nama,
                idAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode,
                profileAgen : getAgenProfile($rootScope.lisTrayeks[j].tempatBerangkat[i].kode),

                jmlKursi : jmlKursi,
                jmlRupiah : jmlRupiah,
                komisiAgen : komisiAgen,
                biayaAdmin : biayaAdmin,
                dataPenumpang : []
              });
            }
            if (agen) {
              step1 = $filter('filter')(step1, {idAgen : agen.id}, true);
            }
          }
          if (step1.length > 0) {
            var totalJual = 0;
            var totalKursi = 0;
            totalKomisi = 0;
            var totalBiayaAdmin = 0;
            var totalSetoranPo = 0;
            var totalSetoranAgen = 0;
            step1.forEach(function (doc) {
              // totalJual = totalJual + doc.totalBayar;
              // totalKomisi = totalKomisi + doc.totalKomisi;
              // totalKursi = totalKursi + doc.jmlPenumpang;
              // totalBiayaAdmin = totalBiayaAdmin + doc.biayaAdmin;
              // totalSetoranPo = totalSetoranPo + doc.setoranPo;
              // totalSetoranAgen = totalSetoranAgen + doc.setoranAgen;
              setoran = setoran + doc.setoran;
              totalJual = totalJual + doc.jmlRupiah;
              totalKomisi = totalKomisi + doc.komisiAgen;
              totalKursi = totalKursi + doc.jmlKursi;
              totalBiayaAdmin = totalBiayaAdmin + doc.biayaAdmin;
            });
            datas.push({
              namaTrayek : $rootScope.lisTrayeks[j].value,
              idTrayek : $rootScope.lisTrayeks[j].id,
              color : chartColors[j],
              rute : $rootScope.lisTrayeks[j].rute,
              arah : $rootScope.lisTrayeks[j].arah,
              namaBus : namaBus,
              transaksi : step1,
              totalJual : totalJual,
              totalKomisi : totalKomisi,
              totalKursi : totalKursi,
              totalBiayaAdmin : totalBiayaAdmin,
              totalSetoranPo : totalSetoranPo,
              totalSetoranAgen : totalSetoranAgen,
              setoran : totalJual - totalKomisi,
              dataPenumpang : []
            });
          }
        }

        // for (var i = 0; i < data.penjualanHariIni.length; i++) {
        //   for (var x = 0; x < datas.length; x++) {
        //     if (data.penjualanHariIni[i].idTrayek == datas[x].idTrayek) {
        //       datas[x].dataPenumpang = data.penjualanHariIni[i].dataPenumpang;
        //     }
        //   }
        // }
        for (var i = 0; i < data.length; i++) {
          for (var x = 0; x < datas.length; x++) {
            if (data[i].idTrayek == datas[x].idTrayek) {
              datas[x].dataPenumpang = data[i].dataPenumpang;
            }
          }
        }
        for (i = 0; i < datas.length; i++) {
          for (var z = 0; z < datas[i].dataPenumpang.length; z++) {
            for (var y = 0; y < datas[i].transaksi.length; y++) {
              if (datas[i].dataPenumpang[z].agenBeli == datas[i].transaksi[y].namaAgen) {
                datas[i].transaksi[y].dataPenumpang.push(datas[i].dataPenumpang[z]);
              }
            }
          }
        }

        var data1 = [];
        var final = [] ;
        final[0] = agen;
        final[0].totalBiayaAdmin = 0;
        final[0].totalJual = 0;
        final[0].totalKomisi = 0;
        final[0].totalKursi = 0;
        final[0].setoran = 0;
        datas.forEach(function (doc) {
          for (var i = 0; i < doc.transaksi[0].dataPenumpang.length; i++) {
            doc.transaksi[0].dataPenumpang[i].namaTrayek = doc.namaTrayek;
            doc.transaksi[0].dataPenumpang[i].namaBus = doc.namaBus;
          }
          doc.dataPenumpang = doc.transaksi[0].dataPenumpang;
          data1.push(doc);
          final[0].totalBiayaAdmin = final[0].totalBiayaAdmin + doc.totalBiayaAdmin;
          final[0].totalJual = final[0].totalJual + doc.totalJual;
          final[0].totalKomisi = final[0].totalKomisi + doc.totalKomisi;
          final[0].totalKursi = final[0].totalKursi + doc.totalKursi;
          final[0].setoran = final[0].setoran + doc.setoran;
        });
        data1 = $filter('orderBy')(data1, '-totalJual');
        final[0].transaksi = data1;
        return final;
      };


      return {
        getData : getData
      };
    }

})();
