(function () {
  angular.module('BlurAdmin.pages.akap')
  .controller('laporanPerTanggalCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $window, uiGridConstants, $uibModal, baConfig, salesByTrayekPrint, salesByTrayekChart, salesByTrayekData, salesByAgenData, salesByAgenChart, salesByAgenPrint, orderByAgenData, salesByAllTrayekData, salesByAllTrayekChart, salesByAllTrayekPrint, orderByAgenPrint, orderByAllAgenPrint) {

    $state.current.tag = 'E.1';

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    $scope.today = date.getTime();

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 10) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    var now = yyyy + '-' + bln + '-' + dd;
    $scope.now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);
    $scope.limit = now;

    $scope.loadData = false;
    $scope.exportPage = false;

    $scope.noData = {
      status : true,
      message : ""
    };

    $scope.getJamBerangkat = function (idTrayek) {
      $http.get(API_ENDPOINT.url + '/trayek/' + idTrayek + '/subtrayeks').success(function (data) {
        var jamBerangkat = ['Semua Keberangkatan'];
        // data.forEach(function (item) {
        //   jamBerangkat.push(item.tempatberangkat[0].waktutiba);
        // });
        $scope.listJamBerangkat =  jamBerangkat;
        if ($scope.listJamBerangkat.length === 2) {
          $scope.jamBerangkat = $scope.listJamBerangkat[1];
        }
      });
    };

    // $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/trayeks')
    // .success(function(data){
    //   // $scope.trayeeks = [];
    //   $scope.trayeks = [
    //     {
    //       namaChannel : "Semua Trayek",
    //       // value : '',
    //       idChannel : null
    //     }
    //   ];
    //   data.forEach(function(response){
    //     $scope.trayeks.push({
    //       idChannel: response.id,
    //       namaChannel: response.trayek
    //     })
    //   });
    //   // console.log(data);
    // });

    var getPage = function() {


      if (!$scope.tanggal || !$scope.transaksi || !$scope.jenisTransaksi) {
        alert('Semua data harus diisi!');
        return false;
      }

      $scope.exportPage = false;

      // var url = API_ENDPOINT.url +'/po/dashboard?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;
      // var url = API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;
      var datas;
      /**
        * Penjualan Per Trayek
        */
      if ($scope.transaksi == "penjualan" && $scope.jenisTransaksi == "trayek") {
          if (!$scope.trayek) {
            alert('Semua data harus diisi!');
            return false;
          }
          if ($scope.trayek.name == 'Semua Trayek') {
            $scope.jamBerangkat = 'Semua Keberangkatan';
          }
          if (!$scope.jamBerangkat) {
            alert('Semua data harus diisi!');
            return false;
          }
          $scope.loadData = true;
            if ($scope.trayek.name === 'Semua Trayek') {

              $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal).success(function(data) {

                $scope.datas = salesByAllTrayekData.getData(data, $scope.tanggal).detail;
                // $scope.total = salesByAllTrayekData.getData(data, $scope.tanggal).total;
                $http.get(API_ENDPOINT.url +'/po/dashboard?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal).success(function(data) {
                  $scope.total = {
                    kursi : data.totalPenjualanHariIni.jmlKursiTerjual,
                    penjualan : data.totalPenjualanHariIni.jmlPenjualan,
                    komisiAgen  : data.totalPenjualanHariIni.komisiAgen,
                    biayaAdmin : data.totalPenjualanHariIni.biayaAdmin,
                    setoran : data.totalPenjualanHariIni.setoran
                  }
                });
                salesByAllTrayekChart.barChart($scope.datas);

                $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/penjualan/salesByAllTrayek/salesByAllTrayek.header.html";
                $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/penjualan/salesByAllTrayek/salesByAllTrayek.template.html";
                $scope.loadData = false;

                $scope.exportPdf = function () {
                  salesByAllTrayekPrint.exportPdf($scope.datas, $scope.tanggal);
                };

                $scope.exportXls = function(){
                  $scope.exportPage = true;
                  var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                  });
                  saveAs(blob, "Laporan penjualan trayek " + $scope.tanggal + ".xls");
                  $scope.exportPage = false;
                };

              });

            } else {

              $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal).success(function(data) {

                $scope.datas = salesByTrayekData.getData(data, $scope.trayek.id, $scope.jamBerangkat, $scope.tanggal);
                // console.log($scope.trayek.id);
                salesByTrayekChart.barChart($scope.datas[0].transaksi);

                $scope.exportPdf = function () {
                  salesByTrayekPrint.exportPdf($scope.datas, $scope.tanggal, $scope.jamBerangkat);
                };

                $scope.exportXls = function(){
                  $scope.exportPage = true;
                  var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                  });
                  saveAs(blob, "Laporan penjualan trayek " + $scope.tanggal + ".xls");
                  $scope.exportPage = false;
                };

                $scope.next = function (trayek) {
                  // $state.go('dashboard.transaksi.setoran', {
                  $state.go('dashboard.akap.transaksi.setoran', {
                    query : {
                      tgl : $scope.tanggal,
                      transaksi : 'penjualan',
                      trayek : trayek,
                    }
                  });
                };

                $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/penjualan/salesByTrayek/salesByTrayek.header.html";
                $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/penjualan/salesByTrayek/salesByTrayek.template.html";
                $scope.loadData = false;

              });
            }
      } else
      /**
        * Penjualan Per Agen
        */
      if ($scope.transaksi == "penjualan" && $scope.jenisTransaksi == "agen") {
        if (!$scope.agen) {
          alert('Semua data harus diisi!');
          return false;
        }
        $scope.loadData = true;
        $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal).success(function(data) {
          // console.log($scope.agen)
          $scope.datas = salesByAgenData.getData(data, $scope.agen);
          $scope.loadData = false;

          salesByAgenChart.barChart($scope.datas[0].transaksi);

          $scope.exportPdf = function () {
            salesByAgenPrint.exportPdf($scope.datas, $scope.tanggal);
          };

          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan penjualan agen " + $scope.tanggal + ".xls");
            $scope.exportPage = false;
          };

          $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/penjualan/salesByAgen/salesByAgen.template.html";
          $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/penjualan/salesByAgen/salesByAgen.header.html";
        });
      } else
      /**
        * Pemesanan Per Agen
        */
      if ($scope.transaksi == "pemesanan" && $scope.jenisTransaksi == "agen") {
        if (!$scope.agen) {
          alert('Semua data harus diisi!');
          return false;
        }
        $scope.loadData = true;
        // console.log($scope.agen);
        // $http.get(url).success(function (data) {
        $http.get(API_ENDPOINT.url + '/po/setoran/pemesanan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal).success(function(data) {
          if ($scope.agen.name === "Semua Agen") {
            $scope.datas = data;
            $scope.loadData = false;

            $scope.toAgenOrder = function (agen) {
              $scope.agen = {
                name : agen.namaAgen,
                value : agen.namaAgen,
                id : agen.idAgen
              };
              $scope.tampil();
            };

            $scope.exportPdf = function () {
              orderByAllAgenPrint.exportPdf($scope.datas, $scope.tanggal);
            };

            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan tiket " + $scope.tanggal + ".xls");
              $scope.exportPage = false;
            };

            $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/pemesanan/orderByAllAgen/orderByAllAgen.template.html";
            $timeout(function () {
              $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/pemesanan/orderByAllAgen/orderByAllAgen.header.html";
            }, 200);
          }
          else {
            $scope.datas = orderByAgenData.getData(data, $scope.agen);
            $scope.loadData = false;
            $scope.exportPdf = function () {
              orderByAgenPrint.exportPdf($scope.datas, $scope.tanggal);
            };

            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan tiket " + $scope.tanggal + ".xls");
              $scope.exportPage = false;
            };

            $scope.showPassengerData = function (info, data, isPassangerDatas) {
              $uibModal.open({
                animation: true,
                templateUrl: '/app/pages/akap/laporan/perTanggal/modals/modal.main.html',
                controller: 'buyerDatasCtrl',
                size: 'md',
                resolve: {
                  items: function () {
                    return {
                      isPassangerDatas : isPassangerDatas,
                      data : data,
                      info : info,
                    };
                  }
                }
              });
            };

            $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/pemesanan/orderByAgen/orderByAgen.template.html";
            $timeout(function () {
              $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/pemesanan/orderByAgen/orderByAgen.header.html";
            }, 200);
          }
        });
      } else
      // pemesanan otobus berdasarkan agen
      if ($scope.transaksi == "pemesanan_otobus" && $scope.jenisTransaksi == "agen") {
        if (!$scope.agen) {
          alert('Semua data harus diisi!');
          return false;
        }
        $scope.loadData = true;

        $http.get(API_ENDPOINT.url + 'po/laporan/otobus/agen/tgl?idPo=' + $rootScope.user.idPo + '&idagen=' + $scope.agen.id + '&tgl=' + $scope.tanggal).success(function(data) {
          if ($scope.agen.name === "Semua Agen") {
            $scope.datas = data;
            $scope.loadData = false;

            $scope.exportPdf = function () {
              orderByAllAgenPrint.exportPdf($scope.datas, $scope.tanggal);
            };

            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan tiket otobus " + $scope.tanggal + ".xls");
              $scope.exportPage = false;
            };

            $scope.toAgenOrder = function (agen) {
              $scope.agen = {
                name : agen.namaAgen,
                value : agen.namaAgen,
                id : agen.idAgen
              };
              $scope.tampil();
            };

            $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/pemesananOtobus/agen/orderByAllAgen/orderByAllAgenOtobus.template.html";
            $timeout(function () {
              $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/pemesananOtobus/agen/orderByAllAgen/orderByAllAgenOtobus.header.html";
            }, 200);

          } else {
            $scope.datas = orderByAgenData.getData(data, $scope.agen);
            $scope.loadData = false;

            $scope.exportPdf = function () {
              orderByAgenPrint.exportPdf($scope.datas, $scope.tanggal);
            };

            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan tiket otobus" + $scope.tanggal + ".xls");
              $scope.exportPage = false;
            };

            $scope.showPassengerData = function (info, data, isPassangerDatas) {
              $uibModal.open({
                animation: true,
                templateUrl: '/app/pages/akap/laporan/perTanggal/modals/modal.main.html',
                controller: 'buyerDatasCtrl',
                size: 'md',
                resolve: {
                  items: function () {
                    return {
                      isPassangerDatas : isPassangerDatas,
                      data : data,
                      info : info,
                    };
                  }
                }
              });
            };

            $scope.templateUrl = "/app/pages/akap/laporan/perTanggal/pemesananOtobus/agen/orderByAgen/orderByAgenOtobus.template.html";
            $timeout(function () {
              $scope.headerUrl = "/app/pages/akap/laporan/perTanggal/pemesananOtobus/agen/orderByAgen/orderByAgenOtobus.header.html";
            }, 200);
          }
        });
      }
    };

    $scope.propertyName = '';
    $scope.reverse = false;

    $scope.sortBy = function(propertyName) {
      $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
    };

    $scope.tampil = function(tanggal) {
      $window.scroll(0,0);
      getPage();
    };

    $scope.toTrayekSales = function (trayek) {
      $scope.transaksi = "penjualan";
      $scope.jenisTransaksi = "trayek";
      $scope.trayek = {
        value : trayek.namaTrayek,
        name : trayek.namaTrayek,
        rute : trayek.rute,
        id : trayek.idTrayek
      };
      $scope.jamBerangkat = 'Semua Keberangkatan';
      $scope.tampil();
    };

    $scope.toAgenSales = function (agen) {
      // console.log(agen)
      $scope.transaksi = "penjualan";
      $scope.jenisTransaksi = "agen";
      $scope.agen = agen.profileAgen;
      $scope.tampil();
      // $scope.agen = '';
    };

    if ($state.params.tgl && $state.params.transaksi == 'penjualan') {
      $scope.tanggal = $state.params.tgl;
      $scope.transaksi = $state.params.transaksi;
      $scope.jenisTransaksi = $state.params.jenisTransaksi;
      $scope.jamBerangkat = 'Semua Keberangkatan';
      $scope.tanggal = $state.params.tgl;
      $scope.trayek = {
        value : $state.params.trayek,
        name : $state.params.trayek,
        id : $state.params.idTrayek,
        rute : $state.params.rute,
      };
      $scope.trayek.name = $state.params.trayek;
      $scope.getJamBerangkat($state.params.idTrayek);
      $scope.tampil();
    } else
      if ($state.params.tgl && $state.params.transaksi == 'pemesanan') {
      $scope.tanggal = $state.params.tgl;
      $scope.jenisTransaksi = 'agen';
      $scope.transaksi = $state.params.transaksi;
      $scope.agen = {
        value : $state.params.agen.namaAgen,
        name : $state.params.agen.namaAgen,
        id : $state.params.agen.idAgen
      };
      $scope.tampil();
    } else
      if($state.params.tgl && $state.params.transaksi == 'pemesanan_otobus') {
      $scope.tanggal = $state.params.tgl;
      $scope.jenisTransaksi = 'agen';
      $scope.transaksi = $state.params.transaksi;
      $scope.agen = {
        value : $state.params.agen.namaAgen,
        name : $state.params.agen.namaAgen,
        id : $state.params.agen.idAgen
      };
      $scope.tampil();
    }

    $scope.showPassengerData = function (info, data, isPassangerDatas) {
      $uibModal.open({
        animation: true,
        // templateUrl: '/app/pages/laporan/modals/modal.main.html',
        templateUrl: '/app/pages/akap/laporan/perTanggal/modals/modal.main.html',
        controller: 'passangerDatasCtrl',
        size: 'md',
        resolve: {
          items: function () {
            return {
              isPassangerDatas : isPassangerDatas,
              data : data,
              info : info,
              tglBerangkat : $scope.tanggal
            };
          }
        }
      });
    };

  });
})();
