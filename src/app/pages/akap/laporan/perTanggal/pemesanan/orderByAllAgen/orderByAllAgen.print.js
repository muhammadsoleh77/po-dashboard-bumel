(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('orderByAllAgenPrint', orderByAllAgenPrint);

    function orderByAllAgenPrint($rootScope, $filter) {

      var exportPdf = function(datas, tanggal){
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Pemesanan Tiket',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        docDefinition.content.push({
          columns : [
            {
              width: 70,
              text: 'Agen',
              bold: true,
            },
            {
              width: 'auto',
              text : ': ' + 'Semua Agen',
            }
          ]
        });
        var table = {
          color: '#444',
          margin: [0, 5, 0, 10],
          table: {
            widths: [120,'*','*','*','*','*','*'],
            headerRows: 2,
            fontSize : 8,
            body: [
              [
                {text: 'Agen Penjual', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Kursi Terpesan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Penjualan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Komisi Agen', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Setoran', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Admin Sistem', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Pendapatan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {},
                {},
                {}
              ]
            ]
          }
        };

        datas.forEach(function (doc) {
          table.table.body.push(
          [
            doc.namaAgen,
            { text : doc.kursiTerpesan, alignment : 'right' },
            { text : $filter('currency')(doc.penjualan, '', 0), alignment : 'right' },
            { text : $filter('currency')(doc.komisiAgen, '', 0), alignment : 'right' },
            { text : $filter('currency')(doc.setoran, '', 0), alignment : 'right' },
            { text : $filter('currency')(doc.biayaAdmin, '', 0), alignment : 'right' },
            { text : $filter('currency')((doc.setoran - doc.biayaAdmin), '', 0), alignment : 'right' },
          ]
        );
        });

        docDefinition.content.push(table);
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
