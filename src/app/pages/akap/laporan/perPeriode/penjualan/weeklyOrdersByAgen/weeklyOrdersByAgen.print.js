(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('weeklyOrdersByAgenPrint', weeklyOrdersByAgenPrint);

    function weeklyOrdersByAgenPrint ($rootScope, $filter) {

      var exportPdf = function(datas, weekDates, tglAwal ,tglAkhir, agen, isPemesanan){
        isPemesanan = true;
        datas = $filter('orderBy')(datas, '-jmlPenumpang');
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  (isPemesanan ? 'Data Pemesanan Tiket' : 'Data Penjualan Tiket'),
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tglAwal) + ' - ' + $filter('date_v1')(tglAkhir),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        var table = {
          color: '#444',
          margin: [0, 5, 0, 10],
          table: {
            widths: [10,100,'*','*','*','*','*','*','*'],
            headerRows: 2,
            fontSize : 8,
            body: [
              [
                {text: 'No.', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Agen', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              ],
              [
                // {},
                // {},
                // {},
                // {},
                // {},
                // {},
                // {},
                // {},
                // {}
              ]
            ]
          }
        };

        weekDates.forEach(function (doc) {
          table.table.body[0].push(
            {text: $filter('date')(doc, 'dd/MM/yyyy'), style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2}
          );
        });

        datas.forEach(function (doc, i) {
          var tmp = [
            {text : i+1, alignment : 'right', fontSize : 8},
            {text : doc.namaAgen, fontSize : 8}
          ];
          // console.log(tmp);
          doc.detail.forEach(function (doc2) {
            tmp.push(
              {text : doc2.penumpang, alignment : 'right', fillColor : doc2.penumpang === 0 ? '#adadad' : '', bold : true}
            );
          });
          // console.log(tmp);
          table.table.body.push(tmp);
        });
        docDefinition.content.push(table);
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
