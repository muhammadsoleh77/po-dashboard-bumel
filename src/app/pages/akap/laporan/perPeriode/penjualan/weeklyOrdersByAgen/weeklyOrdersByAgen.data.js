(function () {

    angular
      .module('BlurAdmin.pages.akap')
      .service('weeklyOrdersByAgenData', weeklyOrdersByAgenData);

      function weeklyOrdersByAgenData($rootScope, $filter) {

        var getData = function (data, dates, agen, isPemesanan) {
          isPemesanan = true;
          var getNamaPemesanan = function (idPemesan) {
            for (var i = 0; i < $rootScope.agens.length; i++) {
              if ($rootScope.agens[i].id === idPemesan) {
                idPemesan = $rootScope.agens[i].name;

              }
            }
            return idPemesan;
          };

          if(agen.id === 0) {

            var dataTmp;
            var final = [];
            var dataPenjualan1 = [];
            data.periode.forEach(function(doc) {
              doc.listPeriode.forEach(function(doc2) {
                dataPenjualan1.push({
                  tglTrans : doc.tglTrans,
                  penumpang : doc2.penumpang,
                  idagen : doc2.idagen
                });
              });
            });

            for (var i = 0; i < $rootScope.listAgens.length; i++) {
              var datas = [];
              for (var j = 0; j < dates.length; j++) {
                var step1 = [];
                penumpang = 0;
                dataTmp = $filter('filter')(dataPenjualan1, {idagen : $rootScope.listAgens[i].id}, true);
                dataTmp = $filter('filter')(dataTmp, {tglTrans : dates[j]}, true);
                // dataTmp = $filter('filter')(dataTmp, {idTrayek : $rootScope.lisTrayeks[k].id}, true);
                for (var m = 0; m < dataTmp.length; m++) {
                  penumpang = penumpang + dataTmp[m].penumpang;
                  // console.log(penumpang);
                }
                step1.push({
                  // namaTrayek : $rootScope.lisTrayeks[k].value,
                  // id : $rootScope.lisTrayeks[k].id,
                  date : dates[j],
                  penumpang : penumpang
                });
              // return false;

              penumpang = 0;
              for (var n = 0; n < step1.length; n++) {
                penumpang = penumpang + step1[n].penumpang;
              }

                datas.unshift({
                  date : dates[j],
                  ms : (new Date(dates[j])).getTime(),
                  penumpang : penumpang,
                  detail : step1
                });

                // datas.push({
                //   date : dates[j],
                //   ms : (new Date(dates[j])).getTime(),
                //   penumpang : penumpang,
                //   detail : step1
                // });
            }
            penumpang = 0;
            for (var p = 0; p < datas.length; p++) {
              penumpang = penumpang + datas[p].penumpang;
            }
            final.push({
              namaAgen : $rootScope.listAgens[i].value,
              id : $rootScope.listAgens[i].id,
              penumpang : penumpang,
              detail : datas
            });
          };

          } else {

            var datas = [];
            var step1 = [];
            var final = [];
            var dataPenjualan2 = [];
            var penumpang = 0;
              data.periode.forEach(function (doc) {
                dataPenjualan2.push({
                  tglTrans : doc.tglTrans,
                  penumpang : doc.penumpang,
                  idagen : doc.idagen
                });
                penumpang = penumpang + doc.penumpang
              });

              for (var j = 0; j < dates.length; j++) {
                penumpang = 0;
                dataTmp = $filter('filter')(dataPenjualan2, {tglTrans : dates[j]}, true);
                for (var m = 0; m < dataTmp.length; m++) {
                  penumpang = penumpang + dataTmp[m].penumpang;
                }
                step1.push({
                  penumpang : penumpang
                });

              datas.unshift({
                date : dates[j],
                ms : (new Date(dates[j])).getTime(),
                penumpang : penumpang,
                detail : step1
              });

              // datas.push({
              //   date : dates[j],
              //   ms : (new Date(dates[j])).getTime(),
              //   penumpang : penumpang,
              //   detail : step1
              // });
              }

              final.push({
                namaAgen : getNamaPemesanan(agen.id),
                detail : datas
              });
          }
          // if (agen && agen.id !== 0) {
          //   final = $filter('filter')(final, {id : agen.id}, true);
          // }
        return final;
      };
      return {
        getData : getData
      };
  }


})();
