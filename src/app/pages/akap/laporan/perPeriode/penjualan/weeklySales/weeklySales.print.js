(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('weeklySalesPrint', weeklySalesPrint);

    function weeklySalesPrint ($rootScope, $filter) {

      var exportPdf = function(datas, total, tglAwal ,tglAkhir, trayek, isPemesanan){
        console.log(datas);
        datas = $filter('orderBy')(datas, '-totalJual');
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  (isPemesanan ? 'Data Pemesanan Tiket' : 'Data Penjualan Tiket'),
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tglAwal) + ' - ' + $filter('date_v1')(tglAkhir),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        docDefinition.content.push({
          columns : [
            {
              width: 70,
              text: 'Trayek',
              bold: true,
            },
            {
              width: 'auto',
              text : ': ' + (trayek ? trayek : 'Semua Trayek'),
            }
          ]
        });
        var table = {
          color: '#444',
          margin: [0, 5, 0, 10],
          table: {
            widths: [195,'*','*','*','*'],
            headerRows: 2,
            fontSize : 8,
            body: [
              [
                {text: 'Trayek', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Total Kursi Terjual', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Total Penjualan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Total Komisi Agen', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Total Biaya Admin', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {}
              ]
            ]
          }
        };

        datas.forEach(function (doc) {
          table.table.body.push(
            [
              doc.namaTrayek,
              { text : $filter('currency')(doc.totalKursi, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalJual, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalKomisi, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalBiayaAdmin, '', 0), alignment : 'right' },
            ]
          );
          console.log(table.table.body);
        });
        table.table.body.push(
          [
            { text : 'Total', alignment : 'center', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(total.totalKursi, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(total.totalJual, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(total.totalKomisi, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(total.totalBiayaAdmin, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
          ]
        );

        docDefinition.content.push(table);
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
