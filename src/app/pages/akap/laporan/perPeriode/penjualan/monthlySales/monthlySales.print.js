(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('monthlySalesPrint', monthlySalesPrint);

    function monthlySalesPrint ($rootScope, $filter) {

      var exportPdf = function (datas, agen, tglAwal, tglAkhir, isPemesanan) {
        datas = $filter('orderBy')(datas, '-totalJual');

        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths : ['*','*','*','*'],
              fontSize : 16,
              marginBottom : 5,
              bold : true,
              alignment : 'left',
              text : 'PO.'+ $rootScope.po.nama, style : 'header',
            },
            {
              text : (isPemesanan ? 'Data Pemesanan Tiket' : 'Data Penjualan Tiket'),
              marginBottom : 5,
              fontSize : 12,
              alignment : 'left',
            },
            {
              // text:  'Tanggal : ' + $filter('date_v1')(tglAwal) + ' - ' + $filter('date_v1')(tglAkhir),
              text:  'Tanggal : ',
              fontSize : 10,
              marginBottom : 20,
              alignment : 'left',
            }
          ]
        };
        docDefinition.content.push({
          columns : [
            {
              width : 70,
              text : 'Agen',
              bold : true,
            },
            {
              width : 'auto',
              text : ': ' + (agen ? agen : 'Semua Agen'),
            }
          ]
        });

        var table = {
          color : '#444',
          margin : [0, 5, 0, 10],
          table : {
            widths : [160,'*'],
            headerRows : 2,
            fontSize : 8,
            body : [
              [
                {text: 'Agen', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: $filter(date), style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                // {date: 'dd', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {}
              ]
            ]
          }
        };

        // datas.forEach(function (doc) {
        //   table.table.body.push(
        //     [
        //       doc.namaTempatBrgkt,
        //       { text : $filter('currency')(doc.totalKursi, '', 0), alignment : 'right' },
        //     ]
        //   );
        // });
        // table.table.body.push(
        //   [
        //     { text : 'Total', alignment : 'center', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //     { text : $filter('date_v1')(total.jmlPenumpang, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //   ]
        // );

        docDefinition.content.push(table);
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
