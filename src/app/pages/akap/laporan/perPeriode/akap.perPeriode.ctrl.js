(function () {
  angular.module('BlurAdmin.pages.akap')
  .controller('laporanPerPeriodeCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $window, uiGridConstants, $uibModal, baConfig, getDatePeriod, weeklySalesData, weeklySalesChart, trayekWeeklySalesChart, chartColors, weeklySalesPrint, weeklySalesByAgenData, weeklySalesByAgenPrint, weeklyOrdersByAgenData, weeklyOrdersByAgenPrint, monthlySalesPrint, otobusWeeklySalesData) {

    $state.current.tag = 'E.2';
    $scope.exportPage = false;

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    $scope.today = date.getTime();
    $scope.months = getDatePeriod.months();
    $scope.years = getDatePeriod.years();

    $scope.tahun = new Date().getFullYear();


    var bln, tgl, tglAwal, tglAkhir;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 10) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    var now = yyyy + '-' + bln + '-' + dd;
    $scope.now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);
    $scope.limit = now;
    var getPage = function () {

      if (!$scope.transaksi || !$scope.periode || !$scope.jenisTransaksi) {
        alert("Semua data harus diisi!");
        return false;
      }

      var url;
      $scope.chartTemplate = "";
      $scope.headerUrl = "";
      $scope.templateUrl = "";


      if ($scope.transaksi === 'penjualan' && $scope.periode === 'weekly' && $scope.jenisTransaksi === 'trayek') {

        if (!$scope.startDate || !$scope.trayek) {
          alert("Semua data harus diisi!");
          return false;
        }

        $scope.loadData = true;
        $scope.weekDate = getDatePeriod.getWeekDate($scope.startDate, -1);

        $scope.tglAwal = $scope.weekDate[$scope.weekDate.length-1];
        $scope.tglAkhir = $scope.weekDate[0];

        if ($scope.trayek.id !== null) {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&idtrayek=' + $scope.trayek.id;
        } else {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
        }

        $http.get(url).success(function (data) {
          $scope.formatData = function (input) {
            return $filter('currency')(input, 'Rp.', 0);
          };
          $scope.formatDate = function (x) {
            var result = $filter('date')(x, 'dd MMM');
            return result;
          };
          $scope.formatLabel = function (x) {
            var result = $filter('date_v4')(x);
            return result;
          };
          var total = weeklySalesData.getData(data, $scope.weekDate, $scope.trayek).totalTransaksi;
          if ($scope.trayek.id !== null) {
            $scope.data = $filter('filter')(total, {idTrayek: $scope.trayek.id}, true);
          } else {
            $scope.data = total;
          }
          $scope.datas = weeklySalesData.getData(data, $scope.weekDate, $scope.trayek).transaksi;
          $scope.grandTotal = weeklySalesData.getData(data, $scope.weekDate, $scope.trayek).grandTotal;
          $scope.donutData = weeklySalesChart.donutData(total);
          if ($scope.trayek.id !== null) {
            $scope.donutData = $filter('filter')($scope.donutData, {idTrayek: $scope.trayek.id}, true);
          }
          $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
          $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
          $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
          $scope.colors = chartColors;
          $timeout(function () {
            $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.template.html";
            $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.header.html";
            $scope.chartTemplate = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.chart.template.html";
          }, 100);

          $scope.exportPdf = function () {
            weeklySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir,$scope.trayek.value);
          };

          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan penjualan trayek " + $scope.tglAwal  + ' - ' + $scope.tglAwal + ".xls");
            $scope.exportPage = false;
          };

          $scope.loadData = false;
        });
      } else

      if ($scope.transaksi === 'penjualan' && $scope.periode === 'monthly' && $scope.jenisTransaksi === 'trayek') {

        if (!$scope.bulan || !$scope.trayek) {
          alert("Semua data harus diisi!");
          return false;
        }
        $scope.loadData = true;

        $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
        tglAwal = $scope.monthDate[0].split("-");
        tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

        tglAwal[0] = $scope.tahun;
        tglAkhir[0] = $scope.tahun;

        $scope.tglAwal = tglAwal.join("-");
        $scope.tglAkhir = tglAkhir.join("-");

        if ($scope.trayek.id !== null) {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&idtrayek=' + $scope.trayek.id;
        } else {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
        }

        $http.get(url).success(function (data) {
          $scope.formatData = function (input) {
            return $filter('currency')(input, 'Rp.', 0);
          };
          $scope.formatDate = function (x) {
            var result = $filter('date')(x, 'dd MMM');
            return result;
          };
          $scope.formatLabel = function (x) {
            var result = $filter('date_v4')(x);
            return result;
          };

          var total = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek).totalTransaksi;

          if ($scope.trayek.id !== null) {
            $scope.data = $filter('filter')(total, {idTrayek: $scope.trayek.id}, true);
          } else {
            $scope.data = total;
          }
          $scope.datas = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek).transaksi;
          $scope.grandTotal = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek).grandTotal;
          $scope.donutData = weeklySalesChart.donutData(total);
          if ($scope.trayek.id !== null) {
            $scope.donutData = $filter('filter')($scope.donutData, {idTrayek: $scope.trayek.id}, true);
          }
          $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
          $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
          $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
          $scope.colors = chartColors;
          $timeout(function () {
            $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.template.html";
            $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.header.html";
            $scope.chartTemplate = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.chart.template.html";
          }, 100);
          $scope.exportPdf = function () {
            weeklySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek.value);
          };
          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan penjualan trayek " + $scope.tglAwal  + ' - ' + $scope.tglAwal + ".xls");
            $scope.exportPage = false;
          };
          $scope.loadData = false;
        });
      } else
      if ($scope.transaksi === 'pemesanan' && $scope.periode === 'weekly' && $scope.jenisTransaksi === 'trayek') {

        $scope.periode = 'weekly';

        if (!$scope.startDate || !$scope.trayek) {
          alert("Semua data harus diisi!");
          return false;
        }
        $scope.loadData = true;

        $scope.weekDate = getDatePeriod.getWeekDate($scope.startDate, 1);

        $scope.tglAwal = $scope.weekDate[0];
        $scope.tglAkhir = $scope.weekDate[$scope.weekDate.length-1];

        if ($scope.trayek.id !== null) {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&idtrayek=' + $scope.trayek.id;
        } else {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
        }


        $http.get(url).success(function (data) {
          $scope.formatData = function (input) {
            return $filter('currency')(input, 'Rp.', 0);
          };
          $scope.formatDate = function (x) {
            var result = $filter('date')(x, 'dd MMM');
            return result;
          };
          $scope.formatLabel = function (x) {
            var result = $filter('date_v4')(x);
            return result;
          };

          var total = weeklySalesData.getData(data, $scope.weekDate, $scope.trayek, true).totalTransaksi;

          if ($scope.trayek.id !== null) {
            $scope.data = $filter('filter')(total, {idTrayek: $scope.trayek.id}, true);
          } else {
            $scope.data = total;
          }
          $scope.datas = weeklySalesData.getData(data, $scope.weekDate, $scope.trayek, true).transaksi;
          $scope.grandTotal = weeklySalesData.getData(data, $scope.weekDate, $scope.trayek, true).grandTotal;
          $scope.donutData = weeklySalesChart.donutData(total);
          if ($scope.trayek.id !== null) {
            $scope.donutData = $filter('filter')($scope.donutData, {idTrayek: $scope.trayek.id});
          }
          // $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
          // $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
          // $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
          $scope.lineData = trayekWeeklySalesChart.chartData($scope.datas).lineData;
          $scope.lineKey = trayekWeeklySalesChart.chartData($scope.datas).lineKey;
          $scope.lineLabel = trayekWeeklySalesChart.chartData($scope.datas).lineLabel;
          $scope.colors = chartColors;
          $timeout(function () {
            $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.template.html";
            $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.header.html";
            $scope.chartTemplate = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.chart.template.html";
          }, 100);
          $scope.exportPdf = function () {
            weeklySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek.value, true);
          };
          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan pemesanan trayek " + $scope.tglAwal  + ' - ' + $scope.tglAwal + ".xls");
            $scope.exportPage = false;
          };
          $scope.loadData = false;
        });
      } else
      if ($scope.transaksi === 'pemesanan' && $scope.periode === 'monthly' && $scope.jenisTransaksi === 'trayek') {

        if (!$scope.bulan || !$scope.trayek) {
          alert("Semua data harus diisi!");
          return false;
        }
        $scope.loadData = true;

        $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
        tglAwal = $scope.monthDate[0].split("-");
        tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

        tglAwal[0] = $scope.tahun;
        tglAkhir[0] = $scope.tahun;

        $scope.tglAwal = tglAwal.join("-");
        $scope.tglAkhir = tglAkhir.join("-");

        if ($scope.trayek.id !== null) {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&idtrayek=' + $scope.trayek.id;
        } else {
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
        }

        $http.get(url).success(function (data) {
          $scope.formatData = function (input) {
            return $filter('currency')(input, 'Rp.', 0);
          };
          $scope.formatDate = function (x) {
            var result = $filter('date')(x, 'dd MMM');
            return result;
          };
          $scope.formatLabel = function (x) {
            var result = $filter('date_v4')(x);
            return result;
          };

          var total = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).totalTransaksi;

          if ($scope.trayek.id !== null) {
            $scope.data = $filter('filter')(total, {idTrayek: $scope.trayek.id}, true);
          } else {
            $scope.data = total;
          }
          $scope.datas = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).transaksi;
          $scope.grandTotal = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).grandTotal;
          $scope.donutData = weeklySalesChart.donutData(total);
          if ($scope.trayek.id !== null) {
            $scope.donutData = $filter('filter')($scope.donutData, {idTrayek: $scope.trayek.id}, true);
          }

          $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
          $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
          $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
          $scope.colors = chartColors;
          $timeout(function () {
            $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.template.html";
            $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.header.html";
            $scope.chartTemplate = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.chart.template.html";
          }, 100);
          $scope.exportPdf = function () {
            weeklySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek.value, true);
          };
          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan pemesanan trayek " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
            $scope.exportPage = false;
          };
          $scope.loadData = false;
        });
      } else
        if ($scope.transaksi === 'penjualan' && $scope.periode === 'weekly' && $scope.jenisTransaksi === 'agen') {
          if (!$scope.startDate || !$scope.agen) {
            alert("Semua data harus diisi!");
            return false;
          }

          $scope.loadData = true;
          $scope.weekDate = getDatePeriod.getWeekDate($scope.startDate, -1);
          $scope.weekDateMs = [];
          $scope.weekDate.forEach(function (d) {
            $scope.weekDateMs.unshift((new Date(d).getTime()));
          });
          $scope.tglAwal = $scope.weekDate[$scope.weekDate.length-1];
          $scope.tglAkhir = $scope.weekDate[0];
          // url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;


          $http.get(url).success(function (data) {
            $scope.datas = weeklySalesByAgenData.getData(data, $scope.weekDate, $scope.agen);
            $scope.exportPdf = function () {
              weeklySalesByAgenPrint.exportPdf($scope.datas, $scope.weekDateMs, $scope.tglAwal, $scope.tglAkhir, $scope.agen);
            };
            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan penjualan agen " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
              $scope.exportPage = false;
            };
            $scope.loadData = false;
            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySalesByAgen/weeklySalesPenjualanByAgen.template.html";
            }, 100);
          });
      } else
      if ($scope.transaksi === 'penjualan' && $scope.periode === 'monthly' && $scope.jenisTransaksi === 'agen') {
        if (!$scope.bulan || !$scope.agen) {
          alert("Semua data harus diisi!");
          return false;
        }

        $scope.loadData = true;
        $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
        $scope.monthDateMs = [];
        $scope.monthDate.forEach(function (d) {
          $scope.monthDateMs.push((new Date(d).getTime()));
        });
        // console.log($scope.monthDate);
        tglAwal = $scope.monthDate[0].split("-");
        tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

        tglAwal[0] = $scope.tahun;
        tglAkhir[0] = $scope.tahun;

        $scope.tglAwal = tglAwal.join("-");
        $scope.tglAkhir = tglAkhir.join("-");

        url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
        // console.log(url);

        $http.get(url).success(function (data) {
          $scope.formatData = function (input) {
            return $filter('currency')(input, 'Rp.', 0);
          };
          $scope.formatDate = function (x) {
            var result = $filter('date')(x, 'dd MMM');
            return result;
          };
          $scope.formatLabel = function (x) {
            var result = $filter('date_v4')(x);
            return result;
          };

          var total = weeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).totalTransaksi;

          $scope.datas = weeklySalesByAgenData.getData(data, $scope.monthDate, $scope.agen);
          $scope.exportPdf = function () {
            // monthlySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek.value, true);
            // monthlySalesPrint.exportPdf($scope.data, $scope.tglAwal, $scope.tglAkhir, $scope.agen);
            // alert('maaf... laporan penjualan agen per bulan tidak mendukung pdf');
          };
          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan penjualan agen " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
            $scope.exportPage = false;
          };
          $scope.loadData = false;
          $timeout(function () {
            $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/monthlySales/penjualanMonthlySalesByAgen.template.html";
            $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/monthlySales/monthlySales.header.html";
          }, 100);
        });
      }else
        if ($scope.transaksi === 'pemesanan' && $scope.periode === 'weekly' && $scope.jenisTransaksi === 'agen') {
          if (!$scope.startDate || !$scope.agen) {
            alert("Semua data harus diisi!");
            return false;
          }

          $scope.loadData = true;
          $scope.weekDate = getDatePeriod.getWeekDate($scope.startDate, -1);
          $scope.weekDateMs = [];
          $scope.weekDate.forEach(function (d) {
            $scope.weekDateMs.unshift((new Date(d).getTime()));
          });
          $scope.tglAwal = $scope.weekDate[$scope.weekDate.length-1];
          $scope.tglAkhir = $scope.weekDate[0];
          // $scope.tglAkhir = $scope.weekDate[$scope.weekDate.length-1];
          // $scope.tglAwal = $scope.weekDate[0];
          url = API_ENDPOINT.url +'/po/pemesananperiode?idPo=' + $rootScope.po.idPo + '&idagen=' + $scope.agen.id + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          // url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;


          $http.get(url).success(function (data) {
            $scope.datas = weeklyOrdersByAgenData.getData(data, $scope.weekDate, $scope.agen);
            $scope.exportPdf = function () {
              weeklyOrdersByAgenPrint.exportPdf($scope.datas, $scope.weekDateMs, $scope.tglAwal, $scope.tglAkhir, $scope.agen);
            };
            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan agen " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
              $scope.exportPage = false;
            };
            $scope.loadData = false;
            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySalesByAgen/weeklySalesPemesananByAgen.template.html";
            }, 100);
          });
      }else
        if ($scope.transaksi === 'pemesanan' && $scope.periode === 'monthly' && $scope.jenisTransaksi === 'agen') {
          if (!$scope.bulan || !$scope.agen) {
            alert("Semua data harus diisi!");
            return false;
          }

          $scope.loadData = true;
          $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
          $scope.monthDateMs = [];
          $scope.monthDate.forEach(function (d) {
            // $scope.monthDateMs.push((new Date(d).getTime()));
            $scope.monthDateMs.unshift((new Date(d).getTime()));
          });

          $scope.tglAwal = $scope.monthDate[0];
          $scope.tglAkhir = $scope.monthDate[$scope.monthDate.length-1];

          // url = API_ENDPOINT.url +'/po/pemesananperiode?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          url = API_ENDPOINT.url +'/po/pemesananperiode?idPo=' + $rootScope.po.idPo + '&idagen=' + $scope.agen.id + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          // console.log(url);
          // console.log($scope.monthDate);
          $http.get(url).success(function (data) {

            $scope.datas = weeklyOrdersByAgenData.getData(data, $scope.monthDate, $scope.agen);
            $scope.exportPdf = function () {
              weeklySalesPrint.exportPdf($scope.datas, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek, true);
              // alert('Maaf... laporan pemesanan agen per bulan tidak mendukung pdf');
            };
            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan agen " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
              $scope.exportPage = false;
            };
            $scope.loadData = false;
            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/monthlySales/pemesananMonthlySalesByAgen.template.html";
              $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/monthlySales/monthlySales.header.html";
            }, 100);
          });
        }

        // pemesanan otobus agen
        else if($scope.transaksi === 'pemesanan_otobus' && $scope.periode === 'weekly' && $scope.jenisTransaksi === 'agen'){
          if (!$scope.startDate || !$scope.agen) {
            alert("Semua data harus diisi!");
            return false;
          }

          $scope.loadData = true;
          $scope.weekDate = getDatePeriod.getWeekDate($scope.startDate, -1);
          $scope.weekDateMs = [];
          $scope.weekDate.forEach(function (d) {
            $scope.weekDateMs.unshift((new Date(d).getTime()));
          });
          $scope.tglAwal = $scope.weekDate[$scope.weekDate.length-1];
          $scope.tglAkhir = $scope.weekDate[0];

          url = API_ENDPOINT.url +'/po/laporan/otobus/agen/periode?idPo=' + $rootScope.po.idPo + '&idagen=' + $scope.agen.id + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;

          $http.get(url).success(function(data) {
            $scope.datas = weeklyOrdersByAgenData.getData(data, $scope.weekDate, $scope.agen);
            $scope.exportPdf = function () {
              weeklyOrdersByAgenPrint.exportPdf($scope.datas, $scope.weekDateMs, $scope.tglAwal, $scope.tglAkhir, $scope.agen);
            };
            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan otobus agen " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
              $scope.exportPage = false;
            };
            $scope.loadData = false;

            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/pemesananOtobus/pemesananOtobusAgenWeekly/weeklySalesPemesananByAgen.template.html";
            }, 100);
          })
        }
        else if($scope.transaksi === 'pemesanan_otobus' && $scope.periode === 'monthly' && $scope.jenisTransaksi === 'agen'){
          if (!$scope.bulan || !$scope.agen) {
            alert("Semua data harus diisi!");
            return false;
          }

          $scope.loadData = true;
          $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
          $scope.monthDateMs = [];
          $scope.monthDate.forEach(function (d) {
            // $scope.monthDateMs.push((new Date(d).getTime()));
            $scope.monthDateMs.unshift((new Date(d).getTime()));
          });

          $scope.tglAwal = $scope.monthDate[0];
          $scope.tglAkhir = $scope.monthDate[$scope.monthDate.length-1];

          url = API_ENDPOINT.url +'/po/laporan/otobus/agen/periode?idPo=' + $rootScope.po.idPo + '&idagen=' + $scope.agen.id + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;

          $http.get(url).success(function(data) {
            $scope.datas = weeklyOrdersByAgenData.getData(data, $scope.monthDate, $scope.agen);

            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan otobus agen" + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
              $scope.exportPage = false;
            };
            $scope.loadData = false;

            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/pemesananOtobus/pemesananOtobusAgenMonthly/monthlySalesPemesananByAgen.template.html";
              $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/pemesananOtobus/pemesananOtobusAgenMonthly/monthlySalesPemesananByAgen.header.html";
            }, 100);
          });
        }

        // pemesanan otobus trayek
        else if($scope.transaksi === 'pemesanan_otobus' && $scope.periode === 'weekly' && $scope.jenisTransaksi === 'trayek'){
          $scope.periode = 'weekly';

          if (!$scope.startDate || !$scope.trayek) {
            alert("Semua data harus diisi!");
            return false;
          }
          $scope.loadData = true;

          $scope.weekDate = getDatePeriod.getWeekDate($scope.startDate, 1);

          $scope.tglAwal = $scope.weekDate[0];
          $scope.tglAkhir = $scope.weekDate[$scope.weekDate.length-1];

          if ($scope.trayek.id !== null) {
            url = API_ENDPOINT.url +'/po/laporan/otobus/trayek/periode?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&idtrayek=' + $scope.trayek.id;
          } else {
            url = API_ENDPOINT.url +'/po/laporan/otobus/trayek/periode?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          }

          $http.get(url).success(function(data) {
            $scope.formatData = function (input) {
              return $filter('currency')(input, 'Rp.', 0);
            };
            $scope.formatDate = function (x) {
              var result = $filter('date')(x, 'dd MMM');
              return result;
            };
            $scope.formatLabel = function (x) {
              var result = $filter('date_v4')(x);
              return result;
            };

            var total = otobusWeeklySalesData.getData(data, $scope.weekDate, $scope.trayek, true).totalTransaksi;

            if ($scope.trayek.id !== null) {
              $scope.data = $filter('filter')(total, {idTrayek: $scope.trayek.id}, true);
            } else {
              $scope.data = total;
            }
            $scope.datas = otobusWeeklySalesData.getData(data, $scope.weekDate, $scope.trayek, true).transaksi;
            $scope.grandTotal = otobusWeeklySalesData.getData(data, $scope.weekDate, $scope.trayek, true).grandTotal;
            $scope.donutData = weeklySalesChart.donutData(total);
            if ($scope.trayek.id !== null) {
              $scope.donutData = $filter('filter')($scope.donutData, {idTrayek: $scope.trayek.id});
            }
            // $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
            // $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
            // $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
            $scope.lineData = trayekWeeklySalesChart.chartData($scope.datas).lineData;
            $scope.lineKey = trayekWeeklySalesChart.chartData($scope.datas).lineKey;
            $scope.lineLabel = trayekWeeklySalesChart.chartData($scope.datas).lineLabel;
            $scope.colors = chartColors;
            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/pemesananOtobus/pemesananOtobusTrayekWeekly/weeklySales.template.html";
              $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/pemesananOtobus/pemesananOtobusTrayekWeekly/weeklySales.header.html";
              $scope.chartTemplate = "/app/pages/akap/laporan/perPeriode/pemesananOtobus/pemesananOtobusTrayekWeekly/weeklySales.chart.template.html";
            }, 100);
            $scope.loadData = false;

            $scope.exportPdf = function () {
              weeklySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek.value, true);
            };
            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan otobus trayek " + $scope.tglAwal  + ' - ' + $scope.tglAwal + ".xls");
              $scope.exportPage = false;
            };
          });
        }

        else if($scope.transaksi === 'pemesanan_otobus' && $scope.periode === 'monthly' && $scope.jenisTransaksi === 'trayek'){
          if (!$scope.bulan || !$scope.trayek) {
            alert("Semua data harus diisi!");
            return false;
          }
          $scope.loadData = true;

          $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
          tglAwal = $scope.monthDate[0].split("-");
          tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

          tglAwal[0] = $scope.tahun;
          tglAkhir[0] = $scope.tahun;

          $scope.tglAwal = tglAwal.join("-");
          $scope.tglAkhir = tglAkhir.join("-");

          if ($scope.trayek.id !== null) {
            url = API_ENDPOINT.url +'/po/laporan/otobus/trayek/periode?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&idtrayek=' + $scope.trayek.id;
          } else {
            url = API_ENDPOINT.url +'/po/laporan/otobus/trayek/periode?idPo=' + $rootScope.po.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          }

          $http.get(url).success(function (data) {
            $scope.formatData = function (input) {
              return $filter('currency')(input, 'Rp.', 0);
            };
            $scope.formatDate = function (x) {
              var result = $filter('date')(x, 'dd MMM');
              return result;
            };
            $scope.formatLabel = function (x) {
              var result = $filter('date_v4')(x);
              return result;
            };

            var total = otobusWeeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).totalTransaksi;

            if ($scope.trayek.id !== null) {
              $scope.data = $filter('filter')(total, {idTrayek: $scope.trayek.id}, true);
            } else {
              $scope.data = total;
            }
            $scope.datas = otobusWeeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).transaksi;
            $scope.grandTotal = otobusWeeklySalesData.getData(data, $scope.monthDate, $scope.trayek, true).grandTotal;
            $scope.donutData = weeklySalesChart.donutData(total);
            if ($scope.trayek.id !== null) {
              $scope.donutData = $filter('filter')($scope.donutData, {idTrayek: $scope.trayek.id}, true);
            }

            $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
            $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
            $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
            $scope.colors = chartColors;
            $timeout(function () {
              $scope.templateUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.template.html";
              $scope.headerUrl = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.header.html";
              $scope.chartTemplate = "/app/pages/akap/laporan/perPeriode/penjualan/weeklySales/weeklySales.chart.template.html";
            }, 100);
            $scope.exportPdf = function () {
              weeklySalesPrint.exportPdf($scope.data, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.trayek.value, true);
            };
            $scope.exportXls = function(){
              $scope.exportPage = true;
              var blob = new Blob([document.getElementById('dataTable').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
              });
              saveAs(blob, "Laporan pemesanan trayek " + $scope.tglAwal  + ' - ' + $scope.tglAkhir + ".xls");
              $scope.exportPage = false;
            };
            $scope.loadData = false;
          });
        }
    };


    $scope.tampil = function () {
      getPage();
    };

    if ($state.params.tgl) {
      $scope.startDate = $state.params.tgl;
      $scope.transaksi = $state.params.transaksi;
      $scope.periode = 'weekly';
      $scope.jenisTransaksi = 'trayek';
      $scope.trayek = {
        value : '',
        name : 'Semua Trayek',
        id : null
      };
      $scope.tampil();
    }

    $scope.getPeriode = function () {
      if ($scope.jenisTransaksi === 'agen') {
        $scope.periode = 'weekly';
      }
    };

  });
})();
