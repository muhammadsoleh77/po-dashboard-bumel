(function () {
  angular.module('BlurAdmin.pages.akap')
  .controller('pembagianKomisiAgenCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $window, uiGridConstants, $uibModal, baConfig, getDatePeriod, weeklySalesChart, chartColors, komisiPrint) {

    $state.current.tag = 'E.7';
    $scope.exportPage = false;

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    $scope.today = date.getTime();
    $scope.months = getDatePeriod.months();
    $scope.years = getDatePeriod.years();

    $scope.tahun = new Date().getFullYear();


    var bln, tgl, tglAwal, tglAkhir;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 10) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    var now = yyyy + '-' + bln + '-' + dd;
    $scope.now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);
    $scope.limit = now;
    // var getPage = function () {
    $scope.tampil = function() {

      if (!$scope.bulan) {
        alert("Semua data harus diisi!");
        return false;
      }

      var url;
      $scope.chartTemplate = "";
      $scope.headerUrl = "";
      $scope.templateUrl = "";

      // if ($scope.status === '0' || $scope.status === '1') {

        if (!$scope.bulan || !$scope.status) {
          alert("Semua data harus diisi!");
          return false;
        }
        $scope.loadData = true;

        $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
        tglAwal = $scope.monthDate[0].split("-");
        tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

        tglAwal[0] = $scope.tahun;
        tglAkhir[0] = $scope.tahun;

        $scope.tglAwal = tglAwal.join("-");
        $scope.tglAkhir = tglAkhir.join("-");

        if ($scope.status == 0) {
          url = API_ENDPOINT.url +'po/transaksikomisiagen?idPo=' + $scope.user.idPo + '&tglAwal=' + $scope.tglAwal + '&tglAkhir=' + $scope.tglAkhir + '&status=0';
        } else if ($scope.status == 1) {
          url = API_ENDPOINT.url +'po/transaksikomisiagen?idPo=' + $scope.user.idPo + '&tglAwal=' + $scope.tglAwal + '&tglAkhir=' + $scope.tglAkhir + '&status=1';
        }

        $http.get(url).success(function (data) {
          // console.log($scope.status);

          // $scope.data_komisi = data;

          $scope.data_komisi = [];
          data.forEach(function(response){
            $scope.data_komisi.push({
              namaAgen: response.namaAgen,
              jmlTransaksi: response.jmlTransaksi,
              jmlhHargaJual: response.jmlhHargaJual,
              jmlhKomisi: response.jmlhKomisi
            });
          });

          if ($scope.data_komisi.length === 0) {
            $scope.empty = true;
            $scope.message = "Tidak Ada Data Laporan Komisi";
          } else {
            $scope.empty = false;
          }

          $timeout(function () {
            $scope.templateUrl = "/app/pages/akap/laporan/komisiAgen/akap.pembagianKomisi.tabels.html";
          }, 100);

          $scope.exportPdf = function () {
            komisiPrint.exportPdf($scope.data_komisi, $scope.grandTotal, $scope.tglAwal, $scope.tglAkhir, $scope.status.value);
          };
          $scope.exportXls = function(){
            $scope.exportPage = true;
            var blob = new Blob([document.getElementById('dataTable').innerHTML], {
              type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "Laporan komisi keberangkatan " + $scope.tglAwal  + ' - ' + $scope.tglAwal + ".xls");
            $scope.exportPage = false;
          };
          $scope.loadData = false;
        });
      // }
    };


    // $scope.tampil = function () {
    //   getPage();
    // };

  });
})();
