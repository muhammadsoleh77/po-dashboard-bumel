(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akap')
  .controller('cancelDetailTrayekCtrl', cancelDetailTrayekCtrl);

  function cancelDetailTrayekCtrl(API_ENDPOINT, $scope, $http, $state, items, $uibModal, $uibModalInstance, $rootScope) {

    $scope.cancelDetailTrayek_modal = items.data;
    console.log($scope.cancelDetailTrayek_modal);

    $scope.close = function(){
      $uibModalInstance.close(true);
    }
  }
})();
