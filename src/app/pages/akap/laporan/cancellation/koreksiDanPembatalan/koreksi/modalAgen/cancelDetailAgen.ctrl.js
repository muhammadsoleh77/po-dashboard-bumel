(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akap')
  .controller('cancelDetailAgenCtrl', cancelDetailAgenCtrl);

  function cancelDetailAgenCtrl(API_ENDPOINT, $scope, $http, $state, items, $uibModal, $uibModalInstance, $rootScope) {

    $scope.cancelDetailAgen_modal = items.data;
    console.log($scope.cancelDetailAgen_modal);

    $scope.close = function(){
      $uibModalInstance.close(true);
    }
  }
})();
