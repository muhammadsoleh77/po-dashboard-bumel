(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('refundListData', refundListData);

    function refundListData() {

      // start
      var getTotalRefund = function (d) {
        console.log(d);

        var total_refund = 0;
        d.listLaporan.forEach(function (data) {
          total_refund = total_refund + data.nominalRefund;
        });
        return total_refund;
      };

      return {
        getTotalRefund : getTotalRefund
      };
      // end

    }

})();
