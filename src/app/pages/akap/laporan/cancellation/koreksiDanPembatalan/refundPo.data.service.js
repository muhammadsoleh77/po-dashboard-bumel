(function () {

  angular
    .module('BlurAdmin.pages.akap')
    .service('poListData', poListData);

    function poListData() {

      // start
      var getTotalPo = function (d) {
        console.log(d);

        var total_po = 0;
        d.listLaporan.forEach(function (data) {
          total_po = total_po + data.setoranPo;
        });
        return total_po;
      };

      return {
        getTotalPo : getTotalPo
      };
      // end

    }

})();
