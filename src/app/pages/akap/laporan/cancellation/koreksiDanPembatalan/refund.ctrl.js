( function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.akap')
    .controller('laporanTransaksiRefundCtrl', laporanTransaksiRefundCtrl);

    function laporanTransaksiRefundCtrl ($scope, Restangular, $rootScope, $http, API_ENDPOINT, $state, $uibModal, $window, $timeout, refundListData, poListData) {

      $state.current.tag = 'D.4.3';

      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth();
      var dd = date.getDate();
      var d = date.getDay();

      var url = [
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/refund/laporan-refund.html',
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/refund/laporan-refund-tabel-agen.html',
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/refund/laporan-refund-tabel-trayek.html',
      ];

      $scope.templateUrl = url[0];

      $scope.getTglAwal = function (tglawal) {
        $scope.tglawal = tglawal;
      };

      $scope.getTglAkhir = function (tglakhir) {
        $scope.tglakhir = tglakhir;
      };

      $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/agen').success(function(data){
        $scope.ageens = [
          {
            namaChannel : 'Semua Agen',
            idChannel : null
          }
        ];
        data.forEach(function(response){
          $scope.ageens.push({
            idChannel: response.idChannel,
            namaChannel: response.namaChannel
          })
        });
      });

      $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/trayeks').success(function(data){
        $scope.trayeeks = [
          {
            namaChannel : 'Semua Trayek',
            idChannel : null
          }
        ];
        data.forEach(function(response){
          $scope.trayeeks.push({
            idChannel: response.id,
            namaChannel: response.trayek
          })
        });
      });

      $scope.tampil = function(tglawal, tglakhir, agen, trayek, refund, status) {

        $scope.empty = true;
        $scope.loadData = true;
        $scope.refund = refund;
        $scope.status = status;
        console.log(status);

        $scope.active = 1;
        var perHal = 5;
        var totalGrup = 0;
        var page;
        var perPage = 10;

        if($scope.refund == 'rute_agen') {

          if(agen.idChannel === null) {
            if($scope.status == 0) {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page=0&per_page=' + perPage).success(function(data) {

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[1];
                }
                $scope.loadData = false;
              });
            }
            else {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page=0&per_page=' + perPage).success(function(data) {

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[1];
                }
                $scope.loadData = false;
              });
            }
          }

          if(agen.idChannel !== null) {
            if($scope.status == 0) {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?idagen=' + agen.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page=0&per_page=' + perPage).success(function(data) {

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?idagen=' + agen.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[1];
                }
                $scope.loadData = false;
              });
            }
            else {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?idagen=' + agen.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page=0&per_page=' + perPage).success(function(data) {

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?idagen=' + agen.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[1];
                }
                $scope.loadData = false;
              });
            }
          }
        }

        if($scope.refund == 'rute_trayek') {

          if(trayek.idChannel === null) {
            if($scope.status == 0) {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page=0&per_page=' + perPage).success(function(data){

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[2];
                }
                $scope.loadData = false;
              });
            }
            else {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page=0&per_page=' + perPage).success(function(data){

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[2];
                }
                $scope.loadData = false;
              });
            }
          }

          if(trayek.idChannel !== null) {
            if($scope.status == 0) {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?idtrayek=' + trayek.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page=0&per_page=' + perPage).success(function(data){

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?idtrayek=' + trayek.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=0&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[2];
                }
                $scope.loadData = false;
              });
            }
            else {
              $http.get(API_ENDPOINT.url + 'po/laporan/refund/' + $scope.user.idPo + '?idtrayek=' + trayek.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page=0&per_page=' + perPage).success(function(data){

                $scope.total_refund = refundListData.getTotalRefund(data);
                console.log($scope.total_refund);

                $scope.total_po = poListData.getTotalPo(data);
                console.log($scope.total_po);

                // start pagination
                $scope.datas = data.listLaporan;
                if (data.totalPageSize <= perPage) {
                  page = 1;
                } else {
                  // untuk perhitungan total page
                  page = Math.ceil((data.totalPageSize/perPage));
                }

                // membuat validasi keluaran page berdasarkan totalpage
                if(page > perHal){
                  // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
                  totalGrup = Math.ceil((page/perHal));
                  $scope.totalGrup = totalGrup
                } else{
                  totalGrup = 1;
                }

                // perhitungan pengelompokan tiap grup berisi 5 page
                var grup = []
                for (var i = 1; i <= totalGrup; i++) {
                  var subgrup = []
                  for (var j = 1; j <= page; j++) {
                    if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                      // subgrup.push(j)
                      subgrup.push({
                        page : j,
                        url : API_ENDPOINT.url + '/po/laporan/refund/' + $scope.user.idPo + '?idtrayek=' + trayek.idChannel + '&tglawal=' + tglawal + '&tglakhir=' + tglakhir + '&status=1&page='+ (j-1) + '&per_page=' + perPage,
                        active : j == 1 ? true : false,
                      });
                    }
                  }
                  grup.push(subgrup)
                }
                // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
                console.log(grup);

                $scope.pageActive = 0

                $scope.pages = grup[$scope.pageActive]
                $scope.totalHal = []

                $scope.selectPage = function (page) {
                  $scope.loadData = true;
                  $http.get(page.url).success(function (data) {
                    $scope.total_refund = refundListData.getTotalRefund(data);
                    $scope.total_po = poListData.getTotalPo(data);
                    $scope.datas = data.listLaporan
                    $scope.loadData = false;

                    for (var i = 0; i < $scope.pages.length; i++) {
                      if ($scope.pages[i].page === page.page) {
                        $scope.pages[i].active = true;
                        $scope.active = $scope.pages[i].page ;
                      } else {
                        $scope.pages[i].active = false;
                      }
                    }

                  })
                }

                $scope.nextPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive+1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[0])
                  console.log($scope.pages);
                }

                $scope.prevPage = function(next) {
                  $scope.pages = []
                  $scope.pageActive = $scope.pageActive-1
                  $scope.pages = grup[$scope.pageActive]
                  $scope.selectPage($scope.pages[$scope.pages.length - 1])
                }

                $scope.loadData = false;
                if($scope.datas.length === 0) {
                  $scope.empty = true;
                  // $scope.message = "Tidak Ada Data Pembayaran";
                  $scope.pages = false;
                } else {
                  $scope.empty = false;
                  $scope.templateUrl = url[2];
                }
                $scope.loadData = false;
              });
            }
          }
        }

      };

    };
})();
