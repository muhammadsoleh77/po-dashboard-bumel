( function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.akap')
    .controller('laporanTransaksiPembatalanCtrl', function ($scope, Restangular, $rootScope, $http, API_ENDPOINT, $state, $uibModal, $window) {

      $state.current.tag = 'D.4.2';

      var url = [
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/pembatalan/laporan-pembatalan.html',
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/pembatalan/laporan-pembatalan-tabel-agen.html',
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/pembatalan/laporan-pembatalan-tabel-trayek.html',
      ];

      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth() + 1;
      var dd = date.getDate() - 1;
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (dd < 1) {
        dd = '0' + dd;
      }
      var now = yyyy + '-' + mm + '-' + dd +',23:59';
      $scope.now = new Date(now); // tanggal sekarang
      // $scope.limit = String($scope.now); // tanggal sekarang

      $scope.getTglAwal = function (tglawal) {
        $scope.tglawal = tglawal;
      };

      $scope.getTglAkhir = function (tglakhir) {
        $scope.tglakhir = tglakhir;
      };

      $scope.templateUrl = url[0];
      // $state.current.tag = 'D.4.2';

      $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/agen')
      .success(function(data){
        // $scope.ageens = [];
        $scope.ageens = [
          {
            namaChannel : "Semua Agen",
            // value : '',
            idChannel : null
          }
        ];
        data.forEach(function(response){
          $scope.ageens.push({
            idChannel: response.idChannel,
            namaChannel: response.namaChannel
          })
        });
        // console.log(data);
      });

      $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/trayeks')
      .success(function(data){
        // $scope.trayeeks = [];
        $scope.trayeeks = [
          {
            namaChannel : "Semua Trayek",
            // value : '',
            idChannel : null
          }
        ];
        data.forEach(function(response){
          $scope.trayeeks.push({
            idChannel: response.id,
            namaChannel: response.trayek
          })
        });
        // console.log(data);
      });

      // $scope.tampil = function(tanggal, agen, trayek) {
      //   getPage();
      $scope.tampil = function(tanggal, tglawal, tglakhir, agen, trayek, pembatalan) {

        $scope.loadData = true;
        $scope.pembatalan = pembatalan
        // alert($scope.pembatalan);

        if($scope.pembatalan == "rute_agen") {
          if(agen.idChannel === null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=3').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              for(var i = 0; i <data.length; i++) {
                data[i].statusRefund = data[i].statusRefund === false ? "Dalam Proses" : "Terbayar"
              }
              console.log($scope.datas);
              $scope.templateUrl = url[1];
            });
          }
          else if(agen.idChannel !== null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&idagen=' + agen.idChannel + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=3').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              for(var i = 0; i < data.length; i++) {
                data[i].statusRefund = data[i].statusRefund === false ? "Dalam Proses" : "Terbayar"
              }
              $scope.templateUrl = url[1];
            });
          }
        }

        else if($scope.pembatalan == "rute_trayek") {
          if(trayek.idChannel === null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=3').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              for(var i =0; i < data.length; i++) {
                data[i].statusRefund = data[i].statusRefund === false ? "Dalam Proses" : "Terbayar"
              }
              console.log($scope.datas);
              $scope.templateUrl = url[2];
            });
          }
          else if(trayek.idChannel !== null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&idtrayek=' + trayek.idChannel + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=3').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              for(var i = 0; i < data.length; i++) {
                data[i].statusRefund = data[i].statusRefund === false ? "Dalam Proses" : "Terbayar"
              }
              $scope.templateUrl = url[2];
            });
          }
        }

      };

    });
})();
