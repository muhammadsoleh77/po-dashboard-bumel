( function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.akap')
    .controller('laporanTransaksiKoreksiCtrl', function ($scope, Restangular, $rootScope, $http, API_ENDPOINT, $state, $uibModal, $window, $timeout) {

      $state.current.tag = 'D.4.1';

      var url = [
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/laporan-koreksi.html',
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/laporan-koreksi-tabel-agen.html',
        'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/laporan-koreksi-tabel-trayek.html',
      ];

      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth() + 1;
      var dd = date.getDate() - 1;
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (dd < 1) {
        dd = '0' + dd;
      }
      var now = yyyy + '-' + mm + '-' + dd +',23:59';
      $scope.now = new Date(now); // tanggal sekarang
      // $scope.limit = String($scope.now); // tanggal sekarang

      $scope.getTglAwal = function (tglawal) {
        $scope.tglawal = tglawal;
      };

      $scope.getTglAkhir = function (tglakhir) {
        $scope.tglakhir = tglakhir;
      };

      $scope.templateUrl = url[0];
      $state.current.tag = 'D.4.1';

      $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/agen')
      .success(function(data){
        // $scope.ageens = [];
        $scope.ageens = [
          {
            namaChannel : 'Semua Agen',
            idChannel : null
          }
        ];
        data.forEach(function(response){
          $scope.ageens.push({
            idChannel: response.idChannel,
            namaChannel: response.namaChannel
          })
        });
        // console.log(data);
      });

      $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/trayeks')
      .success(function(data){
        // $scope.trayeeks = [];
        $scope.trayeeks = [
          {
            namaChannel : 'Semua Trayek',
            idChannel : null
          }
        ];
        data.forEach(function(response){
          $scope.trayeeks.push({
            idChannel: response.id,
            namaChannel: response.trayek
          })
        });
        // console.log(data);
      });

      $scope.tampil = function(tanggal, tglawal, tglakhir, agen, trayek, koreksi) {

        // $scope.templateUrl = "";

        // if(!$scope.tanggal || !$scope.agen || !$scope.trayek || !$scope.koreksi) {
        //   alert("Data Harus Diisi semua");
        //   return false;
        // }

        // console.log(agen);
        // $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&idagen=' + agen.idChannel + '&tgl=' + tanggal + '&jenis=2').success(function (data) {
        //
        //   $scope.loadData = false;
        //
        //   $scope.datas = data;
        //   console.log($scope.datas);
        //   $scope.templateUrl = url[1];
        //
        // });

        $scope.loadData = true;
        $scope.koreksi = koreksi
        // alert($scope.pembatalan);

        if($scope.koreksi == "rute_agen") {
          if(agen.idChannel === null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=2').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              // console.log($scope.datas);
              $scope.templateUrl = url[1];

              // Start membuat design Tabel Excel
              var z=1;
              $scope.excelData='';

              $scope.excelData+='<tr><th>No</th><th>Kode Booking</th><th>Nama Penumpang</th><th>No. Tiket</th><th>Tgl Berangkat & agen</th><th>Tgl Dibatalkan</th><th>Tgl Diterima PO</th><th>Tgl Transaksi</th></tr>\n';
              data.forEach(function(x,y){
                x.dataPenumpang.forEach(function(v,w){
                  $scope.excelData+='<tr><td style="text-align:center; vertical-align: middle;" valign="middle" >'+z+'</td><td>'+x.kodeBooking+'<br>'+x.namaTrayek+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=v.nama+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">'+v.nomorTiket+'</td><td>'+x.tglBerangkat+'<br>'+x.namaAgen+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglCancel+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.waktuApprove+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglTransaksi+'</td></tr>';
                  $scope.excelData+='\n';
                  z++;
                });
              });
              // console.log($scope.excelData);

              $scope.exportXls = function() {
                $scope.exportPage = true;
                var excelFile='<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http:\/\/www.w3.org\/TR\/REC-html40">';
  	            excelFile+='<head><meta http-equiv=Content-Type content="text/html; charset=windows-1252"><meta name=ProgId content=Excel.Sheet><meta name=Generator content="Microsoft Excel 11">';
  	            excelFile+='<!--[ifgte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>';
  	            excelFile+='</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table  x:str border=0 >';
  		           excelFile+=$scope.excelData;
  	             excelFile+='</table></body></html>';

                var blob = new Blob([excelFile], {
                  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "Laporan data Void Agen " + tanggal + ".xls");
                $scope.exportPage = false;
              }
              // End Excel

              $scope.cancel_detailAgen = function(index) {

                  $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/modalAgen/cancelDetailAgen.template.html',
                    controller: 'cancelDetailAgenCtrl',
                    size: 'lg',
                    resolve: {
                      items: function() {
                        return {
                          data : $scope.datas[index].dataPenumpang
                        };
                      }
                    }
                  });
              }
            });
          }
          else if(agen.idChannel !== null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&idagen=' + agen.idChannel + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=2').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              console.log($scope.datas);
              $scope.templateUrl = url[1];

              // Start membuat design Tabel Excel
              var z=1;
              $scope.excelData='';

              $scope.excelData+='<tr><th>No</th><th>Kode Booking</th><th>Nama Penumpang</th><th>No. Tiket</th><th>Tgl Berangkat & agen</th><th>Tgl Dibatalkan</th><th>Tgl Diterima PO</th><th>Tgl Transaksi</th></tr>\n';
              data.forEach(function(x,y){
                x.dataPenumpang.forEach(function(v,w){
                  $scope.excelData+='<tr><td style="text-align:center; vertical-align: middle;" valign="middle" >'+z+'</td><td>'+x.kodeBooking+'<br>'+x.namaTrayek+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=v.nama+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">'+v.nomorTiket+'</td><td>'+x.tglBerangkat+'<br>'+x.namaAgen+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglCancel+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.waktuApprove+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglTransaksi+'</td></tr>';
                  $scope.excelData+='\n';
                  z++;
                });
              });
              // console.log($scope.excelData);

              $scope.exportXls = function() {
                $scope.exportPage = true;
                var excelFile='<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http:\/\/www.w3.org\/TR\/REC-html40">';
  	            excelFile+='<head><meta http-equiv=Content-Type content="text/html; charset=windows-1252"><meta name=ProgId content=Excel.Sheet><meta name=Generator content="Microsoft Excel 11">';
  	            excelFile+='<!--[ifgte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>';
  	            excelFile+='</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table  x:str border=0 >';
  		           excelFile+=$scope.excelData;
  	             excelFile+='</table></body></html>';

                var blob = new Blob([excelFile], {
                  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "Laporan data Void Agen " + tanggal + ".xls");
                $scope.exportPage = false;
              }
              // End Excel

              $scope.cancel_detailAgen = function(index) {

                  $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/modalAgen/cancelDetailAgen.template.html',
                    controller: 'cancelDetailAgenCtrl',
                    size: 'lg',
                    resolve: {
                      items: function() {
                        return {
                          data : $scope.datas[index].dataPenumpang
                        };
                      }
                    }
                  });
              }
            });
          }
        }

        else if($scope.koreksi == "rute_trayek") {
          if(trayek.idChannel === null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=2').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              console.log($scope.datas);
              $scope.templateUrl = url[2];

              var z = 1;
              $scope.excelData = '';

              $scope.excelData+='<tr><th>No</th><th>Kode Booking</th><th>Nama Penumpang</th><th>No. Tiket</th><th>Tgl Berangkat & trayek</th><th>Tgl Dibatalkan</th><th>Tgl Diterima PO</th><th>Tgl Trransaksi</th></tr>\n';
              data.forEach(function(x,y){
                x.dataPenumpang.forEach(function(v,w){
                  $scope.excelData+='<tr><td style="text-align:center; vertical-align: middle;" valign="middle" >'+z+'</td><td>'+x.kodeBooking+'<br>'+x.namaTrayek+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=v.nama+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">'+v.nomorTiket+'</td><td>'+x.tglBerangkat+'<br>'+x.namaAgen+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglCancel+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.waktuApprove+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglTransaksi+'</td></tr>';
                  $scope.excelData+='\n';
                  z++;
                });
              });

              $scope.exportXls = function() {
                $scope.exportPage = true;
                var excelFile='<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http:\/\/www.w3.org\/TR\/REC-html40">';
  	            excelFile+='<head><meta http-equiv=Content-Type content="text/html; charset=windows-1252"><meta name=ProgId content=Excel.Sheet><meta name=Generator content="Microsoft Excel 11">';
  	            excelFile+='<!--[ifgte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>';
  	            excelFile+='</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table  x:str border=0 >';
  		           excelFile+=$scope.excelData;
  	             excelFile+='</table></body></html>';

                var blob = new Blob([excelFile], {
                  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "Laporan data Void Trayek " + tanggal + ".xls");
                $scope.exportPage = false;
              }

              $scope.cancel_detailTrayek = function(index) {

                  $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/modalTrayek/cancelDetailTrayek.template.html',
                    controller: 'cancelDetailTrayekCtrl',
                    size: 'lg',
                    resolve: {
                      items: function() {
                        return {
                          data : $scope.datas[index].dataPenumpang
                        };
                      }
                    }
                  });
              }

            });
          }
          else if(trayek.idChannel !== null) {
            $http.get(API_ENDPOINT.url + 'po/laporan/cancelation?idPo=' + $scope.user.idPo + '&idtrayek=' + trayek.idChannel + '&tgl=' + tglawal + '&tglakhir=' + tglakhir + '&jenis=2').success(function(data) {
              $scope.loadData = false;

              $scope.datas = data;
              console.log($scope.datas);
              $scope.templateUrl = url[2];

              var z = 1;
              $scope.excelData = '';

              $scope.excelData+='<tr><th>No</th><th>Kode Booking</th><th>Nama Penumpang</th><th>No. Tiket</th><th>Tgl Berangkat & trayek</th><th>Tgl Dibatalkan</th><th>Tgl Diterima PO</th><th>Tgl Transaksi</th></tr>\n';
              data.forEach(function(x,y){
                x.dataPenumpang.forEach(function(v,w){
                  $scope.excelData+='<tr><td style="text-align:center; vertical-align: middle;" valign="middle" >'+z+'</td><td>'+x.kodeBooking+'<br>'+x.namaTrayek+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=v.nama+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">'+v.nomorTiket+'</td><td>'+x.tglBerangkat+'<br>'+x.namaAgen+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglCancel+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.waktuApprove+'</td><td style="text-align:center; vertical-align: middle;" valign="middle">';
                  $scope.excelData+=x.tglTransaksi+'</td></tr>';
                  $scope.excelData+='\n';
                  z++;
                });
              });

              $scope.exportXls = function() {
                $scope.exportPage = true;
                var excelFile='<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http:\/\/www.w3.org\/TR\/REC-html40">';
  	            excelFile+='<head><meta http-equiv=Content-Type content="text/html; charset=windows-1252"><meta name=ProgId content=Excel.Sheet><meta name=Generator content="Microsoft Excel 11">';
  	            excelFile+='<!--[ifgte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/>';
  	            excelFile+='</x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table  x:str border=0 >';
  		           excelFile+=$scope.excelData;
  	             excelFile+='</table></body></html>';

                var blob = new Blob([excelFile], {
                  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "Laporan data Void Trayek " + tanggal + ".xls");
                $scope.exportPage = false;
              }

              $scope.cancel_detailTrayek = function(index) {

                  $uibModal.open({
                    animation: true,
                    templateUrl: 'app/pages/akap/laporan/cancellation/koreksiDanPembatalan/koreksi/modalTrayek/cancelDetailTrayek.template.html',
                    controller: 'cancelDetailTrayekCtrl',
                    size: 'lg',
                    resolve: {
                      items: function() {
                        return {
                          data : $scope.datas[index].dataPenumpang
                        };
                      }
                    }
                  });
              }
            });
          }
        }

      }

    });
})();
