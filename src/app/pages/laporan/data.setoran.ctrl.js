(function () {
  angular.module('BlurAdmin.pages.laporan')
  .controller('dataSetoranCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {

    $state.current.tag = 'E.4.3';

    $scope.loadData = false;

    var getPage = function() {

      if (!$scope.tanggal || !$scope.jenisSetoran || !$scope.agen) {
        alert("Semua data harus diisi!");
        return false;
      }

      $scope.noData = {
        status : false,
        message : ''
      };

      $scope.loadData = true;

      var url = API_ENDPOINT.url +'/po/histori/agen?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.tanggal;

      $http.get(url).success(function(data) {

        var datas = data;

        if ($scope.agen) {
          $scope.datas = $filter('filter')(datas, {idAgen : $scope.agen.id}, true);
        } else {
          $scope.datas = data;
        }


        $scope.loadData = false;
        $scope.docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Setoran Penjualan',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')($scope.tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };

        $scope.exportPdf = function(){
          $scope.datas.forEach(function (doc) {
            $scope.docDefinition.content.push({
              columns : [
                {
                  width: 70,
                  text: 'Trayek',
                  bold: true,
                },
                {
                  width: 'auto',
                  text : ': ' + doc.namaTrayek,
                }
              ]
            });
            $scope.docDefinition.content.push({
              columns : [
                {
                  width: 70,
                  text: 'Armada',
                  bold: true,
                },
                {
                  width: 'auto',
                  text : ': ' + doc.namaBus,
                }
              ]
            });
            var table = {
        			color: '#444',
              margin: [0, 5, 0, 10],
        			table: {
        				widths: ['*','*','*','*','*'],
        				headerRows: 2,
                fontSize : 8,
        				body: [
        					[
                    {text: 'Jumlah Penjualan ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jumlah Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Komisi Agen ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jumlah Setoran ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Setoran Diterima ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  ],
                  [
                    {},
                    {},
                    {},
                    {},
                    {}
                  ],
                  [
                    { text : $filter('currency')(doc.jmlPenjualan, '', 0), alignment : 'right' },
                    { text : doc.jmlKursiTerjual, alignment : 'right' },
                    { text : $filter('currency')(doc.komisiAgen, '', 0), alignment : 'right' },
                    { text : $filter('currency')(doc.setoran, '', 0), alignment : 'right' },
                    { text : $filter('currency')(doc.setoranDiterima, '', 0), alignment : 'right' },
                  ],
                  [
                    {text: 'Biaya Tambahan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#cdcdcd', fontColor: 'black', colSpan: 3},
                    {},
                    {},
                    {text: 'Jumlah Biaya ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#cdcdcd', fontColor: 'black', colSpan: 2},
                    {},
                  ]
        				]
        			}
        		};

            for (var j = 0; j < doc.biayaOp.length; j++) {
              table.table.body.push(
                [
                  {text: doc.biayaOp[j].keteranganBiaya, colSpan: 3},
                  {},
                  {},
                  {text: $filter('currency')(doc.biayaOp[j].jumlah, '', 0), alignment : 'right', colSpan: 2},
                  {}
                ]
              );
            }

            $scope.docDefinition.content.push(table);
          });
          pdfMake.createPdf($scope.docDefinition).open();
        };

        $scope.exportXls = function(){
          var blob = new Blob([document.getElementById('dataTable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
          });
          saveAs(blob, "Report.xls");
        };

    });
    };

    $scope.tampil = function(tanggal) {
      getPage();
    };
  });
})();
