(function () {
  'use strict';
  angular.module('BlurAdmin.pages.laporan', [
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        // .state('dashboard.laporan', {
        //   url: '/laporan',
        //   template : '<div ui-view></div>',
        //   abstract: true,
        //   title: 'Laporan',
        //   sidebarMeta: {
        //     icon: 'ion-clipboard',
        //     order: 203,
        //   },
        // })
        // .state('dashboard.laporan.transaksi', {
        //   url: '/pertanggal',
        //   templateUrl: 'app/pages/laporan/pertanggal/laporan.transaksi.template.html',
        //   title: 'Transaksi per Tanggal',
        //     controller: 'laporanTransaksiCtrl',
        //     controllerAs: 'laporanTransaksiCtrl',
        //     sidebarMeta: {
        //       order: 1
        //   },
        //   params : {
        //     tgl : '',
        //     trayek : '',
        //     agen : '',
        //     rute : [],
        //     arah : [],
        //     transaksi : '',
        //     jenisTransaksi : '',
        //     idTrayek : null
        //   }
        // })
        // .state('dashboard.laporan.transaksiPeriode', {
        //   url: '/periode',
        //   templateUrl: 'app/pages/laporan/periode/laporan.periode.template.html',
        //   title: 'Transaksi per Periode',
        //     controller: 'laporanPeriodeCtrl',
        //     controllerAs: 'laporanPeriodeCtrl',
        //     sidebarMeta: {
        //       order: 2
        //   },
        //   params : {
        //     tgl : '',
        //     transaksi : '',
        //     jenisTransaksi : '',
        //   }
        // })
        // .state('dashboard.laporan.manifestPenumpang', {
        //   url: '/manifestpenumpang',
        //   templateUrl: 'app/pages/laporan/manifest.penumpang.html',
        //   title: 'Manifest Penumpang',
        //     controller: 'manifestCtrl',
        //     controllerAs: 'manifestCtrl',
        //     sidebarMeta: {
        //       order: 4
        //     }
        // })

        // .state('dashboard.laporan.setoran', {
        //   url: '/setoran',
        //   templateUrl: 'app/pages/laporan/pilih.html',
        //   title: 'Setoran',
        //     controller: 'setoranPenjualanCtrl',
        //     controllerAs: 'setoranPenjualanCtrl',
        //     sidebarMeta: {
        //       order: 5
        //     }
        // })
        // .state('dashboard.laporan.setoranPenjualan', {
        //   url: '/setoranpenjualan',
        //   templateUrl: 'app/pages/laporan/laporan.setoran.penjualan.html',
        //   title: 'Setoran Penjualan',
        //     controller: 'setoranPenjualanCtrl',
        //     controllerAs: 'setoranPenjualanCtrl',
        // })
        // .state('dashboard.laporan.setoranPemesanan', {
        //   url: '/setoranpemesanan',
        //   templateUrl: 'app/pages/laporan/laporan.setoran.pemesanan.html',
        //   title: 'Setoran Pemesanan',
        //     controller: 'setoranPemesananCtrl',
        //     controllerAs: 'setoranPemesananCtrl',
        // })
        // .state('dashboard.laporan.dataSetoran', {
        //   url: '/datasetoran',
        //   templateUrl: 'app/pages/laporan/data.setoran.html',
        //   title: 'Data Setoran Agen',
        //     controller: 'dataSetoranCtrl',
        //     controllerAs: 'dataSetoranCtrl',
        // })

        // .state('dashboard.laporan.adminSistemHistory', {
        //   url: '/admin-sistem/histori',
        //   templateUrl: 'app/pages/laporan/adminSistem/adminSistem.history.template.html',
        //   title: 'Biaya Admin',
        //     controller: 'adminSistemHistoryCtrl',
        //     controllerAs: 'adminSistemHistoryCtrl',
        //     sidebarMeta: {
        //       order: 6
        //     }
        // })
        // .state('dashboard.laporan.historyPembayaran', {
        //   url: '/historypembayaran',
        //   templateUrl: 'app/pages/laporan/history.pembayaran.html',
        //   title: 'History Komisi',
        //     controller: 'historyPembayaranCtrl',
        //     controllerAs: 'historyPembayaranCtrl',
        //     sidebarMeta: {
        //       order: 7
        //     }
        // })
        // .state('dashboard.laporan.perolehanKomisiKeberangkatan', {
        //   url: '/perolehanKomisi',
        //   templateUrl: 'app/pages/laporan/perolehanKomisi/laporan.komisi.template.html',
        //   title: 'Laporan Komisi Keberangkatan Agen',
        //     controller: 'laporanKomisiCtrl',
        //     controllerAs: 'laporanKomisiCtrl',
        //     sidebarMeta: {
        //       order: 8
        //     }
        // })
        ;
  }
})();
