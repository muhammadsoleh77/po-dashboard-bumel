(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('komisiPrint', komisiPrint);

    function komisiPrint ($rootScope, $filter) {

      var exportPdf = function(data_komisi, total, tglAwal ,tglAkhir, trayek, isPemesanan){
        data_komisi = $filter('orderBy')(data_komisi, '-totalJual');
        console.log(data_komisi);
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              // text:  (isPemesanan ? 'Data Pemesanan Tiket' : 'Data Penjualan Tiket'),
              text: 'Data Laporan Komisi Keberangkatan',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tglAwal) + ' - ' + $filter('date_v1')(tglAkhir),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        docDefinition.content.push({
          columns : [
            {
              width: 70,
              text: 'Trayek',
              bold: true,
            },
            {
              width: 'auto',
              text : ': ' + (trayek ? trayek : 'Semua Trayek'),
            }
          ]
        });
        var table = {
          color: '#444',
          margin: [0, 5, 0, 10],
          table: {
            widths: [195,'*','*','*','*'],
            headerRows: 2,
            fontSize : 8,
            body: [
              [
                // {text: 'No.', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Agen', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Jml Transaksi', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Jml Penjualan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Jml Komisi', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {}
              ]
            ]
          }
        };

        data_komisi.forEach(function (doc) {
          table.table.body.push(
            [
              // doc.namaTrayek,
              { text : doc.namaAgen},
              { text : doc.jmlTransaksi},
              { text : $filter('currency')(doc.jmlhHargaJual, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.jmlhKomisi, '', 0), alignment : 'right' },
            ]
          );
        });
        // table.table.body.push(
        //   [
        //     { text : 'Total', alignment : 'center', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //     { text : $filter('currency')(total.totalKursi, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //     { text : $filter('currency')(total.totalJual, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //     { text : $filter('currency')(total.totalKomisi, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //     { text : $filter('currency')(total.totalBiayaAdmin, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
        //   ]
        // );

        docDefinition.content.push(table);
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
