(function () {

  angular
    .module('BlurAdmin.pages.transaksi')
    .controller('adminSistemHistoryCtrl', adminSistemHistoryCtrl);

    function adminSistemHistoryCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, $timeout, getDatePeriod) {

      $state.current.tag = 'E.5';

      $scope.months = getDatePeriod.months();

      $scope.years = getDatePeriod.years();

      $scope.tampil = function () {
        $scope.empty = true;
        $scope.message = "";

        $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
        var tglAwal = $scope.monthDate[0].split("-");
        var tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

        tglAwal[0] = $scope.tahun;
        tglAkhir[0] = $scope.tahun;

        $scope.tglAwal = tglAwal.join("-");
        $scope.tglAkhir = tglAkhir.join("-");

        $scope.loadData = true;
        $http.get(API_ENDPOINT.url + '/po/invoice/tracker/unpayed?idpo=' + $rootScope.user.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir + '&status=1').success(function (data) {
          $scope.datas = data;
          if ($scope.datas.length === 0) {
            $scope.empty = true;
            $scope.message = "Tidak Ada Data Pembayaran";
          } else {
            $scope.empty = false;
          }
          $scope.loadData = false;
          $scope.next = function (date) {
            $state.go('dashboard.transaksi.invoice', {date : date});
          };
        });
      };

      for (var i = 0; i < $scope.months.length; i++) {
        if ((new Date().getMonth()) === ($scope.months[i].index - 1)) {
          $scope.bulan = $scope.months[i];
          $scope.tahun = new Date().getFullYear();
          $scope.tampil();
        }
      }

    }
})();
