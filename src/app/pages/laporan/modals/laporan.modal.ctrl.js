(function () {

  angular
    .module('BlurAdmin.pages.laporan')

    .controller('passangerDatasCtrl', function ($uibModalInstance, items, $scope) {
      var url = [
        '/app/pages/laporan/modals/modal.data.penumpang.html',
        '/app/pages/laporan/modals/modal.profile.penumpang.html',
        '/app/pages/laporan/modals/modal.profile.agen.html',
      ];

      $scope.templateUrl = url[0];
      $scope.info = items.info;
      $scope.data = items.data;
      $scope.tglBerangkat = items.tglBerangkat;

      $scope.showProfile = function (index) {
        $scope.index = index;
        $scope.templateUrl = url[1];
        $scope.profile = $scope.data.dataPenumpang[index];
      };
      $scope.next = function () {
        if ($scope.index < $scope.data.dataPenumpang.length) {
          $scope.index++;
        }
        $scope.profile = $scope.data.dataPenumpang[$scope.index];
      };
      $scope.prev = function () {
        if ($scope.index > 0) {
          $scope.index--;
        }
        $scope.profile = $scope.data.dataPenumpang[$scope.index];
      };
      $scope.back = function () {
        $scope.templateUrl = url[0];
      };
      $scope.test = function (e) {
        var tes = e;
        if (tes.keyCode === 39) {
          if ($scope.index < ($scope.data.dataPenumpang.length-1)) {
            $scope.next();
          }
        } else if (tes.keyCode === 37) {
          $scope.prev();
        }
      };
      $scope.close = function () {
        $uibModalInstance.dismiss();
      };
    })
    .controller('buyerDatasCtrl', function ($uibModalInstance, items, $scope) {
      var url = [
        '/app/pages/laporan/modals/modal.data.penumpang.html',
        '/app/pages/laporan/modals/modal.profile.pemesan.html',
        '/app/pages/laporan/modals/modal.profile.agen.html',
      ];

      if (!items.isPassangerDatas) {
        $scope.templateUrl = url[2];
      } else {
        $scope.templateUrl = url[0];
      }
      $scope.info = items.info;
      $scope.data = {
        dataPenumpang : items.data.daftarPenumpang,
        tglBerangkat : items.data.tglBerangkat,
        trayek : items.data.namaTrayek,
        jamBerangkat : items.data.jamBerangkat,
      };
      $scope.tglBerangkat = items.tglBerangkat;

      $scope.showProfile = function (index) {
        $scope.index = index;
        $scope.templateUrl = url[1];
        $scope.profile = $scope.data.dataPenumpang[index];
      };
      $scope.next = function () {
        if ($scope.index < $scope.data.dataPenumpang.length) {
          $scope.index++;
        }
        $scope.profile = $scope.data.dataPenumpang[$scope.index];
      };
      $scope.prev = function () {
        if ($scope.index > 0) {
          $scope.index--;
        }
        $scope.profile = $scope.data.dataPenumpang[$scope.index];
      };
      $scope.back = function () {
        $scope.templateUrl = url[0];
      };
      $scope.test = function (e) {
        var tes = e;
        if (tes.keyCode === 39) {
          if ($scope.index < ($scope.data.dataPenumpang.length-1)) {
            $scope.next();
          }
        } else if (tes.keyCode === 37) {
          $scope.prev();
        }
      };
      $scope.close = function () {
        $uibModalInstance.dismiss();
      };
    });

})();
