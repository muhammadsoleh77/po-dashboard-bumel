(function () {
  'use strict';

  angular.module('BlurAdmin.pages.laporan')
  .factory('CheckoutAgenRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function CheckoutAgenRepository() {
      AbstractRepository.call(this, restangular, 'checkoutagens');
    }

    AbstractRepository.extend(CheckoutAgenRepository);
    return new CheckoutAgenRepository();
  }
])

.factory('JadwalRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function JadwalRepository() {
      AbstractRepository.call(this, restangular, 'jadwals');
    }

    AbstractRepository.extend(JadwalRepository);
    return new JadwalRepository();
  }
])

.controller('PerimaanAgenCtrl', function ($filter, $scope, $stateParams, $location, CheckoutAgenRepository, $rootScope, $http, API_ENDPOINT, $state) {
  // $scope.tambah = false;
  $scope.checkout = CheckoutAgenRepository.get($stateParams.id).then(function (data) {
    $scope.checkout = data;
    $scope.checkout.tglsetor = $filter('date')(data.tglsetor, 'yyyy-MM-dd');
    $scope.agen = {id: data.idchannel, nama: data.namachannel, lokasi: data.lokasichannel};
  });
  $scope.save = function () {
    $scope.checkout.tglsetor = $filter('date')($scope.checkout.tglsetor, 'yyyy-MM-ddTHH:mm:ss.sssZ');
    $scope.checkout.tglterima = new Date();
    $http.put(API_ENDPOINT.url +'/checkoutagens', $scope.checkout);
      $state.go('app.checkoutagens.listpenjualan');

  };
})

.controller('PenjualanTrayekCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {


 var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };

  // $scope.trayek = {};
  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    enableRowSelection: true,
    // expandableRowTemplate: 'app/pages/laporan/expandGrid.html',
    expandableRowHeight: 150,
    columnDefs: [
      { name: 'kodetrayek', displayName:'Trayek'},
      { name: 'kodesubtrayek', displayName:'Sub Trayek'},
      { name: 'namabus', displayName:'Kode/Plat Bus'},
      { name: 'jumlahkursi', displayName: 'Jumlah Kursi'},
      { name: 'seatsold', displayName:'Kursi Terjual'},
      { name: 'totalpenjualan', cellFilter: 'number', displayName:'Total Penjualan'},

    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
       gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
        $log.log(msg);
      });

      //

    }
  };

  $scope.Delete = function(row) {
      var index = $scope.gridOptions.data.indexOf(row.entity);
      // var data = $scope.gridOptions.data(row);
      if(window.confirm('Apakah anda yakin?')) {
        JadwalRepository.remove(row.entity.idBus).then(function () {
            $scope.subtrayeks = JadwalRepository.getList();
            $scope.gridOptions.data.splice(index, 1);
        });
      }
  };


  var getPage = function() {
    var url = API_ENDPOINT.url +'/jadwaltransaksipo?idpo='+$scope.user.idPo+"&tgl="+$filter('date')($scope.tanggal, "yyyy-MM-dd");

    $http.get(url).success(function(data) {
      // $scope.gridOptions.data = data.content;
      for(var i = 0; i < data.length; i++){
        data[i].subGridOptions = {
          columnDefs: [ {name:"Asal", field:"namaTempatBrgkt"},
          {name:"Tujuan", field:"kotaTujuan"},
          {name:"Jumlah Penumpang", field:"jmlPenumpang"},
          {name:"Total Bayar", cellFilter: 'number', field:"totalBayar"}],
          data: data[i].transaksi
        };
      }
      $scope.gridOptions.data = data;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      var firstRow;
      if (paginationOptions.pageNumber>=1) {
        firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

  });
  };

  $scope.tampil = function() {
    getPage();
  };

  $scope.tampil();
})

.controller('setroanHarianCtrl', function ($scope, API_ENDPOINT, $http, $rootScope, $state, uiGridConstants) {

  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [100, 200],
    paginationPageSize: 100,
    useExternalPagination: true,
    useExternalSorting: false,
    expandableRowTemplate: 'app/pages/expandGrid.html',
    expandableRowHeight: 150,
    columnDefs: [
      { name: 'Penjualan', field: 'jmlHpp', cellFilter: 'currency:"Rp.":0'},
      { name: 'Setoran', field: 'jmlSetoran', cellFilter: 'currency:"Rp.":0'},
      { name: 'Biaya Operasional', field: 'jmlBiayaOp',cellFilter: 'currency:"Rp.":0'},
      { name: 'Biaya Admin', field: 'jmlBiayaAdmin', cellFilter: 'currency:"Rp.":0'},
      { name: 'Setoran Diterima', field: 'jmlSetoranDiterima', cellFilter: 'currency:"Rp.":0' },
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
    }
  };

  var date = new Date();
  var yyyy = date.getFullYear();
  var mm = date.getMonth() + 1;
  var dd = date.getDate();
  if (mm < 10) {
    mm = '0' + mm;
  }
  if (dd < 1) {
    dd = '0' + dd;
  }
  var now = yyyy + '-' + mm + '-' + dd;
  $scope.tanggal = now; // tanggal sekarang

  $scope.tampil = function (tanggal) {
    $scope.gridOptions.data = [];
    $http.get(API_ENDPOINT.url + '/po/keuangan?idPo=' + $rootScope.po.idPo + '&tgl=' + tanggal).success(function (data) {
      data.subGridOptions = {
        columnDefs: [
          { name: 'Penjualan', field: 'jmlHpp', cellFilter: 'currency:"Rp.":0'},
          { name: 'Setoran', field: 'jmlSetoran', cellFilter: 'currency:"Rp.":0'},
          { name: 'Biaya Operasional', field: 'jmlBiayaOp',cellFilter: 'currency:"Rp.":0'},
          { name: 'Biaya Admin', field: 'jmlBiayaAdmin', cellFilter: 'currency:"Rp.":0'},
          { name: 'Setoran Diterima', field: 'jmlSetoranDiterima', cellFilter: 'currency:"Rp.":0' },
        ],
        data : data.setorans
      };
      $scope.gridOptions.data.push(data);
    });
  };
});

})();
