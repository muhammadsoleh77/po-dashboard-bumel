(function () {
  angular.module('BlurAdmin.pages.laporan')
  .controller('manifestCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {

    $state.current.tag = 'E.3';

    var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    $scope.limit = String(date);

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 1) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;

    $scope.loadData = false;

    $scope.noData = {
      status : true,
      message : ""
    };

    $scope.getJamBerangkat = function (idTrayek) {
      $http.get(API_ENDPOINT.url + '/trayek/' + idTrayek + '/subtrayeks').success(function (data) {
        var jamBerangkat = ['Semua Keberangkatan'];
        data.forEach(function (item) {
          jamBerangkat.push(item.tempatberangkat[0].waktutiba);
        });
        $scope.listJamBerangkat =  jamBerangkat;
        if ($scope.listJamBerangkat.length === 2) {
          $scope.jamBerangkat = $scope.listJamBerangkat[1];
        }
      });
    };

    $scope.changeTgl = function () {
      console.log($scope.trayek);
      $scope.dataLoaded = false;
    };

    $scope.showFilter = false;

   var paginationOptions = {
      pageNumber: 0,
      pageSize: 25,
      sort: null
    };

    // $scope.trayek = {};
    $scope.gridOptions = {
      enableColumnResizing: true,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      useExternalSorting: false,
      enableRowSelection: true,
      enableGridMenu: true,
      // expandableRowTemplate: 'app/pages/laporan/expandGrid.html',
      expandableRowHeight: 150,
      columnDefs: [
        { name: 'kursi', displayName:'No.Kursi', width: 100},
        { name: 'nama', displayName:'Nama Penumpang'},
        { name: 'gender', displayName:'Jenis Kelamin'},
        { name: 'kategoriUsia', displayName:'Usia'},
        { name: 'berangkat', displayName:'Berangkat'},
        { name: 'tujuan', displayName:'Tujuan'},
        { name: 'telpon', displayName:'No.Telepon', cellFilter:'is_anak:row.entity.kategoriUsia'},
        { name: 'tglTrans', displayName:'Tgl Transaksi', cellTemplate : "<span>{{ row.entity.tglTrans | date:'dd-MM-yyyy'  }}</span>"},
      ],
      exporterPdfDefaultStyle: {fontSize: 9, alignment: 'left' },
      exporterPdfTableStyle: {margin: [-10, 30, 30, 30]},
      exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'black', alignment: 'center', fillColor: 'orange'},
      exporterPdfFooter: function ( currentPage, pageCount ) {
        return { text: currentPage.toString() + ' dari ' + pageCount.toString(), style: 'footerStyle', alignment : 'center' };
      },
      exporterPdfCustomFormatter: function ( docDefinition ) {
        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
        return docDefinition;
      },
      exporterPdfOrientation: 'portrait',
      exporterPdfPageSize: 'Letter',
      exporterIsExcelCompatible: true,
      exporterOlderExcelCompatibility: true,
      exporterPdfMaxGridWidth: 500,
      exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
          if (sortColumns.length === 0) {
            paginationOptions.sort = null;
          } else {
            paginationOptions.sort = sortColumns[0].sort.direction;
          }
          // getPage();
          $scope.refreshData();
        });
        gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          paginationOptions.pageNumber = newPage-1;
          paginationOptions.pageSize = pageSize;
          getPage();
          $scope.refreshData();
        });
         gridApi.selection.on.rowSelectionChanged($scope,function(row){
          var msg = 'row selected ' + row.isSelected;
          $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
          $log.log(msg);
        });

        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
          var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
          $log.log(msg);
        });
        //

      }
    };

    // $scope.exportPdf = function(){
    //   $scope.gridOptions.exporterFieldCallback =  function (grid, row, col, input) {
    //     if (col.cellFilter) {
    //       input = $filter("is_anak")(input, row.entity.kategoriUsia);
    //     }
    //     return input;
    //   };
    //   $scope.gridApi.exporter.pdfExport( 'visible', 'visible' );
    // };
    $scope.exportXls = function(){
      $scope.gridOptions.exporterFieldCallback =  function (grid, row, col, input) {
        if (col.cellFilter) {
          input = "+62" + input.slice(1);
          input = $filter("is_anak")(input, row.entity.kategoriUsia);
        } else {
        }
        return input;
      };
      var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
      $scope.gridApi.exporter.csvExport( 'all', 'all', myElement );
    };

    $scope.Delete = function(row) {
        var index = $scope.gridOptions.data.indexOf(row.entity);
        // var data = $scope.gridOptions.data(row);
        if(window.confirm('Apakah anda yakin?')) {
          JadwalRepository.remove(row.entity.idBus).then(function () {
              $scope.subtrayeks = JadwalRepository.getList();
              $scope.gridOptions.data.splice(index, 1);
          });
        }
    };


    var getPage = function() {

      if (!$scope.tanggal) {
        return false;
      }

      $scope.gridOptions.exporterPdfHeader = {
        text : [
          {text: "Manifest Penumpang", fontSize: 16, alignment: 'center', margin: [0, 15, 0, 0]},
          '\n' + $filter('date_v1')($scope.tanggal),
        ],
         margin: [0, 8, 0, 20],
         alignment: 'center'
      };

      $scope.gridOptions.exporterCsvFilename = 'Manifest Penumpang ' + $scope.hariIni + '.xls';

      $scope.noData = {
        status : false,
        message : ''
      };

      $scope.showData = {
        status : false,
        message : ''
      };

      $scope.loadData = true;
      $scope.showFilter = false;
      var url = API_ENDPOINT.url +'/po/penumpang?idPo=' + $rootScope.po.idPo + '&tgl=' + $scope.tanggal;

      $http.get(url).success(function(data) {
        // $scope.gridOptions.data = data.content;
        for(var i = 0; i < data.length; i++){
          data[i].subGridOptions = {
            columnDefs: [ {name:"Asal", field:"namaTempatBrgkt"},
            {name:"Tujuan", field:"kotaTujuan"},
            {name:"Jumlah Penumpang", field:"jmlPenumpang"},
            {name:"Total Bayar", cellFilter: 'number', field:"totalBayar"}],
            data: data[i].transaksi
          };
        }
        var dataPenumpang = [];
        var listJamBerangkat = [{
          name : "Semua Jam Berangkat",
          value : ''
        }];
        data.forEach(function (doc) {
          listJamBerangkat.push({
            name : doc.jamBerangkat,
            value : doc.jamBerangkat
          });
          doc.penumpangs.forEach(function (doc2) {
            dataPenumpang.push({
              kursi : doc2.kursi,
              nama : doc2.nama,
              gender : doc2.gender,
              kategoriUsia : doc2.kategoriUsia,
              berangkat : doc2.alamatBrgkt,
              asal : doc2.asal,
              tujuan : doc2.tujuan,
              telpon : doc2.telpon,
              jamBerangkat : doc.jamBerangkat,
              trayek : doc.namaTrayek,
              tgl : doc.tgl,
              tglTrans : doc2.tglTrans
            });
          });
        });

        // $scope.jamBerangkat = listJamBerangkat;
        // });


        $scope.gridOptions.data = dataPenumpang;
        $scope.dataLoaded = true;
        $scope.docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*'],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Manifest Penumpang',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')($scope.tanggal),
              fontSize: 8,
              marginBottom : 20,
              alignment: 'left',
            },
            {
              color: '#444',
              margin: [0, 5, 0, 10],
              table: {
                widths: [50,70,70,40,90,50,50],
                headerRows: 2,
                fontSize : 6,
                body: [
                  [
                    {text: 'No.Kursi ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Nama Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jenis Kelamin ', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Usia', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Tujuan', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'No.Telepon', style: 'tableHeader', alignment: 'center', fontSize: 8, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  ],
                  [
                    {},
                    {},
                    {},
                    {},
                    {},
                    {},
                    {}
                  ]
                ]
              }
            }
          ]
        };

        $scope.exportPdf = function () {
          $scope.gridOptions.data.forEach(function (doc) {
            $scope.docDefinition.content[3].table.body.push([
              {text: doc.kursi, fontSize: 7},
              {text: doc.nama, fontSize: 7, },
              {text: doc.gender, fontSize: 7, },
              {text: doc.kategoriUsia, fontSize: 7},
              {text: doc.berangkat, fontSize: 7},
              {text: doc.tujuan, fontSize: 7},
              {text: $filter('is_anak')(doc.telepon, doc.kategoriUsia), fontSize: 7},
            ]);
          });
          pdfMake.createPdf($scope.docDefinition).open();
        };

        $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 70; // your header height
           if (($scope.gridOptions.data.length * rowHeight + headerHeight) > 400) {
             height = 400;
           } else {
             height = $scope.gridOptions.data.length * rowHeight + headerHeight;
           }
           return {
              height: height + "px"
           };
         };

        if ($scope.gridOptions.data.length === 0) {
          $scope.noData = {
            status : true,
            message : "Tidak ada Data Manifest Penumpang"
          };
        } else {
          $scope.showData = {
            status : true,
            message : 'Manifest Penumpang ' + $scope.hariIni
          };
        }

        $scope.loadData = false;
        $scope.showFilter = true;

        $scope.refreshData = function () {

          // $scope.jam = "Semua Keberangkatan";

          if ($scope.trayek) {
            $scope.getJamBerangkat($scope.trayek.id);
          }

          $scope.gridOptions.data = $filter('filter')(dataPenumpang, $scope.trayek.value);
          if ($scope.jam && $scope.jam === "Semua Keberangkatan") {
            $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, '');
          } else {
            $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, $scope.jam);
          }
          $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, $scope.agen);
        };
    });
    };

    $scope.tampil = function(tanggal) {
      getPage();
      $scope.trayek = '';
      $scope.jam = '';
      $scope.agen = '';
    };
  });
})();
