(function () {
  angular.module('BlurAdmin.pages.laporan')
  .controller('laporanPemesananCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants, $uibModal) {

    $state.current.tag = 'E.2';

    var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();
    $scope.limit = String(date);

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 1) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;
    // $scope.now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);

    $scope.loadData = false;

    $scope.noData = {
      status : true,
      message : ""
    };

    $scope.showData = {
      status : false,
      message : ""
    };

  $scope.exportXls = function(){
    var blob = new Blob([document.getElementById('dataTable').innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xls");
  };

    var getPage = function() {

      if (!$scope.tanggal) {
        return false;
      }

      $scope.showData = {
        status : false,
        message : ''
      };

      $scope.loadData = true;

      var url = API_ENDPOINT.url +'/po/dashboard?idPo=' + $rootScope.po.idPo + '&tgl=' + $scope.tanggal;

      $http.get(url).success(function(data) {

        var dataTmp = [];
        for (var i = 0; i < data.pemesananHariIni.length; i++){
          for (var j = 0; j < data.pemesananHariIni[i].transaksiAgen.length; j++) {
            dataTmp.push({
              namaAgen : data.pemesananHariIni[i].namaAgen,
              idAgen : data.pemesananHariIni[i].idAgen,
              namaTrayek : data.pemesananHariIni[i].transaksiAgen[j].namaTrayek,
              tglBerangkat : data.pemesananHariIni[i].transaksiAgen[j].tglBerangkat,
              jamBerangkat : data.pemesananHariIni[i].transaksiAgen[j].jamBerangkat,
              kursiTerpesan : data.pemesananHariIni[i].transaksiAgen[j].kursiTerpesan,
              jmlPemesanan : data.pemesananHariIni[i].transaksiAgen[j].jmlPemesanan,
              daftarPenumpang : data.pemesananHariIni[i].transaksiAgen[j].daftarPenumpang,
            });
          }
        }

        var datas_1 = [];
        var datas =  [];

        for (var h = 0; h < $scope.listAgens.length; h++) {
          datas_1 = $filter('filter')(dataTmp, {idAgen : $scope.listAgens[h].id}, true);
          var totalPenumpang = 0;
          var totalPenjualan = 0;
          var datas_2 = [];

          for (var k = 0; k < datas_1.length; k++) {
            if (datas_1.length > 0) {
              datas_2.push({
                namaTrayek : datas_1[k].namaTrayek,
                tglBerangkat : datas_1[k].tglBerangkat,
                jamBerangkat : datas_1[k].jamBerangkat,
                kursiTerpesan : datas_1[k].kursiTerpesan,
                jmlPemesanan : datas_1[k].jmlPemesanan,
                daftarPenumpang : datas_1[k].daftarPenumpang,
              });
              totalPenumpang = totalPenumpang + datas_1[k].kursiTerpesan;
              totalPenjualan = totalPenjualan + datas_1[k].jmlPemesanan;
            }
          }

          if (datas_2.length > 0) {
            datas.push({
              namaAgen : $scope.listAgens[h].value,
              transaksi : datas_2,
              totalPenumpang : totalPenumpang,
              totalPenjualan : totalPenjualan,
            });
          }
        }

        if ($scope.agen) {
          datas = $filter('filter')(datas, {idAgen : $scope.agen.id}, true);
        }

        $scope.datas = datas;
        $scope.docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Pemesanan Tiket',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')($scope.tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };

        $scope.showPassengerData = function (info, data, isPassangerDatas) {
          $uibModal.open({
            animation: true,
            templateUrl: '/app/pages/laporan/modals/modal.main.html',
            controller: 'buyerDatasCtrl',
            size: 'md',
            resolve: {
              items: function () {
                return {
                  isPassangerDatas : isPassangerDatas,
                  data : data,
                  info : info,
                };
              }
            }
          });
        };

        $scope.exportPdf = function(){
          $scope.datas.forEach(function (doc) {
            $scope.docDefinition.content.push({
              columns : [
                {
                  width: 70,
                  text: 'Agen',
                  bold: true,
                },
                {
                  width: 'auto',
                  text : ': ' + doc.namaAgen,
                }
              ]
            });
            var table = {
        			color: '#444',
              margin: [0, 5, 0, 10],
        			table: {
        				widths: [150,'*','*','*','*'],
        				headerRows: 2,
                fontSize : 8,
        				body: [
        					[
                    {text: 'Trayek', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Tanggal Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jam Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Jumlah Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                    {text: 'Total Pemesanan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  ],
                  [
                    {},
                    {},
                    {},
                    {},
                    {}
                  ]
        				]
        			}
        		};
            var totalPenumpang = 0;
            var totalPenjualan = 0;
            for (var h = 0; h < doc.transaksi.length; h++) {
              if (doc.transaksi.length > 0) {
                totalPenumpang = totalPenumpang + doc.transaksi[h].jmlPenumpang;
                totalPenjualan = totalPenjualan + doc.transaksi[h].totalBayar;
                table.table.body.push(
                  [
                    doc.transaksi[h].namaTrayek,
                    $filter('date_v1')(doc.transaksi[h].tglBerangkat),
                    doc.transaksi[h].jamBerangkat,
                    { text : doc.transaksi[h].kursiTerpesan, alignment : 'right' },
                    { text : $filter('currency')(doc.transaksi[h].jmlPemesanan, '', 0), alignment : 'right' },
                  ]
                );
              }
              if (h === (doc.transaksi.length  - 1)) {
                table.table.body.push(
                  [
                    { text : 'Total', bold : true, fillColor: '#d2d2d2', fontColor: 'black', colSpan: 3 },
                    {},
                    {},
                    { text : doc.totalPenumpang, alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                    { text : $filter('currency')(doc.totalPenjualan, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                  ]
                );
              }
            }
            $scope.docDefinition.content.push(table);
          });
          pdfMake.createPdf($scope.docDefinition).open();
        };

        $scope.loadData = false;


    });
    };

    $scope.tampil = function(tanggal) {
      getPage();
    };
  });
})();
