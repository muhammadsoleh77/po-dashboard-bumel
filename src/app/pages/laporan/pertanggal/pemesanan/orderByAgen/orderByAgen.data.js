(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('orderByAgenData', orderByAgenData);

    function orderByAgenData($rootScope, $filter) {

      var getAgenProfile = function (idPemesan) {
        for (var i = 0; i < $rootScope.listAgens.length; i++) {
          if ($rootScope.listAgens[i].id === idPemesan) {
            profile = $rootScope.listAgens[i];
          }
        }
        return profile;
      };

      var getData = function (data, agen) {
        var dataTmp = [];
        for (var i = 0; i < data.pemesananHariIni.length; i++){
          for (var j = 0; j < data.pemesananHariIni[i].transaksiAgen.length; j++) {
            dataTmp.push({
              namaAgen : data.pemesananHariIni[i].namaAgen,
              idAgen : data.pemesananHariIni[i].idAgen,
              namaTrayek : data.pemesananHariIni[i].transaksiAgen[j].namaTrayek,
              tglBerangkat : data.pemesananHariIni[i].transaksiAgen[j].tglBerangkat,
              jamBerangkat : data.pemesananHariIni[i].transaksiAgen[j].jamBerangkat,
              kursiTerpesan : data.pemesananHariIni[i].transaksiAgen[j].kursiTerpesan,
              jmlPemesanan : data.pemesananHariIni[i].transaksiAgen[j].jmlPemesanan,
              daftarPenumpang : data.pemesananHariIni[i].transaksiAgen[j].daftarPenumpang,
            });
          }
        }


        var datas_1 = [];
        var datas =  [];

        for (var h = 0; h < $rootScope.listAgens.length; h++) {
          datas_1 = $filter('filter')(dataTmp, {idAgen : $rootScope.listAgens[h].id}, true);
          var totalPenumpang = 0;
          var totalPenjualan = 0;
          var datas_2 = [];

          for (var k = 0; k < datas_1.length; k++) {
            if (datas_1.length > 0) {
              datas_2.push({
                namaTrayek : datas_1[k].namaTrayek,
                tglBerangkat : datas_1[k].tglBerangkat,
                jamBerangkat : datas_1[k].jamBerangkat,
                kursiTerpesan : datas_1[k].kursiTerpesan,
                jmlPemesanan : datas_1[k].jmlPemesanan,
                daftarPenumpang : datas_1[k].daftarPenumpang,
              });
              totalPenumpang = totalPenumpang + datas_1[k].kursiTerpesan;
              totalPenjualan = totalPenjualan + datas_1[k].jmlPemesanan;
            }
          }

          if (datas_2.length > 0) {
            datas.push({
              namaAgen : $rootScope.listAgens[h].value,
              idAgen : $rootScope.listAgens[h].id,
              profile : $rootScope.listAgens[h],
              transaksi : datas_2,
              totalPenumpang : totalPenumpang,
              totalPenjualan : totalPenjualan,
            });
          }
        }

        if (agen) {
          datas = $filter('filter')(datas, {idAgen : agen.id}, true);
        }
        return datas;
      };

      return {
        getData : getData
      };

    }

})();
