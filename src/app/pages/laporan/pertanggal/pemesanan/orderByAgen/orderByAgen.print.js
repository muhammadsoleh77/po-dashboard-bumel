(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('orderByAgenPrint', orderByAgenPrint);

    function orderByAgenPrint($rootScope, $filter) {

      var exportPdf = function(datas, tanggal){
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Pemesanan Tiket',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        datas.forEach(function (doc) {
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Agen',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + doc.namaAgen,
              }
            ]
          });
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Petugas',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + doc.profile.kontakPerson,
              }
            ]
          });
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Alamat',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + doc.profile.alamat,
              }
            ]
          });
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Telepon',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + doc.profile.nomorTelepon,
              }
            ]
          });
          var table = {
            color: '#444',
            margin: [0, 5, 0, 10],
            table: {
              widths: [150,'*','*','*','*'],
              headerRows: 2,
              fontSize : 8,
              body: [
                [
                  {text: 'Trayek', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Tanggal Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Jam Berangkat', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Jumlah Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Total Pemesanan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                ],
                [
                  {},
                  {},
                  {},
                  {},
                  {}
                ]
              ]
            }
          };
          var totalPenumpang = 0;
          var totalPenjualan = 0;
          for (var h = 0; h < doc.transaksi.length; h++) {
            if (doc.transaksi.length > 0) {
              totalPenumpang = totalPenumpang + doc.transaksi[h].jmlPenumpang;
              totalPenjualan = totalPenjualan + doc.transaksi[h].totalBayar;
              table.table.body.push(
                [
                  doc.transaksi[h].namaTrayek,
                  $filter('date_v1')(doc.transaksi[h].tglBerangkat),
                  doc.transaksi[h].jamBerangkat,
                  { text : doc.transaksi[h].kursiTerpesan, alignment : 'right' },
                  { text : $filter('currency')(doc.transaksi[h].jmlPemesanan, '', 0), alignment : 'right' },
                ]
              );
            }
            if (h === (doc.transaksi.length  - 1)) {
              table.table.body.push(
                [
                  { text : 'Total', bold : true, fillColor: '#d2d2d2', fontColor: 'black', colSpan: 3 },
                  {},
                  {},
                  { text : doc.totalPenumpang, alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                  { text : $filter('currency')(doc.totalPenjualan, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                ]
              );
            }
          }
          docDefinition.content.push(table);
        });
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
