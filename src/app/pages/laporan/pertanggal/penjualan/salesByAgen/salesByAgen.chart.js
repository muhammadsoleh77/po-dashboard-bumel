(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('salesByAgenChart', salesByAgenChart);

    function salesByAgenChart ($rootScope, $filter, baConfig) {

      var layoutColors = baConfig.colors;
      var barChart = function (data) {
        barChart = AmCharts.makeChart('barChart', {
          type: 'serial',
          color: layoutColors.defaultText,
          dataProvider: data,
          valueAxes: [
            {
              axisAlpha: 0,
              position: 'left',
              title: 'Penjualan Per Trayek',
              gridAlpha: 0.5,
              gridColor: '#ddd',
            }
          ],
          startDuration: 1,
          graphs: [
            {
              balloonText: '<b>[[category]]: [[value]]</b>',
              fillColorsField: 'color',
              fillAlphas: 0.6,
              lineAlpha: 0.2,
              type: 'column',
              valueField: 'totalJual'
            }
          ],
          chartCursor: {
            categoryBalloonEnabled: false,
            cursorAlpha: 0,
            zoomable: false
          },
          categoryField: 'namaTrayek',
          categoryAxis: {
            gridPosition: 'start',
            labelRotation: 45,
            gridAlpha: 0.5,
            gridColor: '#ddd',
          },
          export: {
            enabled: true
          },
          creditsPosition: 'top-right',
          // pathToImages: layoutPaths.images.amChart
        });
      };

      return {
        barChart : barChart
      };
    }

})();
