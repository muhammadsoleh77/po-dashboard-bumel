(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('salesByAllTrayekPrint', salesByAllTrayekPrint);

    function salesByAllTrayekPrint ($rootScope, $filter) {

      var exportPdf = function(datas, tanggal){
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Penjualan Tiket',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        docDefinition.content.push({
          columns : [
            {
              width: 70,
              text: 'Trayek',
              bold: true,
            },
            {
              width: 'auto',
              text : ': ' + 'Semua Trayek',
            }
          ]
        });
        var table = {
          color: '#444',
          margin: [0, 5, 0, 10],
          table: {
            widths: [190,'*','*','*','*','*'],
            headerRows: 2,
            fontSize : 8,
            body: [
              [
                {text: 'Trayek', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Penjualan', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Komisi Agen', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Setoran', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Admin Sistem', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                {text: 'Pendapatan PO', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
              ],
              [
                {},
                {},
                {},
                {},
                {},
                {}
              ]
            ]
          }
        };
        var totalSetoran = 0;
        var totalPenjualan = 0;
        var totalKomisi = 0;
        var totalPendapatan = 0;
        var totalBiayaAdmin = 0;
        datas.forEach(function (doc) {
          totalPenjualan = totalPenjualan + doc.totalJual;
          totalKomisi = totalKomisi + doc.totalKomisi;
          totalSetoran = totalSetoran + (doc.totalJual - doc.totalKomisi);
          totalBiayaAdmin = totalBiayaAdmin + doc.totalBiayaAdmin;
          totalPendapatan = totalPendapatan + (doc.totalJual - doc.totalKomisi - doc.totalBiayaAdmin);
          table.table.body.push(
            [
              doc.namaTrayek,
              { text : $filter('currency')(doc.totalJual, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalKomisi, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalJual - doc.totalKomisi, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalBiayaAdmin, '', 0), alignment : 'right' },
              { text : $filter('currency')(doc.totalJual - doc.totalKomisi - doc.totalBiayaAdmin, '', 0), alignment : 'right' },
            ]
          );
        });
        table.table.body.push(
          [
            { text : 'Total', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(totalPenjualan, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(totalKomisi, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(totalSetoran, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(totalBiayaAdmin, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
            { text : $filter('currency')(totalPendapatan, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
          ]
        );
        docDefinition.content.push(table);
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
