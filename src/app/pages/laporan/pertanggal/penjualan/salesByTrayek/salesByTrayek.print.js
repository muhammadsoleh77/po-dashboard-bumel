(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('salesByTrayekPrint', salesByTrayekPrint);

    function salesByTrayekPrint ($rootScope, $filter) {

      var exportPdf = function(datas, tanggal, jamBerangkat){
        var docDefinition = {
          defaultStyle : {
            fontSize : 9
          },
          content : [
            {
              widths: ['*','*','*','*',],
              fontSize: 16,
              marginBottom : 5,
              bold: true,
              alignment: 'left',
              text: 'PO.'+$rootScope.po.nama, style: 'header'},
            {
              text:  'Data Penjualan Tiket',
              marginBottom : 5,
              fontSize: 12,
              alignment: 'left',
            },
            {
              text:  'Tanggal : ' + $filter('date_v1')(tanggal),
              fontSize: 10,
              marginBottom : 20,
              alignment: 'left',
            }
          ]
        };
        datas.forEach(function (doc) {
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Trayek',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + doc.namaTrayek,
              }
            ]
          });
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Armada',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + doc.namaBus,
              }
            ]
          });
          docDefinition.content.push({
            columns : [
              {
                width: 70,
                text: 'Jam Berangkat',
                bold: true,
              },
              {
                width: 'auto',
                text : ': ' + jamBerangkat,
              }
            ]
          });
          var table = {
            color: '#444',
            margin: [0, 5, 0, 10],
            table: {
              widths: [120,'*','*','*','*'],
              headerRows: 2,
              fontSize : 8,
              body: [
                [
                  {text: 'Nama Agen', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Jumlah Penumpang', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Penjualan ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Komisi Agen ', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                  {text: 'Biaya Admin', style: 'tableHeader', alignment: 'center', fontSize: 10, bold : true, fillColor: '#f9b51f', fontColor: 'black', rowSpan: 2},
                ],
                [
                  {},
                  {},
                  {},
                  {},
                  {}
                ]
              ]
            }
          };
          var totalPenumpang = 0;
          var totalPenjualan = 0;
          var totalKomisi = 0;
          var totalBiayaAdmin = 0;
          doc.transaksi = $filter('orderBy')(doc.transaksi, '-totalBayar');
          for (var h = 0; h < doc.transaksi.length; h++) {
            if (doc.transaksi.length > 0) {
              totalPenumpang = totalPenumpang + doc.transaksi[h].jmlPenumpang;
              totalPenjualan = totalPenjualan + doc.transaksi[h].totalBayar;
              totalKomisi = totalKomisi + doc.transaksi[h].totalKomisi;
              totalBiayaAdmin = totalBiayaAdmin + doc.transaksi[h].biayaAdmin;
              table.table.body.push(
                [
                  doc.transaksi[h].namaAgen,
                  { text : doc.transaksi[h].jmlPenumpang, alignment : 'right' },
                  { text : $filter('currency')(doc.transaksi[h].totalBayar, '', 0), alignment : 'right' },
                  { text : $filter('currency')(doc.transaksi[h].totalKomisi, '', 0), alignment : 'right' },
                  { text : $filter('currency')(doc.transaksi[h].biayaAdmin, '', 0), alignment : 'right' },
                ]
              );
            }
            if (h === (doc.transaksi.length  - 1)) {
              table.table.body.push(
                [
                  { text : 'Total', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                  { text : totalPenumpang, alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                  { text : $filter('currency')(totalPenjualan, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                  { text : $filter('currency')(totalKomisi, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                  { text : $filter('currency')(totalBiayaAdmin, '', 0), alignment : 'right', bold : true, fillColor: '#d2d2d2', fontColor: 'black' },
                ]
              );
            }
          }
          docDefinition.content.push(table);
        });
        pdfMake.createPdf(docDefinition).open();
      };

      return {
        exportPdf : exportPdf
      };
    }

})();
