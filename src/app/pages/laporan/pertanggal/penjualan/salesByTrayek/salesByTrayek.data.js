// (function () {
//
//   angular
//     .module('BlurAdmin.pages.laporan')
//     .service('salesByTrayekData', salesByTrayekData);
//
//     function salesByTrayekData ($rootScope, $filter, chartColors, $http, API_ENDPOINT ) {
//
//       var getData = function(data, trayek, jam, tgl){
//         var datas = [];
//         var getAgenProfile = function (idPemesan) {
//           for (var i = 0; i < $rootScope.listAgens.length; i++) {
//             if ($rootScope.listAgens[i].id === idPemesan) {
//               profile = $rootScope.listAgens[i];
//             }
//           }
//           return profile;
//         };
//         var dataPenjualan = [];
//         if (jam !== 'Semua Keberangkatan') {
//           data.penjualanHariIni = $filter('filter')(data.penjualanHariIni, {jamBerangkat : jam});
//         }
//         data.penjualanHariIni.forEach(function (doc) {
//           var profile, transaksi;
//           doc.transaksi.forEach(function (doc2) {
//             profile = {
//               namaBus : doc.namaBus === undefined ? "" : doc.namaBus,
//               namaTrayek : doc.namaTrayek,
//               jamBerangkat : doc.jamBerangkat,
//               idTrayek : doc.idTrayek,
//               idIssuer : doc2.transaksi.idIssuer,
//               idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
//               namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
//               idPemesan : getAgenProfile(doc2.transaksi.idIssuer).name,
//               tglBerangkat : doc2.transaksi.tglBerangkat,
//             };
//
//             if (doc2.transaksi.jenis === 1) {
//               transaksi = {
//                 jmlPenumpang : doc2.transaksi.jmlPenumpang,
//                 biayaAdmin : (doc2.transaksi.biayaAdmin + doc2.transaksi.iuranIpomi),
//                 totalBayar : doc2.transaksi.totalBayar,
//                 jenis : doc2.transaksi.jenis,
//                 totalKomisi : doc2.transaksi.idPemesan === doc2.transaksi.idTempatBrgkt ? (doc2.transaksi.komisiPembayaran + doc2.transaksi.komisiPemberangkatan + doc2.transaksi.komisiPenjualan) : doc2.transaksi.komisiPemberangkatan,
//                 setoranPo : doc2.transaksi.idSetoranPo ? doc2.transaksi.totalBayar : 0,
//                 setoranAgen : doc2.transaksi.idSetoranAgen ? doc2.transaksi.totalBayar : 0,
//               };
//             } else if (doc2.transaksi.jenis === 2) {
//               transaksi = {
//                 jmlPenumpang : 0,
//                 biayaAdmin : 0,
//                 totalBayar : 0,
//                 jenis : doc2.transaksi.jenis,
//                 totalKomisi : 0,
//                 setoranPo :  0,
//                 setoranAgen :  0,
//               };
//             } else {
//               transaksi = {
//                 jmlPenumpang : 0,
//                 biayaAdmin : 0,
//                 totalBayar : 0,
//                 jenis : doc2.transaksi.jenis,
//                 totalKomisi :  doc2.transaksi.komisiPemberangkatan,
//                 setoranPo : doc2.transaksi.idSetoranPo ? doc2.transaksi.komisiPemberangkatan : 0,
//                 setoranAgen : doc2.transaksi.idSetoranAgen ? doc2.transaksi.komisiPemberangkatan : 0,
//               };
//             }
//             dataPenjualan.push(Object.assign(transaksi, profile));
//           });
//         });
//         var dataTmp;
//         var jmlPenumpang;
//         var totalBayar;
//         var totalKomisi;
//         var biayaAdmin;
//         var setoranPo;
//         var setoranAgen;
//         for (var j = 0; j < $rootScope.lisTrayeks.length; j++) {
//           var namaBus;
//           if (dataPenjualan.length > 0) {
//             for (i = 0; i < dataPenjualan.length; i++) {
//               if (dataPenjualan[i].idTrayek === $rootScope.lisTrayeks[j].id) {
//                 namaBus = dataPenjualan[i].namaBus;
//                 i = dataPenjualan.length;
//               } else {
//                 namaBus = "";
//               }
//             }
//           } else {
//             namaBus = "";
//           }
//           var step1 = [];
//           for (i = 0; i < $rootScope.lisTrayeks[j].tempatBerangkat.length; i++) {
//             jmlPenumpang = 0;
//             totalBayar = 0;
//             totalKomisi = 0;
//             biayaAdmin = 0;
//             setoranPo = 0;
//             setoranAgen = 0;
//             dataTmp = $filter('filter')(dataPenjualan, {idTrayek : $rootScope.lisTrayeks[j].id}, true);
//             dataTmp = $filter('filter')(dataTmp, {idTempatBrgkt : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode}, true);
//             for (var k = 0; k < dataTmp.length; k++) {
//               jmlPenumpang = jmlPenumpang + dataTmp[k].jmlPenumpang;
//               totalBayar = totalBayar + dataTmp[k].totalBayar;
//               totalKomisi = totalKomisi + dataTmp[k].totalKomisi;
//               biayaAdmin = biayaAdmin + dataTmp[k].biayaAdmin;
//               setoranPo = setoranPo + dataTmp[k].setoranPo;
//               setoranAgen = setoranAgen + dataTmp[k].setoranAgen;
//             }
//             if (jmlPenumpang >= 0) {
//               step1.push({
//                 namaAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].nama,
//                 color : chartColors[i],
//                 idAgen : $rootScope.lisTrayeks[j].tempatBerangkat[i].kode,
//                 profileAgen : getAgenProfile($rootScope.lisTrayeks[j].tempatBerangkat[i].kode),
//                 jmlPenumpang : jmlPenumpang,
//                 totalBayar : totalBayar,
//                 totalKomisi : totalKomisi,
//                 biayaAdmin : biayaAdmin,
//                 setoranPo : setoranPo,
//                 setoranAgen : setoranAgen,
//                 dataPenumpang : []
//               });
//             }
//             if ($rootScope.agen) {
//               step1 = $filter('filter')(step1, {idAgen : $rootScope.agen.id}, true);
//             }
//           }
//           if (step1.length > 0) {
//             var totalJual = 0;
//             var totalKursi = 0;
//             totalKomisi = 0;
//             var totalBiayaAdmin = 0;
//             var totalSetoranPo = 0;
//             var totalSetoranAgen = 0;
//             step1.forEach(function (doc) {
//               totalJual = totalJual + doc.totalBayar;
//               totalKomisi = totalKomisi + doc.totalKomisi;
//               totalKursi = totalKursi + doc.jmlPenumpang;
//               totalBiayaAdmin = totalBiayaAdmin + doc.biayaAdmin;
//               totalSetoranPo = totalSetoranPo + doc.setoranPo;
//               totalSetoranAgen = totalSetoranAgen + doc.setoranAgen;
//             });
//             datas.push({
//               namaTrayek : $rootScope.lisTrayeks[j].value,
//               idTrayek : $rootScope.lisTrayeks[j].id,
//               rute : $rootScope.lisTrayeks[j].rute,
//               arah : $rootScope.lisTrayeks[j].arah,
//               namaBus : namaBus,
//               transaksi : step1,
//               totalJual : totalJual,
//               totalKomisi : totalKomisi,
//               totalKursi : totalKursi,
//               totalBiayaAdmin : totalBiayaAdmin,
//               totalSetoranPo : totalSetoranPo,
//               totalSetoranAgen : totalSetoranAgen,
//               setoran : totalJual - totalKomisi,
//               dataPenumpang : []
//             });
//           }
//         }
//
//         for (var i = 0; i < data.penjualanHariIni.length; i++) {
//           for (var x = 0; x < datas.length; x++) {
//             for (var o = 0; o < data.penjualanHariIni[i].dataPenumpang.length; o++) {
//               if (data.penjualanHariIni[i].idTrayek == datas[x].idTrayek) {
//                 if ($filter('date')(data.penjualanHariIni[i].dataPenumpang[o].tglBayar, 'yyyy-MM-dd', '+0000') === tgl) {
//                   datas[x].dataPenumpang.push(data.penjualanHariIni[i].dataPenumpang[o]);
//                 }
//                 // datas[x].dataPenumpang = data.penjualanHariIni[i].dataPenumpang;
//               }
//             }
//           }
//         }
//
//         for (i = 0; i < datas.length; i++) {
//           for (var z = 0; z < datas[i].dataPenumpang.length; z++) {
//             for (var y = 0; y < datas[i].transaksi.length; y++) {
//               if ($filter('date')(datas[i].dataPenumpang[z].tglBayar, 'yyyy-MM-dd', '+0000') === tgl) {
//                 if (datas[i].dataPenumpang[z].agenBeli == datas[i].transaksi[y].namaAgen) {
//                   datas[i].transaksi[y].dataPenumpang.push(datas[i].dataPenumpang[z]);
//                 }
//               }
//             }
//           }
//         }
//         datas = $filter('filter')(datas, { idTrayek : trayek }, true);
//
//         $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + tgl).success(function (data) {
//           for (var i = 0; i < datas.length; i++) {
//             datas[i].terima = true;
//             for (var j = 0; j < data.length; j++) {
//               if (datas[i].idTrayek === data[j].idTrayek) {
//                 datas[i].terima = false;
//               }
//             }
//           }
//         });
//
//         datas[0].transaksi = $filter('orderBy')(datas[0].transaksi, '-totalBayar');
//         return datas;
//       };
//
//       return {
//         getData : getData
//       };
//     }
//
// })();
