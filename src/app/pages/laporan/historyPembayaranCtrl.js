(function () {
  'use strict';

  angular.module('BlurAdmin.pages.laporan')

  /* Define the Repository that interfaces with Restangular */
  .factory('HistoryPembayaranRepository', ['Restangular', 'AbstractRepository',
    function (restangular, AbstractRepository) {

      function HistoryPembayaranRepository() {
        AbstractRepository.call(this, restangular, 'history');
      }

      AbstractRepository.extend(HistoryPembayaranRepository);
      return new HistoryPembayaranRepository();
    }
  ])

  .controller('historyPembayaranCtrl', function(HistoryPembayaranRepository, $filter, $timeout, $scope, $rootScope, $http, $state, $uibModal, API_ENDPOINT, Restangular) {

    $state.current.tag = 'E.6';

    // $scope.loadData = true;
    $scope.empty = true;
    $scope.message = "";

    $http.get(API_ENDPOINT.url + 'po/' + $scope.user.idPo + '/agen')
    .success(function(data){
      $scope.ageens = [];
      data.forEach(function(response){
        $scope.ageens.push({
          idChannel: response.idChannel,
          namaChannel: response.namaChannel
        })
      });
      // console.log(data);
    });

    $scope.tampil = function() {
      if (!$scope.agen || !$scope.status) {
        alert("Semua data harus diisi!");
        return false;
      }

      var url;
      $scope.chartTemplate = "";
      $scope.headerUrl = "";
      $scope.templateUrl = "";

      $scope.loadData = true;

      if ($scope.status == 0) {
        url = API_ENDPOINT.url +'po/transaksikomisiagen/history?idAgen=' + $scope.agen.idChannel + '&tgl=' + $scope.tanggal + '&status=0';
      } else if ($scope.status == 1) {
        url = API_ENDPOINT.url +'po/transaksikomisiagen/history?idAgen=' + $scope.agen.idChannel + '&tgl=' + $scope.tanggal + '&status=1';
      }

      $http.get(url).success(function (data) {

        for (var i = 0; i < data.length; i++) {
            data[i].statusBayar = data[i].statusBayar === false ? "Belum lunas" : "Lunas"
          }

        $scope.data_komisi = [];
        data.forEach(function(response){
          $scope.data_komisi.push({
            idTransKomisi: response.idTransKomisi,
            tglPembayaran: response.tglPembayaran,
            noTransaksi: response.noTransaksi,
            jmlTagihan: response.jmlTagihan,
            jmlTerbayar: response.jmlTerbayar,
            statusBayar: response.statusBayar,
            namaAgen: response.namaAgen,
            jmlTransaksi: response.jmlTransaksi,
            jmlhHargaJual: response.jmlhHargaJual,
            jmlhKomisi: response.jmlhKomisi
          });
        });

        if ($scope.data_komisi.length === 0) {
          $scope.empty = true;
          $scope.message = "Tidak Ada Data History Komisi";
        } else {
          $scope.empty = false;
        }

        $timeout(function () {
          $scope.templateUrl = "/app/pages/laporan/history.pembayaran.tabels.html";
        }, 100);

        $scope.openHistoriKomisi = function(dataKomisi) {
            $uibModal.open({
              animation: true,
              templateUrl: '/app/pages/laporan/modalKomisi/modalKomisi.main.html',
              controller: 'historiModalCtrl',
              size: 'sm',
              resolve: {
                items: function(){
                  return {
                    data: dataKomisi,
                  };
                }
              }
            });
          };

        $scope.loadData = false;
      });
    }
  });

})();
