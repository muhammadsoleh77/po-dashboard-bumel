(function () {

  angular
    .module('BlurAdmin.pages.laporan')
    .service('weeklySalesChart', weeklySalesChart);

    function weeklySalesChart() {

      var donutData = function (data) {
        var donutData = [];
        data.forEach(function (doc) {
          donutData.push({
            label : doc.namaTrayek,
            idTrayek : doc.idTrayek,
            value : doc.jmlPenjualan || doc.totalJual
          });
        });
        return donutData;
      };

      var chartData = function (data) {

        var lineData = [];
        var lineKey = [];
        var lineLabel = [];
        for (var i = 0; i < data.length; i++) {
          lineData.push({
            date : data[i].date
          });
          lineKey = [];
          lineLabel = [];
          for (var j = 0; j < data[i].transaksi.length; j++) {
            lineData[i][String(j)] = data[i].transaksi[j].totalJual;
            lineKey.push(String(j));
            lineLabel.push(data[i].transaksi[j].namaTrayek);
          }
        }
        return {
          lineData : lineData,
          lineKey : lineKey,
          lineLabel : lineLabel
        };

      };

      return {
        donutData : donutData,
        chartData : chartData
      };
    }
})();
