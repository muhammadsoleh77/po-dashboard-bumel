(function () {

    angular
      .module('BlurAdmin.pages.laporan')
      .service('weeklySalesData', weeklySalesData);

      function weeklySalesData($rootScope, $filter) {

        var getData = function (data, dates, trayek, isPemesanan) {
          var getNamaPemesanan = function (idPemesan) {
            for (var i = 0; i < $rootScope.agens.length; i++) {
              if ($rootScope.agens[i].id === idPemesan) {
                idPemesan = $rootScope.agens[i].name;
              }
            }
            return idPemesan;
          };
          var final = [];
          var transaksi = [];
          var dataPenjualan = [];
          data.forEach(function (doc) {
            doc.transaksi.forEach(function (doc2) {
              if (!isPemesanan) {
                if (doc2.transaksi.jenis === 1) {
                  dataPenjualan.push({
                    namaBus : doc.namaBus,
                    namaTrayek : doc.namaTrayek,
                    idTrayek : doc.idTrayek,
                    idIssuer : doc2.transaksi.idIssuer,
                    namaPemesan : getNamaPemesanan(doc2.transaksi.idPemesan),
                    idPemesan : doc2.transaksi.idPemesan,
                    idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
                    namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
                    tglBerangkat : doc2.transaksi.tglBerangkat,
                    jmlPenumpang : doc2.transaksi.jmlPenumpang,
                    totalBayar : doc2.transaksi.totalBayar,
                    komisi : doc2.transaksi.komisiPembayaran + doc2.transaksi.komisiPemberangkatan + doc2.transaksi.komisiPenjualan,
                    biayaAdmin : doc2.transaksi.biayaAdmin + doc2.transaksi.iuranIpomi,
                    jenis : doc2.transaksi.jenis,
                  });
                }
              } else {
                if (doc2.transaksi.jenis !== 1) {
                  dataPenjualan.push({
                    namaBus : doc.namaBus,
                    namaTrayek : doc.namaTrayek,
                    idTrayek : doc.idTrayek,
                    idIssuer : doc2.transaksi.idIssuer,
                    namaPemesan : getNamaPemesanan(doc2.transaksi.idPemesan),
                    idPemesan : doc2.transaksi.idPemesan,
                    idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
                    namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
                    tglBerangkat : doc2.transaksi.tglBerangkat,
                    jmlPenumpang : doc2.transaksi.jmlPenumpang,
                    totalBayar : doc2.transaksi.totalBayar,
                    komisi : doc2.transaksi.komisiPembayaran + doc2.transaksi.komisiPemberangkatan + doc2.transaksi.komisiPenjualan,
                    biayaAdmin : doc2.transaksi.biayaAdmin + doc2.transaksi.iuranIpomi,
                    jenis : doc2.transaksi.jenis,
                  });
                }
              }
            });
          });
          var dataTmp;
          var jmlPenumpang;
          var totalBayar;
          var komisi;
          var biayaAdmin;
          for (var h = 0; h < dates.length; h++) {
            var datas = [];
            for (i = 0; i < $rootScope.lisTrayeks.length; i++) {
              var step1 = [];
              for (var j = 0; j < $rootScope.listAgens.length; j++) {
                jmlPenumpang = 0;
                totalBayar = 0;
                komisi = 0;
                biayaAdmin = 0;
                dataTmp = $filter('filter')(dataPenjualan, {tglBerangkat : dates[h]});
                dataTmp = $filter('filter')(dataTmp, {idTrayek : $rootScope.lisTrayeks[i].id}, true);
                dataTmp = $filter('filter')(dataTmp, {idPemesan : $rootScope.listAgens[j].id}, true);
                for (var k = 0; k < dataTmp.length; k++) {
                  jmlPenumpang = jmlPenumpang + dataTmp[k].jmlPenumpang;
                  totalBayar = totalBayar + dataTmp[k].totalBayar;
                  komisi = komisi + dataTmp[k].komisi;
                  biayaAdmin = biayaAdmin + dataTmp[k].biayaAdmin;
                }
                if (jmlPenumpang > 0) {
                  step1.push({
                    namaAgen : $rootScope.listAgens[j].value,
                    jmlPenumpang : jmlPenumpang,
                    totalBayar : totalBayar,
                    komisi : komisi,
                    biayaAdmin : biayaAdmin
                  });
                }
              }
              if (step1.length >= 0) {
                var totalJual = 0;
                var jmlKomisi = 0;
                var jmlBiayaAdmin = 0;
                var totalKursi = 0;
                step1.forEach(function (doc) {
                  totalJual = totalJual + doc.totalBayar;
                  totalKursi = totalKursi + doc.jmlPenumpang;
                  jmlKomisi = jmlKomisi + doc.komisi;
                  jmlBiayaAdmin = jmlBiayaAdmin + doc.biayaAdmin;
                });
                datas.push({
                  namaTrayek : $rootScope.lisTrayeks[i].value,
                  idTrayek : $rootScope.lisTrayeks[i].id,
                  transaksi : step1,
                  totalJual : totalJual,
                  totalKursi : totalKursi,
                  jmlKomisi : jmlKomisi,
                  jmlBiayaAdmin : jmlBiayaAdmin,
                });
              }
            }
            if (trayek && trayek.id !== null) {
              datas = $filter('filter')(datas, { idTrayek : trayek.id }, true);
            }
            var totalPenjualan = 0;
            var totalPenumpang = 0;
            var totalKomisi = 0;
            var totalBiayaAdmin = 0;

            datas.forEach(function (doc) {
              totalPenjualan = totalPenjualan + doc.totalJual;
              totalPenumpang = totalPenumpang + doc.totalKursi;
              totalKomisi = totalKomisi + doc.jmlKomisi;
              totalBiayaAdmin = totalBiayaAdmin + doc.jmlBiayaAdmin;
              transaksi.push(doc);
            });
            final.push({
              date : dates[h],
              totalJual : totalPenjualan,
              totalKursi : totalPenumpang,
              totalBiayaAdmin : totalBiayaAdmin,
              totalKomisi : totalKomisi,
              transaksi : datas
            });
          }

          var totalTransaksi = [];

          var grandTotal = {
            totalKursi : 0,
            totalJual : 0,
            totalKomisi : 0,
            totalBiayaAdmin : 0,
          };

          for (i = 0; i < $rootScope.lisTrayeks.length; i++) {
            transaksiTmp = $filter('filter')(transaksi, {idTrayek : $rootScope.lisTrayeks[i].id}, true);
            totalJualTmp = 0;
            totalKursiTmp = 0;
            totalKomisiTmp = 0;
            totalBiayaAdminTmp = 0;
            for (var a = 0; a < transaksiTmp.length; a++) {
              totalJualTmp = totalJualTmp + transaksiTmp[a].totalJual;
              totalKursiTmp = totalKursiTmp + transaksiTmp[a].totalKursi;
              totalKomisiTmp = totalKomisiTmp + transaksiTmp[a].jmlKomisi;
              totalBiayaAdminTmp = totalBiayaAdminTmp + transaksiTmp[a].jmlBiayaAdmin;
            }

            grandTotal.totalKursi = grandTotal.totalKursi + totalKursiTmp;
            grandTotal.totalJual = grandTotal.totalJual + totalJualTmp;
            grandTotal.totalKomisi = grandTotal.totalKomisi + totalKomisiTmp;
            grandTotal.totalBiayaAdmin = grandTotal.totalBiayaAdmin + totalBiayaAdminTmp;

            totalTransaksi.push({
              namaTrayek : $rootScope.lisTrayeks[i].value,
              idTrayek : $rootScope.lisTrayeks[i].id,
              totalJual : totalJualTmp,
              totalKursi : totalKursiTmp,
              totalKomisi : totalKomisiTmp,
              totalBiayaAdmin : totalBiayaAdminTmp,
            });
          }

          return {
            transaksi : final,
            totalTransaksi : totalTransaksi,
            grandTotal : grandTotal
          };
        };


        return {
          getData : getData
        };
      }

})();
