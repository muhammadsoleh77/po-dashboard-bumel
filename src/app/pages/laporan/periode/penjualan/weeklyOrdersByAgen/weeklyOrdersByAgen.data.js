(function () {

    angular
      .module('BlurAdmin.pages.laporan')
      .service('weeklyOrdersByAgenData', weeklyOrdersByAgenData);

      function weeklyOrdersByAgenData($rootScope, $filter) {

        var getData = function (data, dates, agen, isPemesanan) {
          isPemesanan = true;
          var getNamaPemesanan = function (idPemesan) {
            for (var i = 0; i < $rootScope.agens.length; i++) {
              if ($rootScope.agens[i].id === idPemesan) {
                idPemesan = $rootScope.agens[i].name;
              }
            }
            return idPemesan;
          };
          var final = [];
          var transaksi = [];
          var dataPenjualan = [];
          data.forEach(function (doc) {
            doc.transaksi.forEach(function (doc2) {
              if (!isPemesanan) {
                if (doc2.jenis === 1) {
                  dataPenjualan.push({
                    namaBus : doc.namaBus,
                    namaTrayek : doc.namaTrayek,
                    idTrayek : doc.idTrayek,
                    idIssuer : doc2.idIssuer,
                    namaPemesan : getNamaPemesanan(doc2.idPemesan),
                    idPemesan : doc2.idPemesan,
                    idTempatBrgkt : doc2.idTempatBrgkt,
                    namaTempatBrgkt : doc2.namaTempatBrgkt,
                    tglBerangkat : doc2.tglBerangkat,
                    jmlPenumpang : doc2.jmlPenumpang,
                    totalBayar : doc2.totalBayar,
                    jenis : doc2.jenis,
                  });
                }
              } else {
                if (doc2.jenis !== 1) {
                  dataPenjualan.push({
                    namaBus : doc.namaBus,
                    namaTrayek : doc.namaTrayek,
                    idTrayek : doc.idTrayek,
                    idIssuer : doc2.idIssuer,
                    namaPemesan : getNamaPemesanan(doc2.idPemesan),
                    idPemesan : doc2.idPemesan,
                    idTempatBrgkt : doc2.idTempatBrgkt,
                    namaTempatBrgkt : doc2.namaTempatBrgkt,
                    tglTransaksi : $filter('date')(doc2.tglTransaksi, 'yyyy-MM-dd', '+0000'),
                    jmlPenumpang : doc2.jmlPenumpang,
                    totalBayar : doc2.totalBayar,
                    jenis : doc2.jenis,
                  });
                }
              }
            });
          });
          console.log(dataPenjualan);
          var dataTmp;
          var jmlPenumpang;
          var totalBayar;
          for (var i = 0; i < $rootScope.listAgens.length; i++) {
            var datas = [];
            for (var j = 0; j < dates.length; j++) {
              var step1 = [];
              for (var k = 0; k < $rootScope.lisTrayeks.length; k++) {
                jmlPenumpang = 0;
                totalBayar = 0;
                dataTmp = $filter('filter')(dataPenjualan, {idPemesan : $rootScope.listAgens[i].id}, true);
                dataTmp = $filter('filter')(dataTmp, {tglTransaksi : dates[j]}, true);
                dataTmp = $filter('filter')(dataTmp, {idTrayek : $rootScope.lisTrayeks[k].id}, true);
                for (var m = 0; m < dataTmp.length; m++) {
                  jmlPenumpang = jmlPenumpang + dataTmp[m].jmlPenumpang;
                  totalBayar = totalBayar + dataTmp[m].totalBayar;
                }
                step1.push({
                  namaTrayek : $rootScope.lisTrayeks[k].value,
                  id : $rootScope.lisTrayeks[k].id,
                  jmlPenumpang : jmlPenumpang,
                  totalBayar : totalBayar
                });
              }
              jmlPenumpang = 0;
              totalBayar = 0;
              for (var n = 0; n < step1.length; n++) {
                jmlPenumpang = jmlPenumpang + step1[n].jmlPenumpang;
                totalBayar = totalBayar + step1[n].totalBayar;
              }
              datas.unshift({
                date : dates[j],
                ms : (new Date(dates[j])).getTime(),
                jmlPenumpang : jmlPenumpang,
                totalBayar : totalBayar,
                detail : step1
              });
            }
            jmlPenumpang = 0;
            totalBayar = 0;
            for (var p = 0; p < datas.length; p++) {
              jmlPenumpang = jmlPenumpang + datas[p].jmlPenumpang;
              totalBayar = totalBayar + datas[p].totalBayar;
            }
            final.push({
              namaAgen : $rootScope.listAgens[i].value,
              id : $rootScope.listAgens[i].id,
              jmlPenumpang : jmlPenumpang,
              totalBayar : totalBayar,
              detail : datas
            });
          }
          if (agen && agen.id !== 0) {
            final = $filter('filter')(final, {id : agen.id}, true);
          }
          console.log(final);
          return final;
        };


        return {
          getData : getData
        };
      }

})();
