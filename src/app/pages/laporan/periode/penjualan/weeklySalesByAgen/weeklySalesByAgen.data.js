(function () {

    angular
      .module('BlurAdmin.pages.laporan')
      .service('weeklySalesByAgenData', weeklySalesByAgenData);

      function weeklySalesByAgenData($rootScope, $filter) {

        var getData = function (data, dates, agen, isPemesanan) {
          var getNamaPemesanan = function (idPemesan) {
            for (var i = 0; i < $rootScope.agens.length; i++) {
              if ($rootScope.agens[i].id === idPemesan) {
                idPemesan = $rootScope.agens[i].name;
              }
            }
            return idPemesan;
          };
          var final = [];
          var transaksi = [];
          var dataPenjualan = [];
          data.forEach(function (doc) {
            doc.transaksi.forEach(function (doc2) {
              if (!isPemesanan) {
                if (doc2.transaksi.jenis === 1) {
                  dataPenjualan.push({
                    namaBus : doc.namaBus,
                    namaTrayek : doc.namaTrayek,
                    idTrayek : doc.idTrayek,
                    idIssuer : doc2.transaksi.idIssuer,
                    namaPemesan : getNamaPemesanan(doc2.transaksi.idPemesan),
                    idPemesan : doc2.transaksi.idPemesan,
                    idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
                    namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
                    tglBerangkat : doc2.transaksi.tglBerangkat,
                    jmlPenumpang : doc2.transaksi.jmlPenumpang,
                    totalBayar : doc2.transaksi.totalBayar,
                    jenis : doc2.transaksi.jenis,
                  });
                }
              } else {
                if (doc2.transaksi.jenis !== 1) {
                  dataPenjualan.push({
                    namaBus : doc.namaBus,
                    namaTrayek : doc.namaTrayek,
                    idTrayek : doc.idTrayek,
                    idIssuer : doc2.transaksi.idIssuer,
                    namaPemesan : getNamaPemesanan(doc2.transaksi.idPemesan),
                    idPemesan : doc2.transaksi.idPemesan,
                    idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
                    namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
                    tglBerangkat : doc2.transaksi.tglBerangkat,
                    jmlPenumpang : doc2.transaksi.jmlPenumpang,
                    totalBayar : doc2.transaksi.totalBayar,
                    jenis : doc2.transaksi.jenis,
                  });
                }
              }
            });
          });
          var dataTmp;
          var jmlPenumpang;
          var totalBayar;
          for (var i = 0; i < $rootScope.listAgens.length; i++) {
            var datas = [];
            for (var j = 0; j < dates.length; j++) {
              var step1 = [];
              for (var k = 0; k < $rootScope.lisTrayeks.length; k++) {
                jmlPenumpang = 0;
                totalBayar = 0;
                dataTmp = $filter('filter')(dataPenjualan, {idPemesan : $rootScope.listAgens[i].id}, true);
                dataTmp = $filter('filter')(dataTmp, {tglBerangkat : dates[j]}, true);
                dataTmp = $filter('filter')(dataTmp, {idTrayek : $rootScope.lisTrayeks[k].id}, true);
                for (var m = 0; m < dataTmp.length; m++) {
                  jmlPenumpang = jmlPenumpang + dataTmp[m].jmlPenumpang;
                  totalBayar = totalBayar + dataTmp[m].totalBayar;
                }
                step1.push({
                  namaTrayek : $rootScope.lisTrayeks[k].value,
                  id : $rootScope.lisTrayeks[k].id,
                  jmlPenumpang : jmlPenumpang,
                  totalBayar : totalBayar
                });
              }
              jmlPenumpang = 0;
              totalBayar = 0;
              for (var n = 0; n < step1.length; n++) {
                jmlPenumpang = jmlPenumpang + step1[n].jmlPenumpang;
                totalBayar = totalBayar + step1[n].totalBayar;
              }
              datas.unshift({
                date : dates[j],
                ms : (new Date(dates[j])).getTime(),
                jmlPenumpang : jmlPenumpang,
                totalBayar : totalBayar,
                detail : step1
              });
            }
            jmlPenumpang = 0;
            totalBayar = 0;
            for (var p = 0; p < datas.length; p++) {
              jmlPenumpang = jmlPenumpang + datas[p].jmlPenumpang;
              totalBayar = totalBayar + datas[p].totalBayar;
            }
            final.push({
              namaAgen : $rootScope.listAgens[i].value,
              id : $rootScope.listAgens[i].id,
              jmlPenumpang : jmlPenumpang,
              totalBayar : totalBayar,
              detail : datas
            });
          }
          if (agen && agen.id !== 0) {
            final = $filter('filter')(final, {id : agen.id}, true);
          }

          return final;
        };


        return {
          getData : getData
        };
      }

})();
