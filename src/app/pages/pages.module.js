(function () {
  'use strict';

  angular.module('BlurAdmin.pages', [
    'ui.router',
    'BlurAdmin.pages.dashboard',
    'BlurAdmin.pages.home',
    'BlurAdmin.pages.login',
    'BlurAdmin.pages.profile',
    'BlurAdmin.pages.informasi',
    // 'BlurAdmin.pages.jadwal',
    // 'BlurAdmin.pages.keuangan',
    'BlurAdmin.pages.laporan',
    'BlurAdmin.pages.today',
    'BlurAdmin.pages.transaksi',
    'BlurAdmin.pages.akap',
    'BlurAdmin.pages.akdp',
    'BlurAdmin.pages.pariwisata',
    'BlurAdmin.pages.paket',
    'BlurAdmin.pages.travel',
    'BlurAdmin.pages.otodata',
 ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    $urlRouterProvider.otherwise('/dashboard/today');


  }

})();
