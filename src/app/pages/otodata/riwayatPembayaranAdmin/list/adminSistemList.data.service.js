(function () {

  angular
    .module('BlurAdmin.pages.otodata')
    .service('adminSistemListData', adminSistemListData);

    function adminSistemListData() {

      var getTotalAdminSistem = function (d) {
        console.log(d);
        var total = 0;
        d.iv.forEach(function (data) {
          total = total + data.alreadyPayed;
        });
        return total;
      };

      return {
        getTotalAdminSistem : getTotalAdminSistem
      };

    }

})();
