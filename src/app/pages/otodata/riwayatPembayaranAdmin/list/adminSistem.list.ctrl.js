(function () {

  angular
    .module('BlurAdmin.pages.otodata')
    .controller('adminSistemListCtrl', adminSistemListCtrl);

    function adminSistemListCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, $timeout, adminSistemListData, getDatePeriod) {

      $state.current.tag = 'D.2';

      $scope.months = getDatePeriod.months();

      $scope.years = getDatePeriod.years();

      $scope.tampil = function() {
        $scope.empty = true;
        $scope.message = "";

        $scope.monthDate = getDatePeriod.getMonthDate($scope.bulan, $scope.tahun);
        var tglAwal = $scope.monthDate[0].split("-");
        var tglAkhir = $scope.monthDate[$scope.monthDate.length-1].split("-");

        tglAwal[0] = $scope.tahun;
        tglAkhir[0] = $scope.tahun;

        $scope.tglAwal = tglAwal.join("-");
        $scope.tglAkhir = tglAkhir.join("-");

        $scope.loadData = true;
        $scope.active = 1;
        var perHal = 5;
        var totalGrup = 0;
        var page;
        var perPage = 10;

        $http.get(API_ENDPOINT.url + '/po/' + $scope.user.idPo + '/history/invoice?tglAwal=' + $scope.tglAwal + '&tglAkhir=' + $scope.tglAkhir + '&page=0&per_page=' + perPage).success(function (data) {

          $scope.total = adminSistemListData.getTotalAdminSistem(data);
          console.log($scope.total);

          // start pagination
          $scope.datas = data.iv;
          if (data.totalPageSize <= perPage) {
            page = 1;
          } else {
            // untuk perhitungan total page
            page = Math.ceil((data.totalPageSize/perPage));
          }

          // membuat validasi keluaran page berdasarkan totalpage
          if(page > perHal){
            // perhitungan page, jadi 16 page / 5 perhal = didapat 4 grup
            totalGrup = Math.ceil((page/perHal));
            $scope.totalGrup = totalGrup
          } else{
            totalGrup = 1;
          }

          // perhitungan pengelompokan tiap grup berisi 5 page
          var grup = []
          for (var i = 1; i <= totalGrup; i++) {
            var subgrup = []
            for (var j = 1; j <= page; j++) {
              if(j > (i-1)*perHal && (j <= i*perHal && j <= (i)*perHal)){
                // subgrup.push(j)
                subgrup.push({
                  page : j,
                  url : API_ENDPOINT.url + '/po/' + $scope.user.idPo + '/history/invoice?tglAwal=' + $scope.tglAwal + '&tglAkhir=' + $scope.tglAkhir + '&page='+ (j-1) + '&per_page=' + perPage,
                  active : j == 1 ? true : false,
                });
              }
            }
            grup.push(subgrup)
          }
          // total pengelompokkan grup dari totalGrup, yaitu per grup berisi 5 page
          console.log(grup);

          $scope.pageActive = 0

          $scope.pages = grup[$scope.pageActive]
          $scope.totalHal = []

          $scope.selectPage = function (page) {
            $scope.loadData = true;
            $http.get(page.url).success(function (data) {
              $scope.total = adminSistemListData.getTotalAdminSistem(data);
              $scope.datas = data.iv
              $scope.loadData = false;

              for (var i = 0; i < $scope.pages.length; i++) {
                if ($scope.pages[i].page === page.page) {
                  $scope.pages[i].active = true;
                  $scope.active = $scope.pages[i].page ;
                } else {
                  $scope.pages[i].active = false;
                }
              }

            })
          }

          $scope.nextPage = function(next) {
            $scope.pages = []
            $scope.pageActive = $scope.pageActive+1
            $scope.pages = grup[$scope.pageActive]
            $scope.selectPage($scope.pages[0])
            console.log($scope.pages);
          }

          $scope.prevPage = function(next) {
            $scope.pages = []
            $scope.pageActive = $scope.pageActive-1
            $scope.pages = grup[$scope.pageActive]
            $scope.selectPage($scope.pages[$scope.pages.length - 1])
          }
          // end pagination

          $scope.loadData = false;
          if ($scope.datas.length === 0) {
            $scope.empty = true;
            $scope.message = "Tidak Ada Data Pembayaran";
            $scope.pages = false;
          } else {
            $scope.empty = false;
            $scope.templateUrl = "/app/pages/otodata/riwayatPembayaranAdmin/list/adminSistem.list.detail.html";
          }
          $scope.loadData = false;

          $scope.next = function (date) {
            $state.go('dashboard.otodata.transaksi', {date : date});
          };



        }).error(function () {
          $scope.loadData = false;
          $scope.empty = true;
          $scope.message = "Tidak Ada Data";
        });
      };

    }
})();
