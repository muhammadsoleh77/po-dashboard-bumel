(function () {

  angular
    .module('BlurAdmin.pages.otodata')
    .service('getDatePeriod', getDatePeriod);

    function getDatePeriod() {


      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth();
      var dd = date.getDate();
      var d = date.getDay();

      var months = function () {
        return [
          { name: 'Januari', days : 31, mm : "01", index : 1 },
          { name: 'Februari', days : yyyy%4===0 ? 29 : 28, mm : "02", index : 2 },
          { name: 'Maret', days : 31, mm : "03", index : 3 },
          { name: 'April', days : 30, mm : "04", index : 4 },
          { name: 'Mei', days : 31, mm : "05", index : 5 },
          { name: 'Juni', days : 30, mm : "06", index : 6 },
          { name: 'Juli', days : 31, mm : "07", index : 7 },
          { name: 'Agustus', days : 31, mm : "08", index : 8 },
          { name: 'September', days : 30, mm : "09", index : 9 },
          { name: 'Oktober', days : 31, mm : "10", index : 10 },
          { name: 'November', days : 30, mm : "11", index : 11 },
          { name: 'Desember', days : 31, mm : "12", index : 12 },
        ];
      };

      var years = function () {
        var thisYear = new Date().getFullYear();
        var firstYear = 2017;
        var res = [];

        for (var i = firstYear; i <= thisYear; i++) {
            res.push(i);
        }

        return res;

      };

      var getWeekDate = function (startDate, move, add) {
        date = new Date(startDate);
        yyyy = date.getFullYear();
        mm = date.getMonth();
        dd = date.getDate();
        d = date.getDay();
        var bln, tgl;
        if (mm < 10) {
          bln = '0' + (mm + 1);
        }
        if (dd < 10) {
          tgl = '0' + dd;
        } else {
          tgl = dd;
        }

        var days = 7;
        if (add) {
          days = 8;
        }

        var weekDate = [];
        for (var i = 0; i < days; i++) {
          var fullDate;
          if (move === 1) {
            fullDate = new Date(yyyy, mm, dd+i);
          } else {
            fullDate = new Date(yyyy, mm, dd-i);
          }
          var y2 = fullDate.getFullYear();
          var m2 = fullDate.getMonth() + 1;
          var d2 = fullDate.getDate();
          if (m2 < 10) {
            bln = '0' + m2;
          } else {
            bln = m2;
          }
          if (d2 < 10) {
            tgl = '0' + d2;
          } else {
            tgl = d2;
          }
          date = y2 + '-' + bln + '-' + tgl;
          weekDate.push(date);
        }
        return weekDate;
      };

      var get2WeekDate = function (startDate) {
        var week = getWeekDate(startDate, -1);
        var firstWeek = getWeekDate(week[6], 1);
        var secondWeek = getWeekDate(firstWeek[6], 1, true);
        // console.log(firstWeek);
        // console.log(secondWeek);
        secondWeek.splice(0,1);
        var twoWeeks = firstWeek.concat(secondWeek);
        // console.log(twoWeeks);
        return twoWeeks;
      };

      var getMonthDate = function (thisMonth,year) {
        var monthDate = [];
        for (var i = 1; i <= thisMonth.days; i++) {
          var fullDate = new Date(year, thisMonth.index-1, i);
          var y2 = fullDate.getFullYear();
          var m2 = fullDate.getMonth()+1;
          var d2 = fullDate.getDate();
          if (m2 < 10) {
            bln = '0' + m2;
          } else {
            bln = m2;
          }
          if (d2 < 10) {
            tgl = '0' + d2;
          } else {
            tgl = d2;
          }
          date = y2 + '-' + bln + '-' + tgl;
          monthDate.push(date);
        }
        return monthDate;
      };

      return {
        getWeekDate : getWeekDate,
        get2WeekDate : get2WeekDate,
        getMonthDate : getMonthDate,
        months : months,
        years : years
      };

    }

})();
