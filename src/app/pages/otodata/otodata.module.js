(function () {
  'use strict';

  angular.module('BlurAdmin.pages.otodata', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.otodata', {
          url: '/otodata',
          title: 'OTODATA',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              icon: 'ion-power',
              order: 206
            }
        })
        .state('dashboard.otodata.invoice', {
          url: '/historyPembayaranAdmin',
          title: 'Riwayat Pembayaran Admin',
          templateUrl: 'app/pages/otodata/riwayatPembayaranAdmin/list/adminSistem.list.template.html',
          controller: "adminSistemListCtrl",
          controllerAs: "adminSistemListCtrl",
          sidebarMeta: {
              order: 1
            }
        })
              .state('dashboard.otodata.transaksi', {
              url: '/setoran/invoice?date',
              templateUrl: 'app/pages/otodata/riwayatPembayaranAdmin/detail/adminSistem.detail.template.html',
              title: 'E-Invoice',
              controller: "invoiceCtrl",
              controllerAs: "invoiceCtrl",
              params : {
                date : ''
              }
            })
        .state('dashboard.otodata.bdb', {
          url: '/historyBdb',
          title: 'Riwayat Transaksi BDB',
          templateUrl: 'app/pages/otodata/riwayatTransaksiBdb/bdb.template.html',
          controller: 'riwayatBdbCtrl',
          controllerAs: 'riwayatBdbCtrl',
          sidebarMeta: {
            order: 2
          }
        });
        // .state('dashboard.otodata.riwayat', {
        //   url: '/invoice',
        //   title: 'Invoice',
        //   templateUrl: 'app/pages/otodata/riwayat/riwayat.template.html',
        //   controller: 'riwayatCtrl',
        //   controllerAs: 'riwayatCtrl',
        //   sidebarMeta: {
        //       order: 2
        //     }
        // })
        // .state('dashboard.otodata.tagihan', {
        //   url: '/tagihan',
        //   title: 'Tagihan',
        //   templateUrl: 'app/pages/otodata/tagihan/otodata.tagihan.html',
        //   controller: 'tagihanCtrl',
        //   sidebarMeta: {
        //       order: 3
        //     }
        // })
        // .state('dashboard.otodata.invoicePembatalan', {
        //   url: '/invoicePembatalan',
        //   title: 'Invoice Pembatalan',
        //   templateUrl: 'app/pages/otodata/invoicePembatalan/otodata.invoicePembatalan.html',
        //   controller: 'invoicePembatalanCtrl',
        //   sidebarMeta: {
        //       order: 4
        //     }
        // })
  }

})();
