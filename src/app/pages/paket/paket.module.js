(function () {
  'use strict';

  angular.module('BlurAdmin.pages.paket', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.paket', {
          url: '/paket',
          title: 'Paket',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              icon: 'ion-bag',
              order: 208
            }
        });
  }

})();
