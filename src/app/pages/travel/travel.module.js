(function () {
  'use strict';

  angular.module('BlurAdmin.pages.travel', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.travel', {
          url: '/travel',
          title: 'Travel',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              icon: 'ion-briefcase',
              order: 208
            }
        });
  }

})();
