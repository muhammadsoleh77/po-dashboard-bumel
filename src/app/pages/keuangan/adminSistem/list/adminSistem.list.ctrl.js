(function () {

  angular
    .module('BlurAdmin.pages.transaksi')
    .controller('adminSistemListCtrl', adminSistemListCtrl);

    function adminSistemListCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, $timeout, adminSistemListData) {

      $state.current.tag = 'D.2';

      $scope.loadData = true;
      $scope.empty = true;
      $scope.message = "";
      $http.get(API_ENDPOINT.url + '/po/invoice/tracker/unpayed?idpo=' + $rootScope.user.idPo).success(function (data) {
        
        $scope.datas = data;
        $scope.loadData = false;
        $scope.total = adminSistemListData.getTotalAdminSistem(data);
        if ($scope.datas.length === 0) {
          $scope.empty = true;
          $scope.message = "Tidak Ada Data Pembayaran";
        } else {
          $scope.empty = false;
        }
        $scope.loadData = false;
        $scope.next = function (date) {
          $state.go('dashboard.transaksi.invoice', {date : date});
        };
      }).error(function () {
        $scope.loadData = false;
        $scope.empty = true;
        $scope.message = "Tidak Ada Data Pembayaran";
      });

    }
})();
