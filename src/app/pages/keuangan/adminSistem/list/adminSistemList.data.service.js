(function () {

  angular
    .module('BlurAdmin.pages.transaksi')
    .service('adminSistemListData', adminSistemListData);

    function adminSistemListData() {

      var getTotalAdminSistem = function (d) {
        var total = 0;
        d.forEach(function (res) {
          total = total + res.totalBayar;
        });
        return total;
      };

      return {
        getTotalAdminSistem : getTotalAdminSistem
      };

    }

})();
