(function () {
  angular.module('BlurAdmin.pages.laporan')
  .controller('penerimaanPemesananCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {

    $state.current.tag = 'D.1.2';

    var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 1) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;
    var now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);

    $scope.loadData = false;

    $scope.noData = {
      status : true,
      message : ""
    };

    $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/trayeks').success(function (data) {
      $scope.lisTrayeks = [];
      $scope.trayeks = [
      ];
      data.forEach(function (doc) {
        $scope.trayeks.push({
          name : doc.trayek,
          value : doc.trayek
        });
        $scope.lisTrayeks.push({
          name : doc.trayek,
          value : doc.trayek
        });
      });
    });

   var paginationOptions = {
      pageNumber: 0,
      pageSize: 25,
      sort: null
    };

    $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/agen').success(function (data) {
      $scope.listAgens = [];
      $scope.agens = [];
      data.forEach(function (doc) {
        $scope.agens.push({
          name : doc.namaChannel,
          value : doc.namaChannel
        });
        $scope.listAgens.push({
          name : doc.namaChannel,
          value : doc.namaChannel
        });
      });
    });

    // $scope.trayek = {};
    $scope.gridOptions = {
      enableColumnResizing: true,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      showColumnFooter: false,
      useExternalSorting: false,
      enableRowSelection: true,
      // expandableRowTemplate: 'app/pages/laporan/expandGrid.html',
      expandableRowHeight: 150,
      columnDefs: [
        { name: 'no', displayName:'No', width: 70},
        { name: 'namaTrayek', displayName:'Trayek'},
        { name: 'tglBerangkat', displayName:'Tanggal Berangkat', cellFilter:'date_v1', width: 160},
        { name: 'jamBerangkat', displayName:'Jam Berangkat', width: 160},
        { name: 'kursiTerpesan', displayName:'Jumlah Penumpang', cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.kursiTerpesan }}</p>',  aggregationType: uiGridConstants.aggregationTypes.sum, width: 160},
        { name: 'jmlPemesanan', displayName:'Total Pemesanan (Rp)',  aggregationType: uiGridConstants.aggregationTypes.sum, footerCellFilter: "currency:'Rp.':0", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.jmlPemesanan | currency:"":0 }}</p>',},

      ],
      exporterCsvFilename: 'Jumlah Penjualan Per Agen ' + $scope.hariIni +  '.xls',
      exporterPdfDefaultStyle: {fontSize: 11, alignment: 'left' },
      exporterPdfTableStyle: {margin: [-10, 30, 30, 30]},
      exporterPdfTableHeaderStyle: {fontSize: 12, bold: true, italics: false, color: 'white', alignment: 'center', fillColor: 'orange'},
      exporterPdfHeader: {
        text : [
          {text: "Jumlah Penjualan Per Agen", fontSize: 16, alignment: 'center', margin: [0, 15, 0, 0]},
          '\n' + $scope.hariIni,
        ],
         margin: [0, 8, 0, 20],
         alignment: 'center'
      },
      exporterPdfFooter: function ( currentPage, pageCount ) {
        return { text: currentPage.toString() + ' dari ' + pageCount.toString(), style: 'footerStyle' };
      },
      exporterPdfCustomFormatter: function ( docDefinition ) {
        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
        return docDefinition;
      },
      exporterPdfOrientation: 'portrait',
      exporterPdfPageSize: 'Letter',
      exporterIsExcelCompatible: true,
      exporterOlderExcelCompatibility: true,
      exporterPdfMaxGridWidth: 500,
      exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
          if (sortColumns.length === 0) {
            paginationOptions.sort = null;
          } else {
            paginationOptions.sort = sortColumns[0].sort.direction;
          }
          getPage();
        });
        gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          paginationOptions.pageNumber = newPage-1;
          paginationOptions.pageSize = pageSize;
          getPage();
        });
         gridApi.selection.on.rowSelectionChanged($scope,function(row){
          var msg = 'row selected ' + row.isSelected;
          $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
          $log.log(msg);
        });

        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
          var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
          $log.log(msg);
        });
        //

      }
    };

    $scope.exportPdf = function(){
      $scope.gridOptions.exporterFieldCallback =  function (grid, row, col, input) {
        if (col.cellFilter) {
          input = $filter("currency")(input, "Rp.", 0);
        }
        return input;
      };
      $scope.gridApi.exporter.pdfExport( 'visible', 'visible' );
    };
    $scope.exportXls = function(){
      $scope.gridOptions.exporterFieldCallback =  function (grid, row, col, input) {
        if (col.cellFilter) {
          input = input;
        }
        return input;
      };
      var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
      $scope.gridApi.exporter.csvExport( 'visible', 'visible', myElement );
    };

    $scope.Delete = function(row) {
        var index = $scope.gridOptions.data.indexOf(row.entity);
        // var data = $scope.gridOptions.data(row);
        if(window.confirm('Apakah anda yakin?')) {
          JadwalRepository.remove(row.entity.idBus).then(function () {
              $scope.subtrayeks = JadwalRepository.getList();
              $scope.gridOptions.data.splice(index, 1);
          });
        }
    };


    var getPage = function() {
      $scope.noData = {
        status : false,
        message : ''
      };
      $scope.loadData = true;
      var url = API_ENDPOINT.url +'/po/setoran/pemesanan?idPo=' + $rootScope.po.idPo + '&tgl=' + now;

      $http.get(url).success(function(data) {

        for (var i = 0; i < data.length; i++) {
          for (var j = 0; j < data[i].transaksiAgen.length; j++) {
            data[i].transaksiAgen[j] =  {
              index : i,
              no : j + 1,
              daftarPenumpang : data[i].transaksiAgen[j].daftarPenumpang,
              jamBerangkat : data[i].transaksiAgen[j].jamBerangkat,
              jmlPemesanan : data[i].transaksiAgen[j].jmlPemesanan,
              kursiTerpesan : data[i].transaksiAgen[j].kursiTerpesan,
              namaTrayek : data[i].transaksiAgen[j].namaTrayek,
              tglBerangkat : data[i].transaksiAgen[j].tglBerangkat,
              transaksi : data[i].transaksiAgen[j].transaksi,
            };
          }
        }

        var datas = $filter('filter')(data, {namaAgen : $scope.agen});

        if (datas.length > 0) {
          $scope.gridOptions.data = datas[0].transaksiAgen;
        } else {
          $scope.gridOptions.data = [];
          $scope.loadData = false;
        }

        if ($scope.gridOptions.data.length === 0) {
          $scope.noData = {
            status : true,
            message : "Tidak Ada Data Setoran Pemesanan"
          };
        }

        $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           if (($scope.gridOptions.data.length * rowHeight + headerHeight) > 400) {
             height = 400;
           } else {
             height = $scope.gridOptions.data.length * rowHeight + headerHeight;
           }
           return {
              height: height + "px"
           };
         };

        $scope.loadData = false;
        $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;

        var firstRow;
        if (paginationOptions.pageNumber>=1) {
          firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
        } else {
          firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
        }
        // $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });
    };

    $scope.tampil = function(tanggal) {
      if (!$scope.agen) {
        return false;
      } else {
        getPage();
      }
    };
  });
})();
