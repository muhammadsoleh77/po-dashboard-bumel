(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transaksi')
  .factory('CheckoutAgenRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function CheckoutAgenRepository() {
      AbstractRepository.call(this, restangular, 'checkoutagens');
    }

    AbstractRepository.extend(CheckoutAgenRepository);
    return new CheckoutAgenRepository();
  }
])

.factory('JadwalRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function JadwalRepository() {
      AbstractRepository.call(this, restangular, 'jadwals');
    }

    AbstractRepository.extend(JadwalRepository);
    return new JadwalRepository();
  }
])
.controller('setoranAgenCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {
  $state.current.tag = 'D.1';
})
.controller('PenjualanAgenCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {
$scope.channels = Restangular.one('po', $scope.user.idPo).getList('channels').$object;
$scope.channel_change = function(channel) {
  //console.log("cccc");
  $http.get(API_ENDPOINT.url +'/po/'+ $scope.user.idPo +'/agen').success(function(data) {
      $scope.agens = data;
  });
 };

var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };

  // $scope.trayek = {};
  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    enableRowSelection: true,
    // expandableRowTemplate: 'views/expandGrid.html',
    // expandableRowHeight: 150,
    columnDefs: [
      { name: 'tglsetor'},
      { name: 'keterangansetor' },
      { name: 'subtrayek'},
      { name: 'setor'},

      {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ng-click="grid.appScope.aktif(row)" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></a></span>'+
      '&nbsp  <span><a ui-sref="app.checkoutagens.terima({id:row.entity.id})" class="btn btn-warning"><i class="glyphicon glyphicon-check"></i></a></span>'}
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
       gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      // gridApi.selection.on.rowSele


      ctionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      //

    }
  };


  $scope.aktif = function(row) {
    var index = $scope.gridOptions.data.indexOf(row.entity);
    var checkoutagen = {
        id: row.entity.id,
        terima: row.entity.setor,
        tglterima: new Date()
      };

      $http.put(API_ENDPOINT.url +'/checkoutagens', checkoutagen);
      $scope.gridOptions.data.splice(index, 1);
  };

  $scope.terimasemua = function () {
    for (var i=0;i<$scope.mySelectedRows.length;i++){
      var checkoutagen = {
        id: $scope.mySelectedRows[i].id,
        terima: $scope.mySelectedRows[i].setor,
        tglterima: new Date()
      };
      $http.put(API_ENDPOINT.url +'/checkoutagens', checkoutagen);
      var index = $scope.gridOptions.data.indexOf($scope.mySelectedRows[i]);
      $scope.gridOptions.data.splice(index, 1);
    }
  };

  var getPage = function() {
    var url;
    url =  API_ENDPOINT.url+'/po/'+$scope.user.idPo+'/checkoutagens';
    // if ($scope.agen==0 || null || undefined) {
    //   url = API_ENDPOINT.url+'/po/'+$scope.user.idPo+'/checkoutagens';
    // } else {
    //     url = API_ENDPOINT.url+'/agen/'+$scope.agen+'/checkoutagens';
    // }
//?page='+paginationOptions.pageNumber+'&per_page='+paginationOptions.pageSize;
    $http.get(url).success(function(data) {
      $scope.gridOptions.data = data;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      var firstRow;
      if (paginationOptions.pageNumber>=1) {
        firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });

  };

  //console.log("i am here!");

  $scope.tampil = function() {
    getPage();
    // url =  API_ENDPOINT.url+'/po/'+$scope.user.idPo+'/checkoutagens';
  };
})

.controller('PemesananAgenCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {
$scope.channels = Restangular.one('po', $scope.user.idPo).getList('channels').$object;
$scope.channel_change = function(channel) {
  $http.get(API_ENDPOINT.url +'/po/'+$scope.user.idPo+'/agen').success(function(data) {
      $scope.agens = data;
  });
 };

var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };

  // $scope.trayek = {};
  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    enableRowSelection: true,
    // expandableRowTemplate: 'views/expandGrid.html',
    // expandableRowHeight: 150,
    columnDefs: [
      { name: 'tgl'},
      { name: 'keterangansetor' },
      { name: 'setor'},

      {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ng-click="grid.appScope.aktif(row)" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></a></span>'+
      '&nbsp  <span><a ui-sref="app.checkoutagens.terima({id:row.entity.id})" class="btn btn-warning"><i class="glyphicon glyphicon-check"></i></a></span>'}
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
       gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      //

    }
  };


  $scope.aktif = function(row) {
    var index = $scope.gridOptions.data.indexOf(row.entity);
    var checkoutagen = {
        id: row.entity.id,
        terima: row.entity.setor,
        tglterima: new Date()
      };

      $http.put(API_ENDPOINT.url +'/checkoutagens', checkoutagen);
      $scope.gridOptions.data.splice(index, 1);
  };

  $scope.terimasemua = function () {
    for (var i=0;i<$scope.mySelectedRows.length;i++){
      var checkoutagen = {
        id: $scope.mySelectedRows[i].id,
        terima: $scope.mySelectedRows[i].setor,
        tglterima: new Date()
      };
      $http.put(API_ENDPOINT.url +'/checkoutagens', checkoutagen);
      var index = $scope.gridOptions.data.indexOf($scope.mySelectedRows[i]);
      $scope.gridOptions.data.splice(index, 1);
    }
  };

  var getPage = function(agen) {
    var url;
    url = API_ENDPOINT.url + '/checkoutagens/' + agen;
    // if ($scope.agen==0 || null || undefined) {
    //   url = API_ENDPOINT.url+'/po/'+$scope.user.idPo+'/checkoutpemesananagens';
    // } else {
    //     url = API_ENDPOINT.url+'/agen/'+$scope.agen+'/checkoutpesanagens';
    // }
//?page='+paginationOptions.pageNumber+'&per_page='+paginationOptions.pageSize;
    $http.get(url).success(function(data) {
      $scope.gridOptions.data = data;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      var firstRow;
      if (paginationOptions.pageNumber>=1) {
        firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });

  };

  $scope.tampil = function(agen) {
    getPage(agen);
  };
})


.controller('PerimaanAgenCtrl', function ($filter, $scope, $stateParams, $location, CheckoutAgenRepository, $rootScope, $http, API_ENDPOINT, $state) {
  // $scope.tambah = false;
  $scope.checkout = CheckoutAgenRepository.get($stateParams.id).then(function (data) {
    $scope.checkout = data;
    $scope.checkout.tglsetor = $filter('date')(data.tglsetor, 'yyyy-MM-dd');
    $scope.agen = {id: data.idchannel, nama: data.namachannel, lokasi: data.lokasichannel};
  });
  $scope.save = function () {
    $scope.checkout.tglsetor = $filter('date')($scope.checkout.tglsetor, 'yyyy-MM-ddTHH:mm:ss.sssZ');
    $scope.checkout.tglterima = new Date();
    $http.put(API_ENDPOINT.url +'/checkoutagens', $scope.checkout);
      $state.go('app.checkoutagens.listpenjualan');

  };
})

.controller('RealisasiPenjualanCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {


 var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };

  // $scope.trayek = {};
  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    expandableRowTemplate: 'app/pages/expandGrid.html',
    expandableRowHeight: 150,
    columnDefs: [
      { name: 'kodetrayek', displayName:'Trayek'},
      { name: 'kodesubtrayek', displayName:'Sub Trayek'},
      { name: 'namabus', displayName:'Kode/Plat Bus'},
      { name: 'jumlahkursi', displayName: 'Jumlah Kursi'},
      { name: 'seatsold', displayName:'Kursi Terjual'},
      { name: 'totalpenjualan', cellFilter: 'currency:"Rp.":0', displayName:'Total Penjualan'},

    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
      //

    }
  };

  $scope.Delete = function(row) {
      var index = $scope.gridOptions.data.indexOf(row.entity);
      // var data = $scope.gridOptions.data(row);
      if(window.confirm('Apakah anda yakin?')) {
        JadwalRepository.remove(row.entity.idBus).then(function () {
            $scope.subtrayeks = JadwalRepository.getList();
            $scope.gridOptions.data.splice(index, 1);
        });
      }
  };


  var date = new Date();
  var yyyy = date.getFullYear();
  var mm = date.getMonth() + 1;
  var dd = date.getDate();
  if (mm < 10) {
    mm = '0' + mm;
  }
  if (dd < 1) {
    dd = '0' + dd;
  }
  //console.log(dd);
  //console.log(dd.length);
  //console.log(mm);
  //console.log(mm.length);
  var now = yyyy + '-' + mm + '-' + dd;
  // //console.log(now);
  $scope.tanggal = now; // tanggal sekarang

  var getPage = function() {
    var url = API_ENDPOINT.url +'/jadwaltransaksipo?idpo='+$scope.user.idPo+"&tgl=" + $scope.tanggal;
    //console.log(url);

    $http.get(url).success(function(data) {
      // $scope.gridOptions.data = data.content;
      for(var i = 0; i < data.length; i++){
        data[i].subGridOptions = {
          columnDefs: [ {name:"Asal", field:"namaTempatBrgkt"},
          {name:"Tujuan", field:"kotaTujuan"},
          {name:"Jumlah Penumpang", field:"jmlPenumpang"},
          {name:"Total Bayar", cellFilter: 'number', field:"totalBayar"}],
          data: data[i].transaksi
        };
      }
      $scope.gridOptions.data = data;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      var firstRow;
      if (paginationOptions.pageNumber>=1) {
        firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

  });
  };

  $scope.tampil = function() {
    getPage();
  };

})

.controller('penjualanharianCtrl', function ($scope, $uibModal, $state, $http, API_ENDPOINT, $rootScope, $stateParams) {

  $state.current.parrentState = 'dashboard.keuangan.penjualanagen';
  $state.current.parrentTitle = 'Penerimaan Setoran Penjualan';
  $state.current.tag = 'E.1.' + $stateParams.id;

  var date = new Date();
  var yyyy = date.getFullYear();
  var mm = date.getMonth();
  var dd = date.getDate();
  var d = date.getDay();

  var bln, tgl;

  if (mm < 10) {
    bln = '0' + (mm + 1);
  }
  if (dd < 1) {
    tgl = '0' + dd;
  } else {
    tgl = dd;
  }

  var now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);

  $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.po.idPo  + '&tgl=' + now).success(function (data) {
    var detail = data[$stateParams.id];
    //console.log(detail);
    $scope.data = detail;
    $scope.data.biayaOp = [];
    $scope.data.keteranganBiaya = "Penggunaan/Biaya";
    $scope.subtotal = 0;
    $scope.data.diterima = $scope.data.setoran - $scope.subtotal;
  });

  $scope.biayaTambahan = [];

  $scope.openModal = function (data) {
    $uibModal.open({
    animation: true,
    templateUrl: '/app/pages/today/modals/form-modal.html',
    controller: 'penjualanModalsCtrl',
    size: 'md',
    resolve: {
      items: function () {
        return data;
      }
    }
  }).result.then(function (tambahan) {
    if (tambahan.nominal === "") {
      tambahan.nominal = 0;
    }
    $scope.biayaTambahan.push(tambahan);
    //console.log($scope.biayaTambahan);
    $scope.data.diterima = $scope.data.diterima - tambahan.nominal;
  });
  };



  $scope.openTable = function () {
    $scope.detailPenumpang = [];
    for (var x=0;x<$scope.data.transaksi.length;x++) {
      $scope.detailPenumpang[x] = $scope.data.transaksi[x].transaksi;
      $scope.detailPenumpang[x].Penumpang = $scope.data.transaksi[x].daftarPenumpang;
    }
    //console.log($scope.detailPenumpang);

    $uibModal.open({
      animation: true,
      templateUrl: '/app/pages/today/modals/table-modal.html',
      controller: 'penjualanModalsCtrl',
      size: 'lg',
      resolve: {
        items: function () {
          return $scope.detailPenumpang;
        }
      }
    });
  };

  $scope.update = function () {
    $scope.biayaTambahan.forEach(function (doc) {
      if (doc.nominal === undefined || doc.nominal === null || doc.nominal === "") {
        doc.nominal = 0 ;
      }
      $scope.subtotal = $scope.subtotal + doc.nominal;
    });
    $scope.data.setoran = $scope.data.jmlPenjualan - $scope.data.komisiAgen;
    $scope.data.diterima = $scope.data.setoran - $scope.subtotal;
    // return parseInt($scope.data.diterima);
  };

  $scope.remove = function (index) {
    $scope.data.diterima = $scope.data.diterima + parseInt($scope.biayaTambahan[index].nominal);
    $scope.biayaTambahan.splice(index, 1);
    // $scope.data.diterima = $scope.update();
  };

  $scope.kirim = function () {
    $scope.biayaTambahan.forEach(function (doc) {
      $scope.data.biayaOp.push({
        keteranganBiaya : doc.namaBiaya,
        kodeBiaya : doc.idBiayaPo,
        jumlah : parseInt(doc.nominal)
      });
    });
    //console.log($scope.data);
    $http.post(API_ENDPOINT.url + '/po/penerimaan/penjualan', $scope.data).success(function (data) {
      alert("Berhasil Tersimpan");
      $state.go('dashboard.today');
    });
  };

  $scope.kembali = function () {
    $state.go('dashboard.keuangan.penjualanagen');
  };

})

.controller('penjualanModalsCtrl', function ($http, $scope, $uibModalInstance, items, uiGridConstants, API_ENDPOINT, $rootScope) {

  $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/biaya').success(function (data) {
    $scope.listBiaya = data;
  });

  $scope.l = function () {
    console.log($scope.jenisBiaya);
  };

  $scope.tambahan = {
    namaBiaya : '',
    idBiayaPo : '',
    nominal : 0
  };

  $scope.submit = function () {
    if (!$scope.jenisBiaya) {
      return false;
    } else {
      $scope.tambahan.namaBiaya = $scope.jenisBiaya.namaBiaya;
      $scope.tambahan.idBiayaPo = $scope.jenisBiaya.idBiayaPo;
      $scope.ok();
    }
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.tambahan);
  };

  var paginationOptions = {
      pageNumber: 0,
      pageSize: 50,
      sort: null
    };

  $scope.gridOptions = {
    enableColumnResizing: true,
    useExternalSorting: false,
    paginationPageSizes: [50, 75],
    paginationPageSize: 50,
    useExternalPagination: true,
    showColumnFooter: false,
    enableRowSelection: true,
    expandableRowTemplate: 'app/pages/expandGrid.html',
    expandableRowHeight: 150,
    columnDefs: [
      { name: 'No.Kursi', field: 'nomorKursi', width: 90 },
      { name: 'Nama', field: 'nama' },
      { name: 'Jenis Kelamin', field: 'gender' },
      { name: 'Kategori Usia', field: 'kategoriUsia' },
      { name: 'No.Telepon', field: 'telepon', cellFilter:'is_anak:row.entity.kategoriUsia' },
      { name: 'Agan Beli', field: 'agenBeli' },
      { name: 'Waktu Transaksi', field: 'tglBayar', cellFilter: 'date:"HH:mm":"+0000"' },
    ],
    exporterCsvFilename: 'myFile.xls',
    exporterPdfDefaultStyle: {fontSize: 11, alignment: 'center' },
    // exporterPdfTableStyle: {margin: [0, 30, 30, 30]},
    exporterPdfTableHeaderStyle: {fontSize: 12, bold: true, italics: false, color: 'black', alignment: 'center' },
    exporterPdfHeader: { text: "Data Penjualan", fontSize: 16, style: 'headerStyle', alignment: 'center' },
    exporterPdfFooter: function ( currentPage, pageCount ) {
      return { text: currentPage.toString() + ' dari ' + pageCount.toString(), style: 'footerStyle' };
    },
    exporterPdfCustomFormatter: function ( docDefinition ) {
      docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
      docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
      return docDefinition;
    },
    exporterPdfOrientation: 'portrait',
    exporterPdfPageSize: 'A4',
    exporterIsExcelCompatible: true,
    exporterPdfMaxGridWidth: 500,
    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });

      $scope.exportPdf = function(){
        $scope.gridApi.exporter.pdfExport( 'all', 'all' );
      };
      $scope.exportXls = function(){
        var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
        $scope.gridApi.exporter.csvExport( 'all', 'all', myElement );
      };

      var getPage = function () {
        $scope.title = items.title;
        $scope.gridOptions.data =  items.transaksi;
        return  $scope.gridOptions.data;
      };
      getPage();
    }
  };

})
.controller('pemesananModalsCtrl', function ($http, $scope, $uibModalInstance, items, uiGridConstants, API_ENDPOINT, $rootScope) {

  var paginationOptions = {
      pageNumber: 0,
      pageSize: 50,
      sort: null
    };

  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [50, 75],
    paginationPageSize: 50,
    useExternalPagination: true,
    useExternalSorting: false,
    showColumnFooter: false,
    enableRowSelection: true,
    expandableRowTemplate: 'app/pages/expandGrid.html',
    expandableRowHeight: 150,
    columnDefs: [
      { name: 'No.Kursi', field: 'nomorKursi', width: 90 },
      { name: 'Nama', field: 'nama' },
      { name: 'Jenis Kelamin', field: 'gender' },
      { name: 'Kategori Usia', field: 'kategoriUsia' },
      { name: 'No.Telepon', field: 'telepon', cellFilter:'is_anak:row.entity.kategoriUsia' },
      { name: 'Tanggal Berangkat', field: 'tglBerangkat', cellFilter: 'date_v1' },
      { name: 'Agen Berangkat', field: 'agenBerangkat', },
      { name: 'Trayek', field: 'namaTrayek' },
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        getPage();
      });

      var getPage = function () {
        $scope.title = items.title;
        $scope.gridOptions.data =  items.transaksi;
        return  $scope.gridOptions.data;
      };
      getPage();
    }
  };

})

.controller('pemesananharianCtrl', function ($scope, $uibModal, $state, $http, API_ENDPOINT, $rootScope, $stateParams) {

  $state.current.parrentState = 'dashboard.keuangan.pemesananagen';
  $state.current.parrentTitle = 'Penerimaan Setoran Pemesanan';
  $state.current.tag = 'E.2.' + $stateParams.id;

  var date = new Date();
  var yyyy = date.getFullYear();
  var mm = date.getMonth();
  var dd = date.getDate();
  var d = date.getDay();

  var bln, tgl;

  if (mm < 10) {
    bln = '0' + (mm + 1);
  }
  if (dd < 1) {
    tgl = '0' + dd;
  } else {
    tgl = dd;
  }

  var now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);

  $http.get(API_ENDPOINT.url + '/po/setoran/pemesanan?idPo=' + $rootScope.po.idPo + '&tgl=' + now).success(function (data) {
    var detail = data[$stateParams.id];
    //console.log(detail);
    $scope.data = detail;
    $scope.data.biayaOp = [];
    $scope.data.keteranganBiaya = "Penggunaan/Biaya";
    $scope.subtotal = 0;
    $scope.data.diterima = $scope.data.setoran - $scope.subtotal;
  });
  $scope.biayaTambahan = [];
  $scope.openModal = function (data) {
    $uibModal.open({
    animation: true,
    templateUrl: '/app/pages/today/modals/form-modal.html',
    controller: 'penjualanModalsCtrl',
    size: 'md',
    resolve: {
      items: function () {
        return data;
      }
    }
  }).result.then(function (tambahan) {
    if (tambahan.nominal === "") {
      tambahan.nominal = 0;
    }
    $scope.biayaTambahan.push(tambahan);
    //console.log($scope.biayaTambahan);
    $scope.data.diterima = $scope.data.diterima - tambahan.nominal;
  });
  };

  $scope.openTable = function () {
    $scope.detailPenumpang = [];
    for (var x=0;x<$scope.data.transaksiAgen.length;x++) {
      $scope.detailPenumpang[x] = $scope.data.transaksiAgen[x].transaksi;
      $scope.detailPenumpang[x].Penumpang = $scope.data.transaksiAgen[x].daftarPenumpang;
    }
    //console.log($scope.detailPenumpang);

    $uibModal.open({
      animation: true,
      templateUrl: '/app/pages/today/modals/table-modal.html',
      controller: 'penjualanModalsCtrl',
      size: 'lg',
      resolve: {
        items: function () {
          return $scope.detailPenumpang;
        }
      }
    });
  };

  $scope.update = function () {
    $scope.biayaTambahan.forEach(function (doc) {
      if (doc.nominal === undefined || doc.nominal === null || doc.nominal === "") {
        doc.nominal = 0 ;
      }
      $scope.subtotal = $scope.subtotal + doc.nominal;
    });
    $scope.data.setoran = $scope.data.penjualan - $scope.data.komisiAgen;
    $scope.data.diterima = $scope.data.setoran - $scope.subtotal;
    // return parseInt($scope.data.diterima);
  };

  $scope.remove = function (index) {
    $scope.data.diterima = $scope.data.diterima + parseInt($scope.biayaTambahan[index].nominal);
    $scope.biayaTambahan.splice(index, 1);
    // $scope.data.diterima = $scope.update();
  };

  $scope.kirim = function () {
    $scope.biayaTambahan.forEach(function (doc) {
      // $scope.data.biayaOp = $scope.data.biayaOp + parseInt(doc.nominal);
      $scope.data.biayaOp.push({
        keteranganBiaya : doc.jenis,
        jumlah : parseInt(doc.nominal)
      });
    });
    //console.log($scope.data);
    $http.post(API_ENDPOINT.url + '/po/penerimaan/pemesanan', $scope.data).success(function (data) {
      alert("Berhasil Tersimpan");
      $state.go('dashboard.today');
    });
  };

  $scope.kembali = function () {
    $state.go('dashboard.keuangan.pemesananagen');
  };

})

.controller('setoranAdminHarianCtrl', function ($scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {

  $state.current.tag = 'D.2';

  $scope.gridOptions = {
    enableColumnResizing: true,
    paginationPageSizes: [25, 50, 75],
    paginationPageSize: 25,
    useExternalPagination: true,
    // showColumnFooter: false,
    useExternalSorting: false,
    columnDefs: [
      { name: 'Tanggal Transaksi', field: 'tglTransaksi', cellFilter: 'date_v2'},
      // { name: 'Jumlah HPP', field: 'jmlHpp', cellFilter: 'currency:"Rp.":0' },
      { name: 'Jumlah Komisi (Rp)', field: 'jmlKomisi', cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.jmlKomisi }}</p>' },
      { name: 'Jumlah Setoran Admin (Rp)', field: 'jmlSetoran', cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.jmlSetoran }}</p>' },

      {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ng-click="grid.appScope.setor(row.entity)" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i>&nbsp;Setor</a></span>'}
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
       gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
        $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
        $log.log(msg);
      });

    }
  };
  $scope.setor = function (data) {
    // //console.log(data);
    $http.post(API_ENDPOINT.url + '/po/setor/otodata', data).success(function (data) {
      alert(data.message);
      $state.go('dashboard.today');
    });
  };
  $scope.getPage = function () {
    $http.get(API_ENDPOINT.url + '/po/setoran/otodata?idPo=' + $rootScope.po.idPo).success(function (data) {
      var dataSetoran = [];
      dataSetoran.push(data);
      $scope.gridOptions.data = dataSetoran;
      //console.log(data);
    });
  };
  $scope.getPage();
})

.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue);
            });

            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                elem.val($filter(attrs.format)(plainNumber));
                return plainNumber;
            });
        }
    };
}]);

})();
