(function () {
  angular.module('BlurAdmin.pages.laporan')
  .controller('penerimaanPenjualanCtrl', function (JadwalRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {

    $state.current.tag = 'D.1.1';

    var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

    var date = new Date();
    var yyyy = date.getFullYear();
    var mm = date.getMonth();
    var dd = date.getDate();
    var d = date.getDay();

    var bln, tgl;

    if (mm < 10) {
      bln = '0' + (mm + 1);
    }
    if (dd < 1) {
      tgl = '0' + dd;
    } else {
      tgl = dd;
    }

    $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;
    var now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);

    $scope.loadData = false;

    $scope.noData = {
      status : true,
      message : ""
    };

    $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/trayeks').success(function (data) {
      $scope.lisTrayeks = [];
      $scope.trayeks = [
      ];
      data.forEach(function (doc) {
        $scope.trayeks.push({
          name : doc.trayek,
          value : doc.trayek
        });
        $scope.lisTrayeks.push({
          name : doc.trayek,
          value : doc.trayek
        });
      });
    });

    $http.get(API_ENDPOINT.url + '/po/' + $rootScope.user.idPo + '/armadas').success(function (data) {
      $scope.armadas = data;
    });

   var paginationOptions = {
      pageNumber: 0,
      pageSize: 25,
      sort: null
    };

    // $scope.trayek = {};
    $scope.gridOptions = {
      enableColumnResizing: true,
      paginationPageSizes: [25, 50, 75],
      paginationPageSize: 25,
      useExternalPagination: true,
      showColumnFooter: false,
      useExternalSorting: false,
      enableRowSelection: true,
      // expandableRowTemplate: 'app/pages/laporan/expandGrid.html',
      expandableRowHeight: 150,
      columnDefs: [
        { name: 'no', displayName:'No', width: 70},
        { name: 'namaAgen', displayName:'Nama Agen'},
        { name: 'jmlPenumpang', displayName:'Jumlah Penumpang', cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.jmlPenumpang }}</p>', aggregationType: uiGridConstants.aggregationTypes.sum},
        // { name: 'namaTrayek', displayName:'Trayek'},
        { name: 'totalBayar', displayName:'Total Penjualan (Rp)',  aggregationType: uiGridConstants.aggregationTypes.sum, footerCellFilter: "currency:'Rp.':0", cellTemplate: '<p style="text-align:right;padding-right:20px;margin-bottom:0xp;margin-top:5px;">{{ row.entity.totalBayar | currency:"":0 }}</p>',},

      ],
      exporterCsvFilename: 'Jumlah Penjualan Per Agen ' + $scope.hariIni +  '.xls',
      exporterPdfDefaultStyle: {fontSize: 11, alignment: 'left' },
      exporterPdfTableStyle: {margin: [-10, 30, 30, 30]},
      exporterPdfTableHeaderStyle: {fontSize: 12, bold: true, italics: false, color: 'white', alignment: 'center', fillColor: 'orange'},
      exporterPdfHeader: {
        text : [
          {text: "Jumlah Penjualan Per Agen", fontSize: 16, alignment: 'center', margin: [0, 15, 0, 0]},
          '\n' + $scope.hariIni,
        ],
         margin: [0, 8, 0, 20],
         alignment: 'center'
      },
      exporterPdfFooter: function ( currentPage, pageCount ) {
        return { text: currentPage.toString() + ' dari ' + pageCount.toString(), style: 'footerStyle' };
      },
      exporterPdfCustomFormatter: function ( docDefinition ) {
        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
        return docDefinition;
      },
      exporterPdfOrientation: 'portrait',
      exporterPdfPageSize: 'Letter',
      exporterIsExcelCompatible: true,
      exporterOlderExcelCompatibility: true,
      exporterPdfMaxGridWidth: 500,
      exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
      onRegisterApi: function(gridApi) {
        $scope.gridApi = gridApi;
        $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
          if (sortColumns.length === 0) {
            paginationOptions.sort = null;
          } else {
            paginationOptions.sort = sortColumns[0].sort.direction;
          }
          getPage();
        });
        gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
          paginationOptions.pageNumber = newPage-1;
          paginationOptions.pageSize = pageSize;
          getPage();
        });
         gridApi.selection.on.rowSelectionChanged($scope,function(row){
          var msg = 'row selected ' + row.isSelected;
          $scope.mySelectedRows=$scope.gridApi.selection.getSelectedRows();
          $log.log(msg);
        });

        gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
          var msg = 'rows changed ' + rows.length +"barisnya: "+rows;
          $log.log(msg);
        });
        //

      }
    };

    $scope.exportPdf = function(){
      $scope.gridOptions.exporterFieldCallback =  function (grid, row, col, input) {
        if (col.cellFilter) {
          input = $filter("currency")(input, "Rp.", 0);
        }
        return input;
      };
      $scope.gridApi.exporter.pdfExport( 'visible', 'visible' );
    };
    $scope.exportXls = function(){
      $scope.gridOptions.exporterFieldCallback =  function (grid, row, col, input) {
        if (col.cellFilter) {
          input = input;
        }
        return input;
      };
      var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
      $scope.gridApi.exporter.csvExport( 'visible', 'visible', myElement );
    };

    $scope.Delete = function(row) {
        var index = $scope.gridOptions.data.indexOf(row.entity);
        // var data = $scope.gridOptions.data(row);
        if(window.confirm('Apakah anda yakin?')) {
          JadwalRepository.remove(row.entity.idBus).then(function () {
              $scope.subtrayeks = JadwalRepository.getList();
              $scope.gridOptions.data.splice(index, 1);
          });
        }
    };


    var getPage = function() {
      $scope.loadData = true;
      var url = API_ENDPOINT.url +'/po/setoran/penjualan?idPo=' + $rootScope.po.idPo + '&tgl=' + now;

      $http.get(url).success(function(data) {
        var datas = [];
        // $scope.gridOptions.data = data.content;
        for(var i = 0; i < data.length; i++){
          data[i].subGridOptions = {
            columnDefs: [ {name:"Asal", field:"namaTempatBrgkt"},
            {name:"Tujuan", field:"kotaTujuan"},
            {name:"Jumlah Penumpang", field:"jmlPenumpang"},
            {name:"Total Bayar", cellFilter: 'number', field:"totalBayar"}],
            data: data[i].transaksi
          };
        }

        var getNamaPemesanan = function (idPemesan) {

          if (idPemesan === 0) {
            idPemesan = "Otobus";
          }

          for (var i = 0; i < $scope.agens.length; i++) {
            if ($scope.agens[i].id === idPemesan) {
              idPemesan = $scope.agens[i].name;
            }
          }
          return idPemesan;
        };

        var dataPenjualan = [];
        // data.forEach(function (doc) {
        //   doc.transaksi.forEach(function (doc2) {
        //     if (doc2.transaksi.jenis === 1) {
        //       dataPenjualan.push({
        //         namaBus : doc.namaBus,
        //         namaTrayek : doc.namaTrayek,
        //         idTrayek : doc.idTrayek,
        //         idIssuer : doc2.transaksi.idIssuer,
        //         idPemesan : doc2.transaksi.idIssuer,
        //         idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
        //         namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
        //         jmlPenumpang : doc2.transaksi.jmlPenumpang,
        //         totalBayar : doc2.transaksi.totalBayar,
        //         jenis : doc2.transaksi.jenis,
        //       });
        //     }
        //   });
        // });
        for (i = 0; i < data.length; i++) {
          data[i].transaksi.forEach(function (doc2) {
            if (doc2.transaksi.jenis === 1) {
              dataPenjualan.push({
                namaBus : data[i].namaBus,
                namaTrayek : data[i].namaTrayek,
                index : i,
                idTrayek : data[i].idTrayek,
                idIssuer : doc2.transaksi.idIssuer,
                idPemesan : getNamaPemesanan(doc2.transaksi.idIssuer),
                idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
                namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
                jmlPenumpang : doc2.transaksi.jmlPenumpang,
                totalBayar : doc2.transaksi.totalBayar,
                jenis : "Penjualan",
              });
            } else {
              dataPenjualan.push({
                namaBus : data[i].namaBus,
                namaTrayek : data[i].namaTrayek,
                index : i,
                idTrayek : data[i].idTrayek,
                idIssuer : doc2.transaksi.idIssuer,
                idPemesan : getNamaPemesanan(doc2.transaksi.idIssuer),
                idTempatBrgkt : doc2.transaksi.idTempatBrgkt,
                namaTempatBrgkt : doc2.transaksi.namaTempatBrgkt,
                jmlPenumpang : doc2.transaksi.jmlPenumpang,
                totalBayar : doc2.transaksi.totalBayar,
                jenis : "Pemesanan",
              });
            }
          });
        }
        var dataTmp;
        var jmlPenumpang;
        var totalBayar;
        var jenisTransaksi = ["Penjualan", "Pemesanan"];
        for (var h = 0; h < jenisTransaksi.length; h++) {
          for (i = 0; i < $scope.lisTrayeks.length; i++) {
            for (var j = 0; j < $scope.listAgens.length; j++) {
              jmlPenumpang = 0;
              totalBayar = 0;
              dataTmp = $filter('filter')(dataPenjualan, {jenis : jenisTransaksi[h]});
              dataTmp = $filter('filter')(dataTmp, {namaTrayek : $scope.lisTrayeks[i].value});
              dataTmp = $filter('filter')(dataTmp, {idPemesan : $scope.listAgens[j].value});
              for (var k = 0; k < dataTmp.length; k++) {
                jmlPenumpang = jmlPenumpang + dataTmp[k].jmlPenumpang;
                totalBayar = totalBayar + dataTmp[k].totalBayar;
                namaBus = dataTmp[k].namaBus;
                index = dataTmp[k].index;
                jenis = dataTmp[k].jenis;
              }
              if (jmlPenumpang > 0) {
                datas.push({
                  namaAgen : $scope.listAgens[j].value,
                  namaTrayek : $scope.lisTrayeks[i].value,
                  namaBus : namaBus,
                  index : index,
                  jenis : jenis,
                  no : 0,
                  jmlPenumpang : jmlPenumpang,
                  totalBayar : totalBayar,
                });
              }
            }
          }
        }

        // console.log(datas);

        for (i = 0; i < datas.length; i++) {
          datas[i].no = i + 1;
          if (datas[i].jenis === "Pemesanan") {
            datas[i].namaAgen = datas[i].namaAgen + ' ( Pemesanan )';
            datas[i].totalBayar = 0;
          }
        }

        $scope.gridOptions.data = datas;

        $scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 40; // your header height
           if (($scope.gridOptions.data.length * rowHeight + headerHeight) > 400) {
             height = 400;
           } else {
             height = $scope.gridOptions.data.length * rowHeight + headerHeight;
           }
           return {
              height: height + "px"
           };
         };

        $scope.loadData = false;
        $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
        $scope.refreshData = function () {
          $scope.noData = {
            status : false,
            message : ''
          };

          $scope.gridOptions.data = $filter('filter')(datas, {namaTrayek : $scope.trayek});
          $scope.gridOptions.data = $filter('filter')($scope.gridOptions.data, {namaBus : $scope.armada});

          if ($scope.gridOptions.data.length === 0) {
            $scope.noData = {
              status : true,
              message : "Tidak Ada Data Setoran Penjualan"
            };
          }
          // console.log($scope.errorMesssage);
        };
        $scope.refreshData();
        var firstRow;
        if (paginationOptions.pageNumber>=1) {
          firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
        } else {
          firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
        }
        // $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });
    };

    $scope.tampil = function(tanggal) {
      if (!$scope.armada || !$scope.trayek) {
        return false;
      } else {
        getPage();
      }
    };
  });
})();
