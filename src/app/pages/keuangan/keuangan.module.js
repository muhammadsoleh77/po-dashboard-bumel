// (function () {
//   'use strict';
//   angular.module('BlurAdmin.pages.keuangan', [])
//       .config(routeConfig);
//
//   /** @ngInject */
//   function routeConfig($stateProvider) {
//     $stateProvider
//         .state('dashboard.keuangan', {
//           url: '/keuangan',
//           template : '<div ui-view></div>',
//           abstract: true,
//           title: 'Keuangan',
//           sidebarMeta: {
//             icon: 'ion-cash',
//             order: 203,
//           },
//         })
//         .state('dashboard.keuangan.penjualanagen', {
//         url: '/penerimaan/setoranpenjualan',
//         templateUrl: 'app/pages/keuangan/penerimaan.setoran.penjualan.html',
//         title: 'Penerimaan Setoran Penjualan',
//         sidebarMeta: {
//             order: 1,
//         },
//         controller: "penerimaanPenjualanCtrl",
//           controllerAs: "penerimaanPenjualanCtrl",
//       })
//        .state('dashboard.keuangan.pemesananagen', {
//         url: '/penerimaan/setoranpemesanan',
//         templateUrl: 'app/pages/keuangan/penerimaan.setoran.pemesanan.html',
//         title: 'Penerimaan Setoran Pemesanan',
//           controller: "penerimaanPemesananCtrl",
//           controllerAs: "penerimaanPemesananCtrl",
//           sidebarMeta: {
//             order: 2,
//           },
//       })
//         .state('dashboard.keuangan.penjualanharian', {
//         url: '/penjualanharian/{id}',
//         templateUrl: 'app/pages/keuangan/keuangan.penjualanharian.html',
//         title: 'Penjualan Hari Ini',
//           controller: "penjualanharianCtrl",
//           controllerAs: "penjualanharianCtrl",
//       })
//         .state('dashboard.keuangan.pemesananharian', {
//         url: '/pemesananharian/{id}',
//         templateUrl: 'app/pages/keuangan/keuangan.pemesananharian.html',
//         title: 'Pemesanan Hari Ini',
//           controller: "pemesananharianCtrl",
//           controllerAs: "pemesananharianCtrl",
//       })
//         .state('dashboard.keuangan.setoranadmin', {
//         url: '/setoran/admin',
//         templateUrl: 'app/pages/keuangan/keuangan.setoran-admin.html',
//         title: 'Setoran Admin Hari Ini',
//           controller: "setoranAdminHarianCtrl",
//           controllerAs: "setoranAdminHarianCtrl",
//           sidebarMeta: {
//             order: 5,
//           },
//       });
//
//   }
// })();
