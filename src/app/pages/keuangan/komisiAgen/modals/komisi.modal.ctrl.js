(function () {

  angular
    .module('BlurAdmin.pages.transaksi')
    .controller('komisiModalCtrl', komisiModalCtrl)
    .controller('komisiModalProcessCtrl', komisiModalProcessCtrl);

    function komisiModalCtrl($uibModalInstance, items, $scope, $http, API_ENDPOINT, $rootScope) {

      $scope.width = '30vw!important;';

      $scope.templateUrl = '/app/pages/keuangan/komisiAgen/modals/modal.data.tiket.html';
      $scope.data = items.data;

      $http.get(API_ENDPOINT.url + '/po/transaksikomisiagen/agen/detail?idPo=' + $rootScope.user.idPo + '&idAgen=' + $scope.data.idAgen + '&tgl=' + $scope.data.tglTransaksi).success(function (data) {
        $scope.data.dataTicket = data;
      })
      .error(function () {
        $scope.data.dataTicket = [];
      });

      $scope.close = function () {
        $uibModalInstance.dismiss();
      };

    }

    function komisiModalProcessCtrl($uibModalInstance, items, $scope, $http, API_ENDPOINT, $rootScope, $state) {

      $scope.width = '40vw!important;';

      $scope.data = items.data;
      $scope.templateUrl = '/app/pages/keuangan/komisiAgen/modals/modal.komisi.confirm.html';

      $scope.close = function () {
        $uibModalInstance.dismiss();
      };

      $scope.save = function () {
        $scope.data.diterima = parseInt($scope.data.diterima);
        if ($scope.data.status) {
          $scope.data.status = true;
        } else {
          $scope.data.status = null;
        }
        $http.post(API_ENDPOINT.url + '/po/transaksikomisiagen/bayar', $scope.data).success(function () {
          $state.go('dashboard.transaksi.komisiAgen');
          $uibModalInstance.dismiss();
        });
      };

    }

})();
