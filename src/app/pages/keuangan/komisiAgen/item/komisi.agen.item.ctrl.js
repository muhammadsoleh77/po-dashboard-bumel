(function () {

  angular
    .module('BlurAdmin.pages.transaksi')
    .controller('komisiAgenItemCtrl', komisiAgenItemCtrl);

    function komisiAgenItemCtrl($scope, $rootScope, $http, $state, API_ENDPOINT, $filter, $timeout, adminSistemListData, $uibModal) {

      $state.current.tag = 'D.3.' + $state.params.index;

      if (!$state.params.data) {
        $state.go('dashboard.transaksi.komisiAgen');
      }

      $scope.loadData = true;
      $scope.empty = true;
      $scope.message = "";
      if ($state.params.namaAgen) {
        $scope.namaAgen = $state.params.namaAgen;
      }
      $http.get(API_ENDPOINT.url + '/po/transaksikomisiagen/agen?idPo=' + $rootScope.user.idPo + '&idAgen=' + $state.params.idAgen).success(function (data) {
        $scope.datas = data;
        if ($scope.datas.length === 0) {
          $scope.empty = true;
          $scope.message = "Tidak Ada Data Pembayaran";
        } else {
          $scope.empty = false;
        }

        $scope.loadData = false;

        $scope.bayar = function () {
          $state.go('dashboard.transaksi.komisiAgenProcess', {data : $state.params.data, index : $state.params.index});
        };

        $scope.openTicketData = function (dataTicket) {
          $uibModal.open({
            animation : true,
            templateUrl : '/app/pages/keuangan/komisiAgen/modals/modal.main.html',
            controller : 'komisiModalCtrl',
            size : 'sm',
            resolve : {
              items : function () {
                return {
                  data : dataTicket,
                };
              }
            }
          });
        };
        $scope.next = function (date) {
          $state.go('dashboard.transaksi.invoice', {date : date});
        };
      }).error(function () {
        $scope.loadData = false;
        $scope.empty = true;
        $scope.message = "Tidak Ada Data Pembayaran";
      });

    }
})();
