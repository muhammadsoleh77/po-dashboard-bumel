(function () {
  'use strict';
angular.module('BlurAdmin.pages.pengguna')

/* Define the Repository that interfaces with Restangular */
.factory('UserRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function UserRepository() {
      AbstractRepository.call(this, restangular, 'users');
    }

    AbstractRepository.extend(UserRepository);
    return new UserRepository();
  }
])

/* Here the controllers are defines */
.controller('UsersListCtrl', function ($scope, UserRepository, $http, API_ENDPOINT) {
  $scope.users = UserRepository.getList();
  $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 4, $scope.maxSize = 5;

  // $scope.makeTodos = function() {
  //   $scope.todos = [];
  //   for (i=1;i<=1000;i++) {
  //     $scope.todos.push({ text:"todo "+i, done:false});
  //   }
  // };
  // $scope.makeTodos(); 

  $scope.$watch("currentPage + numPerPage", function() {
    var begin = (($scope.currentPage - 1) * $scope.numPerPage), end = begin + $scope.numPerPage;

    $scope.filteredTodos = $scope.users.slice(begin, end);
  });

  $scope.aktifkan = function (user) {
    var pengguna = {
      id: user.id,
      enabled: true
    }

    $http.put(API_ENDPOINT.url +'/user/aktifkan', pengguna);
    var index = $scope.users.indexOf(user);
    $scope.users.splice(index, 1); 
    //masukkan lagi dengan value yang beda
    user = {
      id: user.id,
      name: user.name,
      email: user.email,
      jenis: user.jenis,
      enabled: true
    }
    $scope.users.splice(index, 0, user);
    // $scope.$apply();
    // $scope.users = UserRepository.getList();
  }

  $scope.nonaktifkan = function (user) {
    var pengguna = {
      id: user.id,
      enabled: false
    }

    $http.put(API_ENDPOINT.url +'/user/aktifkan', pengguna);
    //hapus dulu
    var index = $scope.users.indexOf(user);
    $scope.users.splice(index, 1); 
    //masukkan lagi dengan value yang beda
    user = {
      id: user.id,
      name: user.name,
      email: user.email,
      jenis: user.jenis,
      enabled: false
    }
    $scope.users.splice(index, 0, user); 


  }
  $scope.delete = function (data) {
    if(window.confirm('Apakah anda yakin?')) {
      UserRepository.remove(data).then(function () {
          $scope.users = UserRepository.getList();
        });
    }
  };
})

.controller('UsersMainCtrl', function($scope, $http, uiGridConstants, API_ENDPOINT, $state, Restangular, UserRepository, $rootScope) {
  // $scope.users = Restangular.all('users').getList().$object;
  var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };

  $scope.gridOptions = {
    paginationPageSizes: [25, 50, 100],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    columnDefs: [
      { name: 'id' },
      { name: 'name', enableSorting: false },
      {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ui-sref="app.users.edit({id:row.entity.id})" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i></a></span>'+
      
      '&nbsp  <span><a ui-sref="app.users.roles({id:row.entity.id})" class="btn btn-success"><i class="glyphicon glyphicon-list"></i></a></span>'+
      '&nbsp  <span><a ng-click="grid.appScope.Delete(row)" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a></span>'}
    
      // { name: 'company', enableSorting: false } ng-click="grid.appScope.Edit(row)"
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length == 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
    }
  };

  var getPage = function() {
    var url=API_ENDPOINT.url +'/users?page='+paginationOptions.pageNumber+'&per_page='+paginationOptions.pageSize;
    
    $http.get(url).success(function(data) {
      
      $scope.gridOptions.data = data;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      if (paginationOptions.pageNumber>=1) {
        var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        var firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });
 
  };
 
       getPage();
  
  
  $scope.Delete = function(row) {
      var index = $scope.gridOptions.data.indexOf(row.entity);
      // var data = $scope.gridOptions.data(row);
      if(window.confirm('Apakah anda yakin menghapus '+row.entity.namaUser+'?')) {
        UserRepository.remove(row.entity.idUser).then(function () {
            // $scope.armadas = UserRepository.getList();
            $scope.gridOptions.data.splice(index, 1);
        });
      }
  };

  $rootScope.users = $scope.users;

  $scope.Edit = function(row) {
      $rootScope.user = UserRepository.get(row.entity.id).then(function (data) {
        $rootScope.user = data;
        $rootScope.users = $scope.users;
        $state.go('app.users.edit', {id: row.entity.id});
      });
  };

  $scope.roles = function(row) {
      // $rootScope.user = UserRepository.get(row.entity.idUser).then(function (data) {
      //   $rootScope.user = data;
      //   $rootScope.users = $scope.users;
        $state.go('app.users.roles', {id: row.entity.idUser});
      // });
  };

})

.controller('RolesMainCtrl', function($scope, $filter, $stateParams, uiGridConstants, API_ENDPOINT, $http, $state, Restangular, UserRepository, $rootScope) {
  $scope.id = $stateParams.id; //tidak perlu ambil data user, cukup id nya aja
  $scope.roles = Restangular.all('roles').getList().$object;
  var paginationOptions = {
    pageNumber: 0,
    pageSize: 25,
    sort: null
  };

  $scope.gridOptions = {
    paginationPageSizes: [25, 50, 100],
    paginationPageSize: 25,
    useExternalPagination: true,
    useExternalSorting: false,
    columnDefs: [
      { name: 'id' },
      { name: 'name', enableSorting: false },
      {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ng-click="grid.appScope.Delete(row)" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a></span>'}
    
      // { name: 'company', enableSorting: false } ng-click="grid.appScope.Edit(row)"
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length == 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
    }
  };

  var getPage = function() {
    var url = API_ENDPOINT.url +'/user/'+$scope.id+'/roles';
    
    $http.get(url).success(function(data) {
      
      $scope.gridOptions.data = data.roles;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      if (paginationOptions.pageNumber>=1) {
        var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        var firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });
 
  };
 
    getPage();
  
  $scope.Delete = function(row) {
      var index = $scope.gridOptions.data.indexOf(row.entity);
      // var data = $scope.gridOptions.data(row);
      if(window.confirm('Apakah anda yakin menghapus '+row.entity.name+'?')) {
        var pengguna = {
          id: $scope.id,
          idRole: row.entity.id
        };
        $http.put(API_ENDPOINT.url+'/kurangroleuser' , pengguna);
        $scope.gridOptions.data.splice(index, 1);
      }
  };

  $scope.tambah = function () {
    var found = $filter('getByIdRole')($scope.roles, $scope.role);
    var pengguna = {
          id: $scope.id,
          idRole: $scope.role
        };
    $http.post(API_ENDPOINT.url+'/roleuser' , pengguna);
    $scope.gridOptions.data.push(found);
  }
  
})

.controller('UsersItemCtrl', function ($scope, $stateParams, UserRepository) {
  $scope.user = UserRepository.get($stateParams.id).then(function (data) {
    $scope.user = data;
  });
})

.controller('UsersEditCtrl', function ($scope, $stateParams, $location, UserRepository) {
  $scope.user = UserRepository.get($stateParams.id).then(function (data) {
    $scope.user = data;
  });
  $scope.save = function () {
    $scope.user.put().then(function () {
      $location.path('/user/' + $stateParams.id);
    });
  };
})

.controller('UsersAddCtrl', function ($scope, $location, UserRepository, $http, API_ENDPOINT, $state) {
  $scope.save = function () {
    
    $http.post(API_ENDPOINT.url +'/user/register', $scope.user);
    $state.go('app.users.list');
    
  };
})

.controller('UsersAddAdminCtrl', function ($scope, $location, UserRepository, $http, API_ENDPOINT) {
  $scope.isAdmin=false;
  $scope.isPo=false;
  $scope.isAgen=false;

  //untuk menghidupkan dropdown user: admin & user tanpa PO, dst..
  $scope.jenises = [
            {id: "1", nama: "ADMIN"},
            {id: "2", nama: "PO"},
            {id: "3", nama: "OWNER"},
            {id: "4", nama: "AGEN"},
            {id: "5", nama: "USER"}
  ];
  $scope.jenis = $scope.jenises[4].id;
  $scope.select_change = function(jenis) {
    //console.log("Jesnisssssss:  "+jenis)
      switch (jenis) {
      case '1':
        $scope.isAdmin=false;
        $scope.isPo=false;
        $scope.isAgen=false;
        break;
      case '2':
        $scope.isAdmin=false;
        $scope.isPo=true;
        $scope.isAgen=false;
        $http.get(API_ENDPOINT.url +'/pilihpo').success(function(data) {
                  $scope.pos = data;
                  $scope.po = $scope.pos[0].id;
        });
        break;
      case '3':  
        $scope.isAdmin=false;
        $scope.isPo=true;
        $scope.isAgen=false;
        $http.get(API_ENDPOINT.url +'/pilihpo').success(function(data) {
                  $scope.pos = data;
                  $scope.po = $scope.pos[0].id;
        });
        break;
      case '4':  
        $scope.isAdmin=false;
        $scope.isPo=false;
        $scope.isAgen=true;
        $http.get(API_ENDPOINT.url +'/pilihpo').success(function(data) {
                  $scope.pos = data;
                  $scope.po = $scope.pos[0].id;
        });
        $http.get(API_ENDPOINT.url +'/agens').success(function(data) {
                  $scope.channels = data;
                  $scope.channel = $scope.channels[0].idChannel;
        });
        break;
      default:
        $scope.isAdmin=false;
        $scope.isPo=false;
        $scope.isAgen=false;
        break;
      }
    };

  $scope.save = function () {
    // if($scope.isAdmin) {
    //   $scope.user.jenis =
    // }
    $http.post(API_ENDPOINT.url +'/registerbyadmin', $scope.user);
    $state.go('app.users.list');
    // UserRepository.create($scope.user).then(function () {
    //   $location.path('/user');
    // });
  };
});
})();