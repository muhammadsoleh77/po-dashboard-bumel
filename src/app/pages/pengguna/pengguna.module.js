(function () {
  'use strict';

  angular.module('BlurAdmin.pages.pengguna', [
    'BlurAdmin.pages.pengguna.hakakses',
    'BlurAdmin.pages.pengguna.user',
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.pengguna', {
          url: '/pengguna',
          template : '<ui-view></ui-view>',
          abstract: true,
          title: 'Pengguna',
          sidebarMeta: {
            icon: 'ion-gear-a',
            order: 200,
          },
        });
  }

})();