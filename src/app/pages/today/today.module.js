(function () {
  'use strict';

  angular.module('BlurAdmin.pages.today', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.today', {
          url: '/today',
          params: {
            loggedIn : null,
            po : '',
            logo : ''
          },
          templateUrl: 'app/pages/today/today.html',
          controller: 'todayCtrl',
          title: 'Hari Ini',
          sidebarMeta: {
            icon: 'ion-ios-pulse-strong',
            order: 0,
          },
        });
  }

})();
