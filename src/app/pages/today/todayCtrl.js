(function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.today')
    .controller('todayCtrl', todayCtrl);

    /** @ngInject */
    function todayCtrl ($scope, $uibModal, $state, API_ENDPOINT, $http, $rootScope, $interval, $window, $filter, $location, getDatePeriod, weeklySalesData, weeklySalesChart, chartColors, $timeout, salesByAllTrayekData) {

      $state.current.tag = 'A';

      $scope.loggedIn = $state.params.loggedIn;

      $rootScope.dataLoaded = 0;

      if ($state.params.loggedIn) {
        $rootScope.isLoggedIn = $state.params.loggedIn;
        $scope.poName = $state.params.po;
        $scope.poLogo = $state.params.logo;
        $scope.class = "greyin";

        $scope.next = function () {
          $window.scrollTo(0,0);
          $scope.loggedIn = false;
          $scope.class = "greyout";
        };

        $scope.nextPage = function (url) {
          $state.go(url);
        };
      }

      $scope.jadwalPosition = 0;

      $scope.toJadwal = function () {
        $window.scrollTo(0, angular.element("#jadwal").prop("offsetTop"));
      };

      $scope.toUpdateAgen = function () {
        $window.scrollTo(0, angular.element("#agenList").prop("offsetTop"));
      };

      $scope.openJadwalDetail = function (events, jadwal, detail, e) {
        if (!jadwal.available) {
          $uibModal.open({
            animation: true,
            templateUrl: '/app/pages/today/modals/jadwal/create.jadwal.html',
            controller: 'thisWeekJadwalCtrl',
            size: 'sm',
            resolve: {
              items: function () {
                return {
                  jadwal : null,
                  trayek : detail.trayek,
                  idTrayek : detail.idTrayek,
                  tanggal : jadwal.tanggal
                };
              }
            }
          });
        } else {
          $uibModal.open({
            animation: true,
            templateUrl: '/app/pages/today/modals/jadwal/main.html',
            controller: 'thisWeekJadwalCtrl',
            size: 'md',
            resolve: {
              items: function () {
                return {
                  jadwal : events,
                };
              }
            }
          }).result
            .then(function () {
              $window.scrollTo(0, (e.pageY - e.screenY));
            });
        }
      };

      $scope.getData = function (reload) {

        // end point approval
        $http.get(API_ENDPOINT.url + 'po/cancel/notifikasi/' + $scope.user.idPo).success(function(data) {
          $scope.notif_approval = data;
          console.log(data);
          // if($scope.notif_approval.length === 0){
          //   $scope.empty = false;
          //   $scope.message = "kosong";
          // }
        });

        $scope.lihat_data = function() {
          $http.get(API_ENDPOINT.url + '/po/cancel/approval/' + $scope.user.idPo).success(function(data) {
            $state.go('dashboard.akap.transaksi.approvals', {
              data: data
            });
            // $uibModal.open({
            //   animation: true,
            //   templateUrl: 'app/pages/today/modals/approval/main.html',
            //   controller: 'approvalCtrl',
            //   size: 'lg',
            //   resolve: {
            //     items: function () {
            //       return {
            //         data : data,
            //       };
            //     }
            //   }
            // })
            // .result.then(function (res) {
            //   $http.get(API_ENDPOINT.url + 'po/cancel/notifikasi/' + $scope.user.idPo).success(function(data) {
            //     $scope.notif_approval = data;
            //     console.log(data);
            //   });
            // })
          });
        }
        // end

        $rootScope.dataLoaded++;

        $scope.lastUpdate = Date.now();

        var namaBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        var namaHari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];

        var date = new Date();
        var yyyy = date.getFullYear();
        var mm = date.getMonth();
        var dd = date.getDate();
        var d = date.getDay();
        $scope.today = date.getTime();
        var bln, tgl;
        if (mm + 1 < 10) {
          bln = '0' + (mm + 1);
        } else {
          bln = mm + 1;
        }
        if (dd < 10) {
          tgl = '0' + dd;
        } else {
          tgl = dd;
        }

        $scope.clock = "Memuat Jam...";
        $scope.lastUpdate = Date.now();
        $scope.timeInterval = 1000;

        var tick = function () {
          $scope.clock = Date.now();
          $timeout(tick, $scope.timeInterval);
        };

        $timeout(tick, $scope.timeInterval);

        $scope.hariIni = namaHari[d] + ', ' + dd + " " + namaBulan[mm] + " " + yyyy;
        $scope.now = String(yyyy) + '-' + String(bln) + '-' + String(tgl);

        var twoWeeks = [];

        for (var i = 0; i < 15; i++) {
          var fullDate = new Date(yyyy, mm, dd+i);
          var y2 = fullDate.getFullYear();
          var m2 = fullDate.getMonth() + 1;
          var d2 = fullDate.getDate();
          if (m2 < 10) {
            bln = '0' + m2;
          } else {
            bln = m2;
          }
          if (d2 < 10) {
            tgl = '0' + d2;
          } else {
            tgl = d2;
          }
          date = y2 + '-' + bln + '-' + tgl;
          twoWeeks.push({
            day: date,
            status : 'kosong',
            detail : {}
          });
        }

        $scope.twoWeeks = twoWeeks;

        $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.now).success(function(data) {
          if (reload === true) {
            $scope.getChart();
            // $scope.reload = false;
          }
          $scope.data = {};

          $scope.data = {
            penjualan : data,
            pemesanan : data
          };

          $scope.getDonutChart = function () {
            $scope.formatData = function (input) {
              return $filter('currency')(input, 'Rp.', 0);
            };
            $scope.donutData = weeklySalesChart.donutData(salesByAllTrayekData.getData(data).detail);
          };
        });

        if (reload === true) {
          $scope.reload = true;
        }
        $http.get(API_ENDPOINT.url + '/po/dashboard?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.now).success(function (data) {
          if (reload === true) {
            $scope.getChart();
            // $scope.reload = false;
          }
          $scope.data = {};

          $scope.data = {
            // penjualan : data.penjualanHariIni,
            // pemesanan : data.pemesananHariIni,
            totalPenjualan : data.totalPenjualanHariIni,
            totalPemesanan : data.totalPemesananHariIni,
            totalPemesananOtobus : data.totalPemesananOtobus
          };

          // $scope.getDonutChart = function () {
          //   $scope.formatData = function (input) {
          //     return $filter('currency')(input, 'Rp.', 0);
          //   };
          //   $scope.donutData = weeklySalesChart.donutData(salesByAllTrayekData.getData(data).detail);
          // };

          // if ($scope.data.penjualan.length === 0) {
          if ($scope.data.length === 0) {
            $scope.penjualanKosong = "Belum Ada Penjualan";
          } else {
            $scope.penjualanKosong = "";
          }

          // if ($scope.data.pemesanan.length === 0) {
          if ($scope.data.length === 0) {
            $scope.pemesananKosong = "Belum Ada Pemesanan";
          } else {
            $scope.pemesananKosong = "";
          }

          $scope.trayeks = data.trayeks;
          // console.log($scope.trayeks);

          // var dataTrayeks=[];

          // $scope.jadwalBulans =
          // data.trayeks.forEach(function(x){
          //   x.jadwalBulanIni.forEach(function(y){
          //     y.namabus;
          //   });
          // });
          // console.log($scope.jadwalBulans);

          $scope.jadwals = [];
          $scope.thisWeek = [];
          var dayJadwal=[];
          var seriBus;
          var available;
          var detail;
          var status;
          var seatSold;
          var i;
          var j;
          var k;
          for (j = 0; j < $scope.trayeks.length; j++) {
            var trayekJadwal = [];
            for (i = 0; i < 6; i++) {
              $scope.thisWeek.push($scope.twoWeeks[i].day);
              if ($scope.trayeks[j].jadwalBulanIni.length === 0) {
                available = false;
                detail = [];
                status = false;
              } else {
                available = false;
                detail = [];
                seriBus=[];
                status = false;
                seatSold = 0;
                for (k = 0; k < $scope.trayeks[j].jadwalBulanIni.length; k++) {
                  if ($scope.twoWeeks[i].day === $scope.trayeks[j].jadwalBulanIni[k].tanggal) {
                    available = true;
                    detail.push($scope.trayeks[j].jadwalBulanIni[k]);
                    seriBus.push($scope.trayeks[j].jadwalBulanIni[k].namabus);
                    seatSold = seatSold + $scope.trayeks[j].jadwalBulanIni[k].seatSold;
                    status = $scope.trayeks[j].jadwalBulanIni[k].status;
                  }
                }
              }
              trayekJadwal.push({
                seriBus : seriBus,
                available : available,
                seatSold : seatSold,
                detail : detail,
                status : status,
                tanggal : $scope.twoWeeks[i].day
              });
            }
            dayJadwal.push({
              kodetrayek : $scope.trayeks[j].kodetrayek,
              namabus : seriBus,
              trayek : $scope.trayeks[j].trayek,
              idTrayek : $scope.trayeks[j].id,
              jadwal : trayekJadwal
            });
          }
          $scope.thisWeek = [];
          for (i = 0; i < 6; i++) {
            $scope.thisWeek.push($scope.twoWeeks[i].day);
          }
          $scope.jadwals = dayJadwal;
        });

        $http.get(API_ENDPOINT.url + '/po/' + $rootScope.user.idPo + '/agen/update').success(function (res) {
          $scope.updateAgen = res;

          $scope.approve = function (id, index) {
            $scope.updateAgen[index].clicked = true;
            $http.post(API_ENDPOINT.url + '/po/agen/update/2/' + id).success(function () {
              $scope.updateAgen[index].status = 'APPROVED';
              $scope.toUpdateAgen();
            }).error(function (e) {
              $scope.updateAgen[index].clicked = false;
            });
          };

          $scope.reject = function (id, index) {
            $scope.updateAgen[index].clicked = true;
            $http.post(API_ENDPOINT.url + '/po/agen/update/1/' + id).success(function () {
              $scope.updateAgen[index].status = 'REJECTED';
              $scope.toUpdateAgen();
            }).error(function (e) {
              $scope.updateAgen[index].clicked = false;
            });
          };

          $scope.open = function (data, index) {
            $uibModal.open({
              animation: true,
              templateUrl: '/app/pages/today/modals/updateAgen/updateAgen.modal.html',
              controller: 'updateAgenModalCtrl',
              size: 'md',
              resolve: {
                items: function () {
                  return {
                    data : data,
                  };
                }
              }
            }).result
              .then(function (approved) {
                if (approved) {
                  $scope.updateAgen[index].status = 'APPROVED';
                  $scope.toUpdateAgen();
                } else {
                  $scope.updateAgen[index].status = 'REJECTED';
                  $scope.toUpdateAgen();
                }
              });
          };

        });
        $rootScope.isLoggedIn = false;

        if ($rootScope.createJadwal) {
          $scope.toJadwal();
          $rootScope.createJadwal = false;
        }
      };

      var tes = 0;

      $rootScope.$watch('isLoggedIn', function () {
        tes++;
        if ((tes === 1) && $rootScope.isLoggedIn && ($rootScope.dataLoaded < 1)) {
          $timeout(function () {
            $scope.getData(true);
          }, 1);
        }
      });

      $scope.getChart = function () {
        if ($rootScope.agens && $rootScope.lisTrayeks && $rootScope.agens.length > 0 && $rootScope.lisTrayeks.length > 0) {

          $scope.openWeeklySales = function () {
            // $state.go('dashboard.laporan.transaksiPeriode', {
            $state.go('dashboard.akap.laporan.perPeriode', {
              tgl : $scope.now,
              transaksi : 'penjualan'
            });
          };

          $scope.openWeeklyOrders = function () {
            // $state.go('dashboard.laporan.transaksiPeriode', {
            $state.go('dashboard.akap.laporan.perPeriode', {
              tgl : $scope.now,
              transaksi : 'pemesanan'
            });
          };

          var twoWeekDate = getDatePeriod.get2WeekDate($scope.now);

          var firstWeek = twoWeekDate.splice(0,7);

          $scope.weekDate = getDatePeriod.get2WeekDate($scope.now);
          $scope.tglAkhir = $scope.weekDate[$scope.weekDate.length-1];
          $scope.tglAwal = $scope.weekDate[0];
          var url = API_ENDPOINT.url +'/po/setoran/trayek?idPo=' + $rootScope.user.idPo + '&tglawal=' + $scope.tglAwal + '&tglakhir=' + $scope.tglAkhir;
          $http.get(url).success(function (data) {
            $scope.chartTemplate = "";
            $scope.chartTemplate2 = "";
            $scope.formatData = function (input) {
              return $filter('currency')(input, 'Rp.', 0);
            };
            $scope.formatDate = function (x) {
              var result = $filter('date')(x, 'dd MMM');
              return result;
            };
            $scope.formatLabel = function (x) {
              var result = $filter('date_v4')(x);
              return result;
            };
            /*
             * DATA PENJUALAN PERMINGGU
             */
            $scope.datas = weeklySalesData.getData(data, firstWeek, $scope.trayek).transaksi;
            $scope.lineData = weeklySalesChart.chartData($scope.datas).lineData;
            $scope.lineKey = weeklySalesChart.chartData($scope.datas).lineKey;
            $scope.lineLabel = weeklySalesChart.chartData($scope.datas).lineLabel;
            $scope.colors = chartColors;
            /*
             * DATA PEMESANAN PERMINGGU
             */
            $scope.datas2 = weeklySalesData.getData(data, twoWeekDate, $scope.trayek, true).transaksi;
            $scope.lineData2 = weeklySalesChart.chartData($scope.datas2).lineData;
            $scope.lineKey2 = weeklySalesChart.chartData($scope.datas2).lineKey;
            $scope.lineLabel2 = weeklySalesChart.chartData($scope.datas2).lineLabel;
            $scope.colors = chartColors;
            $timeout(function () {
              $scope.chartTemplate = "/app/pages/laporan/periode/penjualan/weeklySales/weeklySales.chart.template.html";
              $scope.chartTemplate2 = "/app/pages/today/revenue/chartTemplate/weeklyOrder.chart.template.html";
              $scope.reload = false;
            }, 500);

          });
        } else {
          var load = 0;
          $rootScope.$watch('agens', function () {
            load++;
            if (load <= 1) {
              $scope.getChart();
              $scope.getDonutChart();
            }
          });
        }
      };

        $scope.$watch('data', function () {
          $scope.getDonutChart();
        });

          $scope.openDetailPenjualan = function (data) {
            $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.now).success(function(data) {
              $uibModal.open({
                animation: true,
                templateUrl: '/app/pages/today/modals/sales/sales.modal.html',
                controller: 'salesModalCtrl',
                size: 'md',
                resolve: {
                  items: function () {
                    return {
                      data : data,
                      tgl : $scope.now
                    };
                  }
                }
              });
            });
          };

          $scope.openDetailPemesanan = function (data) {
            $http.get(API_ENDPOINT.url + '/po/setoran/pemesanan?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.now).success(function(data) {
              $uibModal.open({
                animation: true,
                templateUrl: '/app/pages/today/modals/order/order.modal.html',
                controller: 'orderModalCtrl',
                size: 'lg',
                resolve: {
                  items: function () {
                    return {
                      data : data,
                      tgl : $scope.now
                    };
                  }
                }
              });
            });
          };

          $scope.openDetailPemesananOtobus = function (data) {
            $http.get(API_ENDPOINT.url + '/po/detail/otobus?idPo=' + $rootScope.user.idPo + '&tgl=' + $scope.now).success(function(data) {
              $uibModal.open({
                animation: true,
                templateUrl: '/app/pages/today/modals/orderOtobus/orderOtobus.modal.html',
                controller: 'orderOtobusModalCtrl',
                size: 'lg',
                resolve: {
                  items: function () {
                    return {
                      data : data,
                      tgl : $scope.now
                    };
                  }
                }
              });
            });
          };

      // $scope.openDetailPenjualan = function (data) {
      //   $uibModal.open({
      //     animation: true,
      //     templateUrl: '/app/pages/today/modals/sales/sales.modal.html',
      //     controller: 'salesModalCtrl',
      //     size: 'md',
      //     resolve: {
      //       items: function () {
      //         return {
      //           data : data,
      //           tgl : $scope.now
      //         };
      //       }
      //     }
      //   });
      // };

      // $scope.openDetailPemesanan = function (data) {
      //
      //   $uibModal.open({
      //     animation: true,
      //     templateUrl: '/app/pages/today/modals/order/order.modal.html',
      //     controller: 'orderModalCtrl',
      //     size: 'lg',
      //     resolve: {
      //       items: function () {
      //         return {
      //           data : data,
      //           tgl : $scope.now
      //         };
      //       }
      //     }
      //   });
      // };
    }

})();
