(function () {

  'use strict';

  angular.module('BlurAdmin.pages.today')
      .directive('updateAgen', updateAgen);

  /** @ngInject */
  function updateAgen() {
    return {
      restrict: 'E',
      // controller: 'todayCtrl',
      templateUrl: 'app/pages/today/updateAgen/updateAgen.template.html'
    };
  }

})();
