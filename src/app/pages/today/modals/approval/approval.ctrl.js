(function () {
  'use strict';

  angular.module('BlurAdmin.pages.today')
  .controller('approvalCtrl', approvalCtrl);

  function approvalCtrl(API_ENDPOINT, $http, $uibModal, $uibModalInstance, $scope, items, $state, $rootScope) {

    var templateUrl = [
      'app/pages/today/modals/approval/approval.template.html',
    ];

    $scope.templateUrl = templateUrl[0];

    $http.get(API_ENDPOINT.url + 'po/cancel/approval/' + $scope.user.idPo).success(function(data) {
      $scope.approval = data.cancelApproval;
      console.log($scope.approval);

      // if($scope.approval.length === 0){
      //   alert('data kosong');
      //   $scope.empty = false;
      // }

      var body = {
        transaksiDetail : []
      };

      $scope.klik_approve = function(index) {

        body.transaksiDetail = $scope.approval[index]['transaksiDetail']

        $http.put(API_ENDPOINT.url + 'po/cancelation/void/' + $scope.approval[index].idtransaksi + '/' + $scope.approval[index].idVoid + '?status=true', body).success(function(data) {
          alert('Approve berhasil');
          $scope.approval.splice(index, 1)
        });
      }

      $scope.klik_cancel = function(index) {

        body.transaksiDetail = $scope.approval[index]['transaksiDetail']

        $http.put(API_ENDPOINT.url + 'po/cancelation/void/' + $scope.approval[index].idtransaksi + '/' + $scope.approval[index].idVoid + '?status=false', body).success(function(data) {
          alert('Cancel berhasil');
          $scope.approval.splice(index, 1)
        });
      }

      $scope.klik_detail = function(index) {
        $http.get(API_ENDPOINT.url + 'po/cancel/approval/' + $scope.user.idPo).success(function(data) {
          $uibModal.open({
            animation: true,
            templateUrl: 'app/pages/today/modals/approval/detail.html',
            size: 'lg',
            resolve: {
              index: function() {
                return {
                  data: data,
                };
              }
            }
          })
        });
      }

    });

    $scope.close = function () {
      $uibModalInstance.close($scope.approval);
    };
  };
})();
