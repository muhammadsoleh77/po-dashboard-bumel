(function () {
  angular
    .module('BlurAdmin.pages.today')
    .controller('salesModalCtrl', salesModalCtrl);

    function salesModalCtrl($uibModalInstance, items, $scope, $filter, $state, $http, API_ENDPOINT, $rootScope) {

      $scope.openDetail = function (trayek) {
        // $state.go('dashboard.laporan.transaksi',
        $state.go('dashboard.akap.laporan.perTanggal',
        {
          tgl : items.tgl,
          trayek : trayek.namaTrayek,
          idTrayek : trayek.idTrayek ,
          rute : trayek.rute,
          arah : trayek.arah,
          transaksi : 'penjualan',
          jenisTransaksi : 'trayek'
        });
        $uibModalInstance.dismiss();
      };
      $scope.next = function (trayek) {
        $state.go('dashboard.akap.transaksi.setoran', {
          query : {
            tgl : items.tgl,
            transaksi : 'penjualan',
            trayek : trayek,
          }
        });
        $uibModalInstance.dismiss();
      };

      $scope.close = function () {
        $uibModalInstance.dismiss();
      };

      var getAgenProfile = function (idPemesan) {
        if (idPemesan === 0) {
          profile = {
            name : "",
            value : "",
            alamat : "",
            kontakPerson : "",
            nomorTelepon : "",
            id : 0,
          }
        }
        for (var i = 0; i < $scope.listAgens.length; i++) {
          if ($scope.listAgens[i].id === idPemesan) {
            profile = $scope.listAgens[i];
          }
        }
        return profile;
      };

      var datas = [];
      var dataPenjualan = [];

      items.data.forEach(function (doc) {
        var profile, transaksi;

        doc.transaksi.forEach(function (doc2) {
          profile = {
            jmlKursiTerjual : doc.jmlKursiTerjual,
            jmlPenjualan : doc.jmlPenjualan,
            komisiAgen : doc.komisiAgen,
            setoran : doc.setoran,
            biayaAdmin : doc.biayaAdmin,

            namaBus : doc.namaBus === undefined ? "" : doc.namaBus,
            namaTrayek : doc.namaTrayek,
            jamBerangkat : doc.jamBerangkat,
            idTrayek : doc.idTrayek,
            idAgen : doc2.idAgen,
          };

          if (doc2.jenis === 1) {
            transaksi = {
              jmlKursi : doc2.jmlKursi,
              biayaAdmin : doc2.biayaAdmin,
              jmlRupiah : doc2.jmlRupiah,
              jenis : doc2.jenis,
              komisiAgen : doc2.komisiAgen,
            };
          } else if (doc2.jenis === 2) {
            transaksi = {
              jmlKursi : 0,
              biayaAdmin : 0,
              jmlRupiah : 0,
              jenis : doc2.jenis,
              komisiAgen : 0,
            };
          } else {
            transaksi = {
              jmlKursi : 0,
              biayaAdmin : 0,
              jmlRupiah : 0,
              jenis : doc2.jenis,
              komisiAgen : doc2.komisiAgen,
            };
          }
          dataPenjualan.push(Object.assign(transaksi, profile));
        });
      });

      var jmlKursiTerjual, jmlPenjualan, setoran;

      var dataTmp;
      var jmlKursi;
      var jmlRupiah;
      var komisiAgen;
      var biayaAdmin;
      for (var j = 0; j < $scope.lisTrayeks.length; j++) {
        var namaBus;
        if (dataPenjualan.length > 0) {
          for (i = 0; i < dataPenjualan.length; i++) {
            if (dataPenjualan[i].namaTrayek === $scope.lisTrayeks[j].value) {
              namaBus = dataPenjualan[i].namaBus;

              jmlKursiTerjual = dataPenjualan[i].jmlKursiTerjual;
              jmlPenjualan = dataPenjualan[i].jmlPenjualan;
              komisiAgen = dataPenjualan[i].komisiAgen;
              setoran = dataPenjualan[i].setoran;
              biayaAdmin = dataPenjualan[i].biayaAdmin;

              i = dataPenjualan.length;
            } else {
              namaBus = "";
            }
          }
        } else {
          namaBus = "";
        }
        var step1 = [];
        for (i = 0; i < $scope.lisTrayeks[j].tempatBerangkat.length; i++) {
          jmlKursiTerjual = 0;
          setoran = 0;
          jmlKursi = 0;
          jmlRupiah = 0;
          komisiAgen = 0;
          biayaAdmin = 0;
          dataTmp = $filter('filter')(dataPenjualan, {idTrayek : $scope.lisTrayeks[j].id}, true);
          dataTmp = $filter('filter')(dataTmp, {idAgen : $scope.lisTrayeks[j].tempatBerangkat[i].kode}, true);

          for (var k = 0; k < dataTmp.length; k++) {

            setoran = setoran + (dataTmp[k].jmlRupiah - dataTmp[k].komisiAgen);
            jmlKursi = jmlKursi + dataTmp[k].jmlKursi;
            jmlRupiah = jmlRupiah + dataTmp[k].jmlRupiah;
            komisiAgen = dataTmp[k].komisiAgen;
            biayaAdmin = dataTmp[k].biayaAdmin;

            jmlKursiTerjual = jmlKursiTerjual + dataTmp[k].jmlKursiTerjual;
            // console.log(biayaAdmin);
          }
          // if (jmlPenumpang >= 0) {
          if (jmlKursi >= 0) {
            step1.push({
              namaAgen : $scope.lisTrayeks[j].tempatBerangkat[i].nama,
              idAgen : $scope.lisTrayeks[j].tempatBerangkat[i].kode,
              // profileAgen : getAgenProfile($scope.lisTrayeks[j].tempatBerangkat[i].kode),

              jmlKursi : jmlKursi,
              jmlRupiah : jmlRupiah,
              komisiAgen : komisiAgen,
              biayaAdmin : biayaAdmin,
              jmlKursiTerjual : jmlKursiTerjual,
              dataPenumpang : []
            });
          }
          if ($scope.agen) {
            step1 = $filter('filter')(step1, {idAgen : $scope.agen.id}, true);
          }
        }
        if (step1.length > 0) {

          var totalJual = 0;
          var totalKursi = 0;
          var totalKomisi = 0;
          var jmlKursiTerjual = 0;
          var totalBiayaAdmin = 0;
          var totalSetoranPo = 0;
          var totalSetoranAgen = 0;
          step1.forEach(function (doc) {
            // jmlPenjualan = jmlPenjualan + doc.jmlPenjualan;
            // komisiAgen = komisiAgen + doc.komisiAgen;
            // jmlKursiTerjual = jmlKursiTerjual + doc.jmlKursiTerjual;
            setoran = setoran + doc.setoran;
            // totalBiayaAdmin = totalBiayaAdmin + doc.biayaAdmin;
            // totalJual = totalJual + doc.totalBayar;
            totalJual = totalJual + doc.jmlRupiah;
            totalKomisi = totalKomisi + doc.komisiAgen;
            totalKursi = totalKursi + doc.jmlKursi;
            jmlKursiTerjual = jmlKursiTerjual + doc.jmlKursiTerjual;
            totalBiayaAdmin = totalBiayaAdmin + doc.biayaAdmin;
            // totalSetoranPo = totalSetoranPo + doc.setoranPo;
            // totalSetoranAgen = totalSetoranAgen + doc.setoranAgen;
          });
          datas.push({
            jmlKursiTerjual : jmlKursiTerjual,
            jmlPenjualan : jmlPenjualan,
            komisiAgen : komisiAgen,
            setoran : setoran,
            biayaAdmin : biayaAdmin,

            namaTrayek : $scope.lisTrayeks[j].value,
            idTrayek : $scope.lisTrayeks[j].id,
            rute : $scope.lisTrayeks[j].rute,
            arah : $scope.lisTrayeks[j].arah,
            namaBus : namaBus,
            transaksi : step1,
            // totalJual : jmlPenjualan,
            // totalKomisi : komisiAgen,
            // totalKursi : jmlKursiTerjual,
            // totalBiayaAdmin : biayaAdmin,
            totalJual : totalJual,
            totalKomisi : totalKomisi,
            totalKursi : totalKursi,
            totalBiayaAdmin : totalBiayaAdmin,
            totalSetoranPo : totalSetoranPo,
            totalSetoranAgen : totalSetoranAgen,
            setoran : totalJual - totalKomisi,
            dataPenumpang : []
          });
          // console.log(datas);
        }
      }

      for (var i = 0; i < items.data.length; i++) {
        for (var x = 0; x < datas.length; x++) {
          if (items.data[i].namaTrayek == datas[x].namaTrayek) {
            datas[x].dataPenumpang = items.data[i].dataPenumpang;
          }
        }
      }

      for (i = 0; i < datas.length; i++) {
        for (var z = 0; z < datas[i].dataPenumpang.length; z++) {
          for (var y = 0; y < datas[i].transaksi.length; y++) {
            if (datas[i].dataPenumpang[z].agenBeli == datas[i].transaksi[y].namaAgen) {
              datas[i].transaksi[y].dataPenumpang.push(datas[i].dataPenumpang[z]);
            }
          }
        }
      }


      $scope.datas = $filter('orderBy')(datas, '-totalJual');

      $http.get(API_ENDPOINT.url + '/po/setoran/penjualan?idPo=' + $rootScope.user.idPo + '&tgl=' + items.tgl).success(function (data) {

        // $scope.datas = data;

        for (var i = 0; i < $scope.datas.length; i++) {
          $scope.datas[i].terima = true;
          for (var j = 0; j < data.length; j++) {
            if (data[j].setoran > 0) {
              if ($scope.datas[i].idTrayek === data[j].idTrayek) {
                $scope.datas[i].terima = false;
              }
            }
          }
        }

      });

    }
})();
