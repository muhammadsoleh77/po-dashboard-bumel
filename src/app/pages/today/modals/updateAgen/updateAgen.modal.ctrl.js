(function () {

  angular
    .module('BlurAdmin.pages.today')
    .controller('updateAgenModalCtrl', updateAgenModalCtrl);

    function updateAgenModalCtrl($uibModalInstance, items, $scope, $sce, $http, API_ENDPOINT) {
      $scope.agen = items.data;

      $scope.close = function () {
        $uibModalInstance.dismiss();
      };

      var mapUrl = 'https://www.google.com/maps/embed/v1/place?q=' + $scope.agen.latitude + ',' + $scope.agen.longitude + '&key=AIzaSyBFRjgng4mClcT1MbC5IZt9juyUpUjJ9Mw';

      $scope.mapUrl = $sce.trustAsResourceUrl(mapUrl);

      $scope.approve = function () {
        $scope.clicked = true;
        $http.post(API_ENDPOINT.url + '/po/agen/update/2/' + $scope.agen.idChannelTemp).success(function () {
          $uibModalInstance.close(true);
        }).error(function (e) {
          $scope.clicked = false;
        });
      };

      $scope.reject = function () {
        $scope.clicked = true;
        $http.post(API_ENDPOINT.url + '/po/agen/update/1/' + $scope.agen.idChannelTemp).success(function () {
          $uibModalInstance.close(false);
        }).error(function (e) {
          $scope.clicked = false;
        });
      };

    }

})();
