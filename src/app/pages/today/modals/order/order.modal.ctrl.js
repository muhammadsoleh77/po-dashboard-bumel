(function () {
  angular
    .module('BlurAdmin.pages.today')
    .controller('orderModalCtrl', orderModalCtrl);

    function orderModalCtrl($uibModalInstance, items, $scope, $filter, $state, API_ENDPOINT, $http, $rootScope) {

      $scope.openDetail = function (agen) {
          // $state.go('dashboard.laporan.transaksi',
          $state.go('dashboard.akap.laporan.perTanggal',
          {
            tgl : items.tgl,
            agen : agen,
            transaksi : 'pemesanan',
            jenisTransaksi : 'agen'
          });
          $uibModalInstance.dismiss();
      };

      $scope.close = function () {
        $uibModalInstance.dismiss();
      };

      $scope.next = function (agen) {
        $state.go('dashboard.akap.transaksi.setoran', {
          query : {
            tgl : items.tgl,
            transaksi : 'pemesanan',
            agen : agen,
          }
        });
        $uibModalInstance.dismiss();
      };

      $scope.datas = items.data;

      $http.get(API_ENDPOINT.url + '/po/setoran/pemesanan?idPo=' + $rootScope.user.idPo + '&tgl=' + items.tgl).success(function (data) {
        for (var i = 0; i < $scope.datas.length; i++) {
          $scope.datas[i].terima = true;
          for (var j = 0; j < data.length; j++) {
            if ($scope.datas[i].namaAgen === data[j].namaAgen) {
              $scope.datas[i].terima = false;
            }
          }
        }
      });

    }
})();
