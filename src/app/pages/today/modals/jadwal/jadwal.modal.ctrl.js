/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.today')
      .controller('thisWeekJadwalCtrl', thisWeekJadwalCtrl);

  /** @ngInject */
  function thisWeekJadwalCtrl($uibModalInstance, $scope, items, $state, $rootScope) {

    var templateUrl = [
      '/app/pages/today/modals/jadwal/jadwal.html',
      '/app/pages/today/modals/jadwal/detail.html',
    ];

    $scope.back = function () {
      $scope.templateUrl = templateUrl[0];
    };

    $scope.templateUrl = templateUrl[0];

    $scope.jadwal = items.jadwal;

    $scope.showDetail = function (detail) {
      $scope.detail = detail;
      $scope.templateUrl = templateUrl[1];
    };

    $scope.addJadwal = function () {
      $rootScope.selectedTrayekTmp = items.trayek;
      $rootScope.selectedIdTrayekTmp = items.idTrayek;
      $rootScope.selectedTanggalTmp = items.tanggal;
      $uibModalInstance.dismiss();
      // $state.go('dashboard.jadwal.add');
      $state.go('dashboard.akap.jadwalLayanan.rencanaJadwal');
    };
    $scope.close = function () {
      $uibModalInstance.close(true);
    };
    $scope.recreate = function () {
      $rootScope.selectedTrayekTmp = '';
      $rootScope.selectedTanggalTmp = '';
      $uibModalInstance.close(true);
    };
    $scope.toHome = function () {
      $uibModalInstance.dismiss();
      $state.go('dashboard.today');
    };
  }
})();
