/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.today')
      .directive('thisWeek', thisWeek);

  /** @ngInject */
  function thisWeek() {
    return {
      restrict: 'E',
      // controller: 'todayCtrl',
      templateUrl: 'app/pages/today/thisWeek/thisWeek.template.html'
    };
  }
})();
