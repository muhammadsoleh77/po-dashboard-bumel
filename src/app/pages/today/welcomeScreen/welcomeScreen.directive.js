/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.today')
      .directive('welcomeScreen', welcomeScreen);

  /** @ngInject */
  function welcomeScreen() {
    return {
      restrict: 'E',
      // controller: 'todayCtrl',
      templateUrl: 'app/pages/today/welcomeScreen/welcomeScreen.template.html'
    };
  }
})();
