/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.today')
      .directive('revenue', revenue);

  /** @ngInject */
  function revenue() {
    return {
      restrict: 'E',
      // controller: 'todayCtrl',
      templateUrl: 'app/pages/today/revenue/revenue.template.html'
    };
  }
})();
