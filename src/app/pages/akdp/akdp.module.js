(function () {
  'use strict';

  angular.module('BlurAdmin.pages.akdp', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.akdp', {
          url: '/akdp',
          title: 'AKDP',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              icon: 'ion-android-bus',
              order: 205
            }
        })
        .state('dashboard.akdp.pengaturan', {
          url: '/pengaturan',
          title: 'Pengaturan',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              // icon: 'ion-android-bus',
              order: 206
            }
        })
            .state('dashboard.akdp.pengaturan.trayek', {
              url: '/trayek',
              title: 'Trayek',
              templateUrl: 'app/pages/akdp/pengaturan/akdp.trayek.html',
              controller: 'akdpTrayekCtrl',
              sidebarMeta: {
                  order: 1
                }
            })
            .state('dashboard.akdp.pengaturan.subtrayek', {
              url: '/subtrayek',
              title: 'Sub Trayek',
              templateUrl: 'app/pages/akdp/pengaturan/akdp.subtrayek.html',
              controller: 'akdpSubTrayekCtrl',
              sidebarMeta: {
                  order: 2
                }
            })
        .state('dashboard.akdp.transaksi', {
          url: '/transaksi',
          title: 'Transaksi',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              order: 207
            }
        })
            .state('dashboard.akdp.transaksi.terimasetoran', {
              url: '/terimasetoran',
              title: 'Penerimaan Setoran',
              templateUrl: 'app/pages/akdp/transaksi/terimaSetoran/akdp.terimasetoran.html',
              controller: 'akdpTerimaSetoranCtrl',
              sidebarMeta: {
                  order: 1
                }
            })
        .state('dashboard.akdp.laporan', {
          url: '/laporan',
          title: 'Laporan',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              order: 208
            }
        })
            .state('dashboard.akdp.laporan.terimasetoran', {
              url: '/terimasetoran',
              title: 'Penerimaan Setoran',
              templateUrl: 'app/pages/akdp/laporan/akdp.laporanterimasetoran.html',
              controller: 'akdpLaporanTerimaSetoranCtrl',
              sidebarMeta: {
                  order: 1
                }
            })
            .state('dashboard.akdp.laporan.hasiljualtiket', {
              url: '/hasiljualtiket',
              title: 'Hasil Penjualan Tiket',
              templateUrl: 'app/pages/akdp/laporan/akdp.hasiljualtiket.html',
              controller: 'akdpHasilJualTiketCtrl',
              sidebarMeta: {
                  order: 2
                }
            })
        .state('dashboard.akdp.bumel', {
          url: '/bumel',
          title: 'BUMEL',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              order: 209
            }
        })
            .state('dashboard.akdp.bumel.trayek', {
              url: '/trayek',
              title: 'Trayek',
              templateUrl: 'app/pages/akdp/bumel/trayek/akdp.bumel.html',
              controller: 'akdpBumelCtrl',
              sidebarMeta: {
                  order: 1
                }
            })
            .state('dashboard.akdp.bumel.crewTarif', {
              url: '/crewTarif',
              title: 'Crew & Tarif',
              templateUrl: 'app/pages/akdp/bumel/crewTarif/akdp.crewTarif.html',
              controller: 'akdpBumelTarifCtrl',
              sidebarMeta: {
                  order: 2
                }
            });
  }

})();
