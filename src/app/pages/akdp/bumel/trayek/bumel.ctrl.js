(function() {

  angular
    .module('BlurAdmin.pages.akdp')
    .controller('akdpBumelCtrl', akdpBumelCtrl);

    function akdpBumelCtrl($http, API_ENDPOINT, $scope, $state){

      $scope.testdate = new Date("2000-01-01T09:00:00.000");

      $scope.bumels = [
        {id:'1', trayek:'K. Wisata - Monas', waktuTiba:'05.00'},
        {id:'2', trayek:'Monas - K. Wisata', waktuTiba:'07.45'}
      ];

      $scope.selectedRow = null;

      $scope.setClickedRow = function(index) {
        $scope.selectedRow = index;
      }

      $scope.moveUp = function(num) {
        if (num > 0) {
          tmp = $scope.data[num - 1];
          $scope.data[num - 1] = $scope.data[num];
          $scope.data[num] = tmp;
          $scope.selectedRow--;
        }
      }

      $scope.moveDown = function(num) {
        if (num < $scope.data.length - 1) {
          tmp = $scope.data[num + 1];
          $scope.data[num + 1] = $scope.data[num];
          $scope.data[num] = tmp;
          $scope.selectedRow++;
        }
      }

      $scope.data = []

      $scope.add = function(trayek){

        $scope.data.push({
          trayek: trayek.trayek,
          waktuTiba: trayek.waktuTiba
        })

        $scope.loadData = true;

        $scope.templateUrl = "/app/pages/akdp/bumel/trayek/akdp.bumelList.html";

        $scope.loadData = false;
      };

      $scope.delete = function(index){
        $scope.data.splice(index, 1);
      };

    };
})();
