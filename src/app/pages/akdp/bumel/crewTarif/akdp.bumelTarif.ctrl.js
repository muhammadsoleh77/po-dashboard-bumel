(function() {

  angular
    .module('BlurAdmin.pages.akdp')
    .controller('akdpBumelTarifCtrl', akdpBumelTarifCtrl);

    function akdpBumelTarifCtrl($http, API_ENDPOINT, $scope, $state){

      $scope.armadas = [
        {id:'001', namaArmada:'OH-165RX'},
        {id:'002', namaArmada:'OH-180ZZ'},
        {id:'003', namaArmada:'OH-999MM'}
      ];

      $scope.crews = [
        {idCrew:'01', namaCrew:'Rangga'},
        {idCrew:'02', namaCrew:'Alam'},
        {idCrew:'03', namaCrew:'Sueb'}
      ];

      $scope.layanans = [
        {kdLayanan:'1', namaLayanan:'K.Wisata - Monas'},
        {kdLayanan:'2', namaLayanan:'Monas - K.Wisata'}
      ];

      $scope.listTicket = []

      $scope.add = function(armada, tanggal, crew, layanan){

        $scope.listTicket.push({
          namaArmada: armada.namaArmada,
          namaCrew: crew.namaCrew,
          namaLayanan: layanan.namaLayanan,
          // tanggal: tanggal
        });

        $scope.loadData = true;
        $scope.templateUrl = "/app/pages/akdp/bumel/crewTarif/akdp.crewTarifList.html";
        $scope.loadData = false;
      };

    };
})();
