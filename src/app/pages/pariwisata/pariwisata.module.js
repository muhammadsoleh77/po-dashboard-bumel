(function () {
  'use strict';

  angular.module('BlurAdmin.pages.pariwisata', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.pariwisata', {
          url: '/pariwisata',
          title: 'Pariwisata',
          template : '<div ui-view></div>',
          abstract: true,
          sidebarMeta: {
              icon: 'ion-ios-world-outline',
              order: 207
            }
        });
  }

})();
