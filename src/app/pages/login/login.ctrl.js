(function () {
  'use strict';

  angular.module('BlurAdmin.pages.login')
    .controller('loginCtrl', loginCtrl);

  /** @ngInject */
  function loginCtrl($rootScope, $http, API_ENDPOINT, $q, $scope, AuthService, $state, Restangular) {
  $scope.user = {
    username: '',
    password: ''
  };

  // Loader
  $scope.loading = false;

  $scope.submitData = function () {
    $scope.errorSubmit = false;
    if (!$scope.user.username || !$scope.user.password) {
      $scope.errorSubmit = "Semua Data Harus Diisi";
      return false;
    } else {
      $scope.login();
    }
  };

  $scope.login = function() {
    $scope.errorSubmit = "";
    $scope.loading = true;
    AuthService.login($scope.user)
    .error(function (err) {
      $scope.loading = false;
      $scope.errorSubmit = "Anda Belum Terdaftar";
      //console.log(err);
    })
    .then(function(msg) {
      // //console.log(msg);
      $scope.pengguna = function() {
        var deferred = $q.defer();
        $http.get(API_ENDPOINT.url +'/user').success(function(data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      };

      $scope.pengguna().then(function(data){
          if (data === undefined) {
                  alert('Gagal');
                  //console.log('Nooo');
          } else {
              $rootScope.user = data;

              $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/trayeks').success(function (data) {
                $rootScope.lisTrayeks = [];
                $rootScope.trayeks = [
                  {
                    name : "Semua Trayek",
                    value : '',
                    id : null
                  }
                ];
                data.forEach(function (doc) {
                  $rootScope.trayeks.push({
                    name : doc.trayek,
                    value : doc.trayek,
                    id : doc.id,
                    tempatBerangkat : doc.tempatberangkat,
                    rute : doc.lintasan,
                    arah : doc.arahTrayek,
                  });
                  $rootScope.lisTrayeks.push({
                    id : doc.id,
                    name : doc.trayek,
                    value : doc.trayek,
                    tempatBerangkat : doc.tempatberangkat,
                    rute : doc.lintasan,
                    arah : doc.arahTrayek,
                  });
                });
              });

              var datas = {};

              if (data.roles.PO_ADMIN === true || data.roles[('ROLE_ADMIN')] || data.roles[('PO_OWNER')]) {
                $http.get(API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/agen').success(function (data) {
                  $rootScope.listAgens = [];
                  $rootScope.namaAgen = [];
                  $rootScope.agens = [
                    {
                      name : "Semua Agen",
                      value : '',
                      id : 0
                    }
                  ];
                  data.forEach(function (doc) {
                    $rootScope.agens.push({
                      name : doc.namaChannel,
                      value : doc.namaChannel,
                      id : doc.idChannel
                    });
                    $rootScope.listAgens.push({
                      name : doc.namaChannel,
                      value : doc.namaChannel,
                      alamat : doc.alamat,
                      kontakPerson : doc.kontakPerson,
                      nomorTelepon : doc.nomorTelepon,
                      id : doc.idChannel,
                    });
                    $rootScope.namaAgen.push(doc.namaChannel);
                  });
                });
                $http.get(API_ENDPOINT.url + '/po/' + $rootScope.user.idPo).success(function (po) {
                  datas = {
                    // akap : po.akap,
                    // akdp : po.akdp,
                    po : po.nama,
                    logo : po.logo,
                  };

                  $rootScope.isLoggedIn = true;
                  $state.go('dashboard.today', {po : datas.po, logo : datas.logo, loggedIn : true});

                });

              } else {
                  alert('User tersebut tidak dapat mengakses aplikasi ini');
                  $scope.loading = false;
                  //console.log('Noooooorrr');
                  AuthService.logout();
                  $state.go('login');
              }
          }
      });
      // $state.go('app.booking');
      // //console.log('Sukses');
    });

  };

  $scope.pesan = "";
  }

})();
