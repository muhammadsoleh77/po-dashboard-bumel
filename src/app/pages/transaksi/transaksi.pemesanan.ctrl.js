(function () {
  'use strict';

  angular
    .module('BlurAdmin.pages.transaksi')
    .controller('transaksiPemesananCtrl', function ($scope, Restangular, $rootScope, $http, API_ENDPOINT, $state, $uibModal, $window) {

      /**
       * Path untuk template halaman pemesanan
       */
      var url = [
        'app/pages/transaksi/pemesanan/pesan-tiket.html',
        'app/pages/transaksi/pemesanan/pilih-agen.html',
        'app/pages/transaksi/pemesanan/pilih-bus.html',
        'app/pages/transaksi/pemesanan/detail-bus.html',
        'app/pages/transaksi/pemesanan/data-penumpang.html',
        'app/pages/transaksi/pemesanan/bayar-tiket.html',
      ];


      $scope.penumpangs = [];// Array list penumpang
      $scope.templateUrl = url[0]; // halaman pesan-tiket
      $state.current.tag = 'D.4.2';

      /**
       * GET trayek by ID PO
       */
      $http.get(API_ENDPOINT.url + '/po/' + $rootScope.po.idPo + '/lintasans').success(function (data) {

          $scope.awal = data; // Kota Asal
          /**
           * Hapus kota asal dari list kota tujuan
           */
          $scope.idAwal_change = function (idAwal) {
            $http.get(API_ENDPOINT.url + '/agen/' + $rootScope.po.idPo + '/tujuankotas/' + idAwal).success(function (data) {
              $scope.akhir = data;
            });
          };

      });

      /**
       * GET agen by ID PO
       */
      $http.get(API_ENDPOINT.url +'/po/'+ $scope.user.idPo +'/agen').success(function(data) {
          $scope.agens = data;
      });

      var date = new Date();
      var yyyy = date.getFullYear();
      var mm = date.getMonth() + 1;
      var dd = date.getDate();
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (dd < 1) {
        dd = '0' + dd;
      }
      var now = yyyy + '-' + mm + '-' + dd +',23:59';
      $scope.now = new Date(now); // tanggal sekarang
      $scope.limit = String($scope.now); // tanggal sekarang

      $scope.next = function () {
        /**
        * Ke halaman pilih-bus
        */
        $scope.errorNext = "";
        $scope.errorDate = "";
        $rootScope.listTicket = [];
        $rootScope.listAgen = [];
        if (new Date($scope.tanggal).getTime() < $scope.now.getTime()) {
          $scope.errorDate = "Jangan pilih tanggal hari ini dan tanggal sebelumnya";
          return false;
        }
        $window.scrollTo(0,0);
        if (!$scope.agen || !$scope.tujuan || !$scope.asal || !$scope.tanggal || !$scope.kelas || !$scope.jmlPenumpang) {
          $scope.errorNext = "Semua data harus diisi!";
          return false;
        }
        $scope.pesanError = "Mencari Tiket...";
        /**
         * GET Jadwal berdasarkan asal dan tujuan
         */
        $http.get(API_ENDPOINT.url + '/agen/gettotalpage/pemesanan/' + $scope.user.idPo + '/' + $scope.asal + '/' + $scope.tujuan + '?tglBerangkat=' + $scope.tanggal + '&ekonomi=' + $scope.kelas + '&jmlKursi=' + $scope.jmlPenumpang).success(function (length) {
          var page;
          var agenValid = length.agenValid;
          if (length.total_page <= 10) {
            page = 1;
          } else {
            page = Math.round((length.total_page/10));
          }
          $scope.pages = [];
          for (var i = 0; i < page; i++) {
            $scope.pages.push({
              page : i,
              url : API_ENDPOINT.url + '/agen/detailpemesanan/' + $scope.user.idPo + '/' + $scope.asal + '/' + $scope.tujuan,
              active : false,
              query : {
                tglBerangkat : $scope.tanggal,
                ekonomi : parseInt($scope.kelas),
                jmlKursi : parseInt($scope.jmlPenumpang),
                agenValid : agenValid,
                page : i,
                per_page : length.total_page < 10 ? length.total_page : 10
              }
            });
          }

          $scope.selectPage = function (page) {
            $scope.pesanError = "Mencari Agen Keberangkatan...";
            $rootScope.listAgen = [];
            for (var i = 0; i < $scope.pages.length; i++) {
              if ($scope.pages[i].page === page.page) {
                $scope.pages[i].active = true;
              } else {
                $scope.pages[i].active = false;
              }
            }
            $http.post(page.url, page.query).success(function (data) {
              $rootScope.listAgen = data;
              $scope.pesanError = '';
              if (data.length === 0) {
                $scope.pesanError = "Tiket tidak tersedia";
              }
            }).error(function () {
              $scope.pesanError = "Tiket tidak tersedia";
            });
          };
          $scope.selectPage($scope.pages[0]);
        });
        $state.current.tag = 'D.4.2.1';
        $scope.templateUrl = url[1]; // halaman pilih-bus

      };

      $scope.pilihBus = function (agen) {
        $state.current.tag = 'D.4.2.2';
        $scope.templateUrl = url[2];
        $scope.pesanError = 'Mencari Tiket...';
        $scope.listTicket = [];
        $scope.agenKeberangkatan = agen.channel.idChannel;

        $http.get(API_ENDPOINT.url + '/agen/detailpenjualan/' + $scope.user.idPo + '/' + $scope.asal + '/' + $scope.tujuan + '?idAgenBrgkt=' + agen.channel.idChannel + '&tglBerangkat=' + $scope.tanggal + '&ekonomi=' + $scope.kelas + '&jmlKursi=' + $scope.jmlPenumpang).success(function (data) {
          $scope.listTicket = data;
          $scope.pesanError = '';
        });
        $window.scrollTo(0,0);
      };

      /**
       * Kembali ke halaman sebelumnya
       */
      $scope.backSearch = function (index) {
        if (index == 3) {
          $scope.kursis = [];
          $scope.listKursi.forEach(function (doc) {
            $scope.kursis.push({
              kursi : doc
            });
          });
          // console.log($scope.kursis);
          $http.put(API_ENDPOINT.url + '/agen/jadwal/' + $scope.ticketDetail.pemesananJadwal.idJadwal + '/bookingkursi', $scope.kursis).success(function (data) {
            // $uibModalInstance.close($scope.kursiTerpilih);
          });
        }
        $window.scrollTo(0,0);
        $state.current.tag = index > 0 ? 'D.4.2.' + (index) : 'D.4.2'; // halaman pilih-bus
        $scope.templateUrl = url[index];
      };

      $http.get(API_ENDPOINT.url + '/po/' + $rootScope.po.idPo + '/trayeks').success(function (data) {
        $scope.trayeks = data;
      });

      /**
       * bus terpilih
       */
      $scope.detailBus = function (data) {
        $window.scrollTo(0,0);
          $scope.trayek = [];
          for (var x=0;x<$scope.trayeks.length;x++) {
            if ($scope.trayeks[x].id === data.pemesananTrayek.idTrayek) {
              // console.log($scope.trayek[x].id === data.detailPenjualan.pemesananTrayek.idTrayek);
              $scope.trayek.push($scope.trayeks[x]);
            }
          }
          //console.log($scope.trayek);
        $scope.ticketDetail = data;
        $scope.templateUrl = url[3]; // halaman detail-bus
        $state.current.tag = 'D.4.2.3'; // halaman pilih-bus
      };
      $scope.listKursi = []; // Array untuk menyimpan nomor kursi terpilih
      /**
       * Open modal pilih kursi
       */
      $scope.selectSeat = function (data) {
        $uibModal.open({
          animation: true,
          templateUrl: '/app/pages/transaksi/pemesanan/pilih-kursi.html',
          controller: 'seatModalCtrl',
          size: 'lg',
          resolve: {
            items: function () {
                var item = {
                  data : data, // detail-bus
                  max : $scope.jmlPenumpang // penumpang maksimal
                };
              return item;
            }
          },
          backdrop: 'static'
        })
        .result.then(
          function (listKursi) {
            $window.scrollTo(0,0);
            for (var i = 0; i < $scope.penumpangs.length; i++) {
              $scope.penumpangs.splice(i, 1);
            }
            $scope.templateUrl = url[4];
            $state.current.tag = 'D.4.2.4';
             // halaman pilih-bus
             // halaman data-penumpang
            $scope.listKursi = listKursi; // list kursi terpilih
            $scope.noGender = [];
            $scope.noCategory = [];
            $scope.noName = [];
            $scope.noPhone = [];
            for (var j = 0; j < $scope.listKursi.length; j++) {
              $scope.noGender.push({
                status : false,
                message : ''
              });
              $scope.noCategory.push({
                status : false,
                message : ''
              });
              $scope.noName.push({
                status : false,
                message : ''
              });
              $scope.noPhone.push({
                status : false,
                message : ''
              });
            }
          }
        );
      };
      /**
       * Ke halaman pembayaran
       */
      $scope.pembayaran = function () {
        $scope.errorSubmit = "";
        if (Object.keys($scope.penumpangs).length !== $scope.listKursi.length) {
          $scope.errorSubmit = "Harap isi data penumpang!";
          return false;
        }
        var valid = 0;
        for (var x=0;x<$scope.penumpangs.length;x++) {
          $scope.penumpangs[x].id = x + 1;
          $scope.penumpangs[x].kursi = $scope.listKursi[x];

          if (!$scope.penumpangs[x].kategoriUsia) {
            $scope.noCategory[x].status = true;
            $scope.noCategory[x].message = "Pilih Kategori Usia!";
          } else {
            $scope.noCategory[x].status = false;
            $scope.noCategory[x].message = "";
            valid++;
          }
          if (!$scope.penumpangs[x].gender) {
            $scope.noGender[x].status = true;
            $scope.noGender[x].message = "Pilih Jenis Kelamin!";
          } else {
            $scope.noGender[x].status = false;
            $scope.noGender[x].message = "";
            valid++;
          }
          if (!$scope.penumpangs[x].nama) {
            $scope.noName[x].status = true;
            $scope.noName[x].message = "Masukan Nama Penumpang!";
          } else {
            $scope.noName[x].status = false;
            $scope.noName[x].message = "";
            valid++;
          }
          if (!$scope.penumpangs[x].telpon) {
            $scope.noPhone[x].status = true;
            $scope.noPhone[x].message = "Masukan Nomor Telepon Penumpang!";
          } else {
            $scope.noPhone[x].status = false;
            $scope.noPhone[x].message = "";
            valid++;
          }
        }

        if (valid !== ($scope.listKursi.length * 4)) {
          $window.scrollTo(0,0);
          return false;
        }
        $window.scrollTo(0,0);
        $scope.title = "Pemesanan";
        //console.log($scope.penumpangs);
        $scope.templateUrl = url[5];
        $state.current.tag = 'D.4.2.5';
         // halaman bayar-tiket
      };

      /**
       * Validasi Panjang Nomor Telepon
       */
       $scope.phoneNumber = function (index) {
         if ($scope.penumpangs[index].telpon.length > 13) {
              $scope.penumpangs[index].telpon = $scope.penumpangs[index].telpon.substr(0,13);
         }
       };

       /**
        * Validasi Panjang Nama Penumpang
        */
        $scope.passangerName = function (index) {
          if ($scope.penumpangs[index].nama.length > 20) {
               $scope.penumpangs[index].nama = $scope.penumpangs[index].nama.substr(0,20);
          }
        };

        /**
         * Validasi Anak
         */
         $scope.changeCategory = function (index) {
           if ($scope.penumpangs[index].kategoriUsia == "Anak") {
             $scope.penumpangs[index].telpon = '081';
           } else {
             $scope.penumpangs[index].telpon = '';
           }
         };

      /**
       * pesan kursi , nomor kursi diambil dari nomor paling kosong didepan
       */
      $scope.pesanKursi = function (data) {
        $window.scrollTo(0,0);
        $http.get(API_ENDPOINT.url + '/agen/jadwal/' + data.pemesananJadwal.idJadwal + '/kursi').success(function (seat) {
          var s1 = seat.seatMap;
          $scope.tesLayout = [];
          s1.forEach(function (doc) {
            doc.forEach(function (doc1) {
              $scope.tesLayout.push(doc1);
            });
          });
          $scope.listKursi = [];
          $scope.kursiKosong = [];
          for (var i = 0; i < $scope.tesLayout.length; i++) {
            if ($scope.tesLayout[i][4] === '0') {
              $scope.kursiKosong.push($scope.tesLayout[i][2]);
            }
          }
          // $scope.kursiKosong = $scope.kursiKosong.sort(function (a,b) {
          //   return a-b;
          // });
          for (i = 0; i < $scope.kursiKosong.length; i++) {
                if ($scope.listKursi.length < parseInt($scope.jmlPenumpang)) {
                  $scope.listKursi.push(String($scope.kursiKosong[i]));
                } else {
                  i = i + $scope.tesLayout.length;
                }
          }
          $scope.noGender = [];
          $scope.noCategory = [];
          $scope.noName = [];
          $scope.noPhone = [];
          for (var j = 0; j < $scope.listKursi.length; j++) {
            $scope.noGender.push({
              status : false,
              message : ''
            });
            $scope.noCategory.push({
              status : false,
              message : ''
            });
            $scope.noName.push({
              status : false,
              message : ''
            });
            $scope.noPhone.push({
              status : false,
              message : ''
            });
          }
          $scope.kursis = [];
          $scope.listKursi.forEach(function (doc) {
            $scope.kursis.push({
              kursi : doc
            });
          });
          $http.post(API_ENDPOINT.url + '/agen/jadwal/' + $scope.ticketDetail.pemesananJadwal.idJadwal + '/bookingkursi', $scope.kursis).success(function (data) {
            // $uibModalInstance.close($scope.kursiTerpilih);
            $scope.templateUrl = url[4]; // halaman data-penumpang
            $state.current.tag = 'D.4.2.3';

          });
        });
        $scope.terpilih = 0;
      };

      $scope.info = {};
      /**
       * Ke modal tambah info
       */
      $scope.tambahInfo = function () {
        $uibModal.open({
          animation: true,
          templateUrl: '/app/pages/transaksi/pemesanan/tambah-info.html',
          controller: 'infoModalCtrl',
          size: 'lg',
          backdrop: 'static',
          resolve : {
            items : function () {
              return $scope.info;
            }
          }
        })
        .result.then(
          function (info) {
            $scope.info = info;
            $window.scrollTo(0,0);
          }
        );
      };
      $scope.clicked = false;
      /**
       * Bayar Tiket
       */
       $scope.bayar = function () {
         $scope.clicked = true;

         $scope.dataPembayaran = {
           asalKota : $scope.asal,
           tujuanKota : $scope.tujuan,
           idAgenBerangkat : $scope.agenKeberangkatan,
           idPemesan : $scope.agen,
           idIssuer : $scope.agen,
           idJadwal : $scope.ticketDetail.pemesananJadwal.idJadwal,
           jmlPenumpang : $scope.penumpangs.length,
           metodePayment : "Tunai",
           penumpangs : $scope.penumpangs,
           idArahTrayek : $scope.ticketDetail.pemesananSubTrayek.idArahTrayek,
           tgl : $scope.ticketDetail.pemesananJadwal.tglBerangkat,
           waktu : $scope.ticketDetail.pemesananJadwal.waktuBerangkat,
           hargaAsal : $scope.ticketDetail.pemesananJadwal.hargaAsal * $scope.penumpangs.length,
           hargaJual : $scope.ticketDetail.pemesananJadwal.hargaJual * $scope.penumpangs.length,
           iuranIpomi : $scope.ticketDetail.pemesananJadwal.iuranIpomi * $scope.penumpangs.length,
           komisiPemberangkatan : $scope.ticketDetail.pemesananJadwal.komisiPemberangkatan * $scope.penumpangs.length,
           komisiPenjualan : $scope.ticketDetail.pemesananJadwal.komisiPemesanan * $scope.penumpangs.length,
           biayaAdmin : $scope.ticketDetail.pemesananJadwal.biayaAdmin * $scope.penumpangs.length,
           subAgen : $scope.info.subAgen,
           turun : $scope.info.turun,
           penjemputan : $scope.info.turun,
           keterangan : $scope.info.keterangan
         };
         $http.post(API_ENDPOINT.url + '/agen/pesantiket', $scope.dataPembayaran).success(function (data) {
           if (data.status === 212) {
             alert("Mohon Maaf Pilih Ulang kursi");
             $scope.selectSeat($scope.ticketDetail);
           } else {
             $uibModal.open({
               animation: true,
               backdrop: 'static',
               templateUrl: '/app/pages/transaksi/modals/transaksi.hasil.html',
               controller: 'hasilTransaksiCtrl',
               size: 'sm',
               resolve: {
                 items: function () {
                   return {
                     success : true,
                   };
                 }
               }
             });
           }
         }).error(function (err) {
           $uibModal.open({
             animation: true,
             templateUrl: '/app/pages/transaksi/modals/transaksi.hasil.html',
             controller: 'hasilTransaksiCtrl',
             size: 'sm',
             resolve: {
               items: function () {
                 return {
                   success : false,
                 };
               }
             }
           });
           $scope.clicked = false;
         });
       };
     });
  })();
