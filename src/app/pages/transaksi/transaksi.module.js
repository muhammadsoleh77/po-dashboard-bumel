(function () {
  'use strict';

  angular.module('BlurAdmin.pages.transaksi', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        // .state('dashboard.transaksi', {
        //   url: '/transaksi',
        //   templateUrl: 'app/pages/todaytoday.html',
        //   template : '<div ui-view></div>',
        //   abstract: true,
        //   title: 'Transaksi',
        //   sidebarMeta: {
        //     icon: 'ion-calculator',
        //     order: 202,
        //   },
        // })
        //   .state('dashboard.transaksi.transaksimanual', {
        //   url: '/manual',
        //   templateUrl: 'app/pages/transaksi/pilih.html',
        //   title: 'Transaksi Manual Agen',
        //     controller: "transaksiManualCtrl",
        //     controllerAs: "transaksiManualCtrl",
        //     sidebarMeta: {
        //       order: 4,
        //     },
        // })
        //   .state('dashboard.transaksi.transaksipenjualanagen', {
        //   url: '/penjualanagen',
        //   templateUrl: 'app/pages/transaksi/main.html',
        //   title: 'Transaksi Penjualan Agen',
        //     controller: "transaksiPenjualanCtrl",
        //     controllerAs: "transaksiPenjualanCtrl",
        // })
        //
        //   .state('dashboard.transaksi.transaksipemesananagen', {
        //   url: '/pemesananagen',
        //   templateUrl: 'app/pages/transaksi/main.html',
        //   title: 'Transaksi Pemesanan Agen',
        //     controller: "transaksiPemesananCtrl",
        //     controllerAs: "transaksiPemesananCtrl",
        // })
      //   .state('dashboard.transaksi.setoran', {
      //   url: '/setoran',
      //   templateUrl: 'app/pages/keuangan/setoran/keuangan.setoran.template.html',
      //   title: 'Penerimaan Setoran',
      //     controller: "keuanganSetoranCtrl",
      //     controllerAs: "keuanganSetoranCtrl",
      //     sidebarMeta: {
      //       order: 1,
      //     },
      //     params : {
      //       query : {}
      //     }
      //   })
      //     .state('dashboard.transaksi.terimaPenjualan', {
      //     url: '/penerimaan/penjualan',
      //     templateUrl: 'app/pages/keuangan/setoran/penjualan/keuangan.penjualanharian.html',
      //     title: 'Penerimaan Setoran Penjualan',
      //     controller: "terimaPenjualanCtrl",
      //       controllerAs: "terimaPenjualanCtrl",
      //     params : {
      //       data : {},
      //       tgl : ''
      //     }
      //   })
      //   .state('dashboard.transaksi.terimaPemesanan', {
      //   url: '/penerimaan/pemesanan',
      //   templateUrl: 'app/pages/keuangan/setoran/pemesanan/keuangan.pemesananharian.html',
      //   title: 'Penerimaan Setoran Pemesanan',
      //   controller: "terimaPemesananCtrl",
      //     controllerAs: "terimaPemesananCtrl",
      //   params : {
      //     data : {},
      //     tgl : ''
      //   }
      // })
        //  .state('dashboard.transaksi.setoranpemesananagen', {
        //   url: '/penerimaan/setoranpemesanan',
        //   templateUrl: 'app/pages/keuangan/penerimaan.setoran.pemesanan.html',
        //   title: 'Penerimaan Setoran Pemesanan',
        //     controller: "penerimaanPemesananCtrl",
        //     controllerAs: "penerimaanPemesananCtrl",
        // })
        //   .state('dashboard.transaksi.penjualanharian', {
        //   url: '/penjualanharian/{id}',
        //   templateUrl: 'app/pages/keuangan/keuangan.penjualanharian.html',
        //   title: 'Penjualan Hari Ini',
        //     controller: "penjualanharianCtrl",
        //     controllerAs: "penjualanharianCtrl",
        // })
        //   .state('dashboard.transaksi.pemesananharian', {
        //   url: '/pemesananharian/{id}',
        //   templateUrl: 'app/pages/keuangan/keuangan.pemesananharian.html',
        //   title: 'Pemesanan Hari Ini',
        //     controller: "pemesananharianCtrl",
        //     controllerAs: "pemesananharianCtrl",
        // })
          .state('dashboard.transaksi.adminSistem', {
          url: '/setoran/otodata',
          templateUrl: 'app/pages/keuangan/adminSistem/list/adminSistem.list.template.html',
          title: 'Setoran Admin Sistem',
            controller: "adminSistemListCtrl",
            controllerAs: "adminSistemListCtrl",
            sidebarMeta: {
              order: 2,
            },
        })
          .state('dashboard.transaksi.invoice', {
          url: '/setoran/invoice?date',
          templateUrl: 'app/pages/keuangan/adminSistem/detail/adminSistem.detail.template.html',
          title: 'E-Invoice',
          controller: "invoiceCtrl",
          controllerAs: "invoiceCtrl",
          params : {
            date : ''
          }
        })
          // .state('dashboard.transaksi.komisiAgen', {
          //   url : '/komisi/agen/list',
          //   templateUrl : 'app/pages/keuangan/komisiAgen/list/komisi.agen.list.template.html',
          //   title : 'Komisi Agen',
          //   controller : 'komisiAgenListCtrl',
          //   controllerAs : 'komisiAgenListCtrl',
          //   sidebarMeta : {
          //     order : 3
          //   }
          // })
          // .state('dashboard.transaksi.komisiAgenItem', {
          //   url : '/komisi/agen/item?idAgen&namaAgen&index',
          //   templateUrl : 'app/pages/keuangan/komisiAgen/item/komisi.agen.item.template.html',
          //   title : 'Komisi Agen',
          //   controller : 'komisiAgenItemCtrl',
          //   controllerAs : 'komisiAgenItemCtrl',
          //   params : {
          //     idAgen : '',
          //     namaAgen : '',
          //     index : '',
          //     data : null
          //   }
          // })
          // .state('dashboard.transaksi.komisiAgenProcess', {
          //   url : '/komisi/agen/process',
          //   templateUrl : 'app/pages/keuangan/komisiAgen/process/komisi.agen.process.template.html',
          //   title : 'Komisi Agen',
          //   controller : 'komisiAgenProcessCtrl',
          //   controllerAs : 'komisiAgenProcessCtrl',
          //   params : {
          //     data : null,
          //     index : ''
          //   }})
            ;
  }

})();
