(function () {
  'use strict';
angular.module('BlurAdmin.pages.informasi.biaya')

/* Define the Repository that interfaces with Restangular */

.controller('biayaCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {

  $state.current.tag = 'B.6';

  var paginationOptions = {
    pageNumber: 1,
    pageSize: 500,
    sort: null
  };

  $scope.gridOptions = {
    enableColumnResizing: true,
    useExternalSorting: true,
    columnDefs: [
      { name: 'No', cellTemplate: '<span style="margin-left:40%;text-align:right;">{{grid.renderContainers.body.visibleRowCache.indexOf(row) + 1}}</span>' , width:70},
      { name: 'namaBiaya', displayName: 'Nama Biaya'},
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length === 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage-1;
        paginationOptions.pageSize = pageSize;
        getPage();
      });


    }
  };



  var getPage = function() {
    var url= API_ENDPOINT.url +'/po/'+$rootScope.user.idPo+'/biaya';


    $http.get(url).success(function(data) {
      // $scope.gridOptions.data = data.content;
      $scope.gridOptions.data = data;
      $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
      var firstRow;
      if (paginationOptions.pageNumber>=1) {
        firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      } else {
        firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
      }
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);

    });

  };

      getPage();


  $scope.gridOptions.onRegisterApi = function(gridApi){
      //set gridApi on scope
      $scope.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        $log.log(msg);
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        var msg = 'rows changed ' + rows.length;
        $log.log(msg);
      });
    };

  $scope.simpan = function (namaBiaya) {
    if (!$scope.namaBiaya) {
      return false;
    }
    $scope.data = {
      idPo : $rootScope.user.idPo,
      namaBiaya : namaBiaya
    };
    $http.post(API_ENDPOINT.url + '/po/biaya', $scope.data).success(function () {
      getPage();
      $scope.namaBiaya = '';
    });
  };

});


})();
