(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.biaya', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      // .state('dashboard.informasi.biaya', {
      //   url: '/biaya',
      //   abstract: true,
      //   template: '<div ui-view></div>',
      //   title: 'Biaya',
      //     sidebarMeta: {
      //       order: 7,
      //     },
      // })
      .state('dashboard.informasi.biaya.list', {
        url: '',
        templateUrl: 'app/pages/informasi/biaya/biaya.list.html',
          title: 'List Biaya',
          controller: "biayaCtrl",
          controllerAs: "biayaCtrl",

      })
      .state('dashboard.informasi.biaya.add', {
        url: '/item/add',
        templateUrl: 'app/pages/informasi/biaya/biaya.form.html',
          title: 'Tambah Biaya',
          controller: "biayaAddCtrl",
          controllerAs: "biayaAddCtrl",
      });
  }
})();
