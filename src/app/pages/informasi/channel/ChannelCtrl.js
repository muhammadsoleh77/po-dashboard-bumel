(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.channel')

/* Define the Repository that interfaces with Restangular */
.factory('ChannelRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function ChannelRepository() {
      AbstractRepository.call(this, restangular, 'channels');
    }

    AbstractRepository.extend(ChannelRepository);
    return new ChannelRepository();
  }
])


.controller('ChannelCtrl', function (ChannelRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state) {

  $state.current.tag = 'B.3';

  $scope.gridOptions = {
    enableColumnResizing: true,
    useExternalSorting: false,
    columnDefs: [
      { name: 'No', cellTemplate: '<span style="margin-left:40%;text-align:right;">{{grid.renderContainers.body.visibleRowCache.indexOf(row) + 1}}</span>' , width:70},
      { name: 'channelGroup' },
      { name: 'alamat'},
      {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ui-sref="dashboard.informasi.channel.item({id:row.entity.idChannelGroup})" class="btn btn-warning">Lihat Detail</a></span>'}
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length == 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
    }
  };

  var getPage = function() {
    var url=API_ENDPOINT.url +'/po/'+$scope.user.idPo+'/channels';
      $http.get(url).success(function(data) {
      $scope.gridOptions.data = data;
    });

  };

  // $scope.tampil = function() {
    getPage();
  // }


})

.controller('ChannelItemCtrl', function (AgenRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $stateParams) {
  $state.current.parrentState = 'dashboard.informasi.channel.list';
  $state.current.parrentTitle = 'Data Channel Group';
  $state.current.tag = 'B.3.' + $stateParams.id;

  $http.get(API_ENDPOINT.url +'/channels/'+ $stateParams.id).then(function (data) {

    $scope.channel = data.data;
});


});
})();
