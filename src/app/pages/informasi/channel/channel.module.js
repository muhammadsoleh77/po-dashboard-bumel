(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.channel', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.informasi.channel', {
        url: '/channel',
        abstract: true,
        template: '<div ui-view></div>',
        title: 'Channel/Grup Agen',
          sidebarMeta: {
            order: 3,
          },
      })
      .state('dashboard.informasi.channel.list', {
        url: '',
        templateUrl: 'app/pages/informasi/channel/channel.list.html',
          title: 'Data Channel/Grup Agen',
          controller: "ChannelCtrl",
          controllerAs: "channelCtrl",

      })
      .state('dashboard.informasi.channel.item', {
        url: '/:id',
        templateUrl: 'app/pages/informasi/channel/channel.form.html',
          title: 'Detail Channel Group',
          controller: "ChannelItemCtrl",
          controllerAs: "channelItemtrl",
      });
  }
})();
