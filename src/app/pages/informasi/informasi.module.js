(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi', [
    'BlurAdmin.pages.informasi.po',
    // 'BlurAdmin.pages.informasi.channel',
    'BlurAdmin.pages.informasi.agen',
    'BlurAdmin.pages.informasi.armada',
    'BlurAdmin.pages.informasi.trayek',
    'BlurAdmin.pages.informasi.subtrayek',
    'BlurAdmin.pages.informasi.biaya',
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('dashboard.informasi', {
          url: '/informasi',
          template : '<ui-view></ui-view>',
          abstract: true,
          title: 'Profil PO',
          sidebarMeta: {
            icon: 'ion-ios-lightbulb',
            order: 200,
          },
        });
  }

})();
