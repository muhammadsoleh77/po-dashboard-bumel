(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.po', ['ngFileUpload'])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.informasi.po', {
        url: '/po',
        // id: '{user.idPo}',
        templateUrl: 'app/pages/informasi/po/po.form.html',
        // template: '<div ui-view></div>',
        title: 'PO',
          sidebarMeta: {
            order: 1,
          },
          controller: "PosItemCtrl",
          controllerAs: "poCtrl",
      });
      // .state('dashboard.informasi.po', {
      //   url: '',
      //   // id: '{user.idPo}',
      //   templateUrl: 'app/pages/informasi/po/po.form.html',
      //   title: 'Identitas PO',
      //     // sidebarMeta: {
      //     //   order: 1,
      //     // },
      //     controller: "PosItemCtrl",
      //     controllerAs: "poCtrl",
      // });
  }
})();
