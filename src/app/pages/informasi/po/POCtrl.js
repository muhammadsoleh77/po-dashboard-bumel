(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.po')

/* Define the Repository that interfaces with Restangular */
.factory('PoRepository', ['Restangular', 'AbstractRepository',
  function (restangular, AbstractRepository) {

    function PoRepository() {
      AbstractRepository.call(this, restangular, 'pos');
    }

    AbstractRepository.extend(PoRepository);
    return new PoRepository();
  }
])

.controller('PosItemCtrl', function ($scope, $http, API_ENDPOINT, $location, PoRepository, $rootScope, $state, Upload, $window) {

  $window.scrollTo(0,0);

  $state.current.tag = 'B.1';

  $scope.user = $rootScope.user;
  $scope.po = $rootScope.po;

  $scope.tes = {};
  $scope.tambah = false;
  $scope.submitImg = function () {
    $scope.po = {
      nama : 'Luragung Termuda',
      kodepo : 'LT01992',
      namaperusahaan : 'PO. Luragung Jaya',
      alamat : 'Jl. Luragung no.20, Kuningan',
      telepon : '0232-200-222',
      fax : '0232-200-222',
      email : 'luragung@email.com',
      namapimpinan : 'H. Yayan',
      ponsel1 : '085699400333',
      ponsel2 : '085633455662',
      picnama : 'Maman',
      picjabatan : 'Manager',
      pictelpon : '0232-300-039',
      picemail : 'Maman@email.com',
    };
    Upload.base64DataUrl($scope.file).then(function (url) {
      $scope.po.logo = url;
      // //console.log($scope.po.logo);
      $http.post('http://http://dev.otodata.co.id:8383/po', $scope.po).success(function () {
        //console.log("success");
      }).error(function (err) {
        //console.log(err);
      });
    });
  };

  $scope.save = function () {
    $scope.errorMessage = "";
    if (
      !$scope.po.name ||
      !$scope.po.kodepo ||
      !$scope.po.namaperusahaan ||
      !$scope.po.alamat ||
      !$scope.po.telepon ||
      !$scope.po.fax ||
      !$scope.po.email ||
      !$scope.po.namapimpinan ||
      !$scope.po.ponsel1 ||
      !$scope.po.ponsel2 ||
      !$scope.po.picnama ||
      !$scope.po.picjabatan ||
      !$scope.po.pictelpon ||
      !$scope.po.picemail ||
      !$scope.po.akap ||
      !$scope.po.akdp ||
      !$scope.po.pariwisata ||
      !$scope.po.paket ||
      !$scope.po.directagen ||
      !$scope.po.quotaagen ||
      !$scope.po.enabled
    ) {
      $scope.errorMessage = "Semua Data Harus Diisi!";
      return false;
    } else {
      $scope.submitData();
    }

    $scope.submitData = function () {
      $http.post('', $scope.po).success(function () {
        //console.log("oke");
      }).error(function (err) {
        //console.log(err);
      });
    };
  };
});

})();
