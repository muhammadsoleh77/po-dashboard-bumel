(function () {
  'use strict';
  angular.module('BlurAdmin.pages.informasi.subtrayek')

/* Define the Repository that interfaces with Restangular */
// .factory('SubTrayekRepository', ['Restangular', 'AbstractRepository',
//   function (restangular, AbstractRepository) {
//
//     function SubTrayekRepository() {
//       AbstractRepository.call(this, restangular, 'subtrayeks');
//     }
//
//     AbstractRepository.extend(SubTrayekRepository);
//     return new SubTrayekRepository();
//   }
// ])

// .controller('SubTrayekCtrl', function (SubTrayekRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {
.controller('SubTrayekCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout) {

  $state.current.tag = 'B.5';

  $http.get(API_ENDPOINT.url +'/po/'+$scope.user.idPo+'/subtrayeks').success(function(data) {
    $scope.trayeks = data;
  });

  // $scope.gridOptions = {
  //   enableColumnResizing: true,
  //   useExternalSorting: false,
  //   expandableRowTemplate: 'views/expandGrid.html',
  //   expandableRowHeight: 150,
  //   columnDefs: [
  //     { name: 'No', cellTemplate: '<span style="margin-left:40%;text-align:right;">{{grid.renderContainers.body.visibleRowCache.indexOf(row) + 1}}</span>' , width:70},
  //     { displayName: 'ID Trayek', field: 'idtrayek' },
  //     { displayName: 'Kode Detail Layanan Trayek', field: 'kodesub'},
  //     {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ui-sref="dashboard.informasi.subtrayek.item({id:row.entity.subGridOptions.index, trayek:row.entity.idtrayek})" class="btn btn-warning">Lihat Detail</a></span>'}
  //   ],
  //   onRegisterApi: function(gridApi) {
  //     $scope.gridApi = gridApi;
  //     $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
  //       if (sortColumns.length === 0) {
  //         paginationOptions.sort = null;
  //       } else {
  //         paginationOptions.sort = sortColumns[0].sort.direction;
  //       }
  //       getPage();
  //     });
  //   }
  // };

  // var getPage = function() {
  //   var url = API_ENDPOINT.url +'/trayek/'+$scope.trayek+'/subtrayeks';
  //   // var url = API_ENDPOINT.url +'/trayek/6/subtrayeks';
  //
  //    $http.get(url).success(function(data) {
  //
  //     for(var i = 0; i < data.length; i++){
  //       data[i].subGridOptions = {
  //         index : i
  //       };
  //     }
  //     $scope.gridOptions.data = data;
  //   });
  //
  // };

  $scope.tampil = function() {
    // getPage();
    $http.get(API_ENDPOINT.url + '/trayek/' + $scope.trayek + '/subtrayeks').success(function(data) {
      $scope.subtrayek = data;
      $scope.loadData = true;

      $scope.templateUrl = 'app/pages/informasi/subtrayek/subtrayek.detail.html';
      $scope.loadData = false;
    });

    $scope.Lihat = function(index) {
      $state.go('dashboard.informasi.subtrayek.item', {trayek : $scope.subtrayek[index].idtrayek, id : index});
    }
  };

  // $scope.gridOptions.onRegisterApi = function(gridApi){
  //     //set gridApi on scope
  //     $scope.gridApi = gridApi;
  //     gridApi.selection.on.rowSelectionChanged($scope,function(row){
  //       var msg = 'row selected ' + row.isSelected;
  //       $log.log(msg);
  //     });
  //
  //     gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
  //       var msg = 'rows changed ' + rows.length;
  //       $log.log(msg);
  //     });
  //   };

})


// .controller('SubTrayekItemCtrl', function ($scope, $stateParams, SubTrayekRepository) {
//   $scope.subtrayek = SubTrayekRepository.get($stateParams.id).then(function (data) {
//     $scope.subtrayek = data;
//   });
// })

// .controller('SubtrayekItemCtrl', function (SubTrayekRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $stateParams) {
.controller('SubtrayekItemCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $stateParams) {

  $state.current.parrentState = 'dashboard.informasi.subtrayek.list';
  $state.current.parrentTitle = 'Data Detail Layanan Trayek';
  $state.current.tag = 'B.6.' + $stateParams.id;

  $http.get(API_ENDPOINT.url +'/trayek/'+$stateParams.trayek+'/subtrayeks').success(function (data) {
    $scope.data = data[$stateParams.id];
  });
});


})();
