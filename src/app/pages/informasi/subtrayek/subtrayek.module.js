(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.subtrayek', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      // .state('dashboard.informasi.subtrayek', {
      //   url: '/subtrayek',
      //   abstract: true,
      //   template: '<div ui-view></div>',
      //   title: 'Detail Layanan Trayek',
      //     sidebarMeta: {
      //       order: 5,
      //     },
      // })
      .state('dashboard.informasi.subtrayek.list', {
        url: '',
        templateUrl: 'app/pages/informasi/subtrayek/subtrayek.list.html',
          title: 'Data Detail Layanan Trayek',
          controller: "SubTrayekCtrl",
          controllerAs: "subTrayekCtrl",

      })
      .state('dashboard.informasi.subtrayek.item', {
        url: '/detail/{trayek}/{id}',
        templateUrl: 'app/pages/informasi/subtrayek/subtrayek.form.html',
          title: 'Detail Layanan Trayek',
          controller: "SubtrayekItemCtrl",
          controllerAs: "SubtrayekItemCtrl",
      });
  }
})();
