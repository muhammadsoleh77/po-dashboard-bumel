(function () {
  'use strict';
angular.module('BlurAdmin.pages.informasi.armada')

/* Define the Repository that interfaces with Restangular */
// .factory('ArmadaRepository', ['Restangular', 'AbstractRepository',
//   function (restangular, AbstractRepository) {
//
//     function ArmadaRepository() {
//       AbstractRepository.call(this, restangular, 'bus');
//     }
//
//     AbstractRepository.extend(ArmadaRepository);
//     return new ArmadaRepository();
//   }
// ])

// .controller('ArmadaCtrl', function (ArmadaRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, uiGridConstants) {
.controller('ArmadaCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $window) {

  $state.current.tag = 'B.2';

  // var paginationOptions = {
  //   pageNumber: 1,
  //   pageSize: 5,
  //   sort: null
  // };

  $scope.loadData = true;
  $scope.empty = true;
  $scope.message = "";

  $http.get(API_ENDPOINT.url + '/po/' + $scope.user.idPo + '/armadas').success(function(data) {
    $scope.armada = data;

    $scope.loadData = false;
    if ($scope.armada.length === 0) {
      $scope.empty = true;
      $scope.message = "Tidak Ada Data Armada";
    } else {
      $scope.empty = false;
    }
  });

  // $scope.gridOptions = {
  //   enableColumnResizing: true,
  //   // paginationPageSizes: [5, 10, 15],
  //   // paginationPageSize: 5,
  //   // useExternalPagination: true,
  //   useExternalSorting: true,
  //   columnDefs: [
  //     { name: 'No', cellTemplate: '<span style="margin-left:40%;text-align:right;">{{grid.renderContainers.body.visibleRowCache.indexOf(row) + 1}}</span>' , width:70},
  //     { name: 'plat' },
  //     { name: 'id' },
  //     { name: 'kelas'},
  //     {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ng-click="grid.appScope.Edit(row)" class="btn btn-warning">Lihat Detail</a></span>'}
  //   ],
  //   onRegisterApi: function(gridApi) {
  //     $scope.gridApi = gridApi;
  //     $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
  //       if (sortColumns.length == 0) {
  //         paginationOptions.sort = null;
  //       } else {
  //         paginationOptions.sort = sortColumns[0].sort.direction;
  //       }
  //       getPage();
  //     });
  //     // gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
  //     //   paginationOptions.pageNumber = newPage-1;
  //     //   paginationOptions.pageSize = pageSize;
  //     //   getPage();
  //     // });
  //
  //
  //   }
  // };



  // var getPage = function() {
    // var url= API_ENDPOINT.url +'/po/'+$scope.user.idPo+'/armadas';


  //   $http.get(url).success(function(data) {
  //     // $scope.gridOptions.data = data.content;
  //     $scope.gridOptions.data = data;
  //     $scope.gridOptions.totalItems = 100; //data.metadata.totalPages;
  //     var firstRow;
  //     if (paginationOptions.pageNumber>=1) {
  //       firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
  //     } else {
  //       firstRow = (paginationOptions.pageNumber) * paginationOptions.pageSize;
  //     }
  //     $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);
  //
  //   });
  //
  // };
  //
  //     getPage();


  // $scope.gridOptions.onRegisterApi = function(gridApi){
  //     //set gridApi on scope
  //     $scope.gridApi = gridApi;
  //     gridApi.selection.on.rowSelectionChanged($scope,function(row){
  //       var msg = 'row selected ' + row.isSelected;
  //       $log.log(msg);
  //     });
  //
  //     gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
  //       var msg = 'rows changed ' + rows.length;
  //       $log.log(msg);
  //     });
  //   };
  //
  //   $scope.Edit = function(row) {
  //     //dapatkan bus dengan  view terpisah
  //       $rootScope.pos = $scope.pos;
  //       $state.go('dashboard.informasi.armada.item', {id: row.entity.idBus});
  //
  // };
  $scope.Lihat = function(index) {
    //dapatkan bus dengan  view terpisah
      $rootScope.pos = $scope.pos;
      $state.go('dashboard.informasi.armada.item', {id: $scope.armada[index].idBus});
  };

  $scope.Edit = function(index) {
    $rootScope.pos = $scope.pos;
    $state.go('dashboard.informasi.armada.edit', {id: $scope.armada[index].idBus});
  }

  $scope.Hapus = function(index) {
    $http.delete(API_ENDPOINT.url + '/bus/' + $scope.armada[index].idBus).then(function(){
      alert('Apakah yakin armada dihapus ?');
      $window.location.reload();
    });
  };

  $scope.FiturThread = function(index) {
    if(!$scope.armada[index].valid) {
      $http.put(API_ENDPOINT.url + 'bus/' + $scope.armada[index].idBus + '?status=true').success(function(data){
        $scope.armada[index].valid = true;
      });
    }
    else {
      $http.put(API_ENDPOINT.url + 'bus/' + $scope.armada[index].idBus + '?status=false').success(function(data){
        $scope.armada[index].valid = false;
      });
    }
  }

  $scope.add = function() {
    $state.go('dashboard.informasi.armada.add');
  }

})


// .controller('ArmadaItemCtrl', function ($scope, $stateParams, $location, ArmadaRepository, $rootScope, Restangular, $http, API_ENDPOINT, $state) {
.controller('ArmadaItemCtrl', function ($scope, $stateParams, $location, $rootScope, Restangular, $http, API_ENDPOINT, $state, Upload) {

$scope.images = [];
$state.current.parrentState = 'dashboard.informasi.armada.list';
$state.current.parrentTitle = 'Data Armada';
$state.current.tag = 'B.4.' + $stateParams.id;
$scope.bus = $stateParams.data;

$scope.images = [];

$http.get(API_ENDPOINT.url + '/admin/fotopublik/bus?idBus=' + $stateParams.id).success(function (res) {
  console.log(res);
 //  $scope.gambar = res;
 //  angular.forEach($scope.gambar, function(value, key){
 //    if (value.idBus == $scope.bus.id){
 //      if($scope.images.indexOf(value.gambar) < 0 ){
 //        $scope.images.push(value.gambar);
 //        // $scope.imagesOri.push(value.gambar);
 //      }
 //    }
 // });

 $scope.gambar = res;
 angular.forEach($scope.gambar, function (doc){
    $scope.images.push(doc.gambar);
 });

})

$scope.init = function() {
  $scope.bus.fotoPublik = [];
  //console.log('console else 1 :');
  angular.forEach($scope.images, function(value, key){
    var aImage =
      {
        "gambar": value,
        "id": 0,
        "idBus": 0,
        "idChannel": 0,
        "idFas": 0,
        "idPo": 0,
        "status": "string",
        "urutan": 0
      }
      $scope.bus.fotoPublik.push(aImage);
      // console.log(key);
  });

  $scope.name = "Select Files to Upload";
  $scope.images = [];
  $scope.bus.fotoPublik = $scope.images[$scope.images.length - 1];
  console.log($scope.bus.fotoPublik);
  $scope.setImage = function (data) {
  $scope.bus.fotoPublik = $scope.images[data];
  }
  $scope.clearAll = function () {
  $scope.images = [];
  }

  Upload.base64DataUrl($scope.files).then(function (url) {

    $scope.preview = function (data) {
      console.log(data);
      var elem = data.target || data.srcElement;
      for (var i = 0; i < elem.files.length; i++) {
        var file = elem.files[i];
        var reader = new FileReader();

        reader.onload = function (data) {
            $scope.images.push(data.target.result);
            $scope.$apply();
        }
        reader.readAsDataURL(file);
      }
      console.log($scope.images);
    };
  });
}

Restangular.one('busview', $stateParams.id).get().then(function(a) {
  var s1 = a.layout;
  var s2 = s1.replace(/["\n]/gi, "");
  var s3 = s2.split(";");
  var s4 = [];
  $scope.tesLayout = [];
  s3.forEach(function (doc) {
    s4.push(doc.split(","));
  });
  //console.log(s4);
  if (s4[0].length === 6) {
    $scope.tipe2 = true;
  } else {
    $scope.tipe2 = false;
  }
  s4.forEach(function (doc) {
    doc.forEach(function (doc2) {
      $scope.tesLayout.push(doc2.split("|"));
    });
  });
  // console.log($scope.tesLayout);

    $scope.bus = {
    idBus: a.idBus,
    id: a.id,
    plat: a.plat,
    seat: a.seat,
    seatType: a.seatType,
    seatNumber: a.seatNumber,
    kelas: a.kelas,
    layout: a.layout,
    ekonomi: a.ekonomi,
    ac: a.ac,
    recSeat: a.recSeat,
    footRest: a.footRest,
    legRest: a.legRest,
    video:a.video,
    audio:a.audio,
    toilet:a.toilet,
    smokingArea:a.smokingArea,
    wifi:a.wifi,
    airMineral:a.airMineral,
    snack:a.snack,
    voucherMakan:a.voucherMakan,
    selimut:a.selimut,
    bantal:a.bantal,
    bagasi:a.bagasi,
    logo:a.logo,
    po: {idPo: a.idpo}
  };
});

})

.controller('ArmadaEditCtrl', function ($scope, $stateParams, $location, $rootScope, Restangular, $http, API_ENDPOINT, $state, Upload, $window) {

  $state.current.parrentState = 'dashboard.informasi.armada.list';
  $state.current.parrentTitle = 'Data Armada';
  // $state.current.tag = 'B.5.' + $stateParams.id;
  $scope.bus = $state.params.data;

  $scope.images = [];
  $scope.imagesOri = [];

  $http.get(API_ENDPOINT.url + '/admin/fotopublik/bus?idBus=' + $stateParams.id).success(function (res) {
    console.log(res);
   //  $scope.gambar = res;
   //  angular.forEach($scope.gambar, function(value, key){
   //    if (value.idBus == $scope.bus.id){
   //      if($scope.images.indexOf(value.gambar) < 0 ){
   //        $scope.images.push(value.gambar);
   //        // $scope.imagesOri.push(value.gambar);
   //      }
   //    }
   // });

   $scope.gambar = res;
   console.log($scope.gambar);
   angular.forEach($scope.gambar, function (doc){
      $scope.images.push(doc.gambar);
      $scope.imagesOri.push(doc.gambar);

      $scope.removeItem = function(index) {
        if (confirm("Are you sure?")) {
          $http.delete(API_ENDPOINT.url + 'admin/fotopublik/' + doc.id).then(function() {
            // alert("deleted"+ index);
            // $window.location.reload();
            $scope.images.splice(index, 1);
          });
        }
      }
   });

  })

  Restangular.one('busview', $stateParams.id).get().then(function(a) {
    var s1 = a.layout;
    var s2 = s1.replace(/["\n]/gi, "");
    var s3 = s2.split(";");
    var s4 = [];
    $scope.tesLayout = [];
    s3.forEach(function (doc) {
      s4.push(doc.split(","));
    });
    //console.log(s4);
    if (s4[0].length === 6) {
      $scope.tipe2 = true;
    } else {
      $scope.tipe2 = false;
    }
    s4.forEach(function (doc) {
      doc.forEach(function (doc2) {
        $scope.tesLayout.push(doc2.split("|"));
      });
    });
    // console.log($scope.tesLayout);

    $scope.fakeSeat = [];

      for (var i = 0; i < $scope.tesLayout.length; i++) {
        if ($scope.tesLayout[i].length === 5) {
          $scope.fakeSeat.push({
            row : $scope.tesLayout[i][0],
            col : $scope.tesLayout[i][1],
            no : $scope.tesLayout[i][2],
            type : $scope.tesLayout[i][3],
            status : $scope.tesLayout[i][4],
            index : i
          });
        } else {
          $scope.fakeSeat.push({
            row : $scope.tesLayout[i][0],
            col : $scope.tesLayout[i][1],
            no : $scope.tesLayout[i][2],
            type : $scope.tesLayout[i][3],
            index : i
          });
        }

        $scope.seatNo[i] = $scope.tesLayout[i][2] == 'NO' ? '' : $scope.tesLayout[i][2];
      }

      if (a.seatType === "2-2") {
        // $scope.style = 'col-xs-3 s2-2';
        $scope.style = 'col-xs-2 s3-2';
        limit = 4;
      } else {
        $scope.style = 'col-xs-2 s3-2';
        limit = 5;
      }

      console.log($scope.fakeSeat);

      $scope.bus = {
      idBus: a.idBus,
      id: a.id,
      plat: a.plat,
      seat: a.seat,
      seatType: a.seatType,
      seatNumber: a.seatNumber,
      kelas: a.kelas,
      layout: a.layout,
      ekonomi: a.ekonomi,
      ac: a.ac,
      recSeat: a.recSeat,
      footRest: a.footRest,
      legRest: a.legRest,
      video:a.video,
      audio:a.audio,
      toilet:a.toilet,
      smokingArea:a.smokingArea,
      wifi:a.wifi,
      airMineral:a.airMineral,
      snack:a.snack,
      voucherMakan:a.voucherMakan,
      selimut:a.selimut,
      bantal:a.bantal,
      bagasi:a.bagasi,
      logo:a.logo,
      po: {idPo: a.idpo}
    };
  });

  if (!$rootScope.pos) {
        $http.get(API_ENDPOINT.url + '/po').success(function (res) {
          $rootScope.pos = res;
        });
      }

          $scope.bus = {
          voucherMakan : 0,
        };

      var datas = [];
      $scope.seatNo = [];
      $scope.tes = function (totalSeat) {
        $scope.datas.slice($scope.datas.length, $scope.datas.length);
        for (var i=1; i <= totalSeat; i++) {
          datas.push(i);
        }
        return datas;
      };
      $scope.datas = [];
      $scope.fakeSeat = [];
      var u;
      var x;
      var limit;
      $scope.seatChange = function (totalSeat) {
        var alphabet = ['A', 'B', 'NO', 'NO', 'C', 'D', 'E' ];
        var alphabet2 = ['A', 'B', 'NO', 'C', 'D', 'E' ];
        for (u = 0; u < $scope.fakeSeat.length; u++) {
          $scope.fakeSeat.splice(u, $scope.fakeSeat.length);
        }
        for (u = 0; u < $scope.datas.length; u++) {
          $scope.datas.splice(u, $scope.datas.length);
        }
        if ($scope.bus.seatType === "2-2") {
          // $scope.style = 'col-xs-3 s2-2';
          $scope.style = 'col-xs-2 s3-2';
          limit = 4;
        } else {
          $scope.style = 'col-xs-2 s3-2';
          limit = 5;
        }
        $scope.datas = $scope.tes(totalSeat);
        // console.log(datas);
        var y = 1;
        var z = 1;
        for (x=0; x < $scope.datas.length; x++) {
          if (z == 3) {
            if (limit == 4) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : '',
                type : 3,
                index : $scope.fakeSeat.length
              });
              $scope.fakeSeat.push({
                row : y,
                col : z+1,
                no : '',
                type : 3,
                index : $scope.fakeSeat.length
              });
              $scope.fakeSeat.push({
                row : y,
                col : z+2,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet[z+1],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
              z=z+2;
            } else if (limit == 5) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : '',
                type : 3,
                index : $scope.fakeSeat.length
              });
              $scope.fakeSeat.push({
                row : y,
                col : z+1,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet2[z],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
              z++;
            }
          } else {
            if (limit == 4) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet[z-1],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
            } else if (limit == 5) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet2[z-1],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
            }
          }
          if (z==6) {
            // x = 0;
            z = 1;
            y++;
          } else {
            z++;
          }
        }
        $scope.reloadData();
      };

      $scope.reloadData = function () {
        for (x = 0; x < $scope.fakeSeat.length; x++) {
          $scope.seatNo[x] = $scope.fakeSeat[x].no;
        }
        $scope.numb = $scope.seatNo;
        // console.log($scope.seatNo);
      };

      $scope.toNo = function (index) {
        if ($scope.seatNo[index] == 'NO') {
          $scope.seatNo[index] = '';
        }
      };

      $scope.addSeat = function () {
        if ($scope.fakeSeat[$scope.fakeSeat.length - 1].col == "6") {
          $scope.fakeSeat.push({
            row : String(parseInt($scope.fakeSeat[$scope.fakeSeat.length - 1].row) + 1),
            col : "1",
            no : '',
            type : 2,
            index : $scope.fakeSeat.length
          });
        } else {
          $scope.fakeSeat.push({
            row : $scope.fakeSeat[$scope.fakeSeat.length - 1].row,
            col : String(parseInt($scope.fakeSeat[$scope.fakeSeat.length - 1].col) + 1),
            no : '',
            type : 2,
            index : $scope.fakeSeat.length
          });
        }
      };

      $scope.deleteSeat = function () {
        $scope.fakeSeat.splice($scope.fakeSeat.length-1,1);
      };

      $scope.tambah = true;
      $scope.save = function () {

        $scope.clicked = true;
        var seatLayout = '';
        var finalNum = '';

        for (x=0; x < $scope.fakeSeat.length; x++) {
          seatLayout = seatLayout + '"' + $scope.fakeSeat[x].row + '"|"' + $scope.fakeSeat[x].col + '"|"';
          if (!$scope.seatNo[x]) {
            $scope.seatNo[x] = "NO";
          }
          if ($scope.seatNo[x] == "NO") {
            seatLayout = seatLayout + $scope.seatNo[x] + '"|"3"';
          } else {
            seatLayout = seatLayout + $scope.seatNo[x] + '"|"A"|"2"';
            finalNum = finalNum + '"' + $scope.seatNo[x] + '",';
          }
          if ($scope.fakeSeat[x].col == 6) {
            seatLayout = seatLayout + ";";
          } else {
            seatLayout = seatLayout + ",";
          }
        }

        console.log(seatLayout.slice(0, -1));
        console.log(finalNum.slice(0, -1));
        $scope.bus.layout = seatLayout.slice(0, -1);
        $scope.bus.seatNumber = finalNum.slice(0, -1);
        $scope.bus.seat = $scope.bus.seatNumber.split(',').length;

        $scope.bus.fotoPublik = [];
        //console.log('console else 1 :');
        angular.forEach($scope.images, function(value, key){
          // di bawah ini cara kalo data imagenya udah jadi https... barulah perintah imagesOri ini dijalankan, kalo data imagenya masih base64, dia akan menjalankan perintah post setImages yang ada di bawahnya
          if($scope.imagesOri.indexOf(value) < 0){
            var aImage =
              {
                "gambar": value,
                "id": 0,
                "idBus": 0,
                "idChannel": 0,
                "idPo": 0,
                "status": "string",
                "urutan": 0
              }
              $scope.bus.fotoPublik.push(aImage);
               console.log(key);
          }
       });

        // $http.post(API_ENDPOINT.url + '/bus', $scope.bus).success(function () {
        //   $state.go('dashboard.informasi.armada.list');
        // }).error(function (e) {
        //   console.log(e);
        //   $scope.clicked = false;
        // });

        if ($scope.file) {
       Upload.base64DataUrl($scope.file).then(function (url) {
        $scope.bus.logo = url;
        // console.log($scope.bus.logo);
      $http.post(API_ENDPOINT.url + '/bus', $scope.bus).success(function () {
          alert('Armada Berhasil Diedit');
          $state.go('dashboard.informasi.armada.list');
        })
        .error(function (e) {
          console.log(e);
          $scope.clicked = false;
        });
      });
    }
      else if($scope.bus.id && $scope.bus.plat && $scope.bus.kelas && $scope.bus.bagasi) {
        $http.post(API_ENDPOINT.url + '/bus', $scope.bus).success(function () {
            alert('Armada Berhasil Diedit');
            $state.go('dashboard.informasi.armada.list');
          })
          .error(function (e) {
            console.log(e);
            $scope.clicked = false;
          });
        // alert('Harap isi data');
        // $scope.clicked = false;
        // return false;
      }
      else {
        alert('Harap isi data');
        $scope.clicked = false;
        return false;
      }

    };

    $scope.name = "Select Files to Upload";
    $scope.images = [];
    $scope.bus.fotoPublik = $scope.images[$scope.images.length - 1];
    console.log($scope.bus.fotoPublik);

    $scope.setImage = function (data) {
    $scope.bus.fotoPublik = $scope.images[data];
    }

    $scope.clearAll = function () {
    $scope.images = [];
    }

    Upload.base64DataUrl($scope.files).then(function (url) {

      $scope.preview = function (data) {
      var elem = data.target || data.srcElement;
      for (var i = 0; i < elem.files.length; i++) {
        var file = elem.files[i];
        var reader = new FileReader();

        reader.onload = function (data) {
            $scope.images.push(data.target.result);
            $scope.$apply();
        }
        reader.readAsDataURL(file);
      }
      console.log($scope.images);
        };
      $scope.removeItem = function(index){
        $scope.images.splice(index, 1);
      }
      });

})

.controller('AddArmadaItemCtrl', function ($scope, $window, $stateParams, $location, $rootScope, Restangular, $http, API_ENDPOINT, $state, Upload) {

  $state.current.parrentState = 'dashboard.informasi.armada.list';
  $state.current.parrentTitle = 'Data Armada';
  // $state.current.tag = 'B.5.' + $stateParams.id;
  $scope.bus = $state.params.data;

  if (!$rootScope.pos) {
        $http.get(API_ENDPOINT.url + '/po').success(function (res) {
          $rootScope.pos = res;
        });
      }

          $scope.bus = {
          voucherMakan : 0,
        };

      var datas = [];
      $scope.seatNo = [];
      $scope.tes = function (totalSeat) {
        $scope.datas.slice($scope.datas.length, $scope.datas.length);
        for (var i=1; i <= totalSeat; i++) {
          datas.push(i);
        }
        return datas;
      };
      $scope.datas = [];
      $scope.fakeSeat = [];
      var u;
      var x;
      var limit;
      $scope.seatChange = function (totalSeat) {
        var alphabet = ['A', 'B', 'NO', 'NO', 'C', 'D', 'E' ];
        var alphabet2 = ['A', 'B', 'NO', 'C', 'D', 'E' ];
        for (u = 0; u < $scope.fakeSeat.length; u++) {
          $scope.fakeSeat.splice(u, $scope.fakeSeat.length);
        }
        for (u = 0; u < $scope.datas.length; u++) {
          $scope.datas.splice(u, $scope.datas.length);
        }
        if ($scope.bus.seatType === "2-2") {
          // $scope.style = 'col-xs-3 s2-2';
          $scope.style = 'col-xs-2 s3-2';
          limit = 4;
        } else {
          $scope.style = 'col-xs-2 s3-2';
          limit = 5;
        }
        $scope.datas = $scope.tes(totalSeat);
        // console.log(datas);
        var y = 1;
        var z = 1;
        for (x=0; x < $scope.datas.length; x++) {
          if (z == 3) {
            if (limit == 4) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : '',
                type : 3,
                index : $scope.fakeSeat.length
              });
              $scope.fakeSeat.push({
                row : y,
                col : z+1,
                no : '',
                type : 3,
                index : $scope.fakeSeat.length
              });
              $scope.fakeSeat.push({
                row : y,
                col : z+2,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet[z+1],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
              z=z+2;
            } else if (limit == 5) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : '',
                type : 3,
                index : $scope.fakeSeat.length
              });
              $scope.fakeSeat.push({
                row : y,
                col : z+1,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet2[z],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
              z++;
            }
          } else {
            if (limit == 4) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet[z-1],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
            } else if (limit == 5) {
              $scope.fakeSeat.push({
                row : y,
                col : z,
                no : $scope.typeNumber ? $scope.datas[x] : y + alphabet2[z-1],
                type : 'A',
                status : 2,
                index : $scope.fakeSeat.length
              });
            }
          }
          if (z==6) {
            // x = 0;
            z = 1;
            y++;
          } else {
            z++;
          }
        }
        $scope.reloadData();
      };

      $scope.reloadData = function () {
        for (x = 0; x < $scope.fakeSeat.length; x++) {
          $scope.seatNo[x] = $scope.fakeSeat[x].no;
        }
        $scope.numb = $scope.seatNo;
        // console.log($scope.seatNo);
      };

      $scope.toNo = function (index) {
        if ($scope.seatNo[index] == 'NO') {
          $scope.seatNo[index] = '';
        }
      };

      $scope.addSeat = function () {
        if ($scope.fakeSeat[$scope.fakeSeat.length - 1].col == "6") {
          $scope.fakeSeat.push({
            row : String(parseInt($scope.fakeSeat[$scope.fakeSeat.length - 1].row) + 1),
            col : "1",
            no : '',
            type : 2,
            index : $scope.fakeSeat.length
          });
        } else {
          $scope.fakeSeat.push({
            row : $scope.fakeSeat[$scope.fakeSeat.length - 1].row,
            col : String(parseInt($scope.fakeSeat[$scope.fakeSeat.length - 1].col) + 1),
            no : '',
            type : 2,
            index : $scope.fakeSeat.length
          });
        }
      };

      $scope.deleteSeat = function () {
        $scope.fakeSeat.splice($scope.fakeSeat.length-1,1);
      };

      $scope.tambah = true;
      $scope.save = function () {
        $scope.clicked = true;
        var seatLayout = '';
        var finalNum = '';

        var valid = 4;
        $scope.errorForm1 = "";
        $scope.errorForm2 = "";
        $scope.errorForm3 = "";
        $scope.errorForm4 = "";
        // $scope.errorForm5 = false;

        if(!$scope.bus.id){
          $scope.errorForm1 = "Kode Armada Harus diisi!";
          valid--;
        }
        if(!$scope.bus.plat){
          $scope.errorForm2 = "No plat Harus diisi!";
          valid--;
        }
        if(!$scope.bus.kelas){
          $scope.errorForm3 = "Sub Kelas Harus diisi!";
          valid--;
        }
        if(!$scope.bus.seat){
          $scope.errorForm4 = "Jumlah Kursi Harus diisi!";
          valid--;
        }
        // if(valid !== 4){
        //   $window.scrollTo(0,0);
          // $scope.clicked = false;
          // return false;
        // }

        for (x=0; x < $scope.fakeSeat.length; x++) {
          seatLayout = seatLayout + '"' + $scope.fakeSeat[x].row + '"|"' + $scope.fakeSeat[x].col + '"|"';
          if (!$scope.seatNo[x]) {
            $scope.seatNo[x] = "NO";
          }
          if ($scope.seatNo[x] == "NO") {
            seatLayout = seatLayout + $scope.seatNo[x] + '"|"3"';
          } else {
            seatLayout = seatLayout + $scope.seatNo[x] + '"|"A"|"2"';
            finalNum = finalNum + '"' + $scope.seatNo[x] + '",';
          }
          if ($scope.fakeSeat[x].col == 6) {
            seatLayout = seatLayout + ";";
          } else {
            seatLayout = seatLayout + ",";
          }
        }

        $scope.bus.layout = seatLayout.slice(0, -1);
        $scope.bus.seatNumber = finalNum.slice(0, -1);
        // $scope.bus.seat = $scope.bus.seatNumber.split(',').length;
        // console.log($scope.bus.seatNumber.split(',').length);

        $scope.bus.fotoPublik = [];
        //console.log('console else 1 :');
        angular.forEach($scope.images, function(value, key){
          var aImage =
            {
              "gambar": value,
              "id": 0,
              "idBus": 0,
              "idChannel": 0,
              "idPo": 0,
              "status": "string",
              "urutan": 0
            }
            $scope.bus.fotoPublik.push(aImage);
             console.log(key);
       });

        // $http.post(API_ENDPOINT.url + '/bus', $scope.bus).success(function () {
        //   $state.go('dashboard.informasi.armada.list');
        // }).error(function (e) {
        //   console.log(e);
        //   $scope.clicked = false;
        // });

        if ($scope.file) {
       Upload.base64DataUrl($scope.file).then(function (url) {
        $scope.bus.logo = url;
        // console.log($scope.bus.logo);
      $http.post(API_ENDPOINT.url + '/bus', $scope.bus).success(function () {
          alert('Armada Berhasil Ditambah');
          $state.go('dashboard.informasi.armada.list');
        })
        .error(function (e) {
          console.log(e);
          $scope.clicked = false;
        });
      });
    }
      else if($scope.bus.id && $scope.bus.plat && $scope.bus.kelas && $scope.bus.bagasi) {
        $http.post(API_ENDPOINT.url + '/bus', $scope.bus).success(function () {
            alert('Armada Berhasil Ditambah');
            $state.go('dashboard.informasi.armada.list');
          })
          .error(function (e) {
            console.log(e);
            $scope.clicked = false;
          });
        // alert('Harap isi data');
        // $scope.clicked = false;
        // return false;
      }
      else {
        alert('Harap isi data');
        $window.scrollTo(0,0);
        $scope.clicked = false;
        return false;
      }

    };

    $scope.name = "Select Files to Upload";
    $scope.images = [];
    $scope.bus.fotoPublik = $scope.images[$scope.images.length - 1];
    $scope.setImage = function (data) {
    $scope.bus.fotoPublik = $scope.images[data];
    }
    $scope.clearAll = function () {
    $scope.images = [];
    }

    Upload.base64DataUrl($scope.files).then(function (url) {

      $scope.preview = function (data) {
      var elem = data.target || data.srcElement;
      for (var i = 0; i < elem.files.length; i++) {
        var file = elem.files[i];
        var reader = new FileReader();

        reader.onload = function (data) {
            $scope.images.push(data.target.result);
            $scope.$apply();
        }
        reader.readAsDataURL(file);
      }
      console.log($scope.images);
        };

      $scope.removeItem = function(index){
        $scope.images.splice(index, 1);
      }

      });

})

})();
