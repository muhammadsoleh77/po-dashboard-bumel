(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.armada', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.informasi.armada', {
        url: '/armada',
        abstract: true,
        template: '<div ui-view></div>',
        title: 'Armada',
          sidebarMeta: {
            order: 2,
          },
      })
      .state('dashboard.informasi.armada.list', {
        url: '',
        templateUrl: 'app/pages/informasi/armada/armada.list.html',
          title: 'Data Armada',
          controller: "ArmadaCtrl",
          controllerAs: "armadaCtrl",

      })
      .state('dashboard.informasi.armada.item', {
        url: '/item/:id',
        templateUrl: 'app/pages/informasi/armada/armada.form.html',
          title: 'Detail Armada',
          controller: "ArmadaItemCtrl",
          controllerAs: "armadaItemCtrl",
      })
      .state('dashboard.informasi.armada.edit', {
        url: '/edit/:id',
        templateUrl: 'app/pages/informasi/armada/armada.edit.html',
          title: 'Edit Armada',
          controller: "ArmadaEditCtrl",
          controllerAs: "armadaEditCtrl",
      })
      .state('dashboard.informasi.armada.add', {
        url: '/add',
        templateUrl: 'app/pages/informasi/armada/armada.add.form.html',
          title: 'Tambah Armada',
          controller: "AddArmadaItemCtrl",
          controllerAs: "addArmadaItemCtrl",
      });
  }
})();
