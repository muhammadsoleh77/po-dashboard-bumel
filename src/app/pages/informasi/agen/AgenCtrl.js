(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.agen')

/* Define the Repository that interfaces with Restangular */
// .factory('AgenRepository', ['Restangular', 'AbstractRepository',
//   function (restangular, AbstractRepository) {
//
//     function AgenRepository() {
//       AbstractRepository.call(this, restangular, 'agens');
//     }
//
//     AbstractRepository.extend(AgenRepository);
//     return new AgenRepository();
//   }
// ])

// .controller('AgenCtrl', function (AgenRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state) {
.controller('AgenCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state) {

  $state.current.tag = 'B.3';

  $scope.loadData = true;
  $scope.empty = true;
  $scope.message = "";

  $http.get(API_ENDPOINT.url + '/po/' + $scope.user.idPo + '/agen').success(function(data) {
    $scope.agen = data;
    $scope.loadData = false;
    if ($scope.agen.length === 0) {
      $scope.empty = true;
      $scope.message = "Tidak Ada Data Agen";
    } else {
      $scope.empty = false;
    }
  });


  // $scope.gridOptions = {
  //   enableColumnResizing: true,
  //   useExternalSorting: false,
  //   columnDefs: [
  //     { name: 'No', cellTemplate: '<span style="margin-left:40%;text-align:right;">{{grid.renderContainers.body.visibleRowCache.indexOf(row) + 1}}</span>' , width:70},
  //     { name: 'namaChannel' },
  //     { name: 'alamat'},
  //     {name: 'edit', displayName: 'Aksi', cellTemplate: '<span><a ui-sref="dashboard.informasi.agen.item({id:row.entity.idChannel})" class="btn btn-warning">Lihat Detail</a></span>'}
  //
  //     // { name: 'company', enableSorting: false }
  //   ],
  //   onRegisterApi: function(gridApi) {
  //     $scope.gridApi = gridApi;
  //     $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
  //       if (sortColumns.length === 0) {
  //         paginationOptions.sort = null;
  //       } else {
  //         paginationOptions.sort = sortColumns[0].sort.direction;
  //       }
  //       getPage();
  //     });
  //   }
  // };

  // var getPage = function() {
  //   var url;
  //   //ada 4 opsi
  //
  //     // if ($scope.channel===0) {
  //       url = API_ENDPOINT.url +'/po/'+$scope.user.idPo+'/agen';
  //     // }else {
  //       // url = API_ENDPOINT.url +'/agen/'+$scope.channel+ '/' + $scope.idChannelGroup;
  //     // }
  //
  //   $http.get(url).success(function(data) {
  //     // $scope.gridOptions.data = data;
  //     $scope.loadData = false;
  //     $scope.datas = data;
  //   });
  //
  // };

  // getPage();

  // $scope.tampil = function() {
  //   getPage();
  // };

  $scope.Lihat = function(index) {
      // $http.get(API_ENDPOINT.url +'/agen/'+row.entity.idChannel).then(function (data) {
      $http.get(API_ENDPOINT.url +'/agen/'+$scope.agen[index].idChannel).then(function (data) {
        // $rootScope.channel = data;
        $rootScope.channel = {
          idChannel: data.idagen,
          namaChannel: data.namaagen,
          channelGroup: {idChannelGroup: data.idChannelGroup},
          lokasi: {idLokasi: data.idlokasi},
          jenis: data.jenis,
          nomorTelepon: data.nomortelepon,
          alamat: data.alamat,
          kontakPerson: data.kontakperson,
          tempatBerangkat: data.tempatberangkat,
          pool: data.pool,
          idpo: data.idpo,
          idWilayah : data.idWilayah
        };

          // $http.get(API_ENDPOINT.url +'/po/'+row.entity.idChannel+'/channels').success(function(data) {
          $http.get(API_ENDPOINT.url +'/po/'+$scope.agen[index].idChannel+'/channels').success(function(data) {
            $scope.channels = data;

         });
        // $rootScope.channels = $scope.channels;
        // $rootScope.kotas = $scope.kotas;
        // $rootScope.subkotas = Restangular.one('kotas', data.idWilayah).getList('subkotas').$object;
        $state.go('dashboard.informasi.agen.item', {id: $scope.agen[index].idChannel});
        // $state.go('dashboard.pages.informasi.agens.edit', {id: row.entity.idChannel});
    });
    };

  })
  // .controller('AgenItemCtrl', function (AgenRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $stateParams, $sce) {
  .controller('AgenItemCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $stateParams, $sce) {
    $state.current.parrentState = 'dashboard.informasi.agen.list';
    $state.current.parrentTitle = 'Data Agen';
    $state.current.tag = 'B.2.' + $stateParams.id;

    $http.get(API_ENDPOINT.url +'/agen/'+ $stateParams.id).then(function (data) {
      $scope.agen = data.data;
      var mapUrl = 'https://www.google.com/maps/embed/v1/place?q=' + $scope.agen.latitude + ',' + $scope.agen.longitude + '&key=AIzaSyBFRjgng4mClcT1MbC5IZt9juyUpUjJ9Mw';

      $scope.mapUrl = $sce.trustAsResourceUrl(mapUrl);
  });


});

})();
