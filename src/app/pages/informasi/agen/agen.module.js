(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.agen', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.informasi.agen', {
        url: '/agen',
        abstract: true,
        template: '<div ui-view></div>',
        title: 'Agen',
          sidebarMeta: {
            order: 3,
          },
      })
      .state('dashboard.informasi.agen.list', {
        url: '',
        templateUrl: 'app/pages/informasi/agen/agen.list.html',
          title: 'Daftar Agen',
          controller: "AgenCtrl",
          controllerAs: "agenCtrl",

      })
      .state('dashboard.informasi.agen.item', {
        url: '/:id',
        templateUrl: 'app/pages/informasi/agen/agen.form.html',
          title: 'Detail Agen',
          controller: "AgenItemCtrl",
          controllerAs: "agenItemCtrl",
          params : {
            path : null,
            title : null
          }
      });
  }
})();
