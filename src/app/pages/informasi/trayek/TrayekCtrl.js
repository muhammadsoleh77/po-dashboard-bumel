(function () {
  'use strict';
  angular.module('BlurAdmin.pages.informasi.trayek')

/* Define the Repository that interfaces with Restangular */
// .factory('TrayekRepository', ['Restangular', 'AbstractRepository',
//   function (restangular, AbstractRepository) {
//
//     function TrayekRepository() {
//       AbstractRepository.call(this, restangular, 'trayeks');
//     }
//
//     AbstractRepository.extend(TrayekRepository);
//     return new TrayekRepository();
//   }
// ])

.controller('TrayekCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state) {

  $state.current.tag = 'B.4';

  $scope.loadData = true;
  $scope.empty = true;
  $scope.message = "";

  $http.get(API_ENDPOINT.url + '/po/' + $rootScope.po.idPo + '/trayeks').success(function(data) {
    $scope.trayek = data;
    $scope.loadData = false;
    if($scope.trayek.length === 0) {
      $scope.empty = true;
      $scope.message = "Tidak Ada Data Trayek";
    }else {
      $scope.empty = false;
    }
  });

  // $scope.gridOptions = {
  //   enableColumnResizing: true,
  //   useExternalSorting: true,
  //   columnDefs: [
  //     { name: 'No', cellTemplate: '<span style="margin-left:40%;text-align:right;">{{grid.renderContainers.body.visibleRowCache.indexOf(row) + 1}}</span>' , width:70},
  //     { name: 'Trayek', field: 'trayek' },
  //     { name: 'Seri', field: 'kodetrayek' },
  //     {name: 'Aksi', cellTemplate: '<span><a ui-sref="dashboard.informasi.trayek.item({id:row.entity.subGridOptions.index})" class="btn btn-warning">Lihat Detail</a></span>'}
  //   ],
  //   onRegisterApi: function(gridApi) {
  //     $scope.gridApi = gridApi;
  //     $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
  //       if (sortColumns.length === 0) {
  //         paginationOptions.sort = null;
  //       } else {
  //         paginationOptions.sort = sortColumns[0].sort.direction;
  //       }
  //       getPage();
  //     });
  //   }
  // };

  // var getPage = function() {
  //   var url= API_ENDPOINT.url +'/po/'+$rootScope.po.idPo+'/trayeks';
  //
  //   $http.get(url).success(function(data) {
  //     for(var i = 0; i < data.length; i++){
  //       data[i].subGridOptions = {
  //         index : i
  //       };
  //     }
  //     $scope.gridOptions.data = data;
  //   });
  //
  // };
  //
  // getPage();

  $scope.Lihat = function(index){
    $state.go('dashboard.informasi.trayek.item', {id: index});
  }

})

// .controller('TrayekItemCtrl', function (SubTrayekRepository, $filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $stateParams) {
.controller('TrayekItemCtrl', function ($filter, $scope, $rootScope, Restangular, $http, API_ENDPOINT, $state, $log, $timeout, $stateParams) {

  $state.current.parrentState = 'dashboard.informasi.trayek.list';
  $state.current.parrentTitle = 'Data Trayek';
  $state.current.tag = 'B.5.' + $stateParams.id;

  $http.get(API_ENDPOINT.url +'/po/'+$rootScope.po.idPo+'/trayeks').success(function (data) {
    $scope.data = data[$stateParams.id];
  });
});
})();
