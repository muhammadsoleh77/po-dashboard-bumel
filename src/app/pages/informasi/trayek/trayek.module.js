(function () {
  'use strict';

  angular.module('BlurAdmin.pages.informasi.trayek', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      // .state('dashboard.informasi.trayek', {
      //   url: '/trayek',
      //   abstract: true,
      //   template: '<div ui-view></div>',
      //   title: 'Trayek',
      //     sidebarMeta: {
      //       order: 4,
      //     },
      // })
      .state('dashboard.informasi.trayek.list', {
        url: '',
        templateUrl: 'app/pages/informasi/trayek/trayek.list.html',
          title: 'Data Trayek',
          controller: "TrayekCtrl",
          controllerAs: "trayekCtrl",

      })
      .state('dashboard.informasi.trayek.item', {
        url: '/detail/{id}',
        templateUrl: 'app/pages/informasi/trayek/trayek.item.html',
          title: 'Detail Trayek',
          controller: "TrayekItemCtrl",
          controllerAs: "trayekItemCtrl",
      });
  }
})();
